<style type="text/css">
    div.clear
{
    clear: both;
}

div.product-chooser{
    
}

    div.product-chooser.disabled div.product-chooser-item
    {
        zoom: 1;
        filter: alpha(opacity=60);
        opacity: 0.6;
        cursor: default;
    }

    div.product-chooser div.product-chooser-item{
        padding: 11px;
        border-radius: 6px;
        cursor: pointer;
        position: relative;
        border: 1px solid #efefef;
        margin-bottom: 10px;
        margin-left: 10px;
        margin-right: 10x;
    }
    
    div.product-chooser div.product-chooser-item.selected{
        border: 4px solid #428bca;
        background: #efefef;
        padding: 8px;
        filter: alpha(opacity=100);
        opacity: 1;
    }
    
        div.product-chooser div.product-chooser-item img{
            padding: 0;
        }
        
        div.product-chooser div.product-chooser-item span.title{
            display: block;
            margin: 10px 0 5px 0;
            font-weight: bold;
            font-size: 12px;
        }
        
        div.product-chooser div.product-chooser-item span.description{
            font-size: 12px;
        }
        
        div.product-chooser div.product-chooser-item input{
            position: absolute;
            left: 0;
            top: 0;
            visibility:hidden;
        }

</style>

<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Program Selection</h2>
    <em>Form Program Selection</em>
</div>

<div class="main-content">

<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
            </div>
            <div class="widget-content">
                <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">
                            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>participant/selectionProgramProcess" id="inputForm">
                                <fieldset>
                                    <input type="hidden" name="programdegreeid" id="programdegree-id">
                                    <input type="hidden" name="language_delivery_mapping_id" id="languagedeliverymapping-id">

                                    <legend>Program Selection</legend>
                                    <div class="form-group">
                                        <label for="ticket-type" class="col-sm-3 control-label">Select Program</label>
                                        <div class="col-sm-9">
                                            <select id="select-program" name="program_id" class="select2 required" required>
                                                <option value="">-- Select Program --</option>
                                                <?php
                                                    if(!empty($listProgram))
                                                    {

                                                        foreach ($listProgram as $program) {
                                                        $selected = $program['PROGRAMID'] == $participant['PROGRAMID'] ? 'selected' : '';
                                                ?>
                                                        <option data-type="<?php echo $program['PROGRAMTYPE']; ?>" value="<?php echo $program['PROGRAMID'];?>" <?php echo $selected; ?>><?php echo $program['PROGRAMNAME']; ?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="enrollment-nonacademic">
                                        <div class="form-group">
                                            <label for="ticket-type" class="col-sm-3 control-label">Enrollment</label>
                                            <div class="col-sm-9">
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row form-group product-chooser">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div id="enrollment-academic">

                                        <div class="form-group">
                                            <label for="ticket-type" class="col-sm-3 control-label">Enrollment</label>
                                            <div class="col-sm-9">
                                                <select id="select-enrollment" name="enrollment_id" class="select2 required">
                                                    <option value="">-- Select Enrollment --</option>
                                                    <?php /*

                                                    <?php
                                                        if(!empty($listEnrollment))
                                                        {

                                                            foreach ($listEnrollment as $enrollment) {
                                                            $selected = $enrollment['ENROLLMENTID'] == $participant['ENROLLMENTID'] ? 'selected' : '';
                                                    ?>
                                                            <option value="<?php echo $enrollment['ENROLLMENTID'];?>" <?php echo $selected; ?>><?php echo $enrollment['DESCRIPTION']; ?></option>
                                                    <?php
                                                            }
                                                        }
                                                    ?>

                                                    */ ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                        if($participant['PROGRAMTYPE'] == 'ACADEMIC')
                                        {
                                    ?>

                                    <div class="form-group">
                                        <label for="ticket-type" class="col-sm-3 control-label">Degree</label>
                                        <div class="col-sm-9">
                                              <input type="text" name="degreename" id="degree-id" readonly class="form-control">
                                            <input type="hidden" name="degreeid" id="degree-id-hidden">
                                        </div>
                                    </div>
                                    <?php } 
                                   
                                    ?>
                                <?php /*
                                    
                                    <div class="form-group">
                                        <label for="ticket-type" class="col-sm-3 control-label">Degree</label>
                                        <div class="col-sm-9">
                                            <select id="select-degree" name="degree_program_id" class="select2" required>
                                                <option value="">Select Degree</option>

                                            </select>
                                        </div>
                                    </div>

                            */ ?>
                                    <?php 
                                        if($participant['PROGRAMTYPE'] == 'ACADEMIC')
                                        {
                                            // print_r($listLanguageDelivery);
                                            // die();
                                    ?>
                                    <div class="form-group" id="language-delivery-container">
                                        <label for="ticket-type" class="col-sm-3 control-label">Language Delivery</label>
                                        <div class="col-sm-9">
                                            <select id="select-language" name="language_delivery_id" class="select2 required">
                                                <option value=""> -- Select Language Delivery -- </option>
                                                
                                                 <?php
                                                    if(!empty($listLanguageDelivery))
                                                    {

                                                        foreach ($listLanguageDelivery as $data) {
                                                        if($data['LANGUAGEDELIVERYID'] == $languageDeliveryId['LANGUAGEDELIVERYID']){
                                                ?>
                                                        <option value="<?php echo $data['LANGUAGEDELIVERYID'];?>" selected="selected"><?php echo $data['LANGUAGE']; ?></option>
                                                <?php
															}else{ ?>
														<option value="<?php echo $data['LANGUAGEDELIVERYID'];?>"><?php echo $data['LANGUAGE']; ?></option>	
												<?php		}
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php } ?>

                                    <?php  
                                     
                                        {?>
                                            <div class="form-group" id="study-program-container">
                                        <label for="ticket-type" class="col-sm-3 control-label">Study Program</label>
                                        <div class="col-sm-9">
                                            <select id="select-studyprogram" name="study_program_id" class="select2 required">
                                            <option value=""> -- Select Study Program -- </option>
                                          
                                                  <?php
                                                    if(!empty($listStudyProgram))
                                                    {

                                                        foreach ($listStudyProgram as $data) {
                                                       if($data['STUDYPROGRAMID'] == $participant['STUDYPROGRAMID']){
                                                ?>
                                                        <option value="<?php echo $data['STUDYPROGRAMID'];?>" selected="selected"><?php echo $data['STUDYPROGRAMNAME']; ?></option>
                                                <?php
													   }
                                                        }
                                                    }
                                                ?> 

                                           </select>
                                        </div>
                                    </div>
                                    <?php 
                                    }?>
                                    

                                </fieldset>
                        
                        

                    <div class="actions">
                        <!-- <button type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</button> -->
                        <button type="submit" value="program_selection" class="btn btn-primary btn-next">Save & Next <i class="fa fa-arrow-right"></i></button>
                    </div>

                    </form>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>
	</div>

</div>

</div>


</div>
<!-- /main-content -->
</div>

</div>
<!-- /content-wrapper -->

<script type="text/javascript">

    var baseurl = '<?php echo base_url(); ?>';
    
    $(document).ready(function(){
        $("#enrollment-academic").hide();
        $("#enrollment-nonacademic").hide();


        $('div.product-chooser').on('click', '.product-chooser-item', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);
        });

        <?php 
            if(!empty($participant['PROGRAMID']))
            { ?>
                loadEnrollment(<?php echo $participant['ENROLLMENTID']; ?>);
                <?php /*loadDegree(<?php echo $participant['PROGRAMID']?>, <?php echo $participant['DEGREEID'] ?>); */?>
            <?php 
            }
        ?>

        $('#select-enrollment').on("change", function(e) { 
            var value = $('#select-enrollment').select2('val');

            if(value != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                        data:{ enrollmentid:value},
                        success:function(response)
                        {
                            $('#degree-id').val(response.DEGREE);
                            $('#degree-id-hidden').val(response.DEGREEID);
                            $('#programdegree-id').val(response.PROGRAMDEGREEID);
							
							$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                            $("#select-studyprogram").select2("val", 0);
							
                            if(response.SHORTCOURSE == true){
                                $('.study-program-container').hide();
                                $('#language-delivery-container').hide();
                            } else {
                                $('.study-program-container').show();
                                $('#language-delivery-container').show();
                            }
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );
            } else {
                $('#degree-id').val("");
                $('#degree-id-hidden').val("");
                $('#programdegree-id').val("");
                $('.study-program-container').show();
                $('#language-delivery-container').show();
            }
        });
        $("#inputForm").validate({
           /* errorPlacement: function(error, element) {
                if (element.attr("name") == "email" )
                    error.insertAfter(".some-class");
                else if  (element.attr("name") == "phone" )
                    error.insertAfter(".some-other-class");
                else
                    error.insertAfter(element);
            }*/

        });
       // Getstudy();
    function Getstudy(){

            var programdegreeid = $('#programdegree-id').val();
            var languagedeliveryid = $('#select-language').val();
            // alert (programdegreeid);
            if(programdegreeid != "" && languagedeliveryid != "")
            {

                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid},
                        success:function(response)
                        {
                            // alert(response);
                            /* $('#select-studyprogram').html('<option value=""> -- Select Study Program -- </option>');
                            for (i = 0; i < response.length; i++) {
                                    // alert(response[i].STUDYPROGRAMNAME);
                                     $('#select-studyprogram').append('<option value="'+response[i].STUDYPROGRAMID+'"> '+response[i].STUDYPROGRAMNAME+' </option>');
                                } */
                            // $('#languagedeliverymapping-id').val(response.LANGUAGEDELIVERYMAPPINGID);
                            // $('#studyprogram-name').val(response.STUDYPROGRAMNAME);
                            // $('#studyprogram-id').val(response.STUDYPROGRAMID);
							//language delivery change
							$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                            $.each(response, function(index, value) {
                                $("#select-studyprogram").append($('<option>', {value: value['STUDYPROGRAMID'], text: value['STUDYPROGRAMNAME']}));
                            });
                            $("#select-studyprogram").select2("val", 0);
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );   
            } else {
              //  alert('Please select Enrollment data');
                $("#select-language").select2('val', "");
            }



    }        

        $('#select-language').on("change", function(e) { 
        Getstudy();



        });

         $('#select-studyprogram').on("change", function(e) { 
            var programdegreeid = $('#programdegree-id').select2('val');
            var languagedeliveryid = $('#select-language').select2('val');
            // alert(programdegreeid);
            if(programdegreeid != "" && languagedeliveryid != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid},
                        success:function(response)
                        {
							
                           for (i = 0; i < response.length; i++) {
                                    alert(response[i].STUDYPROGRAMNAME);
                                } 
                            // $('#languagedeliverymapping-id').val(response.LANGUAGEDELIVERYMAPPINGID);
                            // $('#studyprogram-name').val(response.STUDYPROGRAMNAME);
                            // $('#studyprogram-id').val(response.STUDYPROGRAMID);

                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );   
            } else {
                alert('Please select Study Program');
                $("#select-studyprogram").select2('val', "");
            }
        });

        $('#select-program').on("change", function(e) { 
            var value = $('#select-program').select2('val');
            var type = $("#select-program").select2().find(":selected").data("type");
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value},
                    success:function(response)
                    {
                        $("#enrollment-academic").hide();
                        $("#enrollment-nonacademic").hide();

                        if(type == 'NON_ACADEMIC') {
                            $("div.product-chooser").html('');
                            $("#enrollment-nonacademic").show('slow');
                            var selected;
                            var checked;
                            $.each(response, function(index, value) {
                                if(index == 0) { selected = 'selected'; checked = 'checked="checked"'; } 
                                else { selected = ''; checked = ''; }

                                if(value['IMAGE'] == null || value['IMAGE'] == '' || value['IMAGE'] == undefined) {
                                    value['IMAGE'] = 'http://via.placeholder.com/230x150?text=IMG';
                                } else {
                                    value['IMAGE'] = baseurl + value['IMAGE'];
                                }

                                $("div.product-chooser").append(sortcourseHtml(value['ENROLLMENTID'], value['DESCRIPTION'], value['PROGRAMDESCRIPTION'], value['IMAGE'], selected, checked));
                            });
                        } else {
                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: 'Select Enrollment Program'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                            });
                            $("#select-enrollment").select2("val", 0);
							
							$('#degree-id').val('');
							
							$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', selected: 'selected', text: '-- Select Study Program --'}));
                            $("#select-studyprogram").select2("val", 0);
                        }
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            );
        });
    });

    //ini untuk edit; beda dengan yang diatas
    function loadEnrollment(enrollment_id) {
        var value = $('#select-program').select2('val');
        var type = $("#select-program").select2().find(":selected").data("type");
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value},
                    success:function(response)
                    {
                        if(type == 'NON_ACADEMIC') {
                            $("div.product-chooser").html('');
                            $("#enrollment-nonacademic").show('slow');
                            var selected;
                            var checked;
                            $.each(response, function(index, value) {
                                if(value['ENROLLMENTID'] == enrollment_id) { selected = 'selected'; checked = 'checked="checked"'; } 
                                else { selected = ''; checked = ''; }

                                if(value['IMAGE'] == null || value['IMAGE'] == '' || value['IMAGE'] == undefined) {
                                    value['IMAGE'] = 'http://via.placeholder.com/230x150?text=IMG';
                                } else {
                                    value['IMAGE'] = baseurl + value['IMAGE'];
                                }

                                $("div.product-chooser").append(sortcourseHtml(value['ENROLLMENTID'], value['DESCRIPTION'], value['PROGRAMDESCRIPTION'], value['IMAGE'], selected, checked));
                            });
                        } else {
                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: 'Select Enrollment Program'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                            });
                            $("#select-enrollment").select2("val", enrollment_id);
                            loadDegree(enrollment_id);
                        }
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            );
    }

    function sortcourseHtml(id, title, description, img_url, selected, checked)
    {
        return '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">' +
            '<div class="product-chooser-item '+selected+' ">' +
                '<a href="'+img_url+'" target="_blank"><img src="'+img_url+'" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="'+title+'"></a>' +
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">' +
                    '<span class="title">'+title+'</span>' +
                    '<span class="description">'+description+'</span>' +
                    '<input type="radio" name="enrollmentn_id" value="'+id+'" '+checked+'>' +
                '</div>' +
                '<div class="clear"></div>' +
            '</div>' +
        '</div>';

        /*var tes1 = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">' +
            '<div class="product-chooser-item">' +
                '<img src="'+img_url+'" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="'+title+'">' +
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">' +
                    '<span class="title">'+title+'</span>' +
                    '<span class="description">'+description+'</span>' +
                    '<input type="radio" name="enrollment_id" value="'+id+'">' +
                '</div>' +
                '<div class="clear"></div>' +
            '</div>' +
        '</div>';

        var tesxx = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">'+
            '<div class="product-chooser-item selected">'+
                '<img src="http://renswijnmalen.nl/bootstrap/desktop_mobile.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile and Desktop">'+
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">'+
                    '<span class="title">Mobile and Desktop</span>'+
                    '<span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</span>' +
                    '<input type="radio" name="product" value="mobile_desktop" checked="checked">'+
                '</div>'+
                '<div class="clear"></div>'+
            '</div>'+
        '</div>';

        var tesxx1 = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">'+
            '<div class="product-chooser-item">'+
                '<img src="http://renswijnmalen.nl/bootstrap/desktop_mobile.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile and Desktop">'+
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">'+
                    '<span class="title">Mobile and Desktop</span>'+
                    '<span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</span>' +
                    '<input type="radio" name="product" value="mobile_desktop">'+
                '</div>'+
                '<div class="clear"></div>'+
            '</div>'+
        '</div>'; */

        //return tesxx + ' ' + tesxx1;
    }

    function loadDegree(enrollment_id)
    {
        $.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                data:{ enrollmentid:enrollment_id},
                success:function(response)
                {
                    $('#degree-id').val(response.DEGREE);
                    $('#degree-id-hidden').val(response.DEGREEID);
                    $('#programdegree-id').val(response.PROGRAMDEGREEID);
                    if(response.SHORTCOURSE == true){
                        $('.study-program-container').hide();
                        $('#language-delivery-container').hide();
                    } else {
                        $('.study-program-container').show();
                        $('#language-delivery-container').show();
                    }
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
    }
	

    <?php /*
    function loadDegree(program_id, degree_id) {
        $.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getListProgramDegree",
                data:{ programId:program_id},
                success:function(response)
                {
                    $('#select-degree').empty();
                    $("#select-degree").append($('<option>', {value: 0, text: 'Select Degree Program'}));
                    $.each(response, function(index, value) {
                        $("#select-degree").append($('<option>', {value: value['DEGREEID'], text: value['DEGREE']}));
                    });
                    $("#select-degree").select2("val", degree_id);
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
    } */ ?>
</script>
<!-- <script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script> -->


