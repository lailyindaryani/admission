-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 09, 2018 at 01:56 PM
-- Server version: 5.6.39
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ioffice_io`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `COUNTRYID` int(11) NOT NULL,
  `COUNTRYNAMEINA` varchar(255) DEFAULT NULL,
  `PHONECODE` varchar(255) DEFAULT NULL,
  `COUNTRYNAMEENG` varchar(255) DEFAULT NULL,
  `BLACKLIST` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`COUNTRYID`, `COUNTRYNAMEINA`, `PHONECODE`, `COUNTRYNAMEENG`, `BLACKLIST`) VALUES
(1, 'Afganistan', '93', 'Afghanistan', NULL),
(2, 'Afrika Selatan', '27', 'South Africa', NULL),
(3, 'Afrika Tengah', '236', 'Central African', NULL),
(4, 'Albania', '355', 'Albania', NULL),
(5, 'Algeria', '213', 'Algeria', NULL),
(6, 'Amerika Serikat', '1', 'United States of America', NULL),
(7, 'Andorra', '376', 'Andorra', NULL),
(8, 'Angola', '244', 'Angola', NULL),
(9, 'Antigua & Barbuda', '1-268', 'Antigua and Barbuda', NULL),
(10, 'Arab Saudi', '966', 'Saudi Arabia', NULL),
(11, 'Argentina', '54', 'Argentina', NULL),
(12, 'Armenia', '374', 'Armenia', NULL),
(13, 'Australia', '61', 'Australia', NULL),
(14, 'Austria', '43', 'Austria', NULL),
(15, 'Azerbaijan', '994', 'Azerbaijan', NULL),
(16, 'Bahama', '1-242', 'Bahama', NULL),
(17, 'Bahrain', '973', 'Bahrain', NULL),
(18, 'Bangladesh', '880', 'Bangladesh', NULL),
(19, 'Barbados', '1-246', 'Barbados', NULL),
(20, 'Belanda', '31', 'Netherlands', NULL),
(21, 'Belarus', '375', 'Belarus', NULL),
(22, 'Belgia', '32', 'Belgium', NULL),
(23, 'Belize', '501', 'Belize', NULL),
(24, 'Benin', '229', 'Benin', NULL),
(25, 'Bhutan', '975', 'Bhutan', NULL),
(26, 'Bolivia', '591', 'Bolivia', NULL),
(27, 'Bosnia & Herzegovina', '387', 'Bosnia and Herzegovina', NULL),
(28, 'Botswana', '267', 'Botswana', NULL),
(29, 'Brasil', '55', 'Brazil', NULL),
(30, 'Britania Raya (Inggris)', '44', 'United Kingdom', NULL),
(31, 'Brunei Darussalam', '673', 'Brunei Darussalam', NULL),
(32, 'Bulgaria', '359', 'Bulgaria', NULL),
(33, 'Burkina Faso', '226', 'Burkina Faso', NULL),
(34, 'Burundi', '257', 'Burundi', NULL),
(35, 'Ceko', '420', 'Czech Republic', NULL),
(36, 'Chad', '235', 'Chad', NULL),
(37, 'Chili', '56', 'Chile', NULL),
(38, 'China', '86', 'China', NULL),
(39, 'Denmark', '45', 'Denmark', NULL),
(40, 'Djibouti', '253', 'Djibouti', NULL),
(41, 'Dominika', '1-767', 'Dominica', NULL),
(42, 'Ekuador', '593', 'Ecuador', NULL),
(43, 'El Salvador', '503', 'El Salvador', NULL),
(44, 'Eritrea', '291', 'Eritrea', NULL),
(45, 'Estonia', '372', 'Estonia', NULL),
(46, 'Ethiopia', '251', 'Ethiopia', NULL),
(47, 'Fiji', '679', 'Fiji', NULL),
(48, 'Filipina', '63', 'Philippines', NULL),
(49, 'Finlandia', '358', 'Finlandia', NULL),
(50, 'Gabon', '241', 'Gabon', NULL),
(51, 'Gambia', '220', 'Gambia', NULL),
(52, 'Georgia', '995', 'Georgia', NULL),
(53, 'Ghana', '233', 'Ghana', NULL),
(54, 'Grenada', '1-473', 'Grenada', NULL),
(55, 'Guatemala', '502', 'Guatemala', NULL),
(56, 'Guinea', '224', 'Guinea', NULL),
(57, 'Guinea Bissau', '245', 'Guinea-Bissau', NULL),
(58, 'Guinea Khatulistiwa', '240', 'Guinea Khatulistiwa', NULL),
(59, 'Guyana', '592', 'Guyana', NULL),
(60, 'Haiti', '509', 'Haiti', NULL),
(61, 'Honduras', '504', 'Honduras', NULL),
(62, 'Hongaria', '36', 'Hungary', NULL),
(63, 'Hongkong', '852', 'Hong Kong', NULL),
(64, 'India', '91', 'India', NULL),
(65, 'Indonesia', '62', 'Indonesia', NULL),
(66, 'Irak', '964', 'Irak', NULL),
(67, 'Iran', '98', 'Iran', NULL),
(68, 'Irlandia', '353', 'Ireland', NULL),
(69, 'Islandia', '354', 'Iceland', NULL),
(70, 'Israel', '972', 'Israel', NULL),
(71, 'Italia', '39', 'Italy', NULL),
(72, 'Jamaika', '1-876', 'Jamaica', NULL),
(73, 'Jepang', '81', 'Japan', NULL),
(74, 'Jerman', '49', 'Germany', NULL),
(75, 'Yordania', '962', 'Jordan', NULL),
(76, 'Kamboja', '855', 'Cambodia', NULL),
(77, 'Kamerun', '237', 'Cameroon', NULL),
(78, 'Kanada', '1', 'Canada', NULL),
(79, 'Kazakhstan', '7', 'Kazakhstan', NULL),
(80, 'Kenya', '254', 'Kenya', NULL),
(81, 'Kirgizstan', '996', 'Kyrgyzstan', NULL),
(82, 'Kiribati', '686', 'Kiribati', NULL),
(83, 'Kolombia', '57', 'Colombia', NULL),
(84, 'Komoro', '269', 'Comoros', NULL),
(85, 'Republik Kongo', '243', 'Congo Republic', NULL),
(86, 'Korea Selatan', '82', 'South Korea', NULL),
(87, 'Korea Utara', '850', 'North Korea', NULL),
(88, 'Kosta Rika', '506', 'Costa Rica', NULL),
(89, 'Kroasia', '385', 'Croatia', NULL),
(90, 'Kuba', '53', 'Cuba', NULL),
(91, 'Kuwait', '965', 'Kuwait', NULL),
(92, 'Laos', '856', 'Laos', NULL),
(93, 'Latvia', '371', 'Latvia', NULL),
(94, 'Lebanon', '961', 'Lebanon', NULL),
(95, 'Lesotho', '266', 'Lesotho', NULL),
(96, 'Liberia', '231', 'Liberia', NULL),
(97, 'Libya', '218', 'Libya', NULL),
(98, 'Liechtenstein', '423', 'Liechtenstein', NULL),
(99, 'Lituania', '370', 'Lithuania', NULL),
(100, 'Luksemburg', '352', 'Luxembourg', NULL),
(101, 'Madagaskar', '261', 'Madagascar', NULL),
(102, 'Makao', '853', 'Macao', NULL),
(103, 'Makedonia', '389', 'Macedonia', NULL),
(104, 'Maladewa', '960', 'Maldives', NULL),
(105, 'Malawi', '265', 'Malawi', NULL),
(106, 'Malaysia', '60', 'Malaysia', NULL),
(107, 'Mali', '223', 'Mali', NULL),
(108, 'Malta', '356', 'Malta', NULL),
(109, 'Maroko', '212', 'Morocco', NULL),
(110, 'Marshall (Kep.)', '692', 'Marshall Islands', NULL),
(111, 'Mauritania', '222', 'Mauritania', NULL),
(112, 'Mauritius', '230', 'Mauritius', NULL),
(113, 'Meksiko', '52', 'Mexico', NULL),
(114, 'Mesir', '20', 'Egypt', NULL),
(115, 'Mikronesia (Kep.)', '691', 'Micronesia', NULL),
(116, 'Moldova', '373', 'Moldova', NULL),
(117, 'Monako', '377', 'Monaco', NULL),
(118, 'Mongolia', '976', 'Mongolia', NULL),
(119, 'Montenegro', '382', 'Montenegro', NULL),
(120, 'Mozambik', '258', 'Mozambique', NULL),
(121, 'Myanmar', '95', 'Myanmar', NULL),
(122, 'Namibia', '264', 'Namibia', NULL),
(123, 'Nauru', '674', 'Nauru', NULL),
(124, 'Nepal', '977', 'Nepal', NULL),
(125, 'Niger', '227', 'Niger', NULL),
(126, 'Nigeria', '234', 'Nigeria', NULL),
(127, 'Nikaragua', '505', 'Nicaragua', NULL),
(128, 'Norwegia', '47', 'Norway', NULL),
(129, 'Oman', '968', 'Oman', NULL),
(130, 'Pakistan', '92', 'Pakistan', NULL),
(131, 'Palau', '680', 'Palau', NULL),
(132, 'Panama', '507', 'Panama', NULL),
(133, 'Pantai Gading', '225', 'Ivory Coast', NULL),
(134, 'Papua Nugini', '675', 'Papua New Guinea', NULL),
(135, 'Paraguay', '595', 'Paraguay', NULL),
(136, 'Perancis', '33', 'France', NULL),
(137, 'Peru', '51', 'Peru', NULL),
(138, 'Polandia', '48', 'Poland', NULL),
(139, 'Portugal', '351', 'Portugal', NULL),
(140, 'Qatar', '974', 'Qatar', NULL),
(141, 'Rep. Dem. Kongo', '242', 'Congo Republic', NULL),
(142, 'Republik Dominika', '1-809; 1-829', 'Dominican Republic', NULL),
(143, 'Rumania', '40', 'Romania', NULL),
(144, 'Rusia', '7', 'Russia', NULL),
(145, 'Rwanda', '250', 'Rwanda', NULL),
(146, 'Saint Kitts and Nevis', '1-869', 'Saint Kitts and Nevis', NULL),
(147, 'Saint Lucia', '1-758', 'Saint Lucia', NULL),
(148, 'Saint Vincent & the Grenadines', '1-784', 'Saint Vincent and the Grenadines', NULL),
(149, 'Samoa', '685', 'Samoa', NULL),
(150, 'San Marino', '378', 'San Marino', NULL),
(151, 'Sao Tome & Principe', '239', 'Sao Tome and Principe', NULL),
(152, 'Selandia Baru', '64', 'New Zealand', NULL),
(153, 'Senegal', '221', 'Senegal', NULL),
(154, 'Serbia', '381', 'Serbia', NULL),
(155, 'Seychelles', '248', 'Seychelles', NULL),
(156, 'Sierra Leone', '232', 'Sierra Leone', NULL),
(157, 'Singapura', '65', 'Singapore', NULL),
(158, 'Siprus', '357', 'Cyprus', NULL),
(159, 'Slovenia', '386', 'Slovenia', NULL),
(160, 'Slowakia', '421', 'Slovakia', NULL),
(161, 'Solomon (Kep.)', '677', 'Solomon Islands', NULL),
(162, 'Somalia', '252', 'Somalia', NULL),
(163, 'Spanyol', '34', 'Spain', NULL),
(164, 'Sri Lanka', '94', 'Sri Lanka', NULL),
(165, 'Sudan', '249', 'Sudan', NULL),
(166, 'Sudan Selatan', '211', 'South Sudan', NULL),
(167, 'Suriah', '963', 'Syria', NULL),
(168, 'Suriname', '597', 'Suriname', NULL),
(169, 'Swaziland', '268', 'Swaziland', NULL),
(170, 'Swedia', '46', 'Sweden', NULL),
(171, 'Swiss', '41', 'Swiss', NULL),
(172, 'Tajikistan', '992', 'Tajikistan', NULL),
(173, 'Tanjung Verde', '238', 'Cape Verde', NULL),
(174, 'Tanzania', '255', 'Tanzania', NULL),
(175, 'Taiwan', '886', 'Taiwan', NULL),
(176, 'Thailand', '66', 'Thailand', NULL),
(177, 'Timor Leste', '670', 'Timor Leste', NULL),
(178, 'Togo', '228', 'Togo', NULL),
(179, 'Tonga', '676', 'Tonga', NULL),
(180, 'Trinidad & Tobago', '1-868', 'Trinidad and Tobago', NULL),
(181, 'Tunisia', '216', 'Tunisia', NULL),
(182, 'Turki', '90', 'Turkey', NULL),
(183, 'Turkmenistan', '993', 'Turkmenistan', NULL),
(184, 'Tuvalu', '688', 'Tuvalu', NULL),
(185, 'Uganda', '256', 'Uganda', NULL),
(186, 'Ukraina', '380', 'Ukraine', NULL),
(187, 'Uni Emirat Arab', '971', 'United Arab Emirates', NULL),
(188, 'Uruguay', '598', 'Uruguay', NULL),
(189, 'Uzbekistan', '998', 'Uzbekistan', NULL),
(190, 'Vanuatu', '678', 'Vanuatu', NULL),
(191, 'Venezuela', '58', 'Venezuela', NULL),
(192, 'Vietnam', '84', 'Vietnam', NULL),
(193, 'Yaman', '967', 'Yemen', NULL),
(194, 'Yunani', '30', 'Greek', NULL),
(195, 'Zambia', '260', 'Zambia', NULL),
(196, 'Zimbabwe', '263', 'Zimbabwe', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `COUPONID` int(11) NOT NULL,
  `COUPONVALUE` varchar(100) DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`COUPONID`, `COUPONVALUE`, `INPUTBY`, `UPDATEBY`, `INPUTDATE`, `UPDATEDATE`) VALUES
(5, 'COUPONTES1', 1, 1, '2017-11-29 11:06:37', '2017-11-29 11:06:37'),
(6, 'COUPONTES2', 1, 1, '2017-11-29 11:07:14', '2017-11-29 11:07:14'),
(7, 'COUPONTES3', 1, 1, '2017-11-29 11:07:14', '2017-11-29 11:07:14'),
(8, 'COUPONTES4', 1, 1, '2017-11-29 11:07:14', '2017-11-29 11:07:14');

-- --------------------------------------------------------

--
-- Table structure for table `couponmapping`
--

CREATE TABLE `couponmapping` (
  `COUPONMAPPINGID` int(11) NOT NULL,
  `COUPONID` int(11) DEFAULT NULL,
  `ENROLLMENTID` int(11) DEFAULT NULL,
  `COUNTRYID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `couponmapping`
--

INSERT INTO `couponmapping` (`COUPONMAPPINGID`, `COUPONID`, `ENROLLMENTID`, `COUNTRYID`) VALUES
(1, 5, 1, 1),
(2, 6, 1, 1),
(3, 7, 2, 1),
(4, 8, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `COURSEID` int(11) NOT NULL,
  `COURSENAME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courselanguage`
--

CREATE TABLE `courselanguage` (
  `COURSEID` int(11) DEFAULT NULL,
  `LANGUAGEDELIVERYMAPPINGID` int(11) DEFAULT NULL,
  `COURSELANGUAGEID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courseselection`
--

CREATE TABLE `courseselection` (
  `COURSESELECTIONID` int(11) NOT NULL,
  `PARTICIPANTID` int(11) DEFAULT NULL,
  `COURSEID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `degree`
--

CREATE TABLE `degree` (
  `DEGREEID` int(11) NOT NULL,
  `DEGREE` varchar(100) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `degree`
--

INSERT INTO `degree` (`DEGREEID`, `DEGREE`, `INPUTDATE`, `UPDATEDATE`, `INPUTBY`, `UPDATEBY`, `TYPE`) VALUES
(1, 'D3', '2017-10-25 00:00:00', NULL, NULL, NULL, 'DEGREE'),
(2, 'S1', '2017-10-25 00:00:00', NULL, NULL, NULL, 'DEGREE'),
(3, 'S2', '2017-10-25 00:00:00', NULL, NULL, NULL, 'DEGREE'),
(4, 'Hybrid Leadership Program', '2017-10-25 00:00:00', NULL, NULL, NULL, 'SHORT COURSE'),
(5, 'Telco Certification Program', '2017-10-25 00:00:00', NULL, NULL, NULL, 'SHORT COURSE'),
(6, 'Culture Learning Program', '2017-10-25 00:00:00', NULL, NULL, NULL, 'SHORT COURSE'),
(7, 'Culture Camp Program', '2017-10-25 00:00:00', NULL, NULL, NULL, 'SHORT COURSE');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `EDUCATIONID` int(11) NOT NULL,
  `EDUCATIONLEVEL` varchar(25) DEFAULT NULL,
  `SCHOOLNAME` varchar(100) NOT NULL,
  `START` int(11) DEFAULT NULL,
  `END` int(11) DEFAULT NULL,
  `GRADE` varchar(25) DEFAULT NULL,
  `DEGREES` varchar(25) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL,
  `PARTICIPANTID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`EDUCATIONID`, `EDUCATIONLEVEL`, `SCHOOLNAME`, `START`, `END`, `GRADE`, `DEGREES`, `INPUTDATE`, `UPDATEDATE`, `INPUTBY`, `UPDATEBY`, `PARTICIPANTID`) VALUES
(8, 'sman 2 cilacap', '', 2001, 2006, 'A', NULL, '2018-03-07 14:48:04', NULL, 24, NULL, 22),
(9, '', '', 0, 0, NULL, '', '2018-03-07 14:48:04', NULL, 24, NULL, 22),
(10, 'HIGHSCHOOL', 'SMA N 1 Bandung', 2016, 2018, 'IPA', NULL, '2018-07-04 13:04:36', NULL, 32, NULL, 30),
(11, 'UNDERGRADUATE', 'Universitas bandung', 2018, 0, NULL, 'D3', '2018-07-04 13:04:36', NULL, 32, NULL, 30),
(12, 'HIGHSCHOOL', 'dfjkdsj dfjkds 3 jdsjfls', 2010, 2013, 'dfdsfd', NULL, '2018-07-04 14:51:53', NULL, 35, NULL, 33),
(13, 'UNDERGRADUATE', 'dfdsgdsgd', 2013, 2016, NULL, 'dgdsgsd', '2018-07-04 14:51:53', NULL, 35, NULL, 33),
(14, 'HIGHSCHOOL', 'SMAN 1 Subang', 2009, 2016, '30.0', NULL, '2018-07-09 10:37:35', NULL, 38, NULL, 36),
(15, 'UNDERGRADUATE', '', 0, 0, NULL, '', '2018-07-09 10:37:35', NULL, 38, NULL, 36),
(16, 'HIGHSCHOOL', 'sma 1 kaknfk ', 2007, 2010, 'dfdsfd', NULL, '2018-07-09 11:22:22', NULL, 35, NULL, 33),
(17, 'UNDERGRADUATE', 'telkom', 2010, 2014, NULL, 'fdnkjdsnfkjs', '2018-07-09 11:22:22', NULL, 35, NULL, 33);

-- --------------------------------------------------------

--
-- Table structure for table `enrollment`
--

CREATE TABLE `enrollment` (
  `ENROLLMENTID` int(11) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `ACADEMICYEAR` int(11) DEFAULT NULL,
  `PERIOD` varchar(100) DEFAULT NULL,
  `STARTDATE` datetime NOT NULL,
  `ENDDATE` datetime NOT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `PROGRAMDEGREEID` int(11) DEFAULT NULL,
  `ENROLLMENTSTATUS` varchar(100) DEFAULT NULL,
  `PROGRAMDESCRIPTION` varchar(1000) DEFAULT NULL,
  `IMAGE` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enrollment`
--

INSERT INTO `enrollment` (`ENROLLMENTID`, `DESCRIPTION`, `ACADEMICYEAR`, `PERIOD`, `STARTDATE`, `ENDDATE`, `INPUTBY`, `UPDATEBY`, `INPUTDATE`, `UPDATEDATE`, `PROGRAMDEGREEID`, `ENROLLMENTSTATUS`, `PROGRAMDESCRIPTION`, `IMAGE`) VALUES
(1, 'Regular Program 2018', 2018, 'Agust 2018', '2018-07-01 00:00:00', '2018-08-01 00:00:00', 1, 1, '2017-11-02 15:11:05', '2018-07-09 11:44:54', 5, 'OPEN', 'Regular Program 2018', NULL),
(2, 'Scholarship Programe 2018', 2018, 'January 2018', '2017-11-02 00:00:00', '2017-11-30 00:00:00', 1, 1, '2017-11-02 15:31:30', '2018-03-07 13:58:02', 4, 'OPEN', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `FACULTYID` int(11) NOT NULL,
  `IGRACIASFACULTYID` int(11) NOT NULL,
  `FACULTYNAMEINA` varchar(255) DEFAULT NULL,
  `FACULTYNAMEENG` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`FACULTYID`, `IGRACIASFACULTYID`, `FACULTYNAMEINA`, `FACULTYNAMEENG`) VALUES
(3, 3, 'ILMU TERAPAN', 'SCHOOL OF APPLIED SCIENCE '),
(4, 4, 'INDUSTRI KREATIF', 'SCHOOL OF CREATIVE INDUSTRIES '),
(5, 5, 'TEKNIK ELEKTRO', 'SCHOOL OF ELECTRICAL ENGINEERING'),
(6, 6, 'REKAYASA INDUSTRI', 'SCHOOL OF INDUSTRIAL ENGINEERING'),
(7, 7, 'INFORMATIKA', 'SCHOOL OF COMPUTING'),
(8, 8, 'EKONOMI DAN BISNIS', 'SCHOOL OF ECONOMICS AND BUSINESS'),
(9, 9, 'KOMUNIKASI DAN BISNIS', 'SCHOOL OF COMMUNICATION AND BUSINESS');

-- --------------------------------------------------------

--
-- Table structure for table `family`
--

CREATE TABLE `family` (
  `FAMILYID` int(11) NOT NULL,
  `FULLNAME` varchar(250) DEFAULT NULL,
  `RELATIONSHIP` varchar(25) DEFAULT NULL,
  `OCCUPATION` varchar(25) DEFAULT NULL,
  `ADDRESS` varchar(1000) DEFAULT NULL,
  `CITY` varchar(100) DEFAULT NULL,
  `COUNTRYID` int(11) DEFAULT NULL,
  `COUNTRY` varchar(100) NOT NULL,
  `ZIPCODE` int(11) DEFAULT NULL,
  `PHONE` varchar(25) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL,
  `PARTICIPANTID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `family`
--

INSERT INTO `family` (`FAMILYID`, `FULLNAME`, `RELATIONSHIP`, `OCCUPATION`, `ADDRESS`, `CITY`, `COUNTRYID`, `COUNTRY`, `ZIPCODE`, `PHONE`, `EMAIL`, `INPUTDATE`, `UPDATEDATE`, `INPUTBY`, `UPDATEBY`, `PARTICIPANTID`) VALUES
(10, 'BApak', 'FATHER', 'kerja', 'cilacap', 'cilcap', 1, 'Afghanistan', 53261, '+93', 'setiyowidayat@gmail.com', '2018-07-04 14:47:42', '2018-07-04 14:47:42', 24, 24, 22),
(11, 'ibu', 'MOTHER', 'irt', 'cilacap', 'bandung', 2, '2', 53261, '+93564768', 'setiyowidayat@gmail.com', '2018-07-04 14:47:42', '2018-07-06 09:29:28', 24, 24, 22),
(12, 'Abi', 'FATHER', 'Indonesia', 'jl jawa', 'bandung', NULL, 'Indonesia', 45678, '+62 82216568446', 'amiranurkhalipah@gmail.com', '2018-07-04 13:03:26', NULL, 32, NULL, 30),
(13, 'Umi', 'MOTHER', 'Indonesia', 'jl jawa', 'bandung', NULL, 'Indonesia', 45678, '+62 82216568446', 'amiranurkhalipah@gmail.com', '2018-07-04 13:03:26', NULL, 32, NULL, 30),
(14, 'dfnksjfs', 'FATHER', 'dkfjlfdfj', 'djfldsjds dskfdsjf ds kjfdsf d', 'bdg', NULL, 'Indonesia', 49163, '0812465564513', 'rizkielisa@gmail.com', '2018-07-04 14:51:19', NULL, 35, NULL, 33),
(15, 'kddlkmglkdsmglsd', 'MOTHER', 'mkldmdslmf', 'ldsmfdlmfds', 'bdg', NULL, 'Indonesia', 49163, '+6254561321323', 'rizkielisa@gmail.com', '2018-07-04 14:51:19', NULL, 35, NULL, 33),
(16, 'Albert Einstein', 'FATHER', 'Indonesia', 'Jl. Telekomunikasi No. 01, Terusan Buah Batu, Sukapura, Dayeuhkolot, Bandung, Jawa Barat 40257', 'Bandung', NULL, 'Indonesia', 40257, '+6281322282656', 'netmedia.userpaid@gmail.com', '2018-07-09 10:37:11', NULL, 38, NULL, 36),
(17, 'Aisyah', 'MOTHER', 'Indonesia', 'Jl. Telekomunikasi No. 01, Terusan Buah Batu, Sukapura, Dayeuhkolot, Bandung, Jawa Barat 40257', 'Bandung', NULL, 'Indonesia', 40257, '+6281395143869', 'usergues.only@gmail.com', '2018-07-09 10:37:11', NULL, 38, NULL, 36),
(18, 'jdsfkdn', 'FATHER', 'dsdsgdsg', 'dgsdgdsgd hfdhfdh 4 hdhdfd', 'sfsafsa', NULL, 'Indonesia', 49163, '+628211234354', 'sdsadas@gmail.com', '2018-07-09 11:21:42', NULL, 35, NULL, 33),
(19, 'rsbfkjsnfkjs', 'MOTHER', 'nvknv', 'lknbklcxnbklcx', 'vdsbdb', NULL, 'Indonesia', 49163, '+628122569554', 'gdssgdsg@gmail.com', '2018-07-09 11:21:42', NULL, 35, NULL, 33);

-- --------------------------------------------------------

--
-- Table structure for table `languagedelivery`
--

CREATE TABLE `languagedelivery` (
  `LANGUAGEDELIVERYID` int(11) NOT NULL,
  `LANGUAGE` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languagedelivery`
--

INSERT INTO `languagedelivery` (`LANGUAGEDELIVERYID`, `LANGUAGE`) VALUES
(1, 'Indonesian'),
(2, 'English');

-- --------------------------------------------------------

--
-- Table structure for table `languagedeliverymapping`
--

CREATE TABLE `languagedeliverymapping` (
  `LANGUAGEDELIVERYMAPPINGID` int(11) NOT NULL,
  `LANGUAGEDELIVERYID` int(11) DEFAULT NULL,
  `PROGRAMDEGREEID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languagedeliverymapping`
--

INSERT INTO `languagedeliverymapping` (`LANGUAGEDELIVERYMAPPINGID`, `LANGUAGEDELIVERYID`, `PROGRAMDEGREEID`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 2, 1),
(11, 2, 2),
(12, 2, 3),
(13, 2, 4),
(14, 2, 5),
(15, 2, 6),
(16, 2, 7),
(17, 2, 8),
(18, 2, 9),
(19, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `PARTICIPANTID` int(11) NOT NULL,
  `REGISTRATIONNUMBER` varchar(100) DEFAULT NULL,
  `FULLNAME` varchar(250) DEFAULT NULL,
  `PASSPORTNO` varchar(25) DEFAULT NULL,
  `PASSPORTVALID` datetime DEFAULT NULL,
  `PASSPORTISSUED` varchar(100) DEFAULT NULL,
  `BIRTHPLACE` varchar(100) DEFAULT NULL,
  `BIRTHDATE` datetime DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `MARITALSTATUS` varchar(25) DEFAULT NULL,
  `RELIGION` varchar(20) DEFAULT NULL,
  `NATIONALITY` varchar(50) DEFAULT NULL,
  `FIRSTLANGUAGE` varchar(50) DEFAULT NULL,
  `PROFICIENCYEN` varchar(50) DEFAULT NULL,
  `PROFICIENCYINA` varchar(50) DEFAULT NULL,
  `ADDRESS` varchar(1000) DEFAULT NULL,
  `CITY` varchar(100) DEFAULT NULL,
  `COUNTRYID` int(100) DEFAULT NULL,
  `ZIPCODE` int(11) DEFAULT NULL,
  `PHONE` varchar(25) DEFAULT NULL,
  `FAX` varchar(25) DEFAULT NULL,
  `PHOTOURL` varchar(500) DEFAULT NULL,
  `PHOTOFILENAME` varchar(100) DEFAULT NULL,
  `ACADEMICYEAR` int(11) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `USERID` int(11) DEFAULT NULL,
  `LANGUAGEDELIVERYMAPPINGID` int(11) DEFAULT NULL,
  `COUPONMAPPINGID` int(11) DEFAULT NULL,
  `ACCEPTANCESTATUS` varchar(10) DEFAULT NULL,
  `NOTE` varchar(1000) DEFAULT NULL,
  `LOAFILE` varchar(1000) DEFAULT NULL,
  `ENROLLMENTID` int(11) DEFAULT NULL,
  `PROGRAMID` int(11) NOT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `PROGRAMTYPE` varchar(25) DEFAULT NULL,
  `CURRENTSTEP` int(1) NOT NULL,
  `CONFIRM` int(1) NOT NULL,
  `CONFIRMDATE` date NOT NULL,
  `INTERVIEWSTATUS` varchar(25) NOT NULL,
  `INTERVIEWDATE` datetime NOT NULL,
  `INTERVIEWSCORE` int(11) NOT NULL,
  `STUDYPROGRAMID` int(11) NOT NULL,
  `ACCEPTANCEDATE` datetime NOT NULL,
  `PAYMENTDATE` datetime NOT NULL,
  `DEADLINEDATE` datetime NOT NULL,
  `ARRIVALDATE` datetime NOT NULL,
  `STARTINGDATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`PARTICIPANTID`, `REGISTRATIONNUMBER`, `FULLNAME`, `PASSPORTNO`, `PASSPORTVALID`, `PASSPORTISSUED`, `BIRTHPLACE`, `BIRTHDATE`, `GENDER`, `MARITALSTATUS`, `RELIGION`, `NATIONALITY`, `FIRSTLANGUAGE`, `PROFICIENCYEN`, `PROFICIENCYINA`, `ADDRESS`, `CITY`, `COUNTRYID`, `ZIPCODE`, `PHONE`, `FAX`, `PHOTOURL`, `PHOTOFILENAME`, `ACADEMICYEAR`, `INPUTDATE`, `UPDATEDATE`, `USERID`, `LANGUAGEDELIVERYMAPPINGID`, `COUPONMAPPINGID`, `ACCEPTANCESTATUS`, `NOTE`, `LOAFILE`, `ENROLLMENTID`, `PROGRAMID`, `STATUS`, `PROGRAMTYPE`, `CURRENTSTEP`, `CONFIRM`, `CONFIRMDATE`, `INTERVIEWSTATUS`, `INTERVIEWDATE`, `INTERVIEWSCORE`, `STUDYPROGRAMID`, `ACCEPTANCEDATE`, `PAYMENTDATE`, `DEADLINEDATE`, `ARRIVALDATE`, `STARTINGDATE`) VALUES
(4, NULL, 'Yudha Febrianta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+6281321202131', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'NON_ACADEMIC', 0, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, NULL, 'Indah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+6282120022283', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'NON_ACADEMIC', 0, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, NULL, 'ghufran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+62857201997', NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'NON_ACADEMIC', 1, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, NULL, 'dahliar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+828121446627', NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'ACADEMIC', 1, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, NULL, 'fgdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+274433', NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'NON_ACADEMIC', 1, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, NULL, 'SETIYO WIDAYAT', '123123', '2018-03-12 00:00:00', 'Indonesia', 'cilacap', '2018-03-12 00:00:00', 'Male', 'married', 'Islam', 'Indonesia', 'Indonesia', 'Fluent', 'Fluent', 'setiyowidayat@gmail.com', 'bandung', 65, 40287, '+6285292457545', '', './uploads/media/participant/requirement/123123/', 'photo_123123.jpg', NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, 2, 3, NULL, 'ACADEMIC', 6, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, NULL, 'amiranurkh', '123', '2018-12-28 00:00:00', 'BUMN', 'Cirebon', '1994-12-28 00:00:00', 'female', 'single', 'ISLAM', 'Indonesia', 'Inggris', 'GOOD', 'GOOD', 'qwe', 'bandung', 65, 2341, '+6285713444428', '', NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, 1, 5, NULL, 'ACADEMIC', 3, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, NULL, 'amira', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Canada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+112345', NULL, NULL, NULL, NULL, NULL, NULL, 28, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'ACADEMIC', 1, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, NULL, 'Alifah Des', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', NULL, NULL, 'OTHER', '1', '', 'FAIR', 'FAIR', '', '', 1, 0, '+6285315981194', '', NULL, NULL, NULL, NULL, '2018-07-09 11:37:22', 30, NULL, NULL, NULL, '', NULL, NULL, 0, NULL, 'ACADEMIC', 1, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, NULL, 'widayat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Indonesia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+6285292457545', NULL, NULL, NULL, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'ACADEMIC', 1, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, NULL, 'Dian Cantik', '12343556788', '2019-07-31 00:00:00', 'Indonesia', 'Ciamis', '2001-07-17 00:00:00', 'female', 'single', 'ISLAM', '1', 'Inggris', 'FLUENT', 'FLUENT', 'jl ahmad yani', 'ciamis', 65, 554444, '+6285713444428', '234556', './uploads/media/participant/requirement/12343556788/', 'photo_12343556788.jpg', NULL, NULL, '2018-07-06 09:22:03', 32, NULL, NULL, 'ACCEPTED', 'iya,...lulus', NULL, 1, 5, NULL, 'ACADEMIC', 7, 1, '2018-08-15', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, NULL, 'Rizki Elisa', '14512152134564231', '2019-01-31 00:00:00', 'ind', 'gjhgj', '1983-03-16 00:00:00', 'Female', 'married', 'Islam', 'Indonesia', 'Englisdh', 'Fluent', 'Fluent', 'rizkielisa@gmail.com', 'bdg', 65, 49567, '+6281222546786', '', './uploads/media/participant/requirement/14512152134564231/', 'photo_14512152134564231.', NULL, NULL, NULL, 35, NULL, NULL, 'ACCEPTED', 'accepted', NULL, NULL, 4, NULL, 'NON_ACADEMIC', 7, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '2018-07-10 00:00:00', '2018-07-13 00:00:00', '2018-07-25 00:00:00', '2018-07-11 00:00:00', '2018-07-31 00:00:00'),
(36, NULL, 'Mochamad Dwi Syafriadi', 'A312249124BC124', '2020-06-15 00:00:00', 'Indonesia', 'Subang', '1998-06-15 00:00:00', 'Male', 'single', 'Islam', 'Indonesia', 'Indonesia', 'Good', 'Good', 'Jl. Telekomunikasi No. 01, Terusan Buah Batu, Sukapura, Dayeuhkolot, Bandung, Jawa Barat 40257', 'Bandung', 65, 40257, '+6281320046281', '', './uploads/media/participant/requirement/A312249124BC124/', 'photo_A312249124BC124.jpg', NULL, NULL, NULL, 38, NULL, NULL, 'ACCEPTED', '', NULL, 2, 3, NULL, 'ACADEMIC', 7, 0, '0000-00-00', 'Accept', '2018-07-05 00:00:00', 100, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, NULL, 'Intan Pinilih Hanistya', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Indonesia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+6281313280191', NULL, NULL, NULL, NULL, NULL, NULL, 39, NULL, NULL, 'ACCEPTED', '', NULL, 2, 3, NULL, 'ACADEMIC', 7, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, NULL, 'Rizki Elisa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Indonesia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+6281222546786', NULL, NULL, NULL, NULL, NULL, NULL, 41, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'ACADEMIC', 1, 0, '0000-00-00', '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `PROGRAMID` int(11) NOT NULL,
  `PROGRAMNAME` varchar(150) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL,
  `PROGRAMTYPE` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`PROGRAMID`, `PROGRAMNAME`, `INPUTDATE`, `UPDATEDATE`, `INPUTBY`, `UPDATEBY`, `PROGRAMTYPE`) VALUES
(1, 'STUDENT EXCHANGE', '2017-10-25 00:00:00', NULL, NULL, NULL, 'ACADEMIC'),
(2, 'CREDIT EARNING', '2017-10-25 00:00:00', NULL, NULL, NULL, 'ACADEMIC'),
(3, 'SCHOLARSHIP', '2017-10-25 00:00:00', NULL, NULL, NULL, 'ACADEMIC'),
(4, 'SHORT COURSE', '2017-10-25 00:00:00', NULL, NULL, NULL, 'NON_ACADEMIC'),
(5, 'REGULAR', '2017-10-25 00:00:00', NULL, NULL, NULL, 'ACADEMIC');

-- --------------------------------------------------------

--
-- Table structure for table `programdegree`
--

CREATE TABLE `programdegree` (
  `PROGRAMDEGREEID` int(11) NOT NULL,
  `DEGREEID` int(11) DEFAULT NULL,
  `PROGRAMID` int(11) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programdegree`
--

INSERT INTO `programdegree` (`PROGRAMDEGREEID`, `DEGREEID`, `PROGRAMID`, `INPUTDATE`, `UPDATEDATE`, `INPUTBY`, `UPDATEBY`) VALUES
(1, 1, 5, '2017-10-25 13:17:02', NULL, NULL, NULL),
(2, 2, 1, '2017-10-25 13:17:02', NULL, NULL, NULL),
(3, 2, 2, '2017-10-25 13:17:02', NULL, NULL, NULL),
(4, 2, 3, '2017-10-25 13:17:02', NULL, NULL, NULL),
(5, 2, 5, '2017-10-25 13:17:02', NULL, NULL, NULL),
(6, 3, 1, '2017-10-25 13:17:02', NULL, NULL, NULL),
(7, 3, 2, '2017-10-25 13:17:02', NULL, NULL, NULL),
(8, 3, 3, '2017-10-25 13:17:02', NULL, NULL, NULL),
(9, 3, 5, '2017-10-25 13:17:02', NULL, NULL, NULL),
(10, 4, 4, '2017-10-25 13:17:02', NULL, NULL, NULL),
(11, 5, 4, '2017-10-25 13:17:02', NULL, NULL, NULL),
(12, 6, 4, '2017-10-25 13:17:02', NULL, NULL, NULL),
(13, 7, 4, '2017-10-25 13:17:02', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questionaire`
--

CREATE TABLE `questionaire` (
  `QUESTIONID` int(11) NOT NULL,
  `QUESTION` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionaire`
--

INSERT INTO `questionaire` (`QUESTIONID`, `QUESTION`) VALUES
(2, 'How did you know about Telkom University');

-- --------------------------------------------------------

--
-- Table structure for table `questionanswer`
--

CREATE TABLE `questionanswer` (
  `QUESTIONANSWERID` int(11) NOT NULL,
  `QUESTIONID` int(11) DEFAULT NULL,
  `ANSWER` varchar(1000) DEFAULT NULL,
  `ANSWERADDVALUE` varchar(1000) DEFAULT NULL,
  `PARTICIPANTID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questionoption`
--

CREATE TABLE `questionoption` (
  `OPTIONID` int(11) NOT NULL,
  `OPTIONVALUE` varchar(100) DEFAULT NULL,
  `ISTEXT` varchar(1) DEFAULT NULL,
  `QUESTIONID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionoption`
--

INSERT INTO `questionoption` (`OPTIONID`, `OPTIONVALUE`, `ISTEXT`, `QUESTIONID`) VALUES
(6, 'Internet', NULL, 2),
(7, 'Telkom University Site', NULL, 2),
(8, 'Social Media', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quizparticipant`
--

CREATE TABLE `quizparticipant` (
  `IDQUIZ` int(11) NOT NULL,
  `PARTICIPANTID` int(11) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  `STATUS` char(1) DEFAULT NULL COMMENT '1 = DONE QUIZ ; 2 = NOT QUIZ'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizparticipant`
--

INSERT INTO `quizparticipant` (`IDQUIZ`, `PARTICIPANTID`, `DATE`, `STATUS`) VALUES
(1, 22, '2018-03-07 00:00:00', '1'),
(2, 23, '2018-03-12 00:00:00', '1'),
(3, 30, '2018-07-04 00:00:00', '1'),
(4, 33, '2018-07-09 00:00:00', '1'),
(5, 37, '2018-07-09 00:00:00', '1'),
(6, 36, '2018-07-09 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `requirementmapping`
--

CREATE TABLE `requirementmapping` (
  `REQUIREMENTID` int(11) DEFAULT NULL,
  `PROGRAMDEGREEID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requirementmapping`
--

INSERT INTO `requirementmapping` (`REQUIREMENTID`, `PROGRAMDEGREEID`) VALUES
(1, 2),
(1, 6),
(2, 2),
(2, 6),
(3, 4),
(4, 4),
(2, 4),
(3, 8),
(4, 8),
(2, 8),
(5, 8),
(6, 8),
(7, 8),
(8, 8),
(11, 8),
(12, 8),
(9, 10),
(10, 10),
(9, 11),
(10, 11),
(9, 12),
(10, 12),
(9, 13),
(10, 13),
(3, 5),
(4, 5),
(3, 9),
(4, 9),
(5, 9),
(6, 9),
(7, 9),
(8, 9),
(11, 9),
(12, 9);

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
--

CREATE TABLE `requirements` (
  `REQUIREMENTID` int(11) NOT NULL,
  `REQUIREMENTNAME` varchar(100) DEFAULT NULL,
  `TYPE` varchar(10) DEFAULT NULL,
  `TEMPLATEFILE` varchar(1000) DEFAULT NULL,
  `TEMPLATEFILEURL` varchar(1000) DEFAULT NULL,
  `PRIVILEGE` varchar(100) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requirements`
--

INSERT INTO `requirements` (`REQUIREMENTID`, `REQUIREMENTNAME`, `TYPE`, `TEMPLATEFILE`, `TEMPLATEFILEURL`, `PRIVILEGE`, `INPUTDATE`, `UPDATEDATE`, `INPUTBY`, `UPDATEBY`) VALUES
(1, 'Enrollment Letter from the Head of Origin Study program', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(2, 'Personal Statement', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(3, 'Certificate', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(4, 'Transcript', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(5, 'Thesis Plan Proposal', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(6, 'Online Test', 'TEXT', NULL, NULL, '2', '2017-10-25 00:00:00', '2018-03-07 13:23:39', NULL, 1),
(7, 'Interview', 'TEXT', NULL, NULL, '2', '2017-10-25 00:00:00', '2018-03-07 13:24:00', NULL, 1),
(8, 'TOEFL / IELTS Certificate', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(9, 'Passport', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(10, 'Payment Slip', 'ATTACHMENT', NULL, NULL, '1', '2017-10-25 00:00:00', NULL, NULL, NULL),
(11, 'CBT', 'TEXT', NULL, NULL, '2', '2017-10-25 00:00:00', '2018-03-07 13:24:19', NULL, 1),
(12, 'Faculty Interview', 'TEXT', NULL, NULL, '2', '2017-10-25 00:00:00', '2018-03-07 13:24:13', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `requirementvalue`
--

CREATE TABLE `requirementvalue` (
  `REQUIREMENTVALUEID` int(11) NOT NULL,
  `REQUIREMENTID` int(11) DEFAULT NULL,
  `PARTICIPANTID` int(11) DEFAULT NULL,
  `VALUE` varchar(250) DEFAULT NULL,
  `FILENAME` varchar(1000) DEFAULT NULL,
  `FILEURL` varchar(1000) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `INPUTBY` int(11) DEFAULT NULL,
  `UPDATEBY` int(11) DEFAULT NULL,
  `VERIFICATIONSTATUS` varchar(100) DEFAULT NULL,
  `VERIFIEDBY` int(11) DEFAULT NULL,
  `VERIFICATIONDATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requirementvalue`
--

INSERT INTO `requirementvalue` (`REQUIREMENTVALUEID`, `REQUIREMENTID`, `PARTICIPANTID`, `VALUE`, `FILENAME`, `FILEURL`, `INPUTDATE`, `UPDATEDATE`, `INPUTBY`, `UPDATEBY`, `VERIFICATIONSTATUS`, `VERIFIEDBY`, `VERIFICATIONDATE`) VALUES
(7, 2, 22, NULL, '22_xz.jpg', '/media/participant/requirement/', '2018-03-07 14:58:14', NULL, 24, NULL, NULL, NULL, NULL),
(8, 3, 22, NULL, '22_xxxx.jpg', '/media/participant/requirement/', '2018-03-07 14:58:14', NULL, 24, NULL, NULL, NULL, NULL),
(9, 4, 22, NULL, '22_z.jpg', '/media/participant/requirement/', '2018-03-07 14:58:14', NULL, 24, NULL, NULL, NULL, NULL),
(10, 3, 30, NULL, '30_Doc1.pdf', '/uploads/media/participant/requirement/12343556788/', '2018-07-04 13:04:52', '2018-07-04 14:07:26', 32, 32, NULL, NULL, NULL),
(11, 4, 30, NULL, '30_22954.jpg', '/uploads/media/participant/requirement/12343556788/', '2018-07-04 13:04:52', NULL, 32, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `studyprogram`
--

CREATE TABLE `studyprogram` (
  `STUDYPROGRAMID` int(11) NOT NULL,
  `STUDYPROGRAMNAME` varchar(100) DEFAULT NULL,
  `IGRACIASSTUDYPROGRAMID` varchar(100) NOT NULL,
  `FACULTYID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studyprogram`
--

INSERT INTO `studyprogram` (`STUDYPROGRAMID`, `STUDYPROGRAMNAME`, `IGRACIASSTUDYPROGRAMID`, `FACULTYID`) VALUES
(3, 'S1 Administrasi Bisnis - Pindahan', '27', 9),
(4, 'S1 Akuntansi - Pindahan', '28', 8),
(5, 'S2 TEKNIK INFORMATIKA REGULER', '81', 7),
(6, 'S2 TEKNIK INFORMATIKA EKSKLUSIF', '82', 7),
(7, 'S2 TEKNIK ELEKTRO TELEKOMUNIKASI REGULER', '83', 5),
(8, 'S2 TEKNIK ELEKTRO TELEKOMUNIKASI MODULAR', '84', 5),
(9, 'S2 TEKNIK ELEKTRO TELEKOMUNIKASI Wireless Communication REGULER', '01', 5),
(10, 'S2 TEKNIK ELEKTRO TELEKOMUNIKASI Telecommunication REGULER', '02', 5),
(11, 'S2 TEKNIK INFORMATIKA Media Informatics', '03', 7),
(12, 'S2 TEKNIK INFORMATIKA Software Engineering', '04', 7),
(13, 'S2 TEKNIK INFORMATIKA Data Mining', '05', 7),
(14, 'S2 TEKNIK ELEKTRO TELEKOMUNIKASI Wireless Communication MODULAR', '06', 5),
(15, 'S2 TEKNIK ELEKTRO TELEKOMUNIKASI Telecommunication Management MODULAR', '07', 5),
(16, 'S1 Teknik Telekomunikasi - Pindahan', '18', 5),
(17, 'S1 Teknik Telekomunikasi', '11', 5),
(18, 'S1 Teknik Elektro', '12', 5),
(19, 'S1 Sistem Komputer', '13', 5),
(20, 'D3 Teknik Telekomunikasi', '14', 3),
(21, 'S1 Teknik Informatika', '31', 7),
(22, 'D3 Teknik Informatika', '32', 3),
(23, 'S1 Teknik Informatika - Pindahan', '19', 7),
(24, 'S1 Sistem Informasi', '22', 6),
(25, 'S1 Teknik Industri', '21', 6),
(26, 'S1 Ilmu Komputasi', '61', 7),
(27, 'S1 Teknik Fisika', '62', 5),
(28, 'S2 Informatika', '77', 7),
(29, 'S2 Elektro - Telekomunikasi', '78', 5),
(30, 'S1 Sistem Komputer - Pindahan', '17', 5),
(31, 'S1 International ICT Business', '40', 8),
(32, 'S1 Manajemen (Manajemen Bisnis Telekomunikasi & Informatika)', '41', 8),
(33, 'S1 Akuntansi', '42', 8),
(34, 'S1 Ilmu Komunikasi', '43', 9),
(35, 'S1 Administrasi Bisnis', '44', 9),
(36, 'S1 Desain Komunikasi Visual - IM', '45', NULL),
(37, 'D3 Manajemen Pemasaran', '51', 3),
(38, 'D3 Teknik Komputer', '71', 3),
(39, 'D3 Manajemen Informatika', '72', 3),
(40, 'D3 Komputerisasi Akuntansi', '73', 3),
(41, 'S1 Desain Komunikasi Visual', '91', 4),
(42, 'S1 Kriya Tekstil dan Mode', '92', 4),
(43, 'S1 Desain Interior', '93', 4),
(44, 'S1 Desain Produk', '94', 4),
(45, 'S1 Seni Rupa', '95', 4),
(46, 'S2 Manajemen', '79', 8),
(47, 'D3 Perhotelan', '54', 3),
(48, 'S1 Teknik Industri (Internasional Class)', '23', 6),
(49, 'S1 Sistem Informasi (International Class)', '24', 6),
(50, 'S2 Teknik Industri', '80', 6),
(51, 'D4 Sistem Multimedia', '33', 3),
(52, 'S1 Digital Public Relations', '47', 9),
(53, 'S1 Teknik Telekomunikasi (International Class)', '26', 5),
(54, 'S1 Teknik Informatika (International Class)', '25', 7),
(55, 'S1 Administrasi Bisnis (International Class)', '46', 9),
(56, 'S1 Teknologi Informasi', '38', 7),
(57, 'S1 Ilmu Komunikasi (International Class)', '48', 9),
(58, 'S1 Teknik Elektro  (International Class)', '96', 5),
(59, 'S1 Desain Komunikasi Visual  (International Class)', '97', 4);

-- --------------------------------------------------------

--
-- Table structure for table `studyprogramlanguage`
--

CREATE TABLE `studyprogramlanguage` (
  `STUDYPROGRAMID` int(11) DEFAULT NULL,
  `LANGUAGEDELIVERYMAPPINGID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studyprogramlanguage`
--

INSERT INTO `studyprogramlanguage` (`STUDYPROGRAMID`, `LANGUAGEDELIVERYMAPPINGID`) VALUES
(54, 19);

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `USERGROUPID` int(11) NOT NULL,
  `USERGROUPNAME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usergroups`
--

INSERT INTO `usergroups` (`USERGROUPID`, `USERGROUPNAME`) VALUES
(1, 'ADMIN'),
(2, 'PARTICIPANT'),
(3, 'STAFF'),
(4, 'STRUCTURAL');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `USERID` int(11) NOT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(1000) DEFAULT NULL,
  `FULLNAME` varchar(250) DEFAULT NULL,
  `ACTIVESTATUS` varchar(1) DEFAULT NULL,
  `CONFIRMATIONKEY` varchar(100) DEFAULT NULL,
  `INPUTDATE` datetime DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `USERGROUPID` int(11) DEFAULT NULL,
  `ISSUBMITQUESTIONAIRE` varchar(1) DEFAULT NULL,
  `CURRENTSTEP` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`USERID`, `EMAIL`, `PASSWORD`, `FULLNAME`, `ACTIVESTATUS`, `CONFIRMATIONKEY`, `INPUTDATE`, `UPDATEDATE`, `USERGROUPID`, `ISSUBMITQUESTIONAIRE`, `CURRENTSTEP`) VALUES
(1, 'admin', '4297f44b13955235245b2497399d7a93', 'Admin IO', 'Y', NULL, '2017-10-26 11:29:56', '2017-10-26 11:30:00', 1, '', 0),
(2, 'user', '4297f44b13955235245b2497399d7a93', 'Partisipan', 'N', NULL, '2017-10-26 12:25:50', '2018-07-06 09:30:12', 2, NULL, 0),
(3, 'staff', '4297f44b13955235245b2497399d7a93', 'Setiyo Widayat', 'Y', NULL, '2017-11-02 15:06:17', '2017-11-02 15:06:22', 3, NULL, 0),
(5, 'muhammad.ghufran4@gmail.com', '4297f44b13955235245b2497399d7a93', 'Muhammad Ghufran', 'Y', NULL, '2018-01-16 01:38:36', NULL, 2, NULL, 0),
(6, 'yudha.febrianta@gmail.com', '387b943fe60ad1074665121965d79289', 'Yudha Febrianta', 'Y', NULL, '2018-01-16 09:01:00', NULL, 2, NULL, 0),
(7, 'rayforhuman@yahoo.com', 'f3385c508ce54d577fd205a1b2ecdfb7', 'Indah', 'Y', NULL, '2018-01-16 09:03:30', NULL, 2, NULL, 0),
(10, 'pingonlineshop240109@gmail.com', '4297f44b13955235245b2497399d7a93', 'ghufran', 'N', '4a9c83fccf8d09f7b5d06dcd817ed5f2', '2018-01-16 09:59:46', NULL, 2, NULL, 0),
(12, 'ananda@telkomuniversity.ac.id', 'ed2b1f468c5f915f3f1cf75d7068baae', 'dahliar', 'Y', '7e4d2d8543d0f9796b78a0d76c43d629', '2018-01-17 10:58:44', NULL, 2, NULL, 0),
(19, 'indah@io.telkomuniversity.ac.id', 'f3385c508ce54d577fd205a1b2ecdfb7', 'fgdf', 'N', '84c19e9bcd2a8400ef72fa127bc4c8bf', '2018-01-18 04:46:16', NULL, 2, NULL, 0),
(24, 'setiyowidayat@gmail.com', '4297f44b13955235245b2497399d7a93', 'SETIYO WIDAYAT', 'Y', '2e8773b2b139cf2902656b764a01bd8b', '2018-03-07 06:29:30', NULL, 2, NULL, 0),
(25, 'amiranurkh@telkomuniversity.ac.id', '202cb962ac59075b964b07152d234b70', 'amiranurkh', 'Y', '4c79fa746c28578ded0f6492ae9cd2ae', '2018-03-12 07:29:10', '2018-03-12 07:33:48', 2, NULL, 0),
(28, 'amiracoklat@gmail.com', '202cb962ac59075b964b07152d234b70', 'amira', 'Y', '72ba9b9337fd1ab50b639cfeda8e5225', '2018-06-06 02:36:12', NULL, 2, NULL, 1),
(30, 'destialifah@gmail.com', '4297f44b13955235245b2497399d7a93', 'Alifah Des', 'N', 'a7fbc3123436c868d0572083726aee23', '2018-07-04 02:05:31', NULL, 2, NULL, 0),
(31, 'widayat.setiyo@gmail.com', '4297f44b13955235245b2497399d7a93', 'widayat', 'Y', '932db5d5768234701fdac0f05c0c5ba7', '2018-07-04 05:40:46', NULL, 2, NULL, 0),
(32, 'fitrianiekadian@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Dian Cantik', 'Y', '5ed3a201277de1f56904cd39d01d4a7b', '2018-07-04 05:50:19', '2018-07-09 02:40:47', 2, NULL, 0),
(35, 'rizkielisa@gmail.com', '6b2244ecf5881e4aa6d4235d16be7b48', 'Rizki Elisa', 'Y', '5991308dc3fa39cf57bd738c8e876644', '2018-07-04 07:35:24', NULL, 2, NULL, 0),
(38, 'syafriadidwi@gmail.com', '4297f44b13955235245b2497399d7a93', 'Mochamad Dwi Syafriadi', 'Y', 'bd870390d9ad50056738dc71f26717d5', '2018-07-06 02:10:59', '2018-07-06 09:25:41', 2, NULL, 0),
(39, 'intanpinilih97@gmail.com', '4297f44b13955235245b2497399d7a93', 'Intan Pinilih Hanistya', 'Y', 'fcc8a6d32ce9dc320fcd47de3316e0f4', '2018-07-06 02:11:46', '2018-07-06 09:35:39', 2, NULL, 0),
(41, 'elisanalawati@telkomuniversity.ac.id', '6b2244ecf5881e4aa6d4235d16be7b48', 'Rizki Elisa', 'Y', '76c9e16b60daed7a4dd3cf77397db0e4', '2018-07-09 02:12:09', NULL, 2, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`COUNTRYID`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`COUPONID`),
  ADD KEY `FK_INPUTBY8` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY8` (`UPDATEBY`);

--
-- Indexes for table `couponmapping`
--
ALTER TABLE `couponmapping`
  ADD PRIMARY KEY (`COUPONMAPPINGID`),
  ADD KEY `FK_COUNTRYID` (`COUNTRYID`),
  ADD KEY `FK_COUPONID` (`COUPONID`),
  ADD KEY `FK_ENROLLMENTID` (`ENROLLMENTID`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`COURSEID`);

--
-- Indexes for table `courselanguage`
--
ALTER TABLE `courselanguage`
  ADD PRIMARY KEY (`COURSELANGUAGEID`),
  ADD KEY `FK_COURSEID2` (`COURSEID`),
  ADD KEY `FK_LANGUAGEDELIVERYMAPPINGID8` (`LANGUAGEDELIVERYMAPPINGID`);

--
-- Indexes for table `courseselection`
--
ALTER TABLE `courseselection`
  ADD PRIMARY KEY (`COURSESELECTIONID`),
  ADD KEY `fk_participantidqw` (`PARTICIPANTID`),
  ADD KEY `fk_courseid12` (`COURSEID`);

--
-- Indexes for table `degree`
--
ALTER TABLE `degree`
  ADD PRIMARY KEY (`DEGREEID`),
  ADD KEY `FK_INPUTBY3` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY3` (`UPDATEBY`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`EDUCATIONID`),
  ADD KEY `FK_PARTICIPANTID1` (`PARTICIPANTID`),
  ADD KEY `FK_INPUTBY1` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY1` (`UPDATEBY`);

--
-- Indexes for table `enrollment`
--
ALTER TABLE `enrollment`
  ADD PRIMARY KEY (`ENROLLMENTID`),
  ADD KEY `FK_INPUTBY7` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY7` (`UPDATEBY`),
  ADD KEY `FK_PROGRAMDEGREEID3` (`PROGRAMDEGREEID`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`FACULTYID`);

--
-- Indexes for table `family`
--
ALTER TABLE `family`
  ADD PRIMARY KEY (`FAMILYID`),
  ADD KEY `FK_PARTICIPANTID` (`PARTICIPANTID`),
  ADD KEY `FK_INPUTBY` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY` (`UPDATEBY`),
  ADD KEY `FK_COUNTRYFAMILY` (`COUNTRYID`);

--
-- Indexes for table `languagedelivery`
--
ALTER TABLE `languagedelivery`
  ADD PRIMARY KEY (`LANGUAGEDELIVERYID`);

--
-- Indexes for table `languagedeliverymapping`
--
ALTER TABLE `languagedeliverymapping`
  ADD PRIMARY KEY (`LANGUAGEDELIVERYMAPPINGID`),
  ADD KEY `FK_LANGUAGEDELIVERYID` (`LANGUAGEDELIVERYID`),
  ADD KEY `FK_PROGRAMDEGREEID` (`PROGRAMDEGREEID`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`PARTICIPANTID`),
  ADD KEY `FK_USERID1` (`USERID`),
  ADD KEY `FK_COUNTRYID1` (`COUNTRYID`),
  ADD KEY `FK_LANGUAGEDELIVERYMAPPINGID1` (`LANGUAGEDELIVERYMAPPINGID`),
  ADD KEY `FK_ENROLLMENTID1` (`ENROLLMENTID`),
  ADD KEY `FK_COUPONMAPPINGID` (`COUPONMAPPINGID`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`PROGRAMID`),
  ADD KEY `FK_INPUTBY2` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY2` (`UPDATEBY`);

--
-- Indexes for table `programdegree`
--
ALTER TABLE `programdegree`
  ADD PRIMARY KEY (`PROGRAMDEGREEID`),
  ADD KEY `FK_INPUTBY4` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY4` (`UPDATEBY`),
  ADD KEY `FK_DEGREEID` (`DEGREEID`),
  ADD KEY `FK_PROGRAMID` (`PROGRAMID`);

--
-- Indexes for table `questionaire`
--
ALTER TABLE `questionaire`
  ADD PRIMARY KEY (`QUESTIONID`);

--
-- Indexes for table `questionanswer`
--
ALTER TABLE `questionanswer`
  ADD PRIMARY KEY (`QUESTIONANSWERID`),
  ADD KEY `FK_QUESTIONID1` (`QUESTIONID`),
  ADD KEY `FK_PARTICIPANTID2` (`PARTICIPANTID`);

--
-- Indexes for table `questionoption`
--
ALTER TABLE `questionoption`
  ADD PRIMARY KEY (`OPTIONID`),
  ADD KEY `FK_QUESTIONID` (`QUESTIONID`);

--
-- Indexes for table `quizparticipant`
--
ALTER TABLE `quizparticipant`
  ADD PRIMARY KEY (`IDQUIZ`);

--
-- Indexes for table `requirementmapping`
--
ALTER TABLE `requirementmapping`
  ADD KEY `FK_REQUIREMENTID` (`REQUIREMENTID`),
  ADD KEY `FK_PROGRAMDEGREEID2` (`PROGRAMDEGREEID`);

--
-- Indexes for table `requirements`
--
ALTER TABLE `requirements`
  ADD PRIMARY KEY (`REQUIREMENTID`),
  ADD KEY `FK_INPUTBY5` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY5` (`UPDATEBY`);

--
-- Indexes for table `requirementvalue`
--
ALTER TABLE `requirementvalue`
  ADD PRIMARY KEY (`REQUIREMENTVALUEID`),
  ADD KEY `FK_INPUTBY9` (`INPUTBY`),
  ADD KEY `FK_UPDATEBY9` (`UPDATEBY`),
  ADD KEY `FK_REQUIREMENTID9` (`REQUIREMENTID`),
  ADD KEY `FK_PARTICIPANTID9` (`PARTICIPANTID`);

--
-- Indexes for table `studyprogram`
--
ALTER TABLE `studyprogram`
  ADD PRIMARY KEY (`STUDYPROGRAMID`),
  ADD KEY `FK_FACULTYID` (`FACULTYID`);

--
-- Indexes for table `studyprogramlanguage`
--
ALTER TABLE `studyprogramlanguage`
  ADD KEY `FK_STUDYPROGRAMID1` (`STUDYPROGRAMID`),
  ADD KEY `FK_LANGUAGEDELIVERYMAPPINGID` (`LANGUAGEDELIVERYMAPPINGID`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`USERGROUPID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`USERID`),
  ADD UNIQUE KEY `EMAIL` (`EMAIL`),
  ADD KEY `FK_USERGROUPID` (`USERGROUPID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `COUPONID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `couponmapping`
--
ALTER TABLE `couponmapping`
  MODIFY `COUPONMAPPINGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `COURSEID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courselanguage`
--
ALTER TABLE `courselanguage`
  MODIFY `COURSELANGUAGEID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courseselection`
--
ALTER TABLE `courseselection`
  MODIFY `COURSESELECTIONID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `degree`
--
ALTER TABLE `degree`
  MODIFY `DEGREEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `EDUCATIONID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `enrollment`
--
ALTER TABLE `enrollment`
  MODIFY `ENROLLMENTID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `FACULTYID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `family`
--
ALTER TABLE `family`
  MODIFY `FAMILYID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `languagedelivery`
--
ALTER TABLE `languagedelivery`
  MODIFY `LANGUAGEDELIVERYID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languagedeliverymapping`
--
ALTER TABLE `languagedeliverymapping`
  MODIFY `LANGUAGEDELIVERYMAPPINGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `PARTICIPANTID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `PROGRAMID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `programdegree`
--
ALTER TABLE `programdegree`
  MODIFY `PROGRAMDEGREEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `questionaire`
--
ALTER TABLE `questionaire`
  MODIFY `QUESTIONID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `questionanswer`
--
ALTER TABLE `questionanswer`
  MODIFY `QUESTIONANSWERID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `questionoption`
--
ALTER TABLE `questionoption`
  MODIFY `OPTIONID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `quizparticipant`
--
ALTER TABLE `quizparticipant`
  MODIFY `IDQUIZ` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `requirements`
--
ALTER TABLE `requirements`
  MODIFY `REQUIREMENTID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `requirementvalue`
--
ALTER TABLE `requirementvalue`
  MODIFY `REQUIREMENTVALUEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `studyprogram`
--
ALTER TABLE `studyprogram`
  MODIFY `STUDYPROGRAMID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `USERGROUPID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `USERID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coupon`
--
ALTER TABLE `coupon`
  ADD CONSTRAINT `FK_INPUTBY8` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY8` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `couponmapping`
--
ALTER TABLE `couponmapping`
  ADD CONSTRAINT `FK_COUNTRYID` FOREIGN KEY (`COUNTRYID`) REFERENCES `country` (`COUNTRYID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_COUPONID` FOREIGN KEY (`COUPONID`) REFERENCES `coupon` (`COUPONID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ENROLLMENTID` FOREIGN KEY (`ENROLLMENTID`) REFERENCES `enrollment` (`ENROLLMENTID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courselanguage`
--
ALTER TABLE `courselanguage`
  ADD CONSTRAINT `FK_COURSEID2` FOREIGN KEY (`COURSEID`) REFERENCES `course` (`COURSEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_LANGUAGEDELIVERYMAPPINGID8` FOREIGN KEY (`LANGUAGEDELIVERYMAPPINGID`) REFERENCES `languagedeliverymapping` (`LANGUAGEDELIVERYMAPPINGID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courseselection`
--
ALTER TABLE `courseselection`
  ADD CONSTRAINT `fk_courseid12` FOREIGN KEY (`COURSEID`) REFERENCES `course` (`COURSEID`),
  ADD CONSTRAINT `fk_participantidqw` FOREIGN KEY (`PARTICIPANTID`) REFERENCES `participants` (`PARTICIPANTID`);

--
-- Constraints for table `degree`
--
ALTER TABLE `degree`
  ADD CONSTRAINT `FK_INPUTBY3` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY3` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `education`
--
ALTER TABLE `education`
  ADD CONSTRAINT `FK_INPUTBY1` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PARTICIPANTID1` FOREIGN KEY (`PARTICIPANTID`) REFERENCES `participants` (`PARTICIPANTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY1` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `enrollment`
--
ALTER TABLE `enrollment`
  ADD CONSTRAINT `FK_INPUTBY7` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PROGRAMDEGREEID3` FOREIGN KEY (`PROGRAMDEGREEID`) REFERENCES `programdegree` (`PROGRAMDEGREEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY7` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `family`
--
ALTER TABLE `family`
  ADD CONSTRAINT `FK_COUNTRYFAMILY` FOREIGN KEY (`COUNTRYID`) REFERENCES `country` (`COUNTRYID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_INPUTBY` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PARTICIPANTID` FOREIGN KEY (`PARTICIPANTID`) REFERENCES `participants` (`PARTICIPANTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `languagedeliverymapping`
--
ALTER TABLE `languagedeliverymapping`
  ADD CONSTRAINT `FK_LANGUAGEDELIVERYID` FOREIGN KEY (`LANGUAGEDELIVERYID`) REFERENCES `languagedelivery` (`LANGUAGEDELIVERYID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PROGRAMDEGREEID` FOREIGN KEY (`PROGRAMDEGREEID`) REFERENCES `programdegree` (`PROGRAMDEGREEID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `FK_COUNTRYID1` FOREIGN KEY (`COUNTRYID`) REFERENCES `country` (`COUNTRYID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_COUPONMAPPINGID` FOREIGN KEY (`COUPONMAPPINGID`) REFERENCES `couponmapping` (`COUPONMAPPINGID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ENROLLMENTID1` FOREIGN KEY (`ENROLLMENTID`) REFERENCES `enrollment` (`ENROLLMENTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_LANGUAGEDELIVERYMAPPINGID1` FOREIGN KEY (`LANGUAGEDELIVERYMAPPINGID`) REFERENCES `languagedeliverymapping` (`LANGUAGEDELIVERYMAPPINGID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_USERID1` FOREIGN KEY (`USERID`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `program`
--
ALTER TABLE `program`
  ADD CONSTRAINT `FK_INPUTBY2` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY2` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `programdegree`
--
ALTER TABLE `programdegree`
  ADD CONSTRAINT `FK_DEGREEID` FOREIGN KEY (`DEGREEID`) REFERENCES `degree` (`DEGREEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_INPUTBY4` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PROGRAMID` FOREIGN KEY (`PROGRAMID`) REFERENCES `program` (`PROGRAMID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY4` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questionanswer`
--
ALTER TABLE `questionanswer`
  ADD CONSTRAINT `FK_PARTICIPANTID2` FOREIGN KEY (`PARTICIPANTID`) REFERENCES `participants` (`PARTICIPANTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_QUESTIONID1` FOREIGN KEY (`QUESTIONID`) REFERENCES `questionaire` (`QUESTIONID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questionoption`
--
ALTER TABLE `questionoption`
  ADD CONSTRAINT `FK_QUESTIONID` FOREIGN KEY (`QUESTIONID`) REFERENCES `questionaire` (`QUESTIONID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requirementmapping`
--
ALTER TABLE `requirementmapping`
  ADD CONSTRAINT `FK_PROGRAMDEGREEID2` FOREIGN KEY (`PROGRAMDEGREEID`) REFERENCES `programdegree` (`PROGRAMDEGREEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_REQUIREMENTID` FOREIGN KEY (`REQUIREMENTID`) REFERENCES `requirements` (`REQUIREMENTID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requirements`
--
ALTER TABLE `requirements`
  ADD CONSTRAINT `FK_INPUTBY5` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY5` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requirementvalue`
--
ALTER TABLE `requirementvalue`
  ADD CONSTRAINT `FK_INPUTBY9` FOREIGN KEY (`INPUTBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PARTICIPANTID9` FOREIGN KEY (`PARTICIPANTID`) REFERENCES `participants` (`PARTICIPANTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_REQUIREMENTID9` FOREIGN KEY (`REQUIREMENTID`) REFERENCES `requirements` (`REQUIREMENTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UPDATEBY9` FOREIGN KEY (`UPDATEBY`) REFERENCES `users` (`USERID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `studyprogram`
--
ALTER TABLE `studyprogram`
  ADD CONSTRAINT `FK_FACULTYID` FOREIGN KEY (`FACULTYID`) REFERENCES `faculty` (`FACULTYID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `studyprogramlanguage`
--
ALTER TABLE `studyprogramlanguage`
  ADD CONSTRAINT `FK_LANGUAGEDELIVERYMAPPINGID` FOREIGN KEY (`LANGUAGEDELIVERYMAPPINGID`) REFERENCES `languagedeliverymapping` (`LANGUAGEDELIVERYMAPPINGID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_STUDYPROGRAMID1` FOREIGN KEY (`STUDYPROGRAMID`) REFERENCES `studyprogram` (`STUDYPROGRAMID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_USERGROUPID` FOREIGN KEY (`USERGROUPID`) REFERENCES `usergroups` (`USERGROUPID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
