(function ($) {
    $.fn.actionHelper = function (options) {

        var defaults = {
            title: "Confirmation for deletion",
            actionType: "delete",
            ipage: "",
            dialogObject: dialogObject,
            addinfo: "",
            confirmationInfo: "",
            addinfoDel: "",
            tableParent: null,
            strCondition: "",
            statusAfterProcessed: "",
            customUrl: "",
            dataParam: null,
            completeAction: function (dataParam) {
            }
        };

        var options = $.extend(defaults, options);

        function clickHandler(element, options) {

            switch (options.actionType.toLowerCase()) {
                case "delete" :
                    var ajaxUrl = libraryAjaxUrl + "ajax." + options.ipage + ".php?&act=delete";
                    if (options.customUrl != "") {
                        ajaxUrl = libraryAjaxUrl + options.customUrl;
                    }
                    dialogObject.dialog({
                        title: options.title
                    });
                    if (options.confirmationInfo == "")
                        options.confirmationInfo = "Are you sure to delete the record with <b>" + options.addinfo + "</b> ?";
                    ShowConfirmation(options.confirmationInfo, function () {
                        ShowLoading();
                        $.ajax({
                            type: "get",
                            dataType: "html",
                            url: ajaxUrl,
                            async: false,
                            data: options.strCondition,
                            success: function (data) {
                                try {
                                    if (data.indexOf("success") != -1) {
                                        ShowNotification("Record with <b>" + options.addinfo + "</b> successfully deleted", "information");
                                        var par = getRowParent(element);
                                        $(par).remove();
                                    } else if (data.indexOf("disactivatingPayroll") != -1) {
                                        ShowNotification("Gaji <b>" + options.addinfo + "</b> berhasil di non-aktifkan", "information");
                                        var par = getRowParent(element);
                                        $(par).remove();
                                    } else if (data.indexOf("unclosePayroll") != -1) {
                                        ShowNotification("Gaji <b>" + options.addinfo + "</b> belum di close", "information");
                                    } else {
                                        ShowNotification("Record failed to be deleted..!!" + (options.addinfoDel || " "), "failed");
                                    }
                                } catch (err) {
                                    ShowNotification("Error happened, request can't be proceed..!!", "error");
                                }
                            },
                            failed: function () {
                                ShowNotification("Unexpected error happened..!!", "error");
                            },
                            error: function () {
                                ShowNotification("Unexpected error happened..!!", "error");
                            }
                        });
                        options.completeAction(options.dataParam);
                    });
                    break;
                case "deletepermanent" :
                    var ajaxUrl = libraryAjaxUrl + "ajax." + options.ipage + ".php?&act=deletetotrash";
                    if (options.customUrl != "") {
                        ajaxUrl = libraryAjaxUrl + options.customUrl;
                    }
                    dialogObject.dialog({
                        title: options.title
                    });
                    if (options.confirmationInfo == "")
                        options.confirmationInfo = "Are you sure to permanently delete the record with <b>" + options.addinfo + "</b> ?";
                    ShowConfirmation(options.confirmationInfo, function () {
                        ShowLoading();
                        $.ajax({
                            type: "get",
                            dataType: "html",
                            url: ajaxUrl,
                            async: false,
                            data: options.strCondition,
                            success: function (data) {
                                try {
                                    if (data.indexOf("success") != -1) {
                                        ShowNotification("Record with <b>" + options.addinfo + "</b> successfully deleted permanently", "information");
                                        var par = getRowParent(element);
                                        $(par).remove();
                                    } else {
                                        ShowNotification("Record failed to be deleted..!!" + (options.addinfoDel || " "), "failed");
                                    }
                                } catch (err) {
                                    ShowNotification("Error happened, request can't be proceed..!!", "error");
                                }
                            },
                            failed: function () {
                                ShowNotification("Unexpected error happened..!!", "error");
                            },
                            error: function () {
                                ShowNotification("Unexpected error happened..!!", "error");
                            }
                        });
                        options.completeAction(options.dataParam);
                    });
                    break;
                case "setstatus" :
                    var ajaxUrl = libraryAjaxUrl + "ajax." + options.ipage + ".php?&act=" + $(element).attr('status');
                    if (options.customUrl != "") {
                        ajaxUrl = libraryAjaxUrl + options.customUrl;
                    }
                    dialogObject.dialog({
                        title: options.title
                    });
                    if (options.confirmationInfo == "")
                        options.confirmationInfo = "Are you sure to " + ($(element).attr('status') == 'active' ? "activate" : "deactivate") + " record with <b>" + options.addinfo + "</b> ?";


                    ShowConfirmation(options.confirmationInfo, function () {
                        ShowLoading();
                        $.ajax({
                            type: "get",
                            dataType: "html",
                            url: ajaxUrl,
                            async: false,
                            data: options.strCondition,
                            success: function (data) {
                                try {
                                    if (data.indexOf("success") != -1) {
                                        var par = getRowParent(element);
                                        if ($(element).attr('status') == 'nonactive') {
                                            ShowNotification("Record with " + options.addinfo + " now not active", "information");
                                            $(".status", par).html('TIDAK');
                                            $(element).children('img').attr('src', imageUrl + 'accept.png');
                                            $(element).attr('title', 'Set active');
                                            $(element).attr('status', 'active');
                                        } else if ($(element).attr('status') == 'active') {
                                            ShowNotification("Record with " + options.addinfo + " now active", "information");
                                            $(".status", par).html('YA');
                                            $(element).children('img').attr('src', imageUrl + 'delete.png');
                                            $(element).attr('title', 'Set nonactive');
                                            $(element).attr('status', 'nonactive');
                                        } else {
                                            ShowNotification("Record with " + options.addinfo + " is now " + $(element).attr('status'), "information");
                                            $(".status", par).html('YA');
                                            $(element).children('img').attr('src', imageUrl + 'delete.png');
                                            $(element).attr('title', 'Set nonactive');
                                            $(element).attr('status', 'nonactive');
                                        }
                                    } else {
                                        ShowNotification("failed to process..!!", "failed");
                                    }
                                } catch (err) {
                                    ShowNotification("Error happened, request can't be proceed..!!", "error");
                                }
                            },
                            failed: function () {
                                ShowNotification("Unexpected error happened..!!", "error");
                            },
                            error: function () {
                                ShowNotification("Unexpected error happened..!!", "error");
                            }
                        });
                        options.completeAction(options.dataParam);
                    });
                    break;
                default:
                    ShowNotification("Action Type not found.", "error");
                    return false;
                    break;
            }
            return false;
        }

        function getRowParent(element) {
            return $(element).parents("tr");
        }

        function getTableParent(element) {
            return $(element).parents("table");
        }

        function init() {
            if (dialogObject == null) {
                loadDialogScript();
                dialogObject = cDialog.dialog({
                    autoOpen: false,
                });
            }
        }

        return this.each(function (index, element) {
            obj = $(element);
            init();
            obj.bind('click', function () {
                clickHandler($(element), options);
                return false;
            });
            // end if

        });
    };
})(jQuery);