var cDialog,dialogObject,dialogObjectConfirmation;
//$(document).ready(function(){
	$(".confirmDiv").css('width','500px');
cDialog=$(".confirmDiv").clone(); 
	
	cDialog.attr('class','messageDiv');
	$('body').append(cDialog);	

function ShowMessage(message, type) {

    bootbox.dialog({
        message: message,
        title: "Message",
        buttons: {
            success: {
                label: "Close",
                className: "btn-success",
                callback: function () {
                    bootbox.hideAll();
                }
            }
        }
    });
}

function loadDialogScript(){
	$.ajax({
		url:javaScriptUrl+"ui/jquery.ui.mouse.min.js",
		dataType:"script",
		async:false,
		type:"get"		
	});
	$.ajax({
		url:javaScriptUrl+"ui/jquery.ui.button.min.js",
		dataType:"script",
		async:false,
		type:"get"		
	});
	$.ajax({
		url:javaScriptUrl+"ui/jquery.ui.draggable.min.js",
		dataType:"script",
		async:false,
		type:"get"		
	});
	$.ajax({
		url:javaScriptUrl+"ui/jquery.ui.position.min.js",
		dataType:"script",
		async:false,
		type:"get"		
	});
	$.ajax({
		url:javaScriptUrl+"ui/jquery.ui.resizable.min.js",
		dataType:"script",
		async:false,
		type:"get"		
	});
	$.ajax({
		url:javaScriptUrl+"ui/jquery.ui.dialog.min.js",
		dataType:"script",
		async:false,
		type:"get"		
	});
}

function ShowConfirm() {
    bootbox.confirm("Are you sure?", function (result) {
        Example.show("Confirm result: " + result);
    });
}

function ShowConfirmation(msg,okFunction, cancelFunction){	
	if(dialogObject==null){
		loadDialogScript();
		dialogObject=cDialog.dialog({
			title:"Confirmation",
			modal:true,
			width:'500px',
			autoOpen:false,
			resizable:true,
			position:'center',
			buttons:{
				"  OK  ":function(){
					$(this).dialog('close');
					okFunction();
				},
				"  Cancel  ":function(){
					$(this).dialog('close');
					if(jQuery.isFunction(cancelFunction))
						cancelFunction();
				},
			}
		});
	}else{
		cDialog.dialog({
				title:"Confirmation",
				modal:true,
				width:'500px',
				autoOpen:false,
				resizable:true,
				position:'center',
				buttons:{	
					"  OK  ":function(){
						$(this).dialog('close');
						okFunction();
					},
					"  Cancel  ":function(){
						$(this).dialog('close');
						if(jQuery.isFunction(cancelFunction))
							cancelFunction();
					}
				}
		});
	}
	$("#loading_box",cDialog).hide();
	$("#isi_pesan",cDialog).show();
	var stringDiv = $("<div><div style='float:left;width:120px;vertical-align:middle' align='center' class='img_notification'><img src='"+imageUrl+"notification/help.png' widht='90px' height='90px'/></div>  <div id='messageContentArea'></div></div");
	$("#messageContentArea", stringDiv).append(msg);
	$("#isi_pesan",cDialog).html(stringDiv);
	cDialog.dialog('open');
}

function ShowLoading() {
    bootbox.dialog({
        message: "<div align='center'><b>Loading...</b><br><img src='../_assets/img/big-ajax-loader.gif'></div>"
    });
}

function HideLoading() {
    bootbox.hideAll();
}


//fungsi untuk menyembunyikan dialog box
function HideDialogBox(){
	if(dialogObject!=null){
		if(dialogObject.dialog( "isOpen" ))
			dialogObject.dialog("close");
	}
}

//fungsi untuk menutup facebox yang aktif pada page dokumen
function CloseFacebox(){	
	jQuery(document).trigger('close.facebox');
}

function replaceNull(aData, value) {
    for (var i = 0; i < aData.length; i++) {
        if (aData[i] == "") {
            aData[i] = value;
        }
    }
    return aData;
}

function htmlDecode(input) {
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function timeAgo(time) {

    switch (typeof time) {
        case 'number':
            break;
        case 'string':
            time = +new Date(time);
            break;
        case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
        default:
            time = +new Date();
    }
    var time_formats = [
        [60, 'seconds', 1], // 60
        [120, '1 minute ago', '1 minute from now'], // 60*2
        [3600, 'minutes', 60], // 60*60, 60
        [7200, '1 hour ago', '1 hour from now'], // 60*60*2
        [86400, 'hours', 3600], // 60*60*24, 60*60
        [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
        [604800, 'days', 86400], // 60*60*24*7, 60*60*24
        [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
        [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
        [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
        [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
        [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
        [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
        [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
        token = 'ago', list_choice = 1;

    if (seconds == 0) {
        return 'Just now'
    }
    if (seconds < 0) {
        seconds = Math.abs(seconds);
        token = 'from now';
        list_choice = 2;
    }
    var i = 0, format;
    while (format = time_formats[i++])
        if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
        }
    return time;
}

function toDate(str) {//format: 2013-01-01 00:00:00
    var thisDate = str;
    var thisDateT = thisDate.substr(0, 10) + "T" + thisDate.substr(11, 8);
    var jDate = new Date(thisDateT);
    return jDate;
}

function ucFirstAllWords(str) {
    str = str.toLowerCase();
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

function getMenu(libajax, parentid, pageid, appid, content) {
    if (parentid == 0) {
        content.loadingOverlay();
    }
    $.ajax({
        url: libajax+"framework.ajax.php?act=getMenu",
        dataType: "html",
        type: "POST",
        data: {
            parentid: parentid,
            pageid: pageid,
            appid: appid
        },
        success: function (data) {
            content.html(data);
            content.show();
            if (parentid == 0) {
                setTimeout(function () {
                    $('.target-menu.active').trigger('click');
                }, 10);

                $('.target-menu').click(function () {
                    var parentid = $(this).attr('parentid');
                    var loading = $(this).find('.target-loading');
                    var content = $(this).find('.target-content');
                    content.show();
                    if (loading.hasClass('target-loading')) {
                        loading.loadingOverlay();
                        getMenu(libajax, parentid, pageid, appid, content);
                    }
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function getNotif(libajax) {
    var oriTitle = document.title;
    var notif = $("#notification");
    var notifCount = 0;
    
    callNotif();
    setInterval(function () {
        callNotif();
    }, 90000);
    
    function callNotif(){
        notif.html('<i class="fa fa-spinner fa-pulse"></i>');
        $.ajax({
            url: libajax + "framework.ajax.php?act=getNotif",
            dataType: "html",
            type: "POST",
            success: function (data) {
                if (data == -1) {
                    location.href = 'index.php';
                } else if (data == 0) {
                    notif.html(data);
                    document.title = oriTitle;
                } else {
                    notif.html(data);
                    document.title = oriTitle + " (" + data + ")";
                }
                notifCount = data;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
    
    $(".notification-item.general").click(function () {
        var content = $(this).find('.target-content');        
        if (notifCount != 0) {
            content.html('<li class="notification-header target-loading"></li>');
        }
        var loading = $(this).find('.target-loading');
            
        if (loading.hasClass('target-loading')) {
            loading.loadingOverlay();
            $.ajax({
                url: libajax + "framework.ajax.php?act=getNotifList",
                dataType: "html",
                type: "POST",
                success: function (data) {
                    content.html(data);
                    notif.html(0);
                    notifCount = 0;
                    document.title = oriTitle;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
        
    });
}

function getMessage(libajax) {
    var message = $("#messages");
    var messageCount = 0;
    
    callMessage();
    setInterval(function () {
        callMessage();
    }, 90000);
    
    function callMessage(){
        message.html('<i class="fa fa-spinner fa-pulse"></i>');
        $.ajax({
            url: libajax + "framework.ajax.php?act=getMessage",
            dataType: "html",
            type: "POST",
            success: function (data) {
                if (data == -1) {
                    location.href = 'index.php';
                } else {
                    message.html(data);
                }
                messageCount = data;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
    
    $(".notification-item.message").click(function () {
        var content = $(this).find('.target-content');        
        if (messageCount != 0) {
            content.html('<li class="notification-header target-loading"></li>');
        }
        var loading = $(this).find('.target-loading');
            
        if (loading.hasClass('target-loading')) {
            loading.loadingOverlay();
            $.ajax({
                url: libajax + "framework.ajax.php?act=getMessageList",
                dataType: "html",
                type: "POST",
                success: function (data) {
                    content.html(data);
                    message.html(0);
                    messageCount = 0;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
        
    });
}

function getUserGroup(libajax, currentUrl) {
    $(".notification-item.usergroup").click(function () {
        var parent = $(this);
        var content = $(this).find('.target-content');
        var loading = $(this).find('.target-loading');

        if (loading.hasClass('target-loading')) {
            loading.loadingOverlay();
            $.ajax({
                url: libajax + "framework.ajax.php?act=getUserGroup",
                dataType: "html",
                type: "POST",
                success: function (data) {
                    content.html(data);

                    $(".change_usergroup").click(function () {
                        var usergroupid = $(this).attr('usergroupid');
                        content.html('<li class="notification-header target-loading"></li>');
                        var loading = parent.find('.target-loading');

                        loading.loadingOverlay();
                        $.ajax({
                            url: libajax + "framework.ajax.php?act=switchUserGroup",
                            dataType: "html",
                            type: "POST",
                            data: {
                                id: usergroupid
                            },
                            success: function (data) {
                                if (data === 'success') {
                                    location.href = currentUrl;
                                } else {
                                    alert('Switch UserGroup Failed');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(errorThrown);
                            }
                        });

                        return false;
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    });
}

$(function(){
	//penanganan event pada aksi pengubahan bahasa yang aktif
	$(".switch_language").click(function(){
		$(".switchLanguageSpan").html("Loading...");
		$.ajax({
			type:"post",
			dataType:"html",
			url:libraryAjaxUrl+"ajax.util.php?act=switch_language",
			data:{ "id" : $(this).attr('rel') },
			success:function(data){
				self.location.href = currentUrl;
			},
			failed:function(){	
				$(".switchLanguageSpan").html("Failed.");
			}
		});			
	});
});


//});