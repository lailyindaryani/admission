<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    private $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('participantmodel');
		$this->load->model('loginmodel');
		date_default_timezone_set('Asia/Jakarta');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->load->view('landing_page');
    }

    public function forgotPassword()
    {
        $this->load->view('forgot_password', $this->data);
    }

    public function academic()
    {
        $this->data['type'] = 'ACADEMIC';
        $this->load->view('login', $this->data);
    }

    public function nonAcademic()
    {
        $this->data['type'] = 'NON-ACADEMIC';
        $this->load->view('login', $this->data);
    }

    public function validateLogin(){
       // print_r($this->input->post());
	   $userid=$this->uri->segment(3);
        if($this->input->post('login')){
			$username=$this->input->post('username');
			$password=$this->input->post('password');
            $dataLogin=$this->loginmodel->Login($username,$password);
			$aktif=$this->loginmodel->CekAktivasiUser($username);
			if(empty($dataLogin) && empty($aktif)){
				$this->session->set_flashdata('msg_error', 'SORRY, YOUR USERNAME OR EMAIL DOES NOT EXIST. PLEASE REGISTER FIRST');
			}else{
				if(sizeof($dataLogin)>0 && $aktif[0]['ACTIVESTATUS']=='Y'){//benar		
                $this->session->set_userdata('login', TRUE);
                $this->session->set_userdata('users', $dataLogin);
					if($dataLogin[0]['USERGROUPID']==ADMINUSERGROUPID){
						redirect(base_url().'admin/dashboardEnrollment/');
					}else  if($dataLogin[0]['USERGROUPID']==PARTICIPANTUSERGROUPID){
						redirect(base_url().'participant/index/');
					}else  if($dataLogin[0]['USERGROUPID']==STAFFUSERGROUPID){
						redirect(base_url().'staff/dashboardEnrollment/');
					} else  if($dataLogin[0]['USERGROUPID']==STRUCTURALUSERGROUPID){
					}
				}else if($username==$aktif[0]['EMAIL'] && md5($password)==$aktif[0]['PASSWORD'] && $aktif[0]['ACTIVESTATUS']=='N'){//username ada, username & 		password benar, belum aktivasi
					$this->session->set_flashdata('msg_warning', "SORRY, YOU DO NOT ACCESS THIS ACCOUNT. PLEASE CHECK EMAIL AND ACTIVATE THE ACCOUNT");
				}else if($username==$aktif[0]['EMAIL'] && md5($password)!=$aktif[0]['PASSWORD']){//password salah
					$this->session->set_flashdata('msg_error', 'SORRY, YOUR PASSWORD IS WRONG');
				}else if($username!=$aktif[0]['EMAIL'] && md5($password)!=$aktif[0]['PASSWORD']) {
					$this->session->set_flashdata('msg_error', 'USERNAME AND PASSWORD DO NOT MATCH');
				}
			}
			if($this->input->post('type')=='ACADEMIC'){
				redirect(base_url().'login/academic/');
			}else{
				redirect(base_url().'login/nonAcademic/');
			}
        }else if($userid!=''){
			
			$dataLoginAs=$this->loginmodel->LoginasStaff($userid);
			/*$users=$this->session->userdata('users');
			$data = array(
				'USERID' =>$dataLoginAs[0]['USERID'],
				'EMAIL'=>$dataLoginAs[0]['EMAIL'],
				'PASSWORD'=>$dataLoginAs[0]['PASSWORD'],
				'FULLNAME'=>$dataLoginAs[0]['FULLNAME'],
				'ACTIVESTATUS' => $dataLoginAs[0]['ACTIVESTATUS'],
				'CONFIRMATIONKEY' => $dataLoginAs[0]['CONFIRMATIONKEY'],
				'INPUTDATE' => $dataLoginAs[0]['INPUTDATE'],
				'UPDATEDATE' => $dataLoginAs[0]['UPDATEDATE'],
				'USERGROUPID' => $dataLoginAs[0]['USERGROUPID'],
				'ISSUBMITQUESTIONAIRE' => $dataLoginAs[0]['ISSUBMITQUESTIONAIRE'],
				'CURRENTSTEP' => $dataLoginAs[0]['CURRENTSTEP']
			);
			//unset($_SESSION['users'][0]['USERID']);
			$this->session->set_userdata('users',$data['USERID']);
			unset($_SESSION['users'][0]['EMAIL']);
			$this->session->set_userdata('users',$data['EMAIL']);
			unset($_SESSION['users'][0]['PASSWORD']);
			$this->session->set_userdata('users',$data['PASSWORD']);
			unset($_SESSION['users'][0]['FULLNAME']);
				$this->session->set_userdata('users',$data['FULLNAME']);
			unset($_SESSION['users'][0]['ACTIVESTATUS']);
				$this->session->set_userdata('users',$data['ACTIVESTATUS']);
			unset($_SESSION['users'][0]['CONFIRMATIONKEY']);
				$this->session->set_userdata('users',$data['CONFIRMATIONKEY']);
			unset($_SESSION['users'][0]['INPUTDATE']);
				$this->session->set_userdata('users',$data['INPUTDATE']);
			unset($_SESSION['users'][0]['UPDATEDATE']);
				$this->session->set_userdata('users',$data['UPDATEDATE']);
			unset($_SESSION['users'][0]['ISSUBMITQUESTIONAIRE']);
				$this->session->set_userdata('users',$data['ISSUBMITQUESTIONAIRE']);
			unset($_SESSION['users'][0]['CURRENTSTEP']);
				$this->session->set_userdata('users',$data['CURRENTSTEP']);
			//echo "<pre>"; print_r($users);
			echo "<pre>"; print_r($this->session->userdata('users'));die;*/
			// session_destroy();
			//$this->session->set_userdata('login', TRUE);
				$users3=$this->session->userdata('users');
				//	echo "<pre>"; print_r($users3);
            $this->session->set_userdata('users', $dataLoginAs);
			$users=$this->session->userdata('users');
			//echo "<pre>"; print_r($users);die;
			redirect(base_url().'participant/index/');
		}
    }

    public function activation()
    {
        $this->load->model('loginmodel');
        $this->load->model('usermodel');
        $key = $this->input->get('key');
        $id = $this->input->get('id');
        
        if(empty($key) || empty($id)) redirect(base_url());

        //$confirmUser   = $this->loginmodel->Activation($id, $key);
        $data = $this->usermodel->GetUserByConfirmationKey($id, $key);
        if(!empty($data)){
            $response = $this->usermodel->update($id, array('ACTIVESTATUS' => 'Y'));
            if($response){
                //$data = $this->usermodel->GetUserByConfirmationKey($id, $key);
                $this->session->set_userdata('login', TRUE);
                $this->session->set_userdata('users', $data);
                $this->session->set_flashdata('msg_success', 'ACTIVATION SUCCESS');
                //redirect(base_url().'participant/selectProgram/');
				redirect(base_url().'participant/index/');
            }
        }

        redirect(base_url());
    }

    function tes()
    {
        $email = $this->input->get('email');
        $response = $this->sendEmail($email, 'tes', 'tes', 'tes', null);
        echo json_encode($response);
    }

    public function httpPostBasicAuth($url, $fields, $username, $password) {
        //url-ify the data for the POST
        $fields_string  = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        $fields_string = rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password); //BASIC AUTH
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string); //HTTP POST
        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $result;
    }

    public function sendEmail($sendTo, $subject, $textBody, $name, $arrAttachment)
    {
        $url = API_URL.'/email/send';
            $fields = array(
                'to' => urlencode($sendTo),
                'subject' => urlencode($subject),
                'content' => urlencode($textBody),
                'toname' => urlencode($name),
                'attacharr' => urlencode($arrAttachment)
            );
            //for basic auth
            $result = $this->httpPostBasicAuth($url, $fields, "ioffice", "Cent4ur");
    }

    public function resetPassword(){
        $email  = trim($this->input->post('email'));
        $this->db->select('*');
        $data   = $this->db->get_where('users', array('EMAIL' => $email))->row_array();

        if($data!=null){
            $subject    = "Password reset on Telkom University International Admission";
            $textBody   = "
            <p style='padding-left: 450px;'><img src='https://upload.wikimedia.org/wikipedia/id/1/1c/Logo_Telkom_University.png' width='129px' height='43px' align='right'/></p><br>
                <p style='text-align: justify;'>You're receiving this email because you requested a password reset for your user account at Telkom University International Admission.<br />Please go to the following page and choose a new password:&nbsp;</p>
                <a href='".base_url()."account/resetPassword?email=".urlencode($email)."&key=".urlencode($data['CONFIRMATIONKEY'])."&id=".urlencode($data['USERID'])."' target='blank'>
                ".base_url()."account/resetPassword?email=".urlencode($email)."&key=".urlencode($data['CONFIRMATIONKEY'])."&id=".urlencode($data['USERID'])."
                </a>
                ";
            $name   = $data['FULLNAME'];
            $this->sendEmail($email,$subject,$textBody,$name,'');
			$this->session->set_flashdata('msg_success', 'SEND EMAIL SUCCESSFULL, PLEASE CHECK YOUR INBOX or SPAM in EMAIL REGISTERED FOR RESET PASSWORD');
            redirect(base_url()."login");
        }else{ echo "error";}
    }

    public function logout(){
        session_destroy();
        redirect(base_url());
    }
}