<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
    private $data;

    public function __construct(){
        parent::__construct();
        $this->isLogin();
		date_default_timezone_set('Asia/Jakarta');
    }

    public function getDegreeByProgramDegree()
    {
        $this->load->model('degreemodel');
        $degreeId = $this->input->post('degreeId');

        $degree = $this->degreemodel->GetDecreeById($degreeId);
        echo json_encode($degree);
    }

    public function getProgramDegree()
    {
        $result = array();
        $this->load->model('programdegreemodel');
        $this->load->model('degreemodel');
        $enrollmentId = $this->input->post('enrollmentid');

        $programdegree = $this->programdegreemodel->GetProgramDegreeByEnrollmentId($enrollmentId);
		
        if(!empty($programdegree)){
            $programdegree['SHORTCOURSE'] = $programdegree['PROGRAMID'] == 4 ? true : false;
        }
        echo json_encode($programdegree);
    }

    public function getStudyProgramLanguage()
    {
        $result = array();
        $this->load->model('languagedeliverymappingmodel');
        $this->load->model('studyprogramlanguagemodel');
        $programdegreeId = $this->input->post('programdegreeid');
        $languagedeliveryid = $this->input->post('languagedeliveryid');
        $languagedeliverymapping = $this->languagedeliverymappingmodel->GetLanguageDeliveryMappingById($languagedeliveryid, $programdegreeId);
		
        if(!empty($languagedeliverymapping))
        {
            $result = $this->studyprogramlanguagemodel->GetProgramDegreeByIdLanguage($languagedeliverymapping['LANGUAGEDELIVERYMAPPINGID']);
        }
    
        echo json_encode($result);
    }

    public function getStudyProgram()
    {
        $result = array();
        $this->load->model('studyprogrammodel');
		$this->load->model('coursemodel');
        $programdegreeId = $this->input->post('programdegreeid');
        $languagedeliveryid = $this->input->post('languagedeliveryid');
		$type = $this->input->post('type');
        if(!empty($programdegreeId) && !empty($languagedeliveryid) && $type=='ACADEMIC'){
			$result = $this->studyprogrammodel->GetStudy($languagedeliveryid, $programdegreeId);
		}elseif(!empty($programdegreeId) && !empty($languagedeliveryid) && $type=='NON_ACADEMIC'){
			$result = $this->coursemodel->getCourse($languagedeliveryid, $programdegreeId);
		}
        echo json_encode($result);
    }
	

	 public function getListRequirementSisa()
    {
       $this->load->model("requirementsmodel");
        $programdegreeid = $this->input->post('programdegreeId');

        $requirements = $this->requirementsmodel->getSisaData($programdegreeid);
        echo json_encode($requirements);
    }
    

    public function getListProgramEnrollment()
    {
		//echo "<pre>"; print_r($_POST);die;
        $this->load->model('enrollmentmodel');
		$this->load->model('participantmodel');
        $programId = $this->input->post('programId');
		$participantid = $this->input->post('participantid');
		$participant = $this->participantmodel->participantData($participantid);
		//echo "<pre>"; print_r($participant);die;
		if($participant['ACCEPTANCESTATUS']!=null){
			//$this->data['listEnrollment'] = $this->enrollmentmodel->getActiveEnrollment();
			 $programenrollment = $this->enrollmentmodel->getEnrollmentByProgramIdA($programId);
		}else{
			 $programenrollment = $this->enrollmentmodel->getEnrollmentByProgramIdB($programId);
		}
		//echo "<pre>"; print_r($participant);
		//echo "<pre>"; print_r($programenrollment);die;
        echo json_encode($programenrollment);
    }

    public function getListProgramDegree()
    {
    	$this->load->model('programdegreemodel');
    	$programId = $this->input->post('programId');

    	$programdegree = $this->programdegreemodel->GetProgramDegreeById($programId);
    	echo json_encode($programdegree);
    }

    public function isLogin(){
        $users=$this->session->userdata('users');
        $users=$users[0];
        if($this->session->userdata('login')){

        }else{
        	echo 'you cannot access this page';
            //redirect(base_url()."login");
        }
    }
	public function getParticipantCountry(){
        $acceptanceStatus = $this->input->post('acceptanceStatus');
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		//echo "<pre>"; print_r($period);die;
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " E.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " E.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('countrymodel');
		$data_country = $this->countrymodel->getCountryByStatus($acceptanceStatus,$academicyear,$periods);
		
		//echo "<pre>"; print_r($data_country);die;
        echo json_encode($data_country);
	}
	
	public function getParticipantStudyProgramByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		//echo "<pre>"; print_r($period);die;
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " E.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " E.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('studyprogrammodel');
		$data_studyprogram = $this->studyprogrammodel->getStudyProgram($academicyear, $periods);
		//echo "<pre>"; print_r($data_country);die;
        echo json_encode($data_studyprogram);
	}
	
	public function getParticipantProgramByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		//echo "<pre>"; print_r($period);die;
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " E.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " E.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		//echo "<pre>"; print_r($academicyear);die;
		$this->load->model('enrollmentmodel');
		$data_enrollment = $this->enrollmentmodel->getEnrollment($academicyear,$periods);
        echo json_encode($data_enrollment);
	}
	
	function getNumberParticipantsByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		//echo "<pre>"; print_r($period);die;
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " e.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " e.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		
		//echo "<pre>"; print($periods);die;
		$this->load->model('participantmodel');
		$number_participants = $this->participantmodel->getNumberParticipants('',$academicyear, $periods);
		//echo "<pre>"; print_r($number_participants);die;
		//$number_participants = $this->participantmodel->getNumberParticipants('',$academicyear);
		echo json_encode($number_participants[0]['JUMLAH']);
	}

    function getNumberAcountsByAcademicYear(){
        $academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " e.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " e.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
        $this->load->model('participantmodel');
        $number_participants = $this->participantmodel->getNumberParticipants('acount',$academicyear, $periods);
		//$number_participants = $this->participantmodel->getNumberParticipants('acount',$academicyear);
        echo json_encode($number_participants[0]['JUMLAH']);
    }
	function getNumberScholarshipsByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " e.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " e.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('participantmodel');
		$number_scholarship = $this->participantmodel->getNumberParticipants('scholarship',$academicyear, $periods);
		echo json_encode($number_scholarship[0]['JUMLAH']);
	}
	function getNumberNotScholarshipByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " e.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " e.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('participantmodel');
		$number_not_scholarship = $this->participantmodel->getNumberParticipants('notscholarship',$academicyear, $periods);
		echo json_encode($number_not_scholarship[0]['JUMLAH']);
	}
	function getStepByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " e.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " e.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$no = $this->input->post('no');
		$this->load->model('participantmodel');
        if($no!=7){
		    $step = $this->participantmodel->getStepParticipants($no,$academicyear,$periods);
        }else{
            $step = $this->participantmodel->getStepSeven($no,$academicyear,$periods);
        }
		//echo "<pre>"; print_r($step);//die;
		echo json_encode($step[0]['JUMLAH']);
	}
	function getCountryByAcademicYear(){
		 $acceptanceStatus = $this->input->post('acceptanceStatus');
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " E.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " E.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('countrymodel');
		$number_country = $this->countrymodel->getNumberCountry($academicyear,$acceptanceStatus,$periods);
		echo json_encode($number_country[0]['JUMLAH']);
	}
	function getProdByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period');
		$this->load->model('participantmodel');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " e.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " e.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$number_students = $this->participantmodel->getNumberParticipants('students',$academicyear,$periods);
		echo json_encode($number_students[0]['JUMLAH']);
	}
	function getInterviewByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$status = $this->input->post('status');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " E.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " E.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('participantmodel');
		$number_interview = $this->participantmodel->getInterview2($academicyear,$status, $periods);
		echo json_encode($number_interview);
	}
	
	function getStatusByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$status = $this->input->post('status');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " E.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " E.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('participantmodel');
		$number_status = $this->participantmodel->getParticipantStatus($status,$academicyear,$periods);
		echo json_encode($number_status[0]['JUMLAH']);
	}
	
	function getConfirmByAcademicYear(){
		$academicyear = $this->input->post('academicyear');
		$status = $this->input->post('status');
		$period = $this->input->post('period');
		$periods ='';
		if(($period!='' || $period!=NULL) && $period!='UNDEFINED YEAR'){
			for($x=0;$x<count($period);$x++){
			if(($x+1)==count($period)){
				$periods .= " E.PERIOD = '".$period[$x]."'";
			}else{
				$periods .= " E.PERIOD = '".$period[$x]."' OR";
			}
		}
		}else if($period=='UNDEFINED YEAR'){
			$periods = 'UNDEFINED YEAR';
		}
		$this->load->model('participantmodel');
		$number_confirm =$this->participantmodel->getConfirmDate($status,$academicyear,$periods);
		echo json_encode($number_confirm[0]['JUMLAH']);
	}
	
	function setDefaultFilter(){
		$this->load->model('enrollmentmodel');
		$academicyear = $this->input->post('academicyear');
		$period = $this->input->post('period'); 
		$response=0;
		$response2=0;
		if($academicyear!=''){
			$getIdFilter= $this->enrollmentmodel->getFilter($academicyear);
			if(count($getIdFilter)==1){
				$datas = array(
					'STATUS' => ''
				);
				$update = $this->enrollmentmodel->updateFilterStatus('Y', 'ACADEMICYEAR', $datas);
				//echo "<pre>"; print($hapus);die;
				//if($update==1){
					$data = array(
						'STATUS' => 'Y'
					);
					$response = $this->enrollmentmodel->updateFilter($getIdFilter[0]['IDDEFAULTFILTER'], $data);
				//}
			}
		}
		//echo "<pre>"; print_r($period);
		//echo "<pre>"; print(count($period));die;
		if($period!='' || $period!=NULL){
			$datas = array(
				'STATUS' => ''
			);
			$update = $this->enrollmentmodel->updateFilterStatus('Y', 'PERIOD', $datas);
			for($x=0;$x<count($period);$x++){
				$getIdFilter= $this->enrollmentmodel->getFilter($period[$x]);
				//echo "<pre>"; print(count($getIdFilter));//die;
				if(count($getIdFilter)==1){					
					//if($update==1){
						$data = array(
							'STATUS' => 'Y'
						);
						$response2 = $this->enrollmentmodel->updateFilter($getIdFilter[0]['IDDEFAULTFILTER'], $data);
					//}
				}
			}
		}
		if($response==1 || $response2==1){
			echo json_encode('SUCCESS');
		}else{
			echo json_encode('FAILED');
		}
	}
	
	function getFilter(){
		$status = $this->input->post('status');
		$this->load->model('enrollmentmodel');
		$data = $this->enrollmentmodel->getFilterStatus('Y',$status);
		//$period = $this->enrollmentmodel->getPeriod();
		//for($x=0;$x<count($period);$x++){
			
		//}
		echo json_encode($data);
	}

	function getPopulateSubject(){
		$this->load->model('exchangemodel');
        $studyprogramid = $_POST['filter1'];
        $enrollmentid = $_POST['filter2'];
        $degree = $_POST['filter3'];
        $curriculum = $_POST['filter4'];
        $semester = $_POST['filter5'];
        $data = $this->exchangemodel->getSubjectMapping($enrollmentid, $studyprogramid, $degree, $curriculum, $semester);
		echo json_encode($data);
	}

	function getStudyExchange(){
		$this->load->model('exchangemodel');
        $data = $this->exchangemodel->getStudyProgram($_POST['filter']);
        echo json_encode($data);
	}

	function getCountSubMapping(){
		$this->load->model('exchangemodel');
		$data = $this->exchangemodel->getCountSubMapping($_POST['filter']);
        // $json_data = array(
        //     'data' => $data
        // );
        echo json_encode($data[0]['COUNTCREDIT']);
	}

	function getCountSubMapping2(){
		$this->load->model('exchangemodel');
		$data = $this->exchangemodel->getCountSubMapping2($_POST['filter1']);
        echo json_encode($data[0]['COUNTCREDIT']);
	}

	function getSubjectMapping(){
		$this->load->model('participantmodel');
		$data = $this->participantmodel->getSubjectMapping($_POST['filter']);
		echo json_encode($data);
	}

	function getStudySubject(){
		$this->load->model('participantmodel');
		$data = $this->participantmodel->getStudySubject($_POST['filter']);
		echo json_encode($data);
	}

	// public function dataTableSubjectMappingExchange(){
	// 	$this->load->model('participantmodel');
 //        $data = $this->participantmodel->getSubjectMapping($_POST['filter']);
 //        $json_data = array(
 //            'data' => $data
 //        );
 //        echo json_encode($json_data);
 //    }

 //     public function dataTableSubjectExchange(){
 //     	$this->load->model('participantmodel');
 //        $data = $this->participantmodel->getSubjectExchange($_POST['filter2']);
 //        $datax = $this->participantmodel->getSubjectMapping($_POST['filter1']);
 //        for($i=0; $i<count($data); $i++){
 //           // $data[$i]['NO'] = $i+1;
 //            $data[$i]['ACTION'] = '<input type="checkbox" class="right" name="" language="'.$data[$i]['LANGUAGE'].'" credit="'.$data[$i]['CREDIT'].'" subjectname="'.$data[$i]['SUBJECTNAME'].'" studyprogramname="'.$data[$i]['STUDYPROGRAMNAME'].'" enrollmentid="'.$data[$i]['ENROLLMENTID'].'" studyprogramid="'.$data[$i]['STUDYPROGRAMID'].'" curiculum="'.$data[$i]['CURICULUMYEAR'].'" id="'.$data[$i]['SUBJECTCODE'].'">';
 //            for($j=0; $j<count($datax); $j++){
 //                if($datax[$j]['SUBJECTCODE']==$data[$i]['SUBJECTCODE']){
 //                    $data[$i]['ACTION'] = '<input type="checkbox" class="right" name="" language="'.$data[$i]['LANGUAGE'].'" credit="'.$data[$i]['CREDIT'].'" subjectname="'.$data[$i]['SUBJECTNAME'].'" studyprogramname="'.$data[$i]['STUDYPROGRAMNAME'].'" enrollmentid="'.$data[$i]['ENROLLMENTID'].'" studyprogramid="'.$data[$i]['STUDYPROGRAMID'].'" curiculum="'.$data[$i]['CURICULUMYEAR'].'" id="'.$data[$i]['SUBJECTCODE'].'" checked>';
 //                    break;
 //                }
 //            }
 //        }
 //        $json_data = array(
 //            'data' => $data
 //        );
 //        echo json_encode($json_data);
 //    }

}
