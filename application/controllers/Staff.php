<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
    private $data;
    private $datatableConfig;

    public function __construct(){
        parent::__construct();

        $this->isLoginStaff();

        date_default_timezone_set('Asia/Jakarta');

        $this->datatableConfig = array(
            "DB_DISPLAY_ERROR"=>false,
            "DB_BREAK_PROCESS"=>false,
            "DB_TYPE"=>"mysql",
            "DB_USERNAME"=>$this->db->username,
            "DB_PASSWORD"=>$this->db->password,
            "DB_HOST"=>$this->db->hostname,
            "DB_NAME_MYSQL"=>$this->db->database,
        );

        $data=array(
            "pagetitle"=>"",
            "msg"=>"",
            "menu"=>"staff/_menu.php",

        );
        $this->data=$data;
		ini_set( 'memory_limit', '200M' );
		ini_set('upload_max_filesize', '200M');  
		ini_set('post_max_size', '200M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);
		
		$this->load->model('enrollmentmodel');
        $this->load->model('participantmodel');
    }
    public function index()
    {
        //$this->load->view('login');
    }
	
	

    public function httpPostBasicAuth($url, $fields, $username, $password) {	
		 if (is_array($fields)) {
            $postvars = http_build_query($fields, NULL, '&');
        }
        //open connection
        $ch = curl_init();	
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password); //BASIC AUTH
		//curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-Type: text/plain charset=\"iso-8859-1\""));
		//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
		//curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars); //HTTP POST
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //execute post
        $result = curl_exec($ch);
		
		if (curl_errno($ch)) {
			$result = curl_error($ch);
		}
		
        //close connection
        curl_close($ch);

        return $result;
    }
    public function sendEmail($sendTo, $subject, $textBody, $name)
    {
		//10.252.252.43 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $url ='https://api.telkomuniversity.ac.id/email/send';//'10.252.252.43/email/send';
		$type = 'gmail.com';
		if(strtolower(substr($sendTo, strlen($sendTo) - strlen($type), strlen($type))) == strtolower($type)){
            $fields = array(
                'to' => $sendTo,
                'subject' => $subject,
                'content' => nl2br($textBody),
				'attach' => $attach
            );
		}else{
			$url ='https://api.telkomuniversity.ac.id/email/send';//'10.252.252.43/email/send';
            $fields = array(
                'to' => $sendTo,
                'subject' => $subject,
                'content' => nl2br($textBody),
				'operatorid' => 4
            );
		}
		//for basic auth
		$result = $this->httpPostBasicAuth($url, $fields, "ioffice", "Cent4ur");
    }
    public function processReminder()
    {            
        if($this->input->post('kirim')){				
			$participantid= $this->input->post('participantid');
			$this->load->model('participantmodel'); 
			$post_data = $this->input->post();
			if(!empty($_FILES['attachment']['name'])){
				//upload file attachment
				$attachment_name_first=str_replace('.','_',$_FILES['attachment']['name']);
				$attachment_name=str_replace(' ','_',$attachment_name_first);
				$path = $_FILES['attachment']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$config_attachment = array(
					'upload_path' => realpath(FCPATH . 'uploads/attachment_email'),
					'overwrite' => TRUE,
					'file_name' => $attachment_name.".".$ext,
					'allowed_types' => '*'
				);
				$ganti_foto=str_replace(' ','_',$_FILES['attachment']['name']);
				$this->upload->initialize($config_attachment);	
			
				if(!$this->upload->do_upload('attachment')){
					$data = array('error' => $this->upload->display_errors());
				}else{
					$data = array('upload_data' => $this->upload->data());
				}
				$attachment_name2=$attachment_name.".".$ext;
				$textBody   = "<p style='text-align: justify;'>".$post_data['pesan']."</p><br><p style='text-align: justify;'>This is the attachment file: </p><br><a href='".base_url().'uploads/attachment_email/'.urlencode($attachment_name2)."' target='blank'>".urlencode($attachment_name2)."</a>";
			}else{
				$textBody = $post_data['pesan'];
			} 
			if(!$this->sendEmail($post_data['email'], 'Step Reminder', $textBody, $post_data['name'], $attach)){
				$this->session->set_flashdata('msg_success', 'SEND EMAIL SUCCESSFULL');
			}else{
				$this->session->set_flashdata('msg_failed', 'FAILED SEND EMAIL');
			}			
			redirect('/staff/participantDetail/'.$participantid);
		}else {
            echo "Email gagal dikirim";
        }        
    }

    function ajaxGetProgram(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('PROGRAMID');
        $this->datatables->SetColumns(array('PROGRAMNAME','INPUTDATE','PROGRAMID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from program ");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }

    function dataTablePopulateEnrollment(){
        $condition = array(
            'ACADEMICYEAR'  => $_GET['academicyear']!=''?$_GET['academicyear']:'null',
            'PROGRAMID'     => $_GET['programid']!=''?$_GET['programid']:'null',
			 'ENROLLMENTSTATUS'     => $_GET['status']!=''?$_GET['status']:'null',
        );

        $query = "SELECT * from enrollment
                  LEFT JOIN (SELECT PROGRAMDEGREEID,PROGRAMID FROM programdegree)AS programdegree using(PROGRAMDEGREEID)
                  LEFT JOIN (SELECT PROGRAMNAME,PROGRAMID FROM program)AS program using (PROGRAMID)
                  LEFT JOIN (SELECT FULLNAME,USERID FROM users)AS users on (enrollment.INPUTBY=users.USERID)
                  ";

        $i = 0;
        foreach ($condition as $key=>$value){
          if($value=='null')
            continue;
          else
            if($i==0)
              $query .= " WHERE $key = '$value'";
            else
              $query .= " AND $key = '$value'";
            $i++;
        }
		
		 $query .= "
				  ORDER BY UPDATEDATE DESC
                  ";
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('ENROLLMENTID');
        $this->datatables->SetColumns(array('DESCRIPTION','PROGRAMNAME','ACADEMICYEAR','PERIOD','STARTDATE','ENDDATE','FULLNAME','ENROLLMENTID'));
        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }

    function dataTablePopulateParticipants(){
        $condition = array(
            'ENROLLMENTID'      => $_GET['enrollmentid']!=''?$_GET['enrollmentid']:'null',
            'ACCEPTANCESTATUS'  => $_GET['acceptance']!=''?$_GET['acceptance']:'null',
            'REQUIREMENTID'     => $_GET['documentid']!=''?$_GET['documentid']:'null',
            'participants.STUDYPROGRAMID'    => $_GET['studyprogramid']!=''?$_GET['studyprogramid']:'null',
            'FACULTYID'         => $_GET['faculty']!=''?$_GET['faculty']:'null',
            'PROGRAMTYPE'       => $_GET['programtype']!=''?$_GET['programtype']:'null',
            'COUNTRYID'         => $_GET['country']!=''?$_GET['country']:'null',
            'PERIOD'            => $_GET['period']!=''?$_GET['period']:'null',
			'SCHOOLYEAR'            => $_GET['schoolyear']!=''?$_GET['schoolyear']:'null',
        );

        $query =    "SELECT DISTINCT PARTICIPANTID,participants.FULLNAME,GENDER,PASSPORTNO,
CASE 
WHEN NATIONALITY IS NULL OR NATIONALITY=COUNTRYID THEN COUNTRYNAMEENG
ELSE NATIONALITY
END AS NATIONALITY,PERIOD,ACCEPTANCESTATUS,ENROLLMENTID,USERID,COUNTRYNAMEENG from participants
                        LEFT JOIN country USING(COUNTRYID)
                          LEFT JOIN users USING(USERID)
                        LEFT JOIN (enrollment
                          LEFT JOIN (SELECT PROGRAMDEGREEID,PROGRAMID,DEGREEID FROM programdegree) AS programdegree
                          USING (PROGRAMDEGREEID)
                        )
                        USING (ENROLLMENTID)
                        LEFT JOIN (languagedeliverymapping
                          LEFT JOIN (studyprogramlanguage) USING (LANGUAGEDELIVERYMAPPINGID)
                          JOIN (SELECT STUDYPROGRAMID,FACULTYID FROM studyprogram) AS STUDYPROGRAM USING (STUDYPROGRAMID)
                        ) USING (LANGUAGEDELIVERYMAPPINGID)
                        LEFT JOIN (requirementvalue)
                          USING (PARTICIPANTID)
						  
                      ";
        $i = 0;
        foreach ($condition as $key=>$value){
            if($value=='null')
                continue;
            else
                if($i==0)
                    $query .= " WHERE $key = '$value'";
                else
                    $query .= " AND $key = '$value'";
                $i++;
        }

//        echo $query;
        $query .= "  ORDER BY participants.UPDATEDATE DESC";
       // echo "<pre>"; print_r($query);die;
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('PARTICIPANTID');
        $this->datatables->SetColumns(array('FULLNAME','PASSPORTNO','GENDER','NATIONALITY','PERIOD','ACCEPTANCESTATUS','PARTICIPANTID','USERID'));
        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }

    public function enrollment(){

        $this->data['pagetitle']="IO Tel-U | Enrollment";

        $this->load->model("programmodel");
        $programs=$this->programmodel->getAllData();

        $qAcademicYear = "SELECT DISTINCT ACADEMICYEAR FROM enrollment ORDER BY UPDATEDATE DESC";
        $academicYear = $this->db->query($qAcademicYear);

        $this->data['programs']=$programs;
        $this->data['academicyears']=$academicYear->result_array();


        $this->load->view('_header',$this->data);
        $this->load->view('staff/enrollment',$this->data);
        $this->load->view('_footer');

    }

    public function participants(){

        $this->data['pagetitle']="IO Tel-U | Participants";

        $enrollmentId=@$_GET['enrollmentId'];

        $this->load->model("requirementsmodel");
        $this->load->model("studyprogrammodel");
        $this->load->model("enrollmentmodel");
        $this->load->model('facultymodel');
        $this->load->model('countrymodel');
		$this->load->model('participantmodel');

        $requirements   = $this->requirementsmodel->getAllData();
        $studyprograms  = $this->studyprogrammodel->getAllData();
        $enrollments    = $this->enrollmentmodel->getAllData();
        $periods        = $this->enrollmentmodel->fetchAvailableAcademicYear();
        $countries      = $this->countrymodel->GetAllCountry();

        $this->data['faculties'] = $this->facultymodel->GetAllData();
        $this->data['enrollmentId']     = $enrollmentId;
        $this->data['requirements']     = $requirements;
        $this->data['studyprograms']    = $studyprograms;
        $this->data['enrollments']      = $enrollments;
        $this->data['periods']          = $periods;
        $this->data['countries']        = $countries;
		$this->data['schoolyears']      = $this->participantmodel->fetchAvailableSchoolYear();

        $this->load->view('_header',$this->data);
        $this->load->view('staff/participants',$this->data);
        $this->load->view('_footer');
    }

    public function ajaxFetchParticipantDetail(){
      $this->load->model('participantmodel');
      $participantId    = $this->uri->segment(3);
      $participant      = $this->participantmodel->GetParticipantDetail($participantId);
      echo json_encode($participant);
    }

    public function participantDetail(){
        $this->data['pagetitle']="IO Tel-U | Participant Detail";

        $participantId  = $this->uri->segment(3);
		
        if(!is_numeric($participantId) || $participantId=="")
            die('undefined participant id');

        $this->load->model('participantmodel');
        $this->load->model('requirementsmodel');
        $this->load->model('familymodel');
        $this->load->model('educationmodel');
		$this->load->model('enrollmentmodel');
		$this->load->model('programmodel');
		$this->load->model('degreemodel');
		$this->load->model('languagedeliverymodel');
		$this->load->model('studyprogrammodel');
		$this->load->model('languagedeliverymappingmodel');
		$this->load->model('coursemodel');
		
		$participant    = $this->participantmodel->GetParticipantDetail($participantId);
		if($participant['PROGRAMTYPE']=='ACADEMIC'){
			$participantDetail = $participant;
		}else{
			$participantDetail = $this->participantmodel->GetParticipantDetailNonAcademic($participantId);
		}
		//echo "<pre>"; print_r($participant);die;
        if($participant==null)
            die('undefined participant id');

        $family         = $this->familymodel->GetFamilyOfUser($participantId);
        $father         = $this->familymodel->fatherData($participantId);
        $mother         = $this->familymodel->motherData($participantId);
        $education      = $this->educationmodel->GetEducationOfUser($participantId);
        $highschool     = $this->educationmodel->highschoolData($participantId);
        $university     = $this->educationmodel->universityData($participantId);

        $qrequirements = "SELECT * FROM requirements
                            LEFT JOIN (SELECT * FROM requirementvalue 
                              WHERE PARTICIPANTID=$participantId) AS requirementvalue
                              USING (REQUIREMENTID)
                            WHERE PRIVILEGE IN (1,3) AND REQUIREMENTID IN (
                                SELECT REQUIREMENTID
                                FROM requirementmapping
                                JOIN programdegree USING (PROGRAMDEGREEID)
                                JOIN enrollment USING (PROGRAMDEGREEID)
                                JOIN participants USING (ENROLLMENTID)
                                WHERE PARTICIPANTID=$participantId
                            )";
//        die(json_encode($participant));

        $requirements = $this->db->query($qrequirements)->result_array();

        $this->data['participantId']    = $participantId;
        $this->data['participant']      = $participantDetail;
        $this->data['family']           = $family;
        $this->data['father']           = $father;
        $this->data['mother']           = $mother;
        $this->data['education']        = $education;
        $this->data['highschool']       = $highschool;
        $this->data['university']       = $university;
        $this->data['requirements']     = $requirements;
		$this->data['academicYear'] = $this->participantmodel->GetAcademicYear($participantId);
		$this->data['typeAccept'] = $participant['ACCEPTANCESTATUS'];
        $this->data['statusSubject'] = $participant['STATUSSUBJECTSETTING'];
		$this->data['idProgram'] = $participant['PROGRAMID'];
        $this->data['enrollmentid'] = $participant['ENROLLMENTID'];
        $this->data['degree'] = $participant['DEGREE'];
        $this->data['programName'] = $participant['PROGRAMNAME'];
		$this->data['typeBeasiswa'] = $participant['SCHOLARSHIPTYPEID'];
		if($participant['ACCEPTANCESTATUS']=='' && $participant['INJECT']!='Y'){
			$this->data['listEnrollment'] = $this->enrollmentmodel->getEnrollmentByProgramIdB($participant['PROGRAMID']);
		}else if($participant['ACCEPTANCESTATUS']!='' && $participant['INJECT']!='Y'){
			$this->data['listEnrollment'] = $this->enrollmentmodel->getEnrollmentByProgramIdA($participant['PROGRAMID']);
		}else{
			$this->data['listEnrollment']='';
		}		
		
        $this->data['listProgram'] = $this->programmodel->GetAllData($this->data['participant']['PROGRAMTYPE']);
        $this->data['listLanguageDelivery'] = $this->languagedeliverymodel->GetAllData();
        $this->data['listDegree']           = $this->degreemodel->GetAllData();
        $this->data['listStudyProgram']       =$this->studyprogrammodel->GetAllData();
		$this->data['listScholarship']       =$this->participantmodel->GetAllScholarship();
		$this->data['listCourse'] = $this->coursemodel->GetAllData();
		$this->data['languageDeliveryId'] = $this->languagedeliverymappingmodel->GetLanguageDeliveryIdById($participant['LANGUAGEDELIVERYMAPPINGID']);
		//echo "<pre>"; print_r($this->data);die;
        $this->load->view('_header',$this->data);
        $this->load->view('staff/participantDetail',$this->data);
        $this->load->view('_footer');
    }

    public function reports(){

        $this->data['pagetitle']="IO Tel-U | Reports";

        $enrollmentId=@$_GET['enrollmentId'];

        $this->load->model("requirementsmodel");
        $this->load->model("studyprogrammodel");
        $this->load->model("enrollmentmodel");
        $this->load->model('facultymodel');
        $this->load->model('countrymodel');

        $requirements   = $this->requirementsmodel->getAllData();
        $studyprograms  = $this->studyprogrammodel->getAllData();
        $enrollments    = $this->enrollmentmodel->getAllData();
        $periods        = $this->enrollmentmodel->fetchAvailableAcademicYear();
        $countries      = $this->countrymodel->GetAllCountry();

        $this->data['faculties'] = $this->facultymodel->GetAllData();
        $this->data['enrollmentId']     = $enrollmentId;
        $this->data['requirements']     = $requirements;
        $this->data['studyprograms']    = $studyprograms;
        $this->data['enrollments']      = $enrollments;
        $this->data['periods']          = $periods;
        $this->data['countries']        = $countries;

//        die(json_encode($periods));

        $this->load->view('_header',$this->data);
        $this->load->view('staff/reports',$this->data);
        $this->load->view('_footer');
    }

    function dataTablePopulateReports(){
        $condition = array(
            'ENROLLMENTID'      => $_GET['enrollmentid']!=''?$_GET['enrollmentid']:'null',
            'ACCEPTANCESTATUS'  => $_GET['acceptance']!=''?$_GET['acceptance']:'null',
            'REQUIREMENTID'     => $_GET['documentid']!=''?$_GET['documentid']:'null',
            'participants.STUDYPROGRAMID'    => $_GET['studyprogramid']!=''?$_GET['studyprogramid']:'null',
            'FACULTYID'         => $_GET['faculty']!=''?$_GET['faculty']:'null',
            'PROGRAMTYPE'       => $_GET['programtype']!=''?$_GET['programtype']:'null',
            'COUNTRYID'         => $_GET['country']!=''?$_GET['country']:'null',
            'PERIOD'            => $_GET['period']!=''?$_GET['period']:'null',
        );

        $query =    "SELECT DISTINCT PARTICIPANTID,FULLNAME,GENDER,PASSPORTNO,CASE 
WHEN NATIONALITY IS NULL OR NATIONALITY=COUNTRYID THEN COUNTRYNAMEENG
ELSE NATIONALITY
END AS NATIONALITY,COUNTRYNAMEENG,DESCRIPTION,PERIOD,ACCEPTANCESTATUS,ENROLLMENTID,STUDYPROGRAMNAME,FACULTYNAMEENG from participants
                        LEFT JOIN country USING(COUNTRYID)
                        LEFT JOIN (enrollment
                          LEFT JOIN (SELECT PROGRAMDEGREEID,PROGRAMID,DEGREEID FROM programdegree) AS programdegree
                          USING (PROGRAMDEGREEID)
                        )
                        USING (ENROLLMENTID)
                        LEFT JOIN (languagedeliverymapping
                          LEFT JOIN (studyprogramlanguage) USING (LANGUAGEDELIVERYMAPPINGID)
                          JOIN (SELECT STUDYPROGRAMID,STUDYPROGRAMNAME,FACULTYID FROM studyprogram) AS STUDYPROGRAM USING (STUDYPROGRAMID)
                          JOIN (SELECT FACULTYID,FACULTYNAMEENG FROM faculty) AS FACULTY USING (FACULTYID)
                        ) USING (LANGUAGEDELIVERYMAPPINGID)
                        LEFT JOIN (requirementvalue)
                          USING (PARTICIPANTID)

                      ";

        $i = 0;
        foreach ($condition as $key=>$value){
            if($value=='null')
                continue;
            else
                if($i==0)
                    $query .= " WHERE $key = '$value'";
                else
                    $query .= " AND $key = '$value'";
            $i++;
        }

//        echo $query;
        $query .= " ORDER BY participants.UPDATEDATE DESC";
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('PARTICIPANTID');
        $this->datatables->SetColumns(array('FULLNAME','PASSPORTNO','GENDER','COUNTRYNAMEENG','DESCRIPTION','PERIOD','FACULTYNAMEENG','STUDYPROGRAMNAME','ACCEPTANCESTATUS','PARTICIPANTID'));
        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }

    public function viewRequirement($participantid,$requirementid){
        $q = "
          SELECT * FROM requirements
          LEFT JOIN (SELECT * FROM requirementvalue 
            WHERE requirementvalue.PARTICIPANTID='$participantid') requirementvalue 
            USING (REQUIREMENTID)
          LEFT JOIN (SELECT USERID,FULLNAME AS UPLOADER FROM users) AS uploader 
            ON (requirementvalue.INPUTBY = uploader.USERID)
          LEFT JOIN (SELECT USERID,FULLNAME AS VERIFICATOR FROM users) AS verificator 
            ON (requirementvalue.VERIFIEDBY = verificator.USERID)
          LEFT JOIN (SELECT FULLNAME AS PARTICIPANTNAME,REGISTRATIONNUMBER,PARTICIPANTID FROM participants) AS participants 
            ON (requirementvalue.PARTICIPANTID = participants.PARTICIPANTID)
          WHERE REQUIREMENTID='$requirementid'
        ";

        $users              = $this->session->userdata('users');
        $users              = $users[0];
        $requirementvalue               = $this->db->query($q)->row_array();
        $this->data['users']            = $users;
        $this->data['requirementvalue'] = $requirementvalue;
        $this->data['privilege']        = json_decode($requirementvalue['PRIVILEGE']);

        $this->data['participantid']    = $participantid;
        $this->load->view('staff/viewRequirement',$this->data);
    }

    public function setRequirementValue(){
        $participantid      = $this->input->post('participantid');
        $requirementvalueid = $this->input->post('requirementvalueid');
        $requirementid      = $this->input->post('requirementid');
        $requirementvalue   = $this->input->post('requirementvalue');
        $updatedate         = date('Y-m-d H:i:s');
        $users              = $this->session->userdata('users');
        $users              = $users[0];
        $updateby           = $users['USERID'];

        if(!$participantid || !$requirementid || !$users)
            die('undefined id');

        if(trim($requirementvalue)!='') {
            $this->db->set('VALUE',$requirementvalue);
            $this->db->set('INPUTBY',$updateby);
            $this->db->set('INPUTDATE',$updatedate);
            $this->db->set('VERIFICATIONSTATUS', "VERIFIED");
            $this->db->set('VERIFICATIONDATE',$updatedate);
            $this->db->set('VERIFIEDBY',$updateby);
        }
        else{
            $this->db->set('VALUE','');
            $this->db->set('INPUTBY',null);
            $this->db->set('INPUTDATE',null);
            $this->db->set('UPDATEDATE',$updatedate);
            $this->db->set('VERIFICATIONSTATUS',"");
            $this->db->set('VERIFICATIONDATE',null);
            $this->db->set('VERIFIEDBY',null);
        }

        $this->db->set('UPDATEBY',$updateby);
        $this->db->set('UPDATEDATE',$updatedate);


        if($requirementvalueid!=''){
            $this->db->where('REQUIREMENTVALUEID',$requirementvalueid);
            $this->db->update('requirementvalue');
        }
        else{
            $this->db->set('PARTICIPANTID',$participantid);
            $this->db->set('REQUIREMENTID',$requirementid);
            $this->db->insert('requirementvalue');
        }

        redirect(base_url()."staff/participantDetail/".$participantid);
    }

    public function verifyRequirementValue($participantid=0,$requirementvalueid=0){
        $updatedate = date('Y-m-d H:i:s');
        $users      = $this->session->userdata('users');
        $users      = $users[0];
        $updateby   = $users['USERID'];

        $this->db->set('VERIFICATIONSTATUS',"VERIFIED");
        $this->db->set('VERIFICATIONDATE',$updatedate);
        $this->db->set('VERIFIEDBY',$updateby);
        $this->db->where('REQUIREMENTVALUEID',$requirementvalueid);
        $this->db->where('PARTICIPANTID',$participantid);
        $this->db->update('requirementvalue');
		
		$qrequirements = "SELECT COUNT(REQUIREMENTID) AS JUMLAH FROM requirements
                            LEFT JOIN (SELECT * FROM requirementvalue 
                              WHERE PARTICIPANTID=$participantid) AS requirementvalue
                              USING (REQUIREMENTID)
                            WHERE PRIVILEGE IN (1,3) AND REQUIREMENTID IN (
                                SELECT REQUIREMENTID
                                FROM requirementmapping
                                JOIN programdegree USING (PROGRAMDEGREEID)
                                JOIN enrollment USING (PROGRAMDEGREEID)
                                JOIN participants USING (ENROLLMENTID)
                                WHERE PARTICIPANTID=$participantid AND VERIFICATIONSTATUS='UNVERIFIED'
                            )";

        $requirements = $this->db->query($qrequirements)->row_array();		
		$participantDetail = $this->participantmodel->GetParticipantDetail($participantid);
		//echo "<pre>"; print_r($participantDetail);die;
		if($requirements['JUMLAH']==0 && $participantDetail['PROGRAMTYPE']=='NON_ACADEMIC'){
			$this->db->set('CURRENTSTEP',7);
			$this->db->where('PARTICIPANTID',$participantid);
			$this->db->update('participants');
		}
		
        redirect(base_url()."staff/participantDetail/".$participantid."#verification-tab");
    }

    public function unverifyRequirementValue($participantid=0,$requirementvalueid=0){
        $this->db->set('VERIFICATIONSTATUS',"UNVERIFIED");
        $this->db->set('VERIFICATIONDATE',null);
        $this->db->set('VERIFIEDBY',null);
        $this->db->where('REQUIREMENTVALUEID',$requirementvalueid);
        $this->db->where('PARTICIPANTID',$participantid);
        $this->db->update('requirementvalue');
        
		$qrequirements = "SELECT COUNT(REQUIREMENTID) AS JUMLAH FROM requirements
                            LEFT JOIN (SELECT * FROM requirementvalue 
                              WHERE PARTICIPANTID=$participantid) AS requirementvalue
                              USING (REQUIREMENTID)
                            WHERE PRIVILEGE IN (1,3) AND REQUIREMENTID IN (
                                SELECT REQUIREMENTID
                                FROM requirementmapping
                                JOIN programdegree USING (PROGRAMDEGREEID)
                                JOIN enrollment USING (PROGRAMDEGREEID)
                                JOIN participants USING (ENROLLMENTID)
                                WHERE PARTICIPANTID=$participantid AND VERIFICATIONSTATUS='UNVERIFIED'
                            )";
		$requirements = $this->db->query($qrequirements)->row_array();		
		$participantDetail = $this->participantmodel->GetParticipantDetail($participantid);
		if($requirements['JUMLAH']!=0 && $participantDetail['PROGRAMTYPE']=='NON_ACADEMIC'){
			$this->db->set('CURRENTSTEP',6);
			$this->db->where('PARTICIPANTID',$participantid);
			$this->db->update('participants');
		}
		redirect(base_url()."staff/participantDetail/".$participantid."#verification-tab");
    }

    public function setAcceptanceStatus(){	
   //echo "<pre>"; print_r($_POST);die();	
		$this->load->model('studyprogramlanguagemodel');
		$this->load->model('programmodel');
		$this->load->model('enrollmentmodel');
		$this->load->model('participantmodel');
		$this->load->model('courselanguagemodel');
		$userid= $this->input->post('userid');
        $participantid      = $this->input->post('idparticipant');
		$academicYear   = $this->input->post('academicYear');
		$academicYear2   = $this->input->post('academicYear2');
        $acceptancestatus   = $this->input->post('acceptancestatus');
        $note               = $this->input->post('note');
        $publishdate        = $this->input->post('publishdate');
		$publishtime       = $this->input->post('publishtime');
		$acceptdate        = $this->input->post('acceptancedate');
        $paymentdate        = $this->input->post('paymentdate');
        $deadlinedate       = $this->input->post('deadlinedate');
        $arrivaldate        = $this->input->post('arrivaldate');
        $startingdate       = $this->input->post('startingdate');
		$startingdateSECE = $this->input->post('startingdateSECE');
		$endingdateSECE   = $this->input->post('endingdateSECE');
		$scholarshiptype    = $this->input->post('typeScholarship');
        $statussubjectsetting = $this->input->post('statussubjectsetting');
        $programname = $this->input->post('thisprogramname');
		if($this->input->post('acceptanceType')==''){
			$participantDetail=$this->participantmodel->GetParticipantDetail($participantid);
			//echo "<pre>"; print_r($participantDetail);die;
			$acceptancetype = $participantDetail['PROGRAMID'];
			$enrollmentid = $participantDetail['ENROLLMENTID'];
			$studyprogramid = $participantDetail['STUDYPROGRAMID'];
			$courseid = $participantDetail['COURSEID'];
		}else{
			$acceptancetype = $this->input->post('acceptanceType');
			$enrollmentid = $this->input->post('enrollment_id');
			$studyprogramid = $this->input->post('study_program_id');
			$courseid = $this->input->post('study_course_id');
		}
		//die;
		$programtypeData = $this->programmodel->getProgramById($acceptancetype)->result_array();
		$programtype = $programtypeData[0]['PROGRAMTYPE'];
		//create school year
		$schoolyear = $academicYear.'/'.$academicYear2;
		if($acceptancestatus=='ACCEPTED'){
			
			if($scholarshiptype!=4 && strpos($programname, 'SCHOLARSHIP') !== false){//SCHOLARSHIPTYPE A,B,C
                $this->db->set('PAYMENTDATE',$paymentdate);
				$this->db->set('SCHOLARSHIPTYPEID',$scholarshiptype);
				$this->db->set('STARTINGDATE',$startingdate); 
			}else if(strpos($programname, 'STUDENT EXCHANGE') !== false || strpos($programname, 'CREDIT EARNING')!== false){//$acceptancetype==1 || $acceptancetype==2){//STUDENT EXCHANGE
                $this->db->set('PAYMENTDATE',$paymentdate);
                $this->db->set('SCHOLARSHIPTYPEID',0);
				$this->db->set('STARTINGDATE',$startingdateSECE); 
				$this->db->set('ENDINGDATE',$endingdateSECE); 
			}else if($scholarshiptype==4){//SCHOLARSHIPTYPE D
                $this->db->set('PAYMENTDATE',$paymentdate);
				$this->db->set('STARTINGDATE',$startingdate); 
                $this->db->set('SCHOLARSHIPTYPEID',$scholarshiptype);
            }else if(strpos($programname, 'STUDENT EXCHANGE') === false && strpos($programname, 'SCHOLARSHIP') === false){//$acceptancetype!=1 && !$acceptancetype!=3){//REGULAR dll
                $this->db->set('PAYMENTDATE',$paymentdate);
                $this->db->set('SCHOLARSHIPTYPEID',0);
				$this->db->set('STARTINGDATE',$startingdate); 
            }

			$this->createRegisNumber($enrollmentid,$acceptancetype,$userid);
			
			$this->db->set('PROGRAMID',$acceptancetype);
			
			$this->db->set('ENROLLMENTID',$enrollmentid);
			if($programtype=='ACADEMIC'){
				$this->db->set('STUDYPROGRAMID',$studyprogramid);

                if($statussubjectsetting=='Y'){
                    $this->db->set('LANGUAGEDELIVERYMAPPINGID',NULL);
                }else{
                    $language_delivery_mapping_id = $this->studyprogramlanguagemodel->GetLanguageDeliveryMappingByStudyPogramId($studyprogramid);    
                    $this->db->set('LANGUAGEDELIVERYMAPPINGID',$language_delivery_mapping_id['LANGUAGEDELIVERYMAPPINGID']);
                }
				
				
				$this->db->set('ACADEMICYEAR',$academicYear);
				$this->db->set('ACCEPTANCEDATE',$acceptdate);
				$this->db->set('DEADLINEDATE',$deadlinedate);
				$this->db->set('SCHOOLYEAR',$schoolyear);
			}else{
				$this->db->set('COURSEID',$courseid);
				$language_delivery_mapping_id = $this->courselanguagemodel->GetLanguageDeliveryMappingByCourseId($courseid);
				$this->db->set('LANGUAGEDELIVERYMAPPINGID', $language_delivery_mapping_id['LANGUAGEDELIVERYMAPPINGID']);
			}						
			$this->db->set('PROGRAMTYPE',$programtype);			
			$this->db->set('ARRIVALDATE',$arrivaldate);			
		}
            $this->db->set('PUBLISHDATE', $publishdate);
			$this->db->set('PUBLISHTIME', $publishtime);
            $this->db->set('UPDATEDATE',date('Y-m-d H:i:s'));
			$this->db->set('ACCEPTANCESTATUS',$acceptancestatus);
			$this->db->set('NOTE',$note);
			$this->db->set('CURRENTSTEP','7');
			$this->db->where('PARTICIPANTID',$participantid);
			$response=$this->db->update('participants');		
		if($response){
			$this->session->set_flashdata('pesan','<script>alert("Success")</script>');
		}else{
			$this->session->set_flashdata('eror','<script>alert("Success")</script>');
		}
		redirect(base_url()."staff/participantDetail/".$participantid."#acceptance-tab");
    }

    public function isLoginStaff(){
        $users=$this->session->userdata('users');
        $users=$users[0];
        if($this->session->userdata('login')&&$users['USERGROUPID']==STAFFUSERGROUPID){

        }else{
            redirect(base_url()."login");
        }
    }
	
	 public function modalEditRegistrationInfo(){
        $this->load->model('programmodel');
        $this->load->model('degreemodel');
        $this->load->model('enrollmentmodel');
        $this->load->model('languagedeliverymodel');
		$this->load->model('languagedeliverymappingmodel');
		$this->load->model('studyprogrammodel');
		$this->load->model('participantmodel');
		$this->load->model('coursemodel');
		$participantId=$this->uri->segment(3);
		$participant = $this->participantmodel->participantDatabyParticipantId($participantId);
		$participantProgramType=$this->uri->segment(4);
		$data['participant'] = $participant;
		$data['listEnrollment'] = $this->enrollmentmodel->getActiveEnrollment();
        $data['listProgram'] = $this->programmodel->GetAllData($participantProgramType);
        $data['listLanguageDelivery'] = $this->languagedeliverymodel->GetAllData();
        $data['listDegree']= $this->degreemodel->GetAllData();
        $data['listStudyProgram']=$this->studyprogrammodel->GetAllData();
		$data['languageDeliveryId'] = $this->languagedeliverymappingmodel->GetLanguageDeliveryIdById($participant['LANGUAGEDELIVERYMAPPINGID']);
		$data['listCourse'] = $this->coursemodel->GetAllData();
        $this->load->view('staff/_editRegistrationInfo',$data);
    }


    public function modalEditProfileInfo(){
        $this->load->model('participantmodel');
        $this->load->model('countrymodel');

        $participantId=$this->uri->segment(3);
        $participant= $this->participantmodel->GetParticipantDetail($participantId);
        $data['participant'] = $participant;
        $data['listCountry'] = $this->countrymodel->GetAllCountry();
        $this->load->view('staff/_editProfileInfo',$data);
    }

    public function modalEditFamilyInfo(){
        $this->load->model('familymodel');
        $this->load->model('countrymodel');

        $familyid       =$this->uri->segment(3);
        $family         = $this->familymodel->getFamilyDetail($familyid);
        $data['family'] = $family;
        $data['listCountry'] = $this->countrymodel->GetAllCountry();
        $this->load->view('staff/_editFamilyInfo',$data);
    }

    public function modalEditEducationInfo(){
        $this->load->model('educationmodel');
		$this->load->model('participantmodel');

        $id                 = $this->uri->segment(3);
        $level              = $this->uri->segment(4);
        $participant		= $this->participantmodel->GetParticipantDetail($id);
        $education          = $this->educationmodel->educationData($id,$level);
        $data['education']  = $education;
		$data['participant']  = $participant;

        $this->load->view('staff/_editEducationInfo',$data);
    }

    public function updateFamilyInfo(){
        $id                     = $this->input->post('familyid');
        $participantid          = $this->input->post('participantid');
        $fullname               = $this->input->post('fullname');
        $address                = $this->input->post('address');
        $city                   = $this->input->post('city');
        $country                = $this->input->post('country');
        $zipcode                = $this->input->post('zipcode');
        $phone                  = $this->input->post('phone');
        $updatedate             = date('Y-m-d H:i:s');

        $this->db->set('FULLNAME',$fullname);
        $this->db->set('ADDRESS',$address);
        $this->db->set('CITY',$city);
        $this->db->set('COUNTRY',$country);
        $this->db->set('ZIPCODE',$zipcode);
        $this->db->set('PHONE',$phone);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->where('FAMILYID',$id);
        $this->db->where('PARTICIPANTID',$participantid);
        $this->db->update('family');

        redirect(base_url()."staff/participantDetail/".$participantid);
    }

    public function updateEducationInfo(){
        $educationid            = $this->input->post('educationid');
        $participantid          = $this->input->post('participantid');
        $schoolname             = $this->input->post('schoolname');
        $start                  = $this->input->post('start');
        $end                    = $this->input->post('end');
        $grade                  = $this->input->post('grade');
        $degree                 = $this->input->post('degree');
        $updatedate             = date('Y-m-d H:i:s');

        $data = array(
            'SCHOOLNAME'    => $schoolname,
            'START'         => $start,
            'END'           => $end,
            'GRADE'         => $grade,
            'DEGREES'        => $degree,
            'UPDATEDATE'    => $updatedate
        );

        $this->load->model('educationmodel');
        $this->educationmodel->update($educationid,$data);

        redirect(base_url()."staff/participantDetail/".$participantid);
    }

    public function updateProfileInfo(){

        $id                     = $this->input->post('participantid');
        $fullname               = $this->input->post('fullname');
        $passportnumber         = $this->input->post('passportnumber');
        $validuntil             = $this->input->post('validuntil');
        $issuedby               = $this->input->post('issuedby');
        $birthplace             = $this->input->post('birthplace');
        $birthdate              = $this->input->post('birthdate');
        $gender                 = $this->input->post('gender');
        $maritalstatus          = $this->input->post('maritalstatus');
        $religion               = $this->input->post('religion');
        $nationality            = $this->input->post('nationality');
        $firstlanguage          = $this->input->post('firstlanguage');
        $proficiencyinbahasa    = $this->input->post('profiency_in_indonesia');
        $proficiencyinenglish   = $this->input->post('profiency_in_english');
        $address                = $this->input->post('address');
        $city                   = $this->input->post('city');
        $country                = $this->input->post('country');
        $zipcode                = $this->input->post('zipcode');
        $phone                  = $this->input->post('phone');
        $fax                    = $this->input->post('fax');
        $note                   = $this->input->post('note');
        $updatedate             = date('Y-m-d H:i:s');

        $this->db->set('FULLNAME',$fullname);
        $this->db->set('PASSPORTNO',$passportnumber);
        $this->db->set('PASSPORTVALID',date('Y-m-d',strtotime($validuntil)));
        $this->db->set('PASSPORTISSUED',$issuedby);
        $this->db->set('BIRTHPLACE',$birthplace);
        $this->db->set('BIRTHDATE',date('Y-m-d',strtotime($birthdate)));
        $this->db->set('GENDER',$gender);
        $this->db->set('MARITALSTATUS',$maritalstatus);
        $this->db->set('RELIGION',$religion);
        $this->db->set('NATIONALITY',$nationality);
        $this->db->set('FIRSTLANGUAGE',$firstlanguage);
        $this->db->set('PROFICIENCYEN',$proficiencyinenglish);
        $this->db->set('PROFICIENCYINA',$proficiencyinbahasa);
        $this->db->set('ADDRESS',$address);
        $this->db->set('CITY',$city);
        $this->db->set('COUNTRYID',$country);
        $this->db->set('ZIPCODE',$zipcode);
        $this->db->set('PHONE',$phone);
        $this->db->set('FAX',$fax);
        $this->db->set('NOTE',$note);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->where('PARTICIPANTID',$id);
        $this->db->update('participants');

        redirect(base_url()."staff/participantDetail/".$id);
    }

    public function updateRegistInfo()
    {					
		$this->load->model('studyprogramlanguagemodel');
		$this->load->model('courselanguagemodel');
        $participantid                = $this->input->post('participant_id');
        $enrollmentid                 = $this->input->post('enrollment_id');
        $programid                    = $this->input->post('program_id');
        $degreeid                     = $this->input->post('degreeid');
		$userid                     = $this->input->post('user_id');
		$thisProgramType 			  = $this->input->post('thisProgramType');
        
        $updatedate                   = date('Y-m-d H:i:s');
				
		if($thisProgramType=='ACADEMIC'){
			$studyprogramid = $this->input->post('study_program_id');
			$languagedeliverymappingid    = $this->studyprogramlanguagemodel->GetLanguageDeliveryMappingByStudyPogramId($studyprogramid);
			$this->db->set('STUDYPROGRAMID',$studyprogramid);
		}else{
			$courseid = $this->input->post('study_course_id');
			$language_delivery_mapping_id = $this->courselanguagemodel->GetLanguageDeliveryMappingByCourseId($courseid);
			$this->db->set('COURSEID',$courseid);
		}
		$this->createRegisNumber($enrollmentid, $programid, $userid);
		$today_year=$this->enrollmentmodel->getEntrollmentById($enrollmentid);
		//echo "<pre>"; print_r($today_year);die;
        $this->db->set('ENROLLMENTID',$enrollmentid);
        $this->db->set('PROGRAMID',$programid);
        $this->db->set('ACADEMICYEAR',$today_year['ACADEMICYEAR']);
        $this->db->set('PROGRAMTYPE',$thisProgramType);
        $this->db->set('LANGUAGEDELIVERYMAPPINGID',$languagedeliverymappingid['LANGUAGEDELIVERYMAPPINGID']);
        $this->db->set('UPDATEDATE',$updatedate);		
        $this->db->where('PARTICIPANTID',$participantid);
        $this->db->update('participants');
		
		redirect(base_url()."staff/participantDetail/".$participantid);
    }

    public function ajaxFetchAcademicYear(){
        $enrollmentid = @$_GET['academicyear'];
        $q = "SELECT DISTINCT ACADEMICYEAR 
              FROM enrollment
              WHERE ENROLLMENTID = '$enrollmentid'";

        $data = $this->db->query($q)->result_array();

        echo json_encode($data);
    }

    public function ajaxFetchProgram(){
        $programtype = @$_GET['programtype'];
        $q = "SELECT PROGRAMID,PROGRAMNAME FROM enrollment
              JOIN programdegree USING(PROGRAMDEGREEID)
              JOIN program USING (PROGRAMID)
              WHERE PROGRAMTYPE='$programtype'
            ";
        $data = $this->db->query($q)->result_array();

      echo json_encode($data);
    }

    public function ajaxFetchEnrollment(){
        $programid = @$_GET['programid'];
		$acceptstatus = @$_GET['acceptstatus'];
		$this->load->model('enrollmentmodel');
		if($acceptstatus!=null){
			//$this->data['listEnrollment'] = $this->enrollmentmodel->getActiveEnrollment();
			 $data = $this->enrollmentmodel->getEnrollmentByProgramIdA($programid);
		}else{
			 $data = $this->enrollmentmodel->getEnrollmentByProgramIdB($programid);
		}
        /*$q = "SELECT ENROLLMENTID,DESCRIPTION FROM enrollment
              JOIN programdegree USING(PROGRAMDEGREEID)
              WHERE PROGRAMID='$programid'
            ";
        $data = $this->db->query($q)->result_array();*/
		
        echo json_encode($data);
    }

    public function ajaxFetchDegree(){
        $enrollmentid = @$_GET['enrollmentid'];
        $q = "SELECT DISTINCT DEGREEID,DEGREE FROM degree
              JOIN programdegree USING(DEGREEID)
              JOIN enrollment USING (PROGRAMDEGREEID)
              WHERE ENROLLMENTID='$enrollmentid'
            ";
        $data = $this->db->query($q)->result_array();

      echo json_encode($data);
    }

    public function ajaxFetchStudyProgram(){
      $programid = @$_GET['programid'];
      $degreeid = @$_GET['degreeid'];
      $q = "SELECT LANGUAGEDELIVERYMAPPINGID, STUDYPROGRAMNAME, LANGUAGE FROM languagedeliverymapping
            JOIN languagedelivery USING (LANGUAGEDELIVERYID)
            JOIN (SELECT PROGRAMDEGREEID from programdegree
              WHERE PROGRAMID='$programid' AND DEGREEID='$degreeid') AS programdegree USING (PROGRAMDEGREEID)
            JOIN studyprogramlanguage USING (LANGUAGEDELIVERYMAPPINGID)
            JOIN studyprogram USING (STUDYPROGRAMID)
          ";
      $data = $this->db->query($q)->result_array();

      echo json_encode($data);
    }
    public function userActivation()
    {
        $this->data['pagetitle']="IO Tel-U | User Activation";

        $this->load->model("usermodel");
        $user=$this->usermodel->getAllData();

        //$this->data['programs']=$programs;


        /*$data['readuser'] = $this->usermodel->readpesanan_admin()->result();
        $this->load->view('v_read_admin_pesanan',$data);*/

        $this->load->view('_header',$this->data);
        $this->load->view('staff/useractivation',$this->data);
        $this->load->view('_footer');
    }

    function dataTableUserActivation(){

        $query = "SELECT * from users
        ORDER BY UPDATEDATE DESC";
        $i = 0;
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('USERID');
        $this->datatables->SetColumns(array('EMAIL','FULLNAME','ACTIVESTATUS','USERID'));
        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
        //echo $this->datatables->GetActiveQuery();
    }
    public function updateUserActivation($id)
   {    
        $this->load->model('usermodel');
        $where = array('userid' => $id);
        //$userid = $this->input->post('userid');
        $active = 'N';
        $data = array(
            'ACTIVESTATUS'    => $active,
        );

        $this->db->set('ACTIVESTATUS',$active);
          $this->db->set('UPDATEDATE',date('Y-m-d H:i:s'));
        $this->db->where('USERID',$id);
        $this->db->update('users');


        $this->load->model('usermodel');
        $this->usermodel->update($id,$data);
        redirect('/staff/useractivation/');

   }
   public function userNonactivated($id)
   {    
        $where = array('userid' => $id);
        //$userid = $this->input->post('userid');
        $active = 'Y';
        $data = array(
            'ACTIVESTATUS'    => $active,
        );

        $this->db->set('ACTIVESTATUS',$active);
        $this->db->set('UPDATEDATE',date('Y-m-d H:i:s'));
        $this->db->where('USERID',$id);
        $this->db->update('users');


        $this->load->model('usermodel');
        $this->usermodel->update($id,$data);
        redirect('/staff/useractivation/');
   }
   public function updateInterview()
   {
    $id                     = $this->input->post('participantid');
    $interviewStatus        = $this->input->post('interviewStatus');
    $interviewDate          = $this->input->post('interviewDate');
    $score                  = $this->input->post('score');


    $this->db->set('INTERVIEWSTATUS',$interviewStatus);
    $this->db->set('INTERVIEWDATE',$interviewDate);
    $this->db->set('INTERVIEWSCORE',$score);
	if($score!=0){
		$this->db->set('CURRENTSTEP',7);
	}	
    $this->db->set('UPDATEDATE',date('Y-m-d H:i:s'));
    $this->db->where('PARTICIPANTID',$id);
    $this->db->update('participants');

        redirect(base_url()."staff/participantDetail/".$id.'#interview-tab');
   }
   
   public function exportExcel(){
		$status=$this->uri->segment(3);	
		$academicyear=$this->uri->segment(4);
		$period=$this->uri->segment(5);
		$this->load->model('participantmodel');
		$data['data_participant'] = $this->participantmodel->modalParticipants($status,$academicyear,$period);
				
		foreach($data as $key){
			foreach($key as $key2){
				//if($key2['ENROLLMENTID']!=0 && $key2['PROGRAMID']!=0){
					$response = $this->createRegisNumber($key2['ENROLLMENTID'], $key2['PROGRAMID'], $key2['USERID']);
				//}else{
					//$response ='SUCCESS';
				//}
			}
		}
		$data['data_participant'] = $this->participantmodel->modalParticipants($status,$academicyear, $period);
		echo "<pre>"; print_r($data);die;
		//if ($response) {
			$data['status'] = $status;
			$this->load->view('admin/exportExcelModal',$data);
		//}
		
   }	
	
	public function dashboardCountry(){	
		$this->data['pagetitle']="IO Tel-U | Dashboard";	
		$this->load->model('countrymodel');
		$this->data['data_country'] = $this->countrymodel->getCountry();
		$this->load->view('_header',$this->data);
        $this->load->view('staff/dashboardCountry',$this->data);
        $this->load->view('_footer');
	}
	
	public function datatableCountryParticipant(){
		$this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('NATIONALITY');
        $this->datatables->SetColumns(array('NATIONALITY','JUMLAH'));
        $this->datatables->SetActiveCounter(true);
		$periods ='';
		if($_GET['period']!=''){
			$period = explode(",",$_GET['period']);
			//echo "<pre>"; print_r($period);die;
			for($x=0;$x<count($period);$x++){
					if($period[$x]=='UNDEFINED YEAR'){
						$period[$x] = "";
					}
					if(($x+1)==count($period)){
						$periods .= " E.PERIOD = '".$period[$x]."'";
					}else{
						
						$periods .= " E.PERIOD = '".$period[$x]."' OR";
					}
			}
		}
			
		
		if($_GET['acceptance']==''){			
			$sql = "SELECT DISTINCT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
LEFT JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
";
			if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){
				$sql .= "  WHERE E.ACADEMICYEAR=".$_GET['academicyear'];
			}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
				$sql .= "  WHERE E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
			}else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){
				$sql .= "  WHERE (".$periods.")";
			}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
				$sql .= "  WHERE E.ACADEMICYEAR IS NULL AND (".$periods.")";
			}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods==''){
				$sql .= "  WHERE E.ACADEMICYEAR IS NULL";
			}else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){
				$sql .= "  WHERE (E.PERIOD IS NULL)";
			}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){
				$sql .= "  WHERE E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
			}
			$sql .= " GROUP BY B.NATIONALITY";
		}else{
			$sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
LEFT JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID) WHERE B.ACCEPTANCESTATUS = '".$_GET['acceptance']."' ";
			if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){
				$sql .= "  AND E.ACADEMICYEAR=".$_GET['academicyear'];
			}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
				$sql .= "  AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
			}else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){
				$sql .= "  AND (".$periods.")";
			}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
				$sql .= "  AND E.ACADEMICYEAR IS NULL AND (".$periods.")";
			}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods==''){
				$sql .= "  AND E.ACADEMICYEAR IS NULL";
			}else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){
				$sql .= "  AND (E.PERIOD IS NULL)";
			}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){
				$sql .= "  AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
			}
			$sql .= " GROUP BY B.NATIONALITY";
		}
		//echo "<pre>"; print_r($sql);//die;
		$this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
	}
	
	public function datatableCountryParticipant2(){
		$this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('NATIONALITY');
        $this->datatables->SetColumns(array('NATIONALITY','JUMLAH'));
		
        $this->datatables->SetActiveCounter(true);
		if($_GET['acceptance']==''){			
			$sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B";
			$sql .= " GROUP BY B.NATIONALITY";
		}else{
			$sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
				WHERE B.ACCEPTANCESTATUS = '".$_GET['acceptance']."'
				";
			$sql .= " GROUP BY B.NATIONALITY";
		}
		$this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
	}
	public function dashboardStudyProgram(){
		 $this->data['pagetitle']="IO Tel-U | Dashboard";
		$this->load->model('studyprogrammodel');
		$this->data['data_studyprogram'] = $this->studyprogrammodel->getStudyProgram('','');
		$this->load->view('_header',$this->data);
        $this->load->view('staff/dashboardStudyProgram',$this->data);
        $this->load->view('_footer');
	}
	
	public function datatableStudyProgram(){
		$this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('STUDYPROGRAMID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','JUMLAH'));
		$periods ='';
		if($_GET['period']!=''){
			$period = explode(",",$_GET['period']);
			//echo "<pre>"; print_r($period);die;
			for($x=0;$x<count($period);$x++){
					if($period[$x]=='UNDEFINED YEAR'){
						$period[$x] = "";
					}
					if(($x+1)==count($period)){
						$periods .= " E.PERIOD = '".$period[$x]."'";
					}else{
						
						$periods .= " E.PERIOD = '".$period[$x]."' OR";
					}
			}
		}
        $this->datatables->SetActiveCounter(true);
		$sql = "SELECT
	C.STUDYPROGRAMID, 
	C.STUDYPROGRAMNAME,
	COUNT(B.STUDYPROGRAMID) AS JUMLAH
FROM studyprogram C
	LEFT JOIN (SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME AS JUMLAH
				FROM studyprogram A
				JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
				JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
				WHERE B.ACCEPTANCESTATUS = 'ACCEPTED'";
		if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){
			$sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear'].")B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){
			$sql .= " AND (".$periods."))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){
			$sql .= " AND (E.PERIOD IS NULL))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}else if($_GET['academicyear']=='UNDEFINED' && $periods=='UNDEFINED YEAR'){
			$sql .= " AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}else if($_GET['academicyear']=='UNDEFINED' && $periods==''){
			$sql .= " AND E.ACADEMICYEAR IS NULL)B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}else if($_GET['academicyear']=='UNDEFINED' && $periods!='' && $periods!='UNDEFINED YEAR'){
			$sql .= " AND E.ACADEMICYEAR IS NULL AND (".$periods."))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}else if($_GET['academicyear']!='UNDEFINED' && $_GET['academicyear']!='' && $periods=='UNDEFINED YEAR'){
			$sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}else{
				$sql .= ")B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		}
		//echo "<pre>"; print_r($sql);
		$this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
	}
	
	public function datatableStudyProgram2(){
		$this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('STUDYPROGRAMID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','JUMLAH'));

        $this->datatables->SetActiveCounter(true);
		
				$sql = "SELECT
	C.STUDYPROGRAMID, 
	C.STUDYPROGRAMNAME,
	COUNT(B.STUDYPROGRAMID) AS JUMLAH
FROM studyprogram C
	LEFT JOIN (SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME AS JUMLAH
				FROM studyprogram A
				JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
				WHERE B.ACCEPTANCESTATUS = 'ACCEPTED')B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, 	C.STUDYPROGRAMNAME";
		
		$this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
	}
	
	public function dashboardEnrollment(){
		$this->data['pagetitle']="IO Tel-U | Dashboard";
		$this->load->model('enrollmentmodel');
		$this->data['data_enrollment'] = $this->enrollmentmodel->getEnrollment('','');
		$this->load->model('countrymodel');
		$this->data['data_country'] = $this->countrymodel->getCountry();
		$this->load->model('studyprogrammodel');
		$this->load->model('participantmodel');
		$this->load->model('countrymodel');
		$this->data['data_studyprogram'] = $this->studyprogrammodel->getStudyProgram('',"");
        $number_acounts = $this->participantmodel->getNumberAcounts('');
		$number_participants = $this->participantmodel->getNumberParticipants('','','');
		$number_scholarship = $this->participantmodel->getNumberParticipants('scholarship','','');
		$number_not_scholarship = $this->participantmodel->getNumberParticipants('notscholarship','','');
		$number_country= $this->countrymodel->getNumberCountry('','','');
		$number_students = $this->participantmodel->getNumberParticipants('students','','');
		$step1 = $this->participantmodel->getStepParticipants(1,'','');
		$step2 = $this->participantmodel->getStepParticipants(2,'','');
		$step3 = $this->participantmodel->getStepParticipants(3,'','');
		$step4 = $this->participantmodel->getStepParticipants(4,'','');
		$step5 = $this->participantmodel->getStepParticipants(5,'','');
		//$step6 = $this->participantmodel->getStepParticipants(6,'');
		$step7 = $this->participantmodel->getStepSeven(7,'','');
		
		$number_accept = $this->participantmodel->getParticipantStatus('ACCEPTED','','');
		$number_unaccept = $this->participantmodel->getParticipantStatus('UNACCEPTED','','');
		$number_unscheduled_interview = $this->participantmodel->getInterview('','unscheduled_interview','');
		$number_not_interview = $this->participantmodel->getInterview('','not_interview','');
		$number_interview = $this->participantmodel->getInterview('','have_interview','');
        $this->data['number_acounts'] = $number_acounts[0]['JUMLAH'];
		$this->data['number_participants'] = $number_participants[0]['JUMLAH'];
		$this->data['number_scholarship'] = $number_scholarship[0]['JUMLAH'];
		$this->data['number_not_scholarship'] = $number_not_scholarship[0]['JUMLAH'];
		$this->data['number_country'] = $number_country[0]['JUMLAH'];
		$this->data['number_students'] = $number_students[0]['JUMLAH'];
		$this->data['step1'] = $step1[0]['JUMLAH'];
		$this->data['step2'] = $step2[0]['JUMLAH'];
		$this->data['step3'] = $step3[0]['JUMLAH'];
		$this->data['step4'] = $step4[0]['JUMLAH'];
		$this->data['step5'] = $step5[0]['JUMLAH'];
		
		$this->data['step7'] = $step7[0]['JUMLAH'];
		$this->data['number_interview'] = $number_interview[0]['JUMLAH'];
		$this->data['number_unscheduled_interview'] = $number_unscheduled_interview[0]['JUMLAH'];
		$this->data['number_not_interview'] = $number_not_interview[0]['JUMLAH'];
		$this->data['number_confirm'] = $this->participantmodel->getConfirmDate('accept','','');
		//$this->data['number_absent'] = $this->participantmodel->getConfirmDate('unaccept','');
		$this->data['number_accept'] = $number_accept[0]['JUMLAH'];
		$this->data['number_unaccept'] = $number_unaccept[0]['JUMLAH'];
		$this->data['academicyear'] = $this->enrollmentmodel->getAcademicYear();
		$this->data['period'] = $this->enrollmentmodel->getPeriod();
		$filter = $this->enrollmentmodel->getFilterStatus('Y','ACADEMICYEAR');
		if(count($filter)==0){
			$this->data['academicyear_filter'] = 0;
		}else{
			$this->data['academicyear_filter'] = $filter;
		}
		//echo "<pre>"; print_r($this->data['academicyear_filter']);die;
		$this->load->view('_header',$this->data);
        $this->load->view('staff/dashboardEnrollment',$this->data);
        $this->load->view('_footer');
	}
	
	public function datatableEnrollment(){
		$this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('PROGRAMID');
        $this->datatables->SetColumns(array('PROGRAMNAME','JUMLAH','PROGRAMID','ACADEMICYEAR'));
        $this->datatables->SetActiveCounter(true);
		//echo "<pre>"; print_r($_GET['academicyear']);
		
		$periods ='';
		if($_GET['period']!=''){
			$period = explode(",",$_GET['period']);
			//echo "<pre>"; print_r($period);die;
			for($x=0;$x<count($period);$x++){
					if($period[$x]=='UNDEFINED YEAR'){
						$period[$x] = "";
					}
					if(($x+1)==count($period)){
						$periods .= " E.PERIOD = '".$period[$x]."'";
					}else{
						
						$periods .= " E.PERIOD = '".$period[$x]."' OR";
					}
			}
		}
		//echo "<pre>"; print_r($periods);
		$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID)
			LEFT JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' ";
		if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){	
			$sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear'];
		}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){	
			$sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
		}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){	
			$sql .= " AND E.ACADEMICYEAR IS NULL AND (".$periods.")";
		}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){	
			$sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
		}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){	
			$sql .= " AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL)";
		}else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){	
			$sql .= " AND (E.PERIOD IS NULL)";
		}else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){	
			$sql .= " AND (".$periods.")";
		}
		
		$sql .= ")B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
					LEFT JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' ";
		
		if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){	
			$sql .= "AND E.ACADEMICYEAR=".$_GET['academicyear'];
		}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){	
			$sql .= "AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
		}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){	
			$sql .= "AND E.ACADEMICYEAR IS NULL AND (".$periods.")";
		}else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){	
			$sql .= "AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
		}else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){	
			$sql .= "AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL)";
		}else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){	
			$sql .= "AND (E.PERIOD IS NULL)";
		}else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){	
			$sql .= " AND (".$periods.")";
		}
		$sql .= ")B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
	    //}else{
	/*	$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID) 
			LEFT JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED')B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
                    B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED')B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
		}*/
		//echo "<pre>"; print_r($sql);die;
		$this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
	}
	
	public function dashboardStudyProgramByEnrollment($programid,$programname,$academicyear){
		//echo "<pre>"; print_r($academicyear);die;
		$this->data['pagetitle']="IO Tel-U | Dashboard";
		$this->load->model('studyprogrammodel');
		if(substr($programname,-4)=='ship'){
			$this->data['data_studyprogramby'] = $this->studyprogrammodel->getStudyProgramScholarship($programid,$academicyear);
		}else{
			$this->data['data_studyprogramby'] = $this->studyprogrammodel->getStudyProgramByEnrollment($programid,$academicyear);
		}				
		$this->data['programid'] = $programid;
		$this->data['academicyear'] = $academicyear;
		$this->data['programname'] = ucwords(strtolower(str_replace('%20',' ',$programname)));
		$this->load->view('_header',$this->data);
        $this->load->view('staff/dashboardStudyProgramByEnrollment',$this->data);
        $this->load->view('_footer');
    }
	
	public function datatableStudyProgramByEnrollment($programid,$programname,$academicyear){
		$this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('STUDYPROGRAMID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','JUMLAH'));
        $this->datatables->SetActiveCounter(true);
		if(substr($programname,-4)=='ship'){			
			$sql = "SELECT A.SCHOLARSHIPTYPEID AS STUDYPROGRAMID, A.SCHOLARSHIPTYPENAME AS STUDYPROGRAMNAME, COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN participants B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
				WHERE B.ACCEPTANCESTATUS = 'ACCEPTED' AND B.SCHOLARSHIPTYPEID='".$programid."'
				";
			if($academicyear!='x'){
				$sql .= " AND B.ACADEMICYEAR=".$academicyear."
					   GROUP BY A.SCHOLARSHIPTYPEID, A.SCHOLARSHIPTYPENAME";
			}else{
				$sql .= " GROUP BY A.SCHOLARSHIPTYPEID, A.SCHOLARSHIPTYPENAME";
			}
		}else{
			$sql = "SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME, COUNT(B.STUDYPROGRAMID) AS JUMLAH
				FROM studyprogram A
				LEFT JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
				WHERE B.ACCEPTANCESTATUS = 'ACCEPTED' AND B.PROGRAMID='".$programid."'
				";
			if($academicyear!='x'){
				$sql .= " AND B.ACADEMICYEAR=".$academicyear."
					   GROUP BY A.STUDYPROGRAMID, A.STUDYPROGRAMNAME";
			}else{
				$sql .= " GROUP BY A.STUDYPROGRAMID, A.STUDYPROGRAMNAME";
			}
		}	
		
		$this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
	}
	
	public function saveRequirement(){
		$participantid = $this->input->post('thisisparticipantId');
		$requirementId = $this->input->post('thisisrequirementId');
		
		$this->load->model('participantmodel');	
		$this->load->model('requirementsvaluemodel');
		$participant= $this->participantmodel->GetParticipantDetail($participantid);
		$users=$this->session->userdata('users');        
        $this->session_user = $users[0];
		
		for($i=0; $i<sizeof($requirementId); $i++){
			$data    = array(
				'REQUIREMENTID' =>  $requirementId[$i],
				'PARTICIPANTID' =>  $participantid,
				'INPUTDATE' =>  date('Y-m-d H:i:s'),
				'INPUTBY' =>  $this->session_user['USERID']
			);
			if(!empty($_FILES[$requirementId[$i].'_data']['name'])){
				$new_name = $participant['PASSPORTNO'];
				if (!is_dir('uploads/media/participant/requirement/' . $new_name)) {
					mkdir('./uploads/media/participant/requirement/' . $new_name, 0777, TRUE);
				}
		
				$upload_config['upload_path']   = "./uploads/media/participant/requirement/". $new_name."/";
				$upload_config['access_path'] 	= "/uploads/media/participant/requirement/". $new_name."/";
				$upload_config['allowed_types'] = "jpg|jpeg|pdf";
				$upload_config['overwrite'] 	= TRUE;
				$upload_config['max_size'] 		= "1024000"; // Can be set to particular file size ; here it is 1 MB
				$upload_config['file_name'] = $requirementId[$i].'_'.$_FILES[$requirementId[$i].'_data']['name'];
				$this->upload->initialize($upload_config);
				$response_upload = $this->upload->do_upload($requirementId[$i].'_data');
				
				if($response_upload){
					$upload_data = $this->upload->data();
					$data['FILENAME'] = str_replace(' ','_',$upload_config['file_name']);
					$data['FILEURL'] = $upload_config['access_path'];
				}			
				$response = $this->requirementsvaluemodel->insert($data);
				if ($response) {
					$this->session->set_flashdata('msg_success', 'UPLOAD DATA SUCCESSFULL');
					redirect(base_url()."staff/participantDetail/".$participantid."#verification-tab");
				} else {
					$this->session->set_flashdata('msg_failed', 'FAILED UPLOAD DATA');
					redirect(base_url()."staff/participantDetail/".$participantid."#verification-tab");
				}
			}/*else if(!empty($this->input->post($requirementId[$i].'_data'))){
				$data['VALUE'] = $this->input->post($requirementId[$i].'_data');
				$response = $this->requirementsvaluemodel->insert($data);
				if ($response) {
					$this->session->set_flashdata('msg_success', 'INSERT DATA SUCCESSFULL');
					redirect(base_url()."staff/participantDetail/".$participantid."#verification-tab");
				} else {
					$this->session->set_flashdata('msg_failed', 'FAILED INSERT DATA');
					redirect(base_url()."staff/participantDetail/".$participantid."#verification-tab");
				}
			}*/
		}		
	}
	
	 public function modalNumberParticipants($no,$nama,$academicyear,$period){
        $this->load->model('participantmodel');	
		$this->data['status'] = $no;
		//echo "<pre>"; print_r($period);die;//2019-8_2019-1
		if($academicyear=='x' || $academicyear==''){
			$academicyear = '';
		}
		if($period=='x' || $period==''){
			$period = '';
		}
		$this->data['academicyear'] = $academicyear;
		$this->data['period'] = $period;
		$this->data['judul'] = str_replace('_',' ',$nama)." ".$academicyear." Period ".str_replace("_",",",$period);
        $this->load->view('staff/_numberParticipants',$this->data);
    }
	
	public function createRegisNumber($enrollment_id, $program_id, $user_id){
		$participant  = $this->participantmodel->participantData($user_id);
		if(strlen($participant['ADMISSIONID'])!=10 || $program_id!=$participant['PROGRAMID']){
			$year= $this->enrollmentmodel->getEntrollmentById($enrollment_id);
			$today_year = $year['ACADEMICYEAR'];
			
			//create REGISTRATION NUMBER 
			$last_admissionid = $this->participantmodel->GetAdmissionId();
			$admissionid = $last_admissionid['ADMISSIONID']+1;
			if((ceil(log10($last_admissionid['ADMISSIONID']))==1 && $last_admissionid['ADMISSIONID']!=9 && $last_admissionid['ADMISSIONID']!=10) || $last_admissionid['ADMISSIONID']==1 || $last_admissionid['ADMISSIONID']==0){
				$admissionid = '000'.$admissionid;
			}else if(((ceil(log10($last_admissionid['ADMISSIONID'])))==2 && $last_admissionid['ADMISSIONID']!=99 && $last_admissionid['ADMISSIONID']!=100) || $last_admissionid['ADMISSIONID']==9 || $last_admissionid['ADMISSIONID']==10){
				$admissionid = '00'.$admissionid;
			}else if((ceil(log10($last_admissionid['ADMISSIONID']))==3 && $last_admissionid['ADMISSIONID']!=999 && $last_admissionid['ADMISSIONID']!=1000) || $last_admissionid['ADMISSIONID']==99 || $last_admissionid['ADMISSIONID']==100){
				$admissionid = '0'.$admissionid;
			}else if(ceil(log10($last_admissionid['ADMISSIONID']))==4 || $last_admissionid['ADMISSIONID']==999 || $last_admissionid['ADMISSIONID']==1000){	
				$admissionid = $admissionid;
			}
			$registration_number=$today_year.'0'.$program_id.$admissionid;
			$participan_data = array('ADMISSIONID' => $registration_number);
			$response = $this->participantmodel->update($user_id, $participan_data);
			return $response;
		}
	}
	
	public function exportExcelModal(){
		$status=$this->uri->segment(3);	
		$academicyear=$this->uri->segment(4);
		$period=$this->uri->segment(5);
		$this->load->model('participantmodel');
		$data['data_participant'] = $this->participantmodel->modalParticipants($status,$academicyear,$period);
				
		foreach($data as $key){
			foreach($key as $key2){
				//if($key2['ENROLLMENTID']!=0 && $key2['PROGRAMID']!=0){
					$response = $this->createRegisNumber($key2['ENROLLMENTID'], $key2['PROGRAMID'], $key2['USERID']);
				//}else{
					//$response ='SUCCESS';
				//}
			}
		}
		$data['data_participant'] = $this->participantmodel->modalParticipants($status,$academicyear, $period);
		//echo "<pre>"; print_r($data);die;
		//if ($response) {
			$data['status'] = $status;
			$this->load->view('staff/exportExcelModal',$data);
		//}
		
   }
	
	 function dataTableModal($status,$academicyear,$period){
       $periods = explode('_',$period);
		$periodss ='';
		if(($periods!='' || $periods!=NULL && $periods!='x') && $periods!='UNDEFINED YEAR' && $periods!='x'){
			for($x=0;$x<count($periods);$x++){
			if(($x+1)==count($periods) && $periods[$x]!='x'){
				$periodss .= " enrollment.PERIOD = '".$periods[$x]."'";
			}else if($periods[$x]!='x'){
				$periodss .= " enrollment.PERIOD = '".$periods[$x]."' OR";
			}
		}
		}else if($periods=='UNDEFINED YEAR'){
			$periodss = 'UNDEFINED YEAR';
		}else if($periods=='x'){
			$periodss ='';
		}else{
			$periodss = $periods;
		}
		//echo "<pre>"; print_r($period);die;
       $sql ="SELECT DISTINCT PARTICIPANTID,participants.FULLNAME,GENDER,PASSPORTNO,CASE 
WHEN NATIONALITY IS NULL OR NATIONALITY=COUNTRYID THEN COUNTRYNAMEENG
ELSE NATIONALITY
END AS NATIONALITY,COUNTRYNAMEENG,DESCRIPTION,PERIOD,ACCEPTANCESTATUS,ENROLLMENTID,STUDYPROGRAMNAME,FACULTYNAMEENG,PROGRAMTYPE from participants
                        LEFT JOIN country USING(COUNTRYID)
                         LEFT JOIN users USING (USERID)
                        LEFT JOIN quizparticipant USING (PARTICIPANTID)
                        LEFT JOIN (enrollment
                          LEFT JOIN (SELECT PROGRAMDEGREEID,PROGRAMID,DEGREEID FROM programdegree) AS programdegree
                          USING (PROGRAMDEGREEID)
                        )
                        USING (ENROLLMENTID)
                        LEFT JOIN (languagedeliverymapping
                          LEFT JOIN (studyprogramlanguage) USING (LANGUAGEDELIVERYMAPPINGID)
                          JOIN (SELECT STUDYPROGRAMID,STUDYPROGRAMNAME,FACULTYID FROM studyprogram) AS STUDYPROGRAM USING (STUDYPROGRAMID)
                          JOIN (SELECT FACULTYID,FACULTYNAMEENG FROM faculty) AS FACULTY USING (FACULTYID)
                        ) USING (LANGUAGEDELIVERYMAPPINGID)
                        LEFT JOIN (requirementvalue)
                          USING (PARTICIPANTID)
                          ";
		if($status=='scholarship'){
			$sql .=" WHERE participants.PROGRAMID=3";
		}else if($status=='notscholarship'){
			$sql .=" WHERE participants.PROGRAMID IN (1,2,4,5)";
		}else if($status==2||$status==3||$status==4||$status==5){
			$sql .=" WHERE participants.CURRENTSTEP=".$status;
		}else if($status==1){
            $sql .=" WHERE participants.CURRENTSTEP = 1
AND participants.PROGRAMID= 0
AND participants.ENROLLMENTID IS NULL
AND participants.STUDYPROGRAMID = 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NULL
AND users.ACTIVESTATUS ='Y'
AND quizparticipant.STATUS = 1";
        }else if($status=='have_interview'){
			$sql .=" where participants.INTERVIEWDATE != '0000-00-00 00:00:00'
and (participants.INTERVIEWSTATUS != '' or participants.INTERVIEWSCORE != 0)
and participants.CURRENTSTEP = 6
";
		}else if($status=='ACCEPTED' || $status=='UNACCEPTED'){
			$sql .=" WHERE (participants.PROGRAMTYPE='ACADEMIC' AND participants.ACCEPTANCESTATUS='".$status."' AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 AND participants.INTERVIEWDATE!='0000-00-00 00:00:00') OR (participants.PROGRAMTYPE='NON_ACADEMIC' AND participants.ACCEPTANCESTATUS='".$status."') ";
		}else if($status=='attend'){
			$sql .=" WHERE participants.ACCEPTANCESTATUS = 'ACCEPTED'
AND participants.INTERVIEWSTATUS = 'Accept'
AND participants.INTERVIEWSCORE !=0
AND participants.INTERVIEWDATE != '0000-00-00 00:00:00' AND participants.LOAFILE IS NOT NULL";
		}else if($status=='absent'){
			$sql .=" WHERE participants.ACCEPTANCESTATUS = 'ACCEPTED'
AND participants.INTERVIEWSTATUS = 'Accept'
AND participants.INTERVIEWSCORE !=0
AND participants.INTERVIEWDATE != '0000-00-00 00:00:00' AND participants.LOAFILE IS NULL";
		}else if($status==7){
			$sql .= "WHERE (participants.PROGRAMTYPE='ACADEMIC' AND participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' OR participants.ACCEPTANCESTATUS IS NULL)
AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 
AND participants.INTERVIEWDATE!='0000-00-00 00:00:00') OR 
(participants.PROGRAMTYPE='NON_ACADEMIC' AND participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' OR participants.ACCEPTANCESTATUS IS NULL)) ";
			/*" WHERE participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' 
OR participants.ACCEPTANCESTATUS IS NULL)
AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 
AND participants.INTERVIEWDATE!='0000-00-00 00:00:00' ";*/
		}else if($status=='not_interview'){
			$sql .= "where participants.INTERVIEWDATE != '0000-00-00 00:00:00'
and participants.CURRENTSTEP =6
and participants.INTERVIEWSCORE = 0 and participants.INTERVIEWSTATUS=''";
		}else if($status=='unscheduled_interview'){
			$sql .= " where participants.INTERVIEWDATE = '00-00-0000'
					and participants.CURRENTSTEP =6 and participants.INTERVIEWSTATUS='' AND participants.INTERVIEWSCORE = 0";
		}else if($status=='acount'){
            $sql .= " WHERE participants.CURRENTSTEP = 1
AND participants.PROGRAMID= 0
AND participants.ENROLLMENTID IS NULL
AND participants.STUDYPROGRAMID = 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NULL";
        }else if($status=='all'){
            $sql .= " WHERE participants.PROGRAMID!= 0
AND participants.ENROLLMENTID IS NOT NULL
AND participants.STUDYPROGRAMID != 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NOT NULL";
        }
		
		/*if($academicyear!='x'){
			$sql .= " WHERE participants.ACADEMICYEAR = ".$academicyear;
		}else */if($academicyear!='x' && $academicyear!='UNDEFINED YEAR'){
			$sql .= " AND enrollment.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=='UNDEFINED YEAR'){
			$sql .= " AND enrollment.ACADEMICYEAR IS NULL";
		}
		if($periodss!='' && $periodss!="UNDEFINED YEAR" && $periodss!='x'){
			$sql .= " AND (".$periodss.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (e.PERIOD IS NULL)";
		}
        $sql .= " ORDER BY participants.UPDATEDATE DESC";
		//echo "<pre>"; print($sql);die;
		//print_r($sql);die;
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('PARTICIPANTID');
        $this->datatables->SetColumns(array('FULLNAME','GENDER','NATIONALITY','DESCRIPTION','FACULTYNAMEENG','STUDYPROGRAMNAME','PARTICIPANTID','PROGRAMTYPE'));
        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }

    function createParticipant(){
        $this->load->model('countrymodel');
        $this->data['pagetitle']="IO Tel-U | Create New Participant";
        $this->data['listCountry'] = $this->countrymodel->GetAllCountry();
        $this->load->view('_header',$this->data);
        $this->load->view('staff/createParticipant',$this->data);
        $this->load->view('_footer');
    }

    function createParticipantProcess(){
        $post_data = $this->input->post();
        $this->load->model('participantmodel');
        $this->load->model('usermodel');
        if($this->input->post()) {
            $insert_id = $this->db->insert_id();
            $participan_data = array(
                'FULLNAME' => $post_data['fullname'],
                'PASSPORTNO' => $post_data['nationality'],
                    'EMAIL' => $post_data['email'],
                    'PASSWORD' => $post_data['password'],
                    'NATIONALITY' => $post_data['nationality'],
                    'PHONE' => $post_data['phone_ext']. "" .$post_data['phone'],
                    'INPUTDATE' => date('Y-m-d H:i:s'),
                    'USERID'    => $insert_id,
                );

            $user_data = array(
                'USERID'    => $insert_id,
                'FULLNAME'  => $post_data['fullname'],
                'NATIONALITY'  => $post_data['nationality'],
                'PHONE'     => $post_data['phone_ext'] . "" . $post_data['phone'],
                'INPUTDATE' => date('Y-m-d H:i:s'),
                'UPDATEDATE' => date('Y-m-d H:i:s'),
            );

            $response = $this->participantmodel->insert($participan_data);
            $response2 = $this->usermodel->insert($user_data);

            if ($response && $response2) {
                $this->session->set_flashdata('msg_success', 'INSERT DATA SUCCESSFULL');
                redirect(base_url() . "staff/participants");
            } else {
                $this->session->set_flashdata('msg_failed', 'FAILED INSERT DATA');
                redirect(base_url() . "staff/participants");
            }
        }else{
            redirect(base_url()."participant/programSelection");
        }
    }

    public function dataTableSubjectMappingExchange(){
        $this->load->model('participantmodel');
        $data = $this->participantmodel->getSubjectMapping2($_POST['filter']);
        // for($i=0; $i<count($data); $i++){
        //    // $data[$i]['NO'] = $i+1;
        //     $data[$i]['ACTION'] = '<input type="checkbox" class="left" name="" language="'.$data[$i]['LANGUAGE'].'" credit="'.$data[$i]['CREDIT'].'" subjectname="'.$data[$i]['SUBJECTNAME'].'" studyprogramname="'.$data[$i]['STUDYPROGRAMNAME'].'" enrollmentid="'.$data[$i]['ENROLLMENTID'].'" studyprogramid="'.$data[$i]['STUDYPROGRAMID'].'" curiculum="'.$data[$i]['CURICULUMYEAR'].'" id="'.$data[$i]['SUBJECTCODE'].'">';
        // }
        $json_data = array(
            'data' => $data
        );
        echo json_encode($json_data);
    }
	
}

