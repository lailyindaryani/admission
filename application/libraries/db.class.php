<?php

class DB
{
    //class properties
    var $config = array();
    var $conn = null;
    var $affectedRows = 0, $numRows = 0;
    var $lastQuery = "";
    var $statement = null;
    var $resQuery = null;
    var $schemaName = null;
    var $lastId = null;
    var $rownum = null;
    var $limitOrder = null;
    /*
    var $tableSchema = array(
        "adminLanguage" => "FRAMEWORK.ADMINLANGUAGE", //udah
        "applications" => "FRAMEWORK.APPLICATIONS", //udah
        "commonLanguage" => "FRAMEWORK.COMMONLANGUAGE", //udah
        "applicationUserGroup" => "FRAMEWORK.APPLICATIONUSERGROUP", //udah
        "applicationPage" => "FRAMEWORK.APPLICATIONPAGE", //udah
        "layouts" => "FRAMEWORK.LAYOUTS", //udah
        "messages" => "FRAMEWORK.MESSAGES", //udah
        "modules" => "FRAMEWORK.MODULES", //udah
        "moduleLanguage" => "FRAMEWORK.MODULELANGUAGE", //udah
        "pages" => "FRAMEWORK.PAGES", //udah
        "pageSort" => "FRAMEWORK.PAGESORT", //udah
        "applicationPageAccess" => "FRAMEWORK.APPLICATIONPAGEACCESS", //udah
        "skins" => "FRAMEWORK.SKINS", //udah
        "userGroups" => "FRAMEWORK.USERGROUPS", //udah
        "userMapping" => "FRAMEWORK.USERMAPPINGVIEW", //udah
        "userMappings" => "FRAMEWORK.USERMAPPING", //udah
        "users" => "FRAMEWORK.USERS", //udah
        "userLogin" => "FRAMEWORK.USERLOGIN", //udah
        "userLoginMapping" => "FRAMEWORK.USERLOGINMAPPING", //udah
        "userGroupMatrix" => "FRAMEWORK.USERGROUPMATRIX" //udah
    );*/
    
    
    var $tableSchema = array(
        "adminLanguage" => "dbo.ADMINLANGUAGE", //udah
        "applications" => "dbo.APPLICATIONS", //udah
        "commonLanguage" => "dbo.COMMONLANGUAGE", //udah
        "applicationUserGroup" => "dbo.APPLICATIONUSERGROUP", //udah
        "applicationPage" => "dbo.APPLICATIONPAGE", //udah
        "layouts" => "dbo.LAYOUTS", //udah
        "messages" => "dbo.MESSAGES", //udah
        "modules" => "dbo.MODULES", //udah
        "moduleLanguage" => "dbo.MODULELANGUAGE", //udah
        "pages" => "dbo.PAGES", //udah
        "pageSort" => "dbo.PAGESORT", //udah
        "applicationPageAccess" => "dbo.APPLICATIONPAGEACCESS", //udah
        "skins" => "dbo.SKINS", //udah
        "userGroups" => "dbo.USERGROUPS", //udah
        "userMapping" => "dbo.USERMAPPINGVIEW", //udah
        "userMappings" => "dbo.USERMAPPING", //udah
        "users" => "dbo.USERS", //udah
        "userLogin" => "dbo.USERLOGIN", //udah
        "userLoginMapping" => "dbo.USERLOGINMAPPING", //udah
        "userGroupMatrix" => "dbo.USERGROUPMATRIX" //udah
    );
    
    var $table = array(
        "adminLanguage" => "ADMINLANGUAGE", //udah
        "applications" => "APPLICATIONS", //udah
        "commonLanguage" => "COMMONLANGUAGE", //udah
        "applicationUserGroup" => "APPLICATIONUSERGROUP", //udah
        "applicationPage" => "APPLICATIONPAGE", //udah
        "layouts" => "LAYOUTS", //udah
        "messages" => "MESSAGES", //udah
        "modules" => "MODULES", //udah
        "moduleLanguage" => "MODULELANGUAGE", //udah
        "pages" => "PAGES", //udah
        "pageSort" => "PAGESORT", //udah
        "applicationPageAccess" => "APPLICATIONPAGEACCESS", //udah
        "skins" => "SKINS", //udah
        "userGroups" => "USERGROUPS", //udah
        "userMapping" => "USERMAPPINGVIEW", //udah
        "userMappings" => "FRAMEWORK.USERMAPPING", //udah
        "users" => "USERS", //udah
        "userLogin" => "USERLOGIN", //udah
        "userLoginMapping" => "USERLOGINMAPPING", //udah
        "userGroupMatrix" => "USERGROUPMATRIX" //udah
    );
/*
    var $tablePK = array(
        "USERS" => "FRAMEWORK.USERS", //udah
        "ANNOUNCEMENT" => "FRAMEWORK.ANNOUNCEMENT",
        "APPLICATIONS" => "FRAMEWORK.APPLICATIONS",
        "MODULES" => "FRAMEWORK.MODULES",
        "PAGES" => "FRAMEWORK.PAGES",
        "USERGROUPS" => "FRAMEWORK.USERGROUPS",
        "USERGROUPSMATRIX" => "FRAMEWORK.USERGROUPMATRIX"
    );
    */
    var $tablePK = array(
        "USERS" => "dbo.USERS", //udah
        "ANNOUNCEMENT" => "dbo.ANNOUNCEMENT",
        "APPLICATIONS" => "dbo.APPLICATIONS",
        "MODULES" => "dbo.MODULES",
        "PAGES" => "dbo.PAGES",
        "USERGROUPS" => "dbo.USERGROUPS",
        "USERGROUPSMATRIX" => "dbo.USERGROUPMATRIX"
    );

    //constructors
    function DB($config = null)
    {
        if ($config != null) {
            $this->config = $config;
            //($this->config['DB_ORACLE_POOLED'])?$this->PConnect():$this->Connect();
            $this->Connect();

        }
        //echo $this->GetConnection."<br>";
    }

    function GetTableArray(){
        if ($this->GetDbType() == "oracle") {
            return $this->table;
        } else if ($this->GetDbType() == "mssql") {
            return $this->tableSchema;
        } else if ($this->GetDbType() == "mysql") {

            return $this->table;
        }
    }
    function SettingDatabase($index, $sLimit, $sOrder)
    {
        if ($this->GetDbType() == "oracle") {
            $this->rownum = ", rownum as id";
            $this->limitOrder = $sLimit . " " . $sOrder;
        } else if ($this->GetDbType() == "mssql") {
            $this->rownum = ", ROW_NUMBER() OVER (ORDER BY $index)  as id";
            $this->limitOrder = $sLimit . " " . $sOrder;
        } else if ($this->GetDbType() == "mysql") {
            $this->rownum = "";
            $this->limitOrder = $sOrder . " " . $sLimit;
        }
    }

    function GetRowNum()
    {
        return $this->rownum;
    }

    function GetLimitOrder()
    {
        return $this->limitOrder;
    }

    //load database config
    function LoadDBConfig()
    {
        include LIBPATH . "db.config.php";
        $this->DBConfig['frameworkConfig'] = $frameworkConfig;
        $this->DBConfig['sdmConfig'] = $sdmConfig;
        $this->DBConfig['escalationConfig'] = $escalationConfig;
        $this->DBConfig['masterdataConfig'] = $masterdataConfig;

    }

    //switch config DB
    function SwitchSetting($configName)
    {
        $this->Setting($this->GetDBConfig($configName));
    }

    //get DB Setting
    function GetDBConfig($configName)
    {
        return $this->DBConfig[$configName];
    }

    //-------------methods--------------
    function Setting($config, $connection = null)
    {
        if ($connection == null) {
            $this->config = $config;
            //($this->config['DB_ORACLE_POOLED'])?$this->PConnect():$this->Connect();
            $this->Connect();
        } else {
            $this->conn = $connection;
        }

    }

    function Connect()
    {

        if ($this->config['DB_TYPE'] == "mysql") {

            $this->conn = @mysql_connect($this->config['DB_HOST'], $this->config['DB_USERNAME'], $this->config['DB_PASSWORD']);
            if (!$this->conn && $this->config['DB_DISPLAY_ERROR']) {
                echo mysql_errno($this->conn) . " : " . @mysql_error($this->conn) . "\n";
                if ($this->config['DB_BREAK_PROCESS']) exit(0);
            }

            $db = @mysql_select_db($this->config['DB_NAME_MYSQL'], $this->conn);
            if (!$db && $this->config['DB_DISPLAY_ERROR']) {
                echo @mysql_errno($this->conn) . " : " . @mysql_error($this->conn) . "\n";
                if ($this->config['DB_BREAK_PROCESS']) exit(0);
            }
        } else if ($this->config['DB_TYPE'] == "oracle") {
            $tns = "(DESCRIPTION =
						(ADDRESS_LIST =
							(ADDRESS = (PROTOCOL = TCP)(HOST = " . $this->config['DB_HOST'] . ")(PORT = 1521)))
						(CONNECT_DATA =
							(SERVICE_NAME = " . $this->config['DB_SERVICE_ORACLE'] . "))
					)";
            //$tns = "//".$this->config['DB_HOST']."/".$this->config['DB_SERVICE_ORACLE'];
            $this->conn = @oci_connect($this->config['DB_USERNAME'], $this->config['DB_PASSWORD'], $tns);
            if (!$this->conn) {
                //echo "<span style='color:#FFFFFF'>Database Oracle Error</span>";
                //   echo "<script>self.location.href='../indeks.php'</script>";
                $e = @oci_error();
                if ($this->config['DB_DISPLAY_ERROR']) {
                    $e = @oci_error(); // For oci_connect errors do not pass a handle
                    @trigger_error(@htmlentities($e['message']), E_USER_ERROR);
                    if ($this->config['DB_BREAK_PROCESS']) exit(0);
                }
            } else {
                $e = @oci_error();
                echo $e['message'];
            }
        } else if ($this->config['DB_TYPE'] == "mssql") {


            if($this->config['MSSQL_DRIVER_TYPE']=='sqlsrv'){

                $serverName = $this->config['DB_HOST']; //serverName\instanceName
                if(isEmpty($this->config['DB_USERNAME'])&&isEmpty($this->config['DB_PASSWORD'])){
                    $connectionInfo = array( "Database"=>$this->config['DB_NAME_MSSQL']);
                }else
                    $connectionInfo = array( "Database"=>$this->config['DB_NAME_MSSQL'], "UID"=>$this->config['DB_USERNAME'], "PWD"=>$this->config['DB_PASSWORD']);
                $this->conn = sqlsrv_connect( $serverName, $connectionInfo);

                 if (!$this->conn && $this->config['DB_DISPLAY_ERROR']) {
                    echo "Connection could not be established.<br />";
                    die( print_r( sqlsrv_errors(), true));
                 }
            }else{
                $this->conn = @mssql_connect($this->config['DB_HOST'], $this->config['DB_USERNAME'], $this->config['DB_PASSWORD']);
                if (!$this->conn && $this->config['DB_DISPLAY_ERROR']) {
                    echo @mssql_get_last_message($this->conn) . " : " . @mssql_get_last_message($this->conn) . "\n";
                    if ($this->config['DB_BREAK_PROCESS']) exit(0);
                }

                $db = @mssql_select_db($this->config['DB_NAME_MSSQL'], $this->conn);
                if (!$db && $this->config['DB_DISPLAY_ERROR']) {
                    echo mssql_get_last_message($this->conn) . " : " . @mssql_get_last_message($this->conn) . "\n";
                    if ($this->config['DB_BREAK_PROCESS']) exit(0);
                }
            }
			

        }
    }

    function PConnect()
    {
        if ($this->config['DB_TYPE'] == "mysql") {

            $this->conn = @mysql_pconnect($this->config['DB_HOST'], $this->config['DB_USERNAME'], $this->config['DB_PASSWORD']);
            if (!$this->conn && $this->config['DB_DISPLAY_ERROR']) {

                echo mysql_errno($this->conn) . " : " . @mysql_error($this->conn) . "\n";
                if ($this->config['DB_BREAK_PROCESS']) exit(0);
            }

            $db = @mysql_select_db($this->config['DB_NAME_MYSQL'], $this->conn);
            if (!$db && $this->config['DB_DISPLAY_ERROR']) {
                echo @mysql_errno($this->conn) . " : " . @mysql_error($this->conn) . "\n";
                if ($this->config['DB_BREAK_PROCESS']) exit(0);
            }
        } else if ($this->config['DB_TYPE'] == "oracle") {
            $tns = "(DESCRIPTION =
						(ADDRESS_LIST =
							(ADDRESS = (PROTOCOL = TCP)(HOST = " . $this->config['DB_HOST'] . ")(PORT = 1521)))
						(CONNECT_DATA =
							(SERVICE_NAME = " . $this->config['DB_SERVICE_ORACLE'] . ")
							(SERVER=POOLED))
					)";
            //$tns = "//".$this->config['DB_HOST']."/".$this->config['DB_SERVICE_ORACLE'];
            $this->conn = @oci_pconnect($this->config['DB_USERNAME'], $this->config['DB_PASSWORD'], $tns);
            if (!$this->conn) {
                $e = @oci_error();
                if ($this->config['DB_DISPLAY_ERROR']) {
                    $e = @oci_error(); // For oci_connect errors do not pass a handle
                    @trigger_error(@htmlentities($e['message']), E_USER_ERROR);
                    if ($this->config['DB_BREAK_PROCESS']) exit(0);
                }
            } else {
                $e = @oci_error();
                echo $e['message'];
            }
        }
    }

    function ExecuteQuery($q)
    {
//        echo $q;
        $this->lastQuery = $q;
        // $this->Connect();

        if ($this->config['DB_TYPE'] == "mysql") {
            $this->statement = @mysql_query($q, $this->conn);
            $this->numRows = @mysql_num_rows($this->statement);
            $this->affectedRows = @mysql_affected_rows($this->statement);

            if (!$this->statement && $this->config['DB_DISPLAY_ERROR']) {
                echo "Query : $q\n";

                echo @mysql_errno($this->conn) . " : " . @mysql_error($this->conn) . "\n";

                if ($this->config['DB_BREAK_PROCESS']) exit(0);
            }

        } else if ($this->config['DB_TYPE'] == "oracle") {
            $this->statement = @oci_parse($this->conn, $q);
            // echo $q;
            if (!$this->statement && $this->config['DB_DISPLAY_ERROR']) {
                $e = @oci_error($this->conn);
                @trigger_error(@htmlentities($e['message']), E_USER_ERROR);
                if ($this->config['DB_BREAK_PROCESS']) exit(0);
            } else if ($this->statement) {
                $this->resQuery = @oci_execute($this->statement);
                // echo $this->statement;
                if (!$this->resQuery && $this->config['DB_DISPLAY_ERROR']) {
                    $e = @oci_error($this->statement);
                    echo @htmlentities($e['message']);
                    echo "\n<pre>\n";
                    echo @htmlentities($e['sqltext']);
                    printf("\n%" . ($e['offset'] + 1) . "s", "^");
                    echo "\n</pre>\n";
                    if ($this->config['DB_BREAK_PROCESS']) exit(0);
                }
                if (@oci_statement_type($this->statement) != "SELECT") {
                    $this->affectedRows = @oci_num_rows($this->statement);
                    //$strLog = "INSERT INTO FRAMEWORK1.HISTORYQUERIES VALUES('".StrToDb($q)."',SYSDATE,'".@GetIPAddressClient()."','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."','".$_SESSION['userid']."','".$_GET['pageid']."')";
                    $strLog = "INSERT INTO HISTORY.HISTORYQUERIES VALUES('" . StrToDb($q) . "',SYSDATE,'" . @GetIPAddressClient() . "','" . $_SERVER['REMOTE_ADDR'] . "','" . $_SERVER['HTTP_USER_AGENT'] . "','" . $_SESSION['userid'] . "','" . $_GET['pageid'] . "')";
                    // echo "<br>$strLog";

                    $statementLog = @oci_parse($this->conn, $strLog);
                    @oci_execute($statementLog);
                    return $this->resQuery;
                }
            }
        } else if ($this->config['DB_TYPE'] == "mssql") {
            if($this->config['MSSQL_DRIVER_TYPE']=='sqlsrv'){
                $this->statement = sqlsrv_query( $this->conn, $q,array());
                
                if (!$this->statement && $this->config['DB_DISPLAY_ERROR']) {
                    if( ($errors = sqlsrv_errors() ) != null) {
                        echo "<pre>"; print_r($errors); echo "</pre>";
                        if ($this->config['DB_BREAK_PROCESS']) exit(0);
                    }
                } elseif ($this->statement) {
                    $this->numRows = sqlsrv_num_rows($this->statement);
                    $this->affectedRows = sqlsrv_rows_affected ( $this->statement );
                }
            }else{
                $this->statement = mssql_query($q, $this->conn);
                
                if (!$this->statement && $this->config['DB_DISPLAY_ERROR']) {
                    trigger_error('Query: ' . $q);
                    trigger_error(htmlentities('MSSQL error: ' . mssql_get_last_message()), E_USER_ERROR);
                    if ($this->config['DB_BREAK_PROCESS']) exit(0);
                } elseif ($this->statement) {
                    $this->numRows = mssql_num_rows($this->statement);
                    $this->affectedRows = mssql_rows_affected($this->conn);
                }
            }
        }
        return $this->statement;
    }


    function GetOciError()
    {
        return ($this->statement != null) ? @oci_error($this->statement) : @oci_error();
    }

    function GetConnection()
    {
        return $this->conn;
    }

    function GetStatement()
    {
        return $this->statement;
    }

    function FetchArray($r = null,$assoc=false)
    {

        $statement = ($r != null) ? $r : $this->statement;
        $arr = null;
        if ($this->config['DB_TYPE'] == "mysql") {
            $arr = @mysql_fetch_array($statement);
        } else if ($this->config['DB_TYPE'] == "oracle") {
            $arr = @oci_fetch_array($statement);
        } else if ($this->config['DB_TYPE'] == "mssql") {
            if($this->config['MSSQL_DRIVER_TYPE']=='sqlsrv'){
                if($assoc)
                    $arr =sqlsrv_fetch_array( $statement,SQLSRV_FETCH_ASSOC);
                else
                    $arr =sqlsrv_fetch_array( $statement);
            }else{
                if($assoc)
                    $arr =@mssql_fetch_assoc( $statement);
                else
                    $arr = @mssql_fetch_array($statement);
            }

        }

        return $arr;
    }

    function Insert($tablename, $arrData,$isRaw=false)
    {

        $fields = array();
        $values = array();
        foreach ($arrData as $key => $val) {
            $fields[] = ($key);
            $values[] = "$val";
        }
		if($isRaw){
				$sqlQuery = "INSERT INTO " . $tablename . " (" . @implode(', ', $fields) . ") VALUES (" . @implode(', ', $values) . ")";
				
		}else
			$sqlQuery = "INSERT INTO " . $this->GetSchemaName() . $tablename . " (" . @implode(', ', $fields) . ") VALUES (" . @implode(', ', $values) . ")";

        //print $sqlQuery;die();
        $val= $this->ExecuteQuery($sqlQuery);
        if ($this->config['DB_TYPE'] == "mysql") {
            $this->lastId = mysql_insert_id($this->conn);

        }
        return $val;
    }
    
    function UpdateBySP($SPname, $arrData)
    {

        $fields = array();
        $values = array();
        foreach ($arrData as $key => $val) {
            $columns[] = "@$key=$val";
        }
		$sqlQuery = "EXEC " . $this->GetSchemaName() . $SPname . "  " . @implode(', ', $columns);
				
		
        //print $sqlQuery;die();
        $val= $this->ExecuteQuery($sqlQuery);
        
        return $val;
    }

    function SetIdentityOn($identityName){

        if ($this->GetDbType() == "mssql" && $identityName!="") {
            $sqlQuery ="SET IDENTITY_INSERT $identityName ON";
            return $this->ExecuteQuery($sqlQuery);
        }
    }

    function SetIdentityOff($identityName){
        if ($this->GetDbType() == "mssql" && $identityName!="") {
            $sqlQuery ="SET IDENTITY_INSERT $identityName OFF";
            return $this->ExecuteQuery($sqlQuery);
        }
    }

    function Update($tablename, $arrData, $condition = "")
    {

        $columns = array();
        foreach ($arrData as $key => $val) {
            $columns[] = "$key=$val";
        }
        $sqlQuery = "UPDATE " . $this->GetSchemaName() . $tablename . " SET " . @implode(', ', $columns) . " " . ($condition ? " WHERE $condition" : "");
        //echo "$sqlQuery";die();
        return $this->ExecuteQuery($sqlQuery);
    }

    function Delete($tablename, $condition = "")
    {
        $sqlQuery = "DELETE FROM " . $this->GetSchemaName() . $tablename . ($condition ? " WHERE $condition" : "");
        //echo "<br>$sqlQuery"; die;
        return $this->ExecuteQuery($sqlQuery);
    }

    function Select($tablename, $fields = "*", $condition = "", $ordering = "")
    {
        $listFields = "*";
        $orderClm = "";

        //$this->Connect();
        if (@is_array($fields)) {
            if (@count($fields) > 1) {
                $listFields = @implode(",", $fields);
            } elseif (@count($fields) == 1) {
                $listFields = $fields[0];
            }
        }

        if (@is_array($ordering)) {
            if (@count($ordering) > 1) {
                $orderClm = @implode(",", $ordering);
            } elseif (@count($ordering) == 1) {
                $orderClm = $ordering[0];
            }
        }
        $sqlQuery = "SELECT $listFields FROM ($tablename) " . (($condition != "") ? " WHERE " . $condition : "") . (($orderClm != "") ? " ORDER BY " . $orderClm : "");
        //echo "<br>$sqlQuery";
        $statement = $this->ExecuteQuery($sqlQuery);

        if ($this->config['DB_TYPE'] == "mysql") {
            return $statement;
        } else if ($this->config['DB_TYPE'] == "oracle") {
            return $this->ReadRow($statement);
        } else {
            return false;
        }
    }


    function ReadRow($statement = null,$assoc=false)
    {
        $count = 0;
        $arrData = null;
        if ($statement != null) {
            while ($row = $this->FetchArray($statement,$assoc)) {

                $vArr = null;
                foreach ($row as $key => $val) {
                    $vArr[$key] = $val;
                }
                $arrData[] = $vArr;
                $count++;
            }
        } else {
            while ($row = $this->FetchArray()) {
                $vArr = null;
                foreach ($row as $key => $val) {
                    $vArr[$key] = $val;
                }
                $arrData[] = $vArr;
                $count++;
            }
        }
        $this->numRows = $count;

        return $arrData;
    }

    function SelectLimit($table, $start = 1, $end = 0, $orderby = "")
    {
        if($this->GetDbType() == 'oracle') {
            $query = "SELECT * FROM ( SELECT ROWNUM RNUM, ZZZ.* from (
                ".$table."
                        ) ZZZ WHERE ROWNUM <= $end ) where RNUM >= $start";

        } else if ($this->GetDbType() == 'mssql'){
            $queryTop=$this->ExecuteQuery("SELECT TOP 1 * FROM ($table)AS ZZZ ");
            $row=$this->FetchArray($queryTop,true);
            $columns=array_keys($row);
            $order = ($orderby) ? $orderby : "ORDER BY [".$columns[0]."]";
            $rownum = ", ROW_NUMBER() OVER (".$order.")  as id";


            $query = "select * from(
			select a.* ".$rownum."
			from ($table) a) b WHERE id between $start AND $end";

        } else if ($this->GetDbType() == 'mysql'){
            $a=$start-1;
            $b=($end-$start)+1;
            $query = "$table LIMIT $a , $b";
        }

        //return $query;
        $statement = $this->ExecuteQuery($query);
        return $this->ReadRow($statement,true);
    }

    function SelectLimitOld($table, $start = 1, $end = 0)
    {
        //include "db.variable.framework.php";
        //$table = $this->GetTableArray();

        //$this->SettingDatabase($modules['moduleId'], $sLimit, $sOrder);
        $rownum = $this->GetRowNum();
        $limitOrder = $this->GetLimitOrder();

        if($this->GetDbType() == 'oracle') {
            $query = "select * from(
			select a.*,row_number()over(order by rownum)rnum
			from ($table) a) where rnum <=" . intval($end) . " and rnum >= " . intval($start);
        } else if ($this->GetDbType() == 'mssql'){
            $query = "select * from(
			select a.* ".$rownum."
			from ($table) a) b $limitOrder";
        } else if ($this->GetDbType() == 'mysql'){
            $query = "select * from(
			select a.* ".$rownum."
			from ($table) a) b $limitOrder";
        }
        echo $query;
        $statement = $this->ExecuteQuery($query);
        return $this->ReadRow($statement);
    }

    function GetTotalRow($dtIndex = "*", $sTable = "")
    {
        $query = "SELECT COUNT(" . ($dtIndex == "*" ? "*" : "a." . $dtIndex) . ") FROM ($sTable) a";
        $resultAll = $this->ExecuteQuery($query);
        $raData = $this->FetchArray($resultAll);
        $totalRecord = $raData[0];
        return $totalRecord;
    }

    function GetNumRows()
    {
        return $this->numRows;
    }

    function GetAffectedRows()
    {
        return $this->affectedRows;
    }

    function GetLastQuery()
    {
        return $this->lastQuery;
    }

    function GetNextSeqValue($seqName,$isRaw=false)
    {
        if ($this->config['DB_TYPE'] == 'mysql') {
            return 0;
        } else if ($this->config['DB_TYPE'] == 'mssql') {

            if($isRaw)
                $statement = $this->ExecuteQuery("SELECT NEXT VALUE FOR  $seqName 'NVALUE' ");

            else
                $statement = $this->ExecuteQuery("SELECT NEXT VALUE FOR ".$this->GetSchemaName()." $seqName 'NVALUE' ");


            $data = $this->FetchArray($statement);
            //$this->lastId = $data['NVALUE'];
           // echo "SELECT ".$this->GetSchemaName()." $seqName() AS NVALUE";
           // $statement = $this->ExecuteQuery("SELECT ".$this->GetSchemaName()." $seqName() AS NVALUE");

            //$data = $this->FetchArray($statement);
            $this->lastId = $data['NVALUE'];
            return $data['NVALUE'];
        } else if ($this->config['DB_TYPE'] == "oracle") {
            $statement = $this->ExecuteQuery("SELECT $seqName.NEXTVAL AS NVALUE FROM DUAL");
            $data = $this->FetchArray($statement);
            $this->lastId = $data['NVALUE'];
            return $data['NVALUE'];

        }
    }

    function GetLastId()
    {
        return $this->lastId;
    }

    function GetDate()
    {
        $loginDate = null;
        if ($this->config['DB_TYPE'] == 'mysql')
            $loginDate = "NOW()";
        else if ($this->config['DB_TYPE'] == 'mssql')
            $loginDate = "getdate()";
        else if ($this->config['DB_TYPE'] == 'oracle')
            $loginDate = "SYSDATE";
        return $loginDate;
    }

    function GetDbType()
    {
        return $this->config['DB_TYPE'];
    }

    function GetSchemaName()
    {
        if ($this->config['DB_TYPE'] == 'mssql')
            return "[".$this->config['DB_NAME_MSSQL'] . "]."."[".$this->config['DB_SCHEMA_MSSQL'] . "].";
        else
            return "";
    }

    function GetCurrSeqValue($seqName)
    {
        $statement = $this->ExecuteQuery("SELECT $seqName.CURRVAL AS NVALUE FROM DUAL");
        $data = $this->FetchArray($statement);
        return $data['NVALUE'];
    }

    //oracle only
    function ExecuteProcedure($procedureName, $arrParameters)
    {
        $query = "begin $procedureName(" . @implode(', ', $arrParameters) . "); end;";
        // echo "<br>$query";
        return $this->ExecuteQuery($query);
    }

    function OciParse($query)
    {
        return @oci_parse($query);
    }

    function OciBindByName($bvName, $variable, $statement = null, $length = -1, $type = 'SQLT_CHR')
    {
        if ($statement == null) return @oci_bind_by_name($this->statement, "':$bvName'", $variable, $length);
        else return @oci_bind_by_name($statement, "':$bvName'", $variable, $length, $type);
    }

    function OciExecute($statement = null)
    {
        if ($statement == null) return @oci_execute($this->statement);
        else return @oci_execute($statement);
    }

    function GetConfiguration()
    {
        return $this->config;
    }

    function LoadSchema($schemaName, $aliasName = "")
    {
        //include_once DBVARPATH."schema.".strtolower($schemaName).".php";
        include_once "dbvariable/schema." . strtolower($schemaName) . ".php";
        $className = $schemaName . "_SCHEMA";
        if ($aliasName != "") {
            if (!isset($this->$aliasName))
                $this->$aliasName = new $className;
        } else {
            if (!isset($this->$schemaName))
                $this->$schemaName = new $className;
        }
    }

    function GetMaximumValue($columnName, $table)
    {
        $query = "SELECT MAX(" . $columnName . ") FROM ($table)";
        $resultAll = $this->ExecuteQuery($query);
        $raData = $this->FetchArray($resultAll);
        $maxValue = $raData[0];
        return $maxValue;
    }

    function GetListColumn($tableName)
    {
        $res = $this->ExecuteQuery("SELECT * from user_tab_columns where table_name='" . $tableName . "' ORDER BY column_id ");
        return $this->ReadRow($res);
    }

    function GetListColumnByQuery($query)
    {
        $arrayData = array();

        $this->ExecuteQuery($query);
        $ncols = oci_num_fields($this->statement);
        for ($i = 1; $i <= $ncols; $i++) {
            $arrayData[($i - 1)]['NAME'] = oci_field_name($this->statement, $i);
            $arrayData[($i - 1)]['TYPE'] = oci_field_type($this->statement, $i);
            $arrayData[($i - 1)]['SIZE'] = oci_field_size($this->statement, $i);
        }
        return $arrayData;
    }

    function SetAffectedRows($affRows)
    {
        $this->affectedRows = $affRows;
    }
    
    function getBIOS($jenisInfo,$tgl_update)
    {

         $query="exec sp_bios @tgl_update='$tgl_update',@jenisInfo='$jenisInfo'";
        //echo $query;
        $statement = $this->ExecuteQuery($query);
        return $this->ReadRow($statement,1);
    }

    function __destruct()
    {
        @oci_free_statement($this->statement);
        @oci_close($this->conn);
        if ($this->config['DB_TYPE'] == "mssql") {
            if($this->config['MSSQL_DRIVER_TYPE']=='sqlsrv'){
                sqlsrv_close( $this->conn);
            }else
			    mssql_close($this->conn);
        }
    }
}

?>
