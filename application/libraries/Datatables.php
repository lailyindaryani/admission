<?php
@session_start();
include_once "db.class.php";
include_once "util.php";
//include_once "..\config\database.php";

/* Deskripsi : Class model datatable
 *
 */
class DataTables extends DB {
    var $dtColumns = array(); //kolom datatable
    var $dtIndex = null; //kolom yang dijadikan index untuk mempercepat count
    var $sWhere = ""; //clause where di query yang dikirim ke dbms server
    var $sOrder = ""; //clause order by di query yang dikirim ke dbms server
    var $sLimit = ""; //clause untuk membatasi result hasil query
    var $sTable = ""; //nama single table atau join table
    var $totalRecord = 0; //total record
    var $filteredRecord = 0; //jumlah record yang telah di filter
    var $output = null; //array output JSON
    var $isCounterActive = false;
    var $totalColumn=0;
	var $encryptColumn="";
    var $activeQuery = "";

    /* constructor */
    function __construct($config) {

        $this->Setting($config);
    }

    /*********************************Methods******************************/
    /* set kolom datatable */
    function SetColumns($columns) {
        if(count($columns)>0) {
            if(!isset($this->totalColumn))
                $this->totalColumn = count($columns);
            for($i=0;$i<count($columns);$i++){
                $this->dtColumns[] = $columns[$i];
            }
        }else{
            $this->dtColumns = null;
            $this->dtColumns = $columns;
        }
    }


    /* set kolom index */
    function SetIndexColumn($idx) {
        $this->dtIndex = $idx;
    }

    /* set table untuk */
    function SetTable($table) {
        $this->sTable = $table;
    }

    function GetRawQuery(){
        return $this->sTable;
    }

    function SetCondition($cond){
        if($this->sWhere==""){
            $this->sWhere = " WHERE ($cond) ";
        }else{
            $this->sWhere .= " AND ($cond) ";
        }
    }

    /* set clause where untuk query */
    function Where(){
        if ( $_GET['sSearch'] != "" )
        {

            $lastWhere=substr($this->sTable, strrpos($this->sTable, ')') + 1);
            //echo $lastWhere;
            if($this->sWhere==""){
                if(stripos($lastWhere, "where") === false)
                    $this->sWhere = "WHERE (";
                else
                    $this->sWhere .= "WHERE (";
                //EDITED BY SETIYO 21 DEC, TADINYA AND ^

            }else{
                $this->sWhere .= "AND (";
            }
            for($i=0;$i<count($this->dtColumns);$i++) {
                $this->sWhere .= "lower(".$this->dtColumns[$i].") LIKE '%".strtolower( $_GET['sSearch'] )."%' OR ";
            }
            $this->sWhere = substr_replace( $this->sWhere, "", -3 );
            $this->sWhere .= ')';
        }
        for($i=0;$i<count($this->dtColumns);$i++) {
            if($_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i]!='') {
                if($this->sWhere == "") {
                    $this->sWhere = "WHERE ";
                }
                else {
                    $this->sWhere .= " AND ";
                }
                $this->sWhere .= "lower(".$this->dtColumns[$i].") LIKE '%".strtolower($_GET['sSearch_'.$i])."%' ";
            }
        }
    }

    function SetOrdering($order){
        if($this->sOrder==""){
            $this->sOrder = " ORDER BY $order ";
        }else{
            $this->sOrder .= ",$order ";
        }
    }

    // function SetOrder(){
    // if ( isset( $_GET['iSortCol_0'] ) )
    // {
    // $this->sOrder = "ORDER BY  ";
    // for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
    // {
    // if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
    // {
    // $this->sOrder .= $this->dtColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
    // ".$_GET['sSortDir_'.$i].", ";
    // }
    // }

    // $this->sOrder = @substr_replace( $this->sOrder, "", -2 );
    // if ( $this->sOrder == "ORDER BY" )
    // {
    // $this->sOrder = "";
    // }
    // }
    // }

    function Order(){
        if ( isset( $_GET['iSortCol_0'] )&&$_GET['sEcho']>0 )
        {
            if($this->sOrder==""){
                $this->sOrder = "ORDER BY  ";
            }else{
                $this->sOrder .=", ";
            }
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $iCount = $this->isCounterActive?(intval( $_GET['iSortCol_'.$i] )-1):intval( $_GET['iSortCol_'.$i] );
                    $this->sOrder .= ($this->dtColumns[ $iCount ]!="")?$this->dtColumns[ $iCount ]." ".$_GET['sSortDir_'.$i].", ":"";
                }
            }
            $this->sOrder = @substr_replace($this->sOrder,"",-2);
            if($this->sOrder == "ORDER BY") {
                $this->sOrder = "";
            }
        }
    }

    /* get data from server */
    function GetDataTables($order=null,$sort=null) {


        $query = "";
        $data = null;
        $table = null;
        $this->Where();
        $this->Order();
        if($this->sOrder=="" && $order!=null){
            $this->sOrder = $order;
        }
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            if(@substr_count(@strtolower($this->sTable),"select")>0)
                $table = $this->sTable;
            else
                $table = "SELECT * FROM $this->sTable";
            if($this->config['DB_TYPE']=='oracle') {
                $query = "SELECT * FROM
					( SELECT ".((count($this->dtColumns)>0)?@str_replace(" , ", " ", @implode(", ", $this->dtColumns)):"a.*").", ROWNUM rnum FROM
					( select * from ($this->sTable $this->sWhere)  $this->sOrder ) a
					WHERE ROWNUM <= ".($_GET['iDisplayStart']+$_GET['iDisplayLength'])." )
					WHERE rnum  >= ".($_GET['iDisplayStart']+1);
            }
            else if ($this->config['DB_TYPE']=='mysql'){
                $curLimit="limit ".$_GET['iDisplayStart'].",".($_GET['iDisplayLength']);
                $curOrder=$this->sOrder;


                //query baru lebih cepet
                $query = "SELECT  ".((count($this->dtColumns)>0)?@str_replace(" , ", " ", @implode(", ", $this->dtColumns)):"a.*")."
					FROM ( $this->sTable $this->sWhere $curOrder $curLimit) c   ";

                //yang lama lebih lambat
                /*$query = "SELECT ".((count($this->dtColumns)>0)?@str_replace(" , ", " ", @implode(", ", $this->dtColumns)):"a.*")."
                    FROM ( select * from ($this->sTable ) c $this->sWhere $this->sOrder) a
                    limit ".$_GET['iDisplayStart'].",".($_GET['iDisplayStart']+$_GET['iDisplayLength']);*/

            }else if($this->config['DB_TYPE']=='mssql'){

                $query = "SELECT * FROM
					(
					    SELECT ".((count($this->dtColumns)>0)?@str_replace(" , ", " ", @implode(", ", $this->dtColumns)):"a.*").", ROW_NUMBER() OVER (ORDER BY $this->dtIndex $sort)  as id FROM
					        ( select * from ($this->sTable  )t  $this->sWhere ) a
					) b
					WHERE id between ".($_GET['iDisplayStart']+1)." AND ".($_GET['iDisplayStart']+$_GET['iDisplayLength'])." ".$this->sOrder;

                // ASLI ==>> ( select * from ($this->sTable $this->sWhere )t   ) a

            }
        } else {
            $query = "SELECT ".((count($this->dtColumns)>0)?@str_replace(" , ", " ", @implode(", ", $this->dtColumns)):"a.*")."
					FROM ($this->sTable) a
					$this->sWhere
					$this->sOrder";
        }
        
        $resultFiltered = $this->ExecuteQuery($query);
        $this->activeQuery = $query;
		
	

        if($this->sWhere!=""){
            // ASLI ==>> $queryF = "SELECT COUNT(a.$this->dtIndex) FROM ($this->sTable $this->sWhere) a ";
            $queryF = "SELECT COUNT(a.$this->dtIndex) FROM ($this->sTable ) a $this->sWhere ";
            $resultF = $this->ExecuteQuery($queryF);
            $raDataF = $this->FetchArray($resultF);
            $this->filteredRecord = $raDataF[0];
            $this->totalRecord = $raDataF[0];
        }else{
            $query = "SELECT COUNT(a.$this->dtIndex) FROM ($this->sTable) a";
            $resultAll = $this->ExecuteQuery($query );
            $raData = $this->FetchArray($resultAll);
            $this->totalRecord = $raData[0];
            $this->filteredRecord = $this->totalRecord;
        }

	

        //echo $query;

        $this->output = array(
            "sEcho" => @intval($_GET['sEcho']),
            "iTotalRecords" => @intval($this->totalRecord),
            "iTotalDisplayRecords" => @intval($this->filteredRecord),
            "aaData" => array()
        );
	
        $counter =intval($_GET['iDisplayStart']);

        while($aRow = $this->FetchArray($resultFiltered)) {
            $row = array();
            $counter++;
            if($this->isCounterActive){
                $row[] = $counter;
            }
            for ( $i=0 ; $i<@count($this->dtColumns) ; $i++ ) {
               /* $search=$_GET['sSearch'];
                if($search!="")
                    $aRow[$this->dtColumns[$i]] = preg_replace("/$search/i", StrToDb($search,'upper'), $aRow[$this->dtColumns[$i]]);
				*/
				if($this->encryptColumn!="" && $i==$this->encryptColumn)
					$row[] = encryptStringArray(@DbToStr($aRow[$this->dtColumns[$i]],'x'));
				else		
					$row[] = @DbToStr($aRow[$this->dtColumns[$i]],'x');


            }
            $tData = ($this->isCounterActive==true)?(@count($this->dtColumns)+1):(@count($this->dtColumns));
            for($j=$tData ;$j<$this->totalColumn ;$j++){
                $row[] = "-";
            }
            $this->output ['aaData'][] = $row;
        }
        return json_encode($this->output);
    }

    function SetTotalColumn($ttl){
        $this->totalColumn = $ttl;
    }
	
	function SetEncryptColumn($i){
        $this->encryptColumn = $i;
    }

    function SetActiveCounter($val=false){
        $this->isCounterActive = $val;
    }

    function GetActiveQuery(){
        return $this->activeQuery;
    }
}
?>