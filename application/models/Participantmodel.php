<?php
class Participantmodel extends CI_Model {

    private $table_name = "participants";
	private $other_table = "loaformat";
    private $table_partisipant_quiz = "quizparticipant";
    private $primary_key = "";
    private $foreign_key = array(
            'USERS'     => 'USERID'
        );

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetParticipantDetail($participantId = 0){
        $sql = "SELECT * FROM participants
        LEFT JOIN (SELECT USERID, EMAIL, ACTIVESTATUS FROM users) AS users USING (USERID)
        LEFT JOIN (SELECT COUNTRYID AS NATIONALITYID,COUNTRYNAMEENG AS NATIONALITYNAME FROM country) c ON (participants.NATIONALITY = c.NATIONALITYID)
        LEFT JOIN country USING (COUNTRYID)        
LEFT JOIN (SELECT DESCRIPTION, PROGRAMDEGREEID, ENROLLMENTID FROM enrollment) AS enrollment ON (enrollment.ENROLLMENTID = participants.ENROLLMENTID)
				LEFT JOIN programdegree USING (PROGRAMDEGREEID)
				LEFT JOIN (SELECT PROGRAMID,PROGRAMNAME,STATUSSUBJECTSETTING  FROM program) AS program ON (program.PROGRAMID = programdegree.PROGRAMID)
                LEFT JOIN degree USING (DEGREEID)
        LEFT JOIN languagedeliverymapping USING (LANGUAGEDELIVERYMAPPINGID)
				LEFT JOIN studyprogramlanguage USING (LANGUAGEDELIVERYMAPPINGID)
				LEFT JOIN studyprogram on (participants.STUDYPROGRAMID = studyprogram.STUDYPROGRAMID)
                LEFT JOIN faculty on (studyprogram.FACULTYID = faculty.FACULTYID)
        WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, array($participantId));
        return $result->row_array();
    }
	
	function GetParticipantDetailNonAcademic($participantId = 0){
		$sql= "SELECT * FROM participants
        JOIN (SELECT USERID, EMAIL, ACTIVESTATUS FROM users) AS users USING (USERID)
        LEFT JOIN (SELECT COUNTRYID AS NATIONALITYID,COUNTRYNAMEENG AS NATIONALITYNAME FROM country) c ON (participants.NATIONALITY = c.NATIONALITYID)
        LEFT JOIN country USING (COUNTRYID)
        LEFT JOIN enrollment USING (ENROLLMENTID)
				LEFT JOIN programdegree USING (PROGRAMDEGREEID)
				LEFT JOIN (SELECT PROGRAMID,PROGRAMNAME FROM program) AS program ON (program.PROGRAMID = programdegree.PROGRAMID)
                LEFT JOIN degree USING (DEGREEID)
        LEFT JOIN languagedeliverymapping USING (LANGUAGEDELIVERYMAPPINGID)
				LEFT JOIN courselanguage USING (LANGUAGEDELIVERYMAPPINGID)
				LEFT JOIN course on (participants.COURSEID = course.COURSEID)
        WHERE PARTICIPANTID = ?";
		  $result = $this->db->query($sql, array($participantId));
        return $result->row_array();
	}
	
	function GetAcademicYear($participantId = 0){
        $sql = "SELECT * FROM participants
        WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, array($participantId));
        return $result->row_array();
    }
	
	function GetAcademicYearByUserId($userId = 0){
        $sql = "SELECT * FROM participants
        WHERE USERID = ?";
        $result = $this->db->query($sql, array($userId));
        return $result->row_array();
    }

    function GetParticipantById($participantId = 0)
    {
        $sql = "SELECT * FROM participants
        LEFT JOIN users USING (USERID)
        LEFT JOIN country USING (COUNTRYID)
        WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, array($participantId));
        return $result->row_array();
    }

     function insert($data)
    {
        return $this->db->insert($this->table_name, $data);
    }

    function update($user_id, $data)
    {
        $this->db->where($this->foreign_key['USERS'], $user_id);
        return $this->db->update($this->table_name, $data);
    }

    function exists($user_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE USERID = ?";
        $result = $this->db->query($sql, array($user_id));

        return $result->result_array();
    }

    function participantData($user_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE USERID = ?";
        $result = $this->db->query($sql, $user_id);
        return $result->row_array();
    }

    function getParticipantData($user_id = 0)
    {
        $sql = "SELECT a.*, b.COUNTRYNAMEENG, c.EMAIL FROM `participants` a join country b on a.COUNTRYID = b.COUNTRYID join users c on a.USERID = c.USERID WHERE a.USERID = ?";
        $result = $this->db->query($sql, $user_id);
        return $result->row_array();
    }

    function getPartisipantQuiz($user_id = 0)
    {
        $sql = "SELECT * FROM $this->table_partisipant_quiz WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, array($user_id));

        return $result->row_array();
    }

    function participantDatabyParticipantId($participantId = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, $participantId);
        return $result->row_array();
    }
	
	function GetLoaNumber($participantId=0){
		$sql = "SELECT convert(SUBSTRING(LOANUMBER, 1, 3),unsigned) AS LOANUMBER, ACTIVEYEAR FROM loaformat WHERE PARTICIPANTID = ? ORDER BY convert(SUBSTRING(LOANUMBER, 1, 3),unsigned) DESC LIMIT 1";//"SELECT MAX(LOANUMBER) as LOANUMBER, ACTIVEYEAR FROM loaformat WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, $participantId);
        return $result->row_array();
	}
	
	function insertLoa($data){
		return $this->db->insert($this->other_table, $data);
	}
	
	function getAllDataBySchoolYear($schoolyear){
		$sql = "SELECT P.ADMISSIONID, P.ENROLLMENTID, P.USERID, P.PROGRAMID, '-' AS NOMORPESERTA, P.FULLNAME, PHONE, EMAIL, BIRTHPLACE, BIRTHDATE, GENDER, ADDRESS, CITY, '-' AS PROPINSIRUMAH,
ZIPCODE, SCHOOLNAME, '-' AS KOTASMA, '-' AS PROPINSISMA, '32' AS IDJALUR, 'INTERNATIONAL' AS JALUR, P.STUDYPROGRAMID, STUDYPROGRAMNAME, INTERVIEWSCORE, ADMISSIONFEE, TUITIONFEE, '0' AS SDP2, '0' AS ASRAMA, '0' AS DISKONUP3, '0' AS DISKONBPP, '0' AS DISKONUP3, '0' AS DISKONSDP2, '0' AS DISKONASRAMA, '0' AS ASURANSI, '-' AS TYPE, PROGRAMNAME, PAYMENTDATE, '-' AS OLDSTUDENTID, '-' AS PILIHANKE, '' AS PASSWORD, SCHOOLYEAR, SCHOLARSHIPTYPEID
FROM participants P 
LEFT JOIN users U ON (P.USERID=U.USERID)
LEFT JOIN education E ON (P.PARTICIPANTID= E.PARTICIPANTID)
JOIN program PR ON (P.PROGRAMID=PR.PROGRAMID)
JOIN studyprogram SP ON (SP.STUDYPROGRAMID=P.STUDYPROGRAMID)
JOIN fee F ON (SUBSTR(STUDYPROGRAMNAME, 1,2)=F.DEGREE)
WHERE P.ACCEPTANCESTATUS='ACCEPTED' AND P.SCHOOLYEAR='".$schoolyear."'
";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getNumberParticipants($status, $academicyear, $period){
		//function getNumberParticipants($status, $academicyear){
		if($status==''){
			/*$sql= "SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants
                WHERE
                PROGRAMID != 0
AND ENROLLMENTID IS NOT NULL
AND STUDYPROGRAMID != 0
AND LANGUAGEDELIVERYMAPPINGID IS NOT NULL";*/
			$sql = "SELECT COUNT(p.PARTICIPANTID) AS JUMLAH
				FROM participants p
				JOIN enrollment e ON (p.ENROLLMENTID=e.ENROLLMENTID)
				WHERE p.PROGRAMID != 0
				AND p.ENROLLMENTID IS NOT NULL
				AND p.STUDYPROGRAMID != 0
				";//AND p.LANGUAGEDELIVERYMAPPINGID IS NOT NULL
		}else if($status=='scholarship'){
			/*$sql= "SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants
				WHERE PROGRAMID = 3";*/
			$sql = "SELECT COUNT(p.PARTICIPANTID) AS JUMLAH
				FROM participants p
				JOIN enrollment e ON (p.ENROLLMENTID=e.ENROLLMENTID)
				WHERE p.PROGRAMID = 3";
		}else if($status=='notscholarship'){
			$sql= "SELECT COUNT(p.PARTICIPANTID) AS JUMLAH
				FROM participants p
				JOIN enrollment e ON (p.ENROLLMENTID=e.ENROLLMENTID)
				WHERE p.PROGRAMID IN (1,2,4,5)";
		}else if($status=='acount'){
            /*$sql= "SELECT COUNT(PARTICIPANTID) AS JUMLAH FROM participants P JOIN users U ON (P.USERID=U.USERID)
				WHERE P.CURRENTSTEP = 1
				AND PROGRAMID= 0
				AND ENROLLMENTID IS NULL
				AND STUDYPROGRAMID = 0
				AND LANGUAGEDELIVERYMAPPINGID IS NULL";*/
			$sql= "SELECT COUNT(p.PARTICIPANTID) AS JUMLAH
				FROM participants p 
				JOIN users u ON (p.USERID=u.USERID)
				LEFT JOIN enrollment e ON (p.ENROLLMENTID=e.ENROLLMENTID)
				WHERE p.CURRENTSTEP = 1
				AND p.PROGRAMID= 0
				AND p.ENROLLMENTID IS NULL
				AND p.STUDYPROGRAMID = 0
				AND p.LANGUAGEDELIVERYMAPPINGID IS NULL";
        }else{
			/*$sql= "SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants
				WHERE ACCEPTANCESTATUS='ACCEPTED' AND PROGRAMID!=0";*/
			$sql="SELECT COUNT(p.PARTICIPANTID) AS JUMLAH
				FROM participants p
				JOIN enrollment e ON (p.ENROLLMENTID=e.ENROLLMENTID)
				WHERE p.ACCEPTANCESTATUS='ACCEPTED' AND p.PROGRAMID!=0";
		}
		/*if($academicyear!=''){
			$sql .= " WHERE ACADEMICYEAR = ".$academicyear;
		}else*/ 
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR = ".$academicyear;
			//$sql .= " AND (E.ACADEMICYEAR = ".$academicyear." OR P.ACADEMICYEAR = ".$academicyear.")";
		}else if($academicyear=="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " AND (".$period.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (e.PERIOD IS NULL)";
		}
		//echo "<pre>"; print($sql);//die;
		$result = $this->db->query($sql);
        return $result->result_array();		
	}
	
	function getStepParticipants($step,$academicyear,$period){
		if($step==7){
			$sql= "SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants 
                WHERE CURRENTSTEP=".$step." AND ACCEPTANCESTATUS=''";
		}else if($step==1){
            /*$sql ="SELECT COUNT(P.PARTICIPANTID) AS JUMLAH FROM participants P JOIN users U ON (P.USERID=U.USERID) JOIN quizparticipant q on (q.PARTICIPANTID=P.PARTICIPANTID)
WHERE P.CURRENTSTEP = 1
AND PROGRAMID= 0
AND ENROLLMENTID IS NULL
AND STUDYPROGRAMID = 0
AND LANGUAGEDELIVERYMAPPINGID IS NULL
AND ACTIVESTATUS ='Y'
AND q.STATUS = 1";*/
			$sql = "SELECT COUNT(P.PARTICIPANTID) AS JUMLAH 
FROM participants P 
JOIN users U ON (P.USERID=U.USERID) 
JOIN quizparticipant q on (q.PARTICIPANTID=P.PARTICIPANTID)
LEFT JOIN enrollment e ON (P.ENROLLMENTID=e.ENROLLMENTID)
WHERE P.CURRENTSTEP = 1
AND P.PROGRAMID= 0
AND P.ENROLLMENTID IS NULL
AND P.STUDYPROGRAMID = 0
AND P.LANGUAGEDELIVERYMAPPINGID IS NULL
AND U.ACTIVESTATUS ='Y'
AND q.STATUS = 1";
        }else{
		/*$sql= "SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants
		WHERE CURRENTSTEP = ".$step;*/
		$sql = "SELECT COUNT(P.PARTICIPANTID) AS JUMLAH
				FROM participants P
				LEFT JOIN enrollment e ON (P.ENROLLMENTID=e.ENROLLMENTID)
				WHERE P.CURRENTSTEP = ".$step;
		}
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " AND (".$period.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (e.PERIOD IS NULL)";
		}		
		$result = $this->db->query($sql);
        return $result->result_array();		
	}
	
	function getStepSeven($step,$academicyear,$period){
		/*$sql= "SELECT SUM(b.JUMLAH) AS JUMLAH FROM (SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants
                WHERE CURRENTSTEP=7 AND (ACCEPTANCESTATUS='' OR ACCEPTANCESTATUS IS NULL) AND INTERVIEWSTATUS!='' 
				AND INTERVIEWSCORE!=0 AND INTERVIEWDATE!='0000-00-00 00:00:00' AND PROGRAMTYPE='ACADEMIC'";*/
		$sql = "SELECT SUM(b.JUMLAH) AS JUMLAH FROM (SELECT COUNT(P.PARTICIPANTID) AS JUMLAH
				FROM participants P
				LEFT JOIN enrollment e ON (P.ENROLLMENTID=e.ENROLLMENTID)
				WHERE P.CURRENTSTEP=7 AND (P.ACCEPTANCESTATUS='' OR P.ACCEPTANCESTATUS IS NULL) AND P.INTERVIEWSTATUS!='' AND  
				P.INTERVIEWSCORE!=0 AND P.INTERVIEWDATE!='0000-00-00 00:00:00' AND P.PROGRAMTYPE='ACADEMIC'";		
		/*"SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants
                WHERE CURRENTSTEP=7 AND (ACCEPTANCESTATUS='' OR ACCEPTANCESTATUS IS NULL) AND INTERVIEWSTATUS!='' AND INTERVIEWSCORE!=0 AND INTERVIEWDATE!='0000-00-00 00:00:00'";
		*/
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " AND (".$period.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (e.PERIOD IS NULL)";
		}
		$sql .= "
		UNION
		SELECT COUNT(P.PARTICIPANTID) AS JUMLAH
        FROM participants P
        LEFT JOIN enrollment e ON (P.ENROLLMENTID=e.ENROLLMENTID)
        WHERE P.CURRENTSTEP=7 AND (P.ACCEPTANCESTATUS='' OR P.ACCEPTANCESTATUS IS NULL) AND 
        P.PROGRAMTYPE='NON_ACADEMIC' ";
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$sql .= " AND e.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " AND (".$period.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (e.PERIOD IS NULL)";
		}
		$sql .= ") as b";
		//echo "<pre>"; print_r($sql);//die;
		$result = $this->db->query($sql);
        return $result->result_array();		
	}
	
	function getParticipantStatus($status,$academicyear,$period){
		/*$sql= "SELECT COUNT(PARTICIPANTID) AS JUMLAH
				FROM participants
               ";*/
		$sql = "SELECT COUNT(P.PARTICIPANTID) AS JUMLAH
				FROM participants P
				LEFT JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)";
		$year = '';
		$per = '';
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$year .= " AND E.ACADEMICYEAR=".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$year .= " AND E.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$per .= " AND (".$period.") ";
		}else if($period=="UNDEFINED YEAR"){
			$per .= " AND (E.PERIOD IS NULL)";
		}
		if($status=='ACCEPTED'){
			//$sql .= " WHERE (PROGRAMTYPE='ACADEMIC' AND INTERVIEWSTATUS!='' AND INTERVIEWSCORE!=0 AND INTERVIEWDATE!='0000-00-00 00:00:00' AND ACCEPTANCESTATUS = 'ACCEPTED') OR (PROGRAMTYPE='NON_ACADEMIC' AND ACCEPTANCESTATUS = 'ACCEPTED')";
			$sql .= " WHERE (P.PROGRAMTYPE='ACADEMIC' AND P.INTERVIEWSTATUS!='' AND P.INTERVIEWSCORE!=0 AND P.INTERVIEWDATE!='0000-00-00 00:00:00' AND P.ACCEPTANCESTATUS = 'ACCEPTED' ".$year.$per.") 
			OR (P.PROGRAMTYPE='NON_ACADEMIC' AND P.ACCEPTANCESTATUS = 'ACCEPTED' ".$year.$per.")";
		}else if($status=='UNACCEPTED'){
			//$sql .= " WHERE (PROGRAMTYPE='ACADEMIC' AND INTERVIEWSTATUS!='' AND INTERVIEWSCORE!=0 AND INTERVIEWDATE!='0000-00-00 00:00:00' AND ACCEPTANCESTATUS = 'UNACCEPTED') OR (PROGRAMTYPE='NON_ACADEMIC' AND ACCEPTANCESTATUS = 'UNACCEPTED')";
			$sql .= " WHERE (P.PROGRAMTYPE='ACADEMIC' AND P.INTERVIEWSTATUS!='' AND P.INTERVIEWSCORE!=0 AND P.INTERVIEWDATE!='0000-00-00 00:00:00' AND P.ACCEPTANCESTATUS = 'UNACCEPTED' ".$year.$per.") OR (P.PROGRAMTYPE='NON_ACADEMIC' AND P.ACCEPTANCESTATUS = 'UNACCEPTED' ".$year.$per.")";
		}
		
		$result = $this->db->query($sql);
        return $result->result_array();			
	}
	
	function getConfirmDate($status,$academicyear,$period){
		/*$sql = "SELECT COUNT(PARTICIPANTID) AS JUMLAH
					FROM participants
					 WHERE ACCEPTANCESTATUS = 'ACCEPTED'
AND INTERVIEWSTATUS = 'Accept'
AND INTERVIEWSCORE !=0
AND INTERVIEWDATE != '0000-00-00 00:00:00'
";*/
		$sql = "SELECT COUNT(P.PARTICIPANTID) AS JUMLAH
FROM participants P
LEFT JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)
WHERE P.ACCEPTANCESTATUS = 'ACCEPTED'
AND P.INTERVIEWSTATUS = 'Accept'
AND P.INTERVIEWSCORE !=0
AND P.INTERVIEWDATE != '0000-00-00 00:00:00'";
		if($status=='accept'){
			$sql .= " AND P.LOAFILE IS NOT NULL";
		}else if($status=='unaccept'){
			$sql .= " AND P.LOAFILE IS NULL";
		}
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$sql .= " AND E.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$sql .= " AND E.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " AND (".$period.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (E.PERIOD IS NULL)";
		}
		$result = $this->db->query($sql);
        return $result->result_array();		
	}
	
	function modalParticipants($status,$academicyear,$period){
		$periods = explode('_',$period);
		$periodss ='';
		if(($periods!='' || $periods!=NULL && $periods!='x') && $periods!='UNDEFINED YEAR' && $periods!='x'){
			for($x=0;$x<count($periods);$x++){
			if(($x+1)==count($periods) && $periods[$x]!='x'){
				$periodss .= " enrollment.PERIOD = '".$periods[$x]."'";
			}else if($periods[$x]!='x'){
				$periodss .= " enrollment.PERIOD = '".$periods[$x]."' OR";
			}
		}
		}else if($periods=='UNDEFINED YEAR'){
			$periodss = 'UNDEFINED YEAR';
		}else if($periods=='x'){
			$periodss ='';
		}else{
			$periodss = $periods;
		}
		//echo "<pre>"; print_r($period);die;
		/*
		DISTINCT ADMISSIONID,users.EMAIL, PHONE,PARTICIPANTID,PROGRAMTYPE,participants.USERID, participants.FULLNAME,participants.ENROLLMENTID, participants.PROGRAMID,participants.USERID,CASE 
WHEN NATIONALITY IS NULL OR NATIONALITY=COUNTRYID THEN COUNTRYNAMEENG
ELSE NATIONALITY
END AS NATIONALITY,COUNTRYNAMEENG,DESCRIPTION,PERIOD,PASSPORTNO,GENDER,ACCEPTANCESTATUS,ENROLLMENTID,STUDYPROGRAMNAME,FACULTYNAMEENG
		*/
       $sql ="SELECT DISTINCT ADMISSIONID,users.EMAIL, PHONE,PARTICIPANTID,PROGRAMTYPE,participants.USERID, participants.FULLNAME,participants.ENROLLMENTID, participants.PROGRAMID,participants.USERID,CASE 
WHEN NATIONALITY IS NULL OR NATIONALITY=COUNTRYID THEN COUNTRYNAMEENG
ELSE NATIONALITY
END AS NATIONALITY,COUNTRYNAMEENG,DESCRIPTION,PERIOD,PASSPORTNO,GENDER,ACCEPTANCESTATUS,ENROLLMENTID,STUDYPROGRAMNAME,FACULTYNAMEENG from participants
                        LEFT JOIN country USING(COUNTRYID)
                         LEFT JOIN users USING (USERID)
                        LEFT JOIN quizparticipant USING (PARTICIPANTID)
                        LEFT JOIN (enrollment
                          LEFT JOIN (SELECT PROGRAMDEGREEID,PROGRAMID,DEGREEID FROM programdegree) AS programdegree
                          USING (PROGRAMDEGREEID)
                        )
                        USING (ENROLLMENTID)
                        LEFT JOIN (languagedeliverymapping
                          LEFT JOIN (studyprogramlanguage) USING (LANGUAGEDELIVERYMAPPINGID)
                          JOIN (SELECT STUDYPROGRAMID,STUDYPROGRAMNAME,FACULTYID FROM studyprogram) AS STUDYPROGRAM USING (STUDYPROGRAMID)
                          JOIN (SELECT FACULTYID,FACULTYNAMEENG FROM faculty) AS FACULTY USING (FACULTYID)
                        ) USING (LANGUAGEDELIVERYMAPPINGID)
                        LEFT JOIN (requirementvalue)
                          USING (PARTICIPANTID)
                          ";
		if($status=='scholarship'){
			$sql .=" WHERE participants.PROGRAMID=3";
		}else if($status=='notscholarship'){
			$sql .=" WHERE participants.PROGRAMID IN (1,2,4,5)";
		}else if($status==2||$status==3||$status==4||$status==5){
			$sql .=" WHERE participants.CURRENTSTEP=".$status;
		}else if($status==1){
            $sql .=" WHERE participants.CURRENTSTEP = 1
AND participants.PROGRAMID= 0
AND participants.ENROLLMENTID IS NULL
AND participants.STUDYPROGRAMID = 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NULL
AND users.ACTIVESTATUS ='Y'
AND quizparticipant.STATUS = 1";
        }else if($status=='have_interview'){
			$sql .=" where participants.INTERVIEWDATE != '0000-00-00 00:00:00'
and (participants.INTERVIEWSTATUS != '' or participants.INTERVIEWSCORE != 0)
and participants.CURRENTSTEP = 6
";
		}else if($status=='ACCEPTED' || $status=='UNACCEPTED'){
			$sql .=" WHERE (participants.PROGRAMTYPE='ACADEMIC' AND participants.ACCEPTANCESTATUS='".$status."' AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 AND participants.INTERVIEWDATE!='0000-00-00 00:00:00') OR (participants.PROGRAMTYPE='NON_ACADEMIC' AND participants.ACCEPTANCESTATUS='".$status."') ";
		}else if($status=='attend'){
			$sql .=" WHERE participants.ACCEPTANCESTATUS = 'ACCEPTED'
AND participants.INTERVIEWSTATUS = 'Accept'
AND participants.INTERVIEWSCORE !=0
AND participants.INTERVIEWDATE != '0000-00-00 00:00:00' AND participants.LOAFILE IS NOT NULL";
		}else if($status=='absent'){
			$sql .=" WHERE participants.ACCEPTANCESTATUS = 'ACCEPTED'
AND participants.INTERVIEWSTATUS = 'Accept'
AND participants.INTERVIEWSCORE !=0
AND participants.INTERVIEWDATE != '0000-00-00 00:00:00' AND participants.LOAFILE IS NULL";
		}else if($status==7){
			$sql .= "WHERE (participants.PROGRAMTYPE='ACADEMIC' AND participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' OR participants.ACCEPTANCESTATUS IS NULL)
AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 
AND participants.INTERVIEWDATE!='0000-00-00 00:00:00') OR 
(participants.PROGRAMTYPE='NON_ACADEMIC' AND participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' OR participants.ACCEPTANCESTATUS IS NULL)) ";
			/*" WHERE participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' 
OR participants.ACCEPTANCESTATUS IS NULL)
AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 
AND participants.INTERVIEWDATE!='0000-00-00 00:00:00' ";*/
		}else if($status=='not_interview'){
			$sql .= "where participants.INTERVIEWDATE != '0000-00-00 00:00:00'
and participants.CURRENTSTEP =6
and participants.INTERVIEWSCORE = 0 and participants.INTERVIEWSTATUS=''";
		}else if($status=='unscheduled_interview'){
			$sql .= " where participants.INTERVIEWDATE = '00-00-0000'
					and participants.CURRENTSTEP =6 and participants.INTERVIEWSTATUS='' AND participants.INTERVIEWSCORE = 0";
		}else if($status=='acount'){
            $sql .= " WHERE participants.CURRENTSTEP = 1
AND participants.PROGRAMID= 0
AND participants.ENROLLMENTID IS NULL
AND participants.STUDYPROGRAMID = 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NULL";
        }else if($status=='all'){
            $sql .= " WHERE participants.PROGRAMID!= 0
AND participants.ENROLLMENTID IS NOT NULL
AND participants.STUDYPROGRAMID != 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NOT NULL";
        }
		
		/*if($academicyear!='x'){
			$sql .= " WHERE participants.ACADEMICYEAR = ".$academicyear;
		}else */if($academicyear!='x' && $academicyear!='UNDEFINED YEAR'){
			$sql .= " AND enrollment.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=='UNDEFINED YEAR'){
			$sql .= " AND enrollment.ACADEMICYEAR IS NULL";
		}
		if($periodss!='' && $periodss!="UNDEFINED YEAR" && $periodss!='x'){
			$sql .= " AND (".$periodss.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (e.PERIOD IS NULL)";
		}
        $sql .= " ORDER BY participants.UPDATEDATE DESC";
		$result = $this->db->query($sql);
         return $result->result_array();		
	}
	
	function fetchAvailableSchoolYear(){
        $sql ="SELECT DISTINCT SCHOOLYEAR
				FROM participants
				WHERE SCHOOLYEAR <> ''";
		$result = $this->db->query($sql);
        return $result->result_array();	
    }
	
	function GetAdmissionId(){
		$sql = "SELECT convert(SUBSTRING(ADMISSIONID, 7, 4),unsigned) AS ADMISSIONID 
				FROM participants 
				ORDER BY convert(SUBSTRING(ADMISSIONID, 7, 4),unsigned) DESC LIMIT 1";
        $result = $this->db->query($sql);
        return $result->row_array();
	}
	
	function getInterview($academicyear,$status,$period){		
		if($status=='unscheduled_interview'){
			/*$sql = "select count(PARTICIPANTID) JUMLAH
					from participants 
					where participants.INTERVIEWDATE = '00-00-0000 00:00:00'
					and participants.CURRENTSTEP = 6 and participants.INTERVIEWSTATUS='' AND participants.INTERVIEWSCORE = 0";*/
			$sql = "select count(P.PARTICIPANTID) JUMLAH
					from participants P
					JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)
					where P.INTERVIEWDATE = '0000-00-00 00:00:00'
					and P.CURRENTSTEP = 6 
					and P.INTERVIEWSTATUS='' 
					AND P.INTERVIEWSCORE = 0 ";		
		}else if($status=='not_interview'){
			/*$sql = "select count(PARTICIPANTID) JUMLAH
					from participants p
					where INTERVIEWDATE != '0000-00-00 00:00:00'
					and CURRENTSTEP = 6
					and INTERVIEWSCORE = 0 and INTERVIEWSTATUS=''";*/
			$sql = "select count(P.PARTICIPANTID) JUMLAH
					from participants P
					JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)
					where P.INTERVIEWDATE != '0000-00-00 00:00:00'
					and P.CURRENTSTEP = 6
					and P.INTERVIEWSCORE = 0 
					and P.INTERVIEWSTATUS=''";
		}else{
			/*$sql = "select count(PARTICIPANTID) JUMLAH
					from participants p
					where INTERVIEWDATE != '0000-00-00 00:00:00'
					and (INTERVIEWSTATUS != '' or INTERVIEWSCORE != 0)
					and CURRENTSTEP=6
					";*/
			$sql = "select count(P.PARTICIPANTID) JUMLAH
					from participants P
					JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)
					where P.INTERVIEWDATE != '0000-00-00 00:00:00'
					and (P.INTERVIEWSTATUS != '' or P.INTERVIEWSCORE != 0)
					and P.CURRENTSTEP=6";
		}
		
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$sql .= " AND E.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$sql .= " AND E.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " AND (".$period.")";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (E.PERIOD IS NULL)";
		}
		$result = $this->db->query($sql);
        return $result->result_array();	
	}
	
	function getInterview2($academicyear,$status, $period){
		$year='';
		$per='';
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$year .= " AND E.ACADEMICYEAR=".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$year .= " AND E.ACADEMICYEAR IS NULL";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$per .= " AND (".$period.") ";
		}else if($period=="UNDEFINED YEAR"){
			$per .= " AND (E.PERIOD IS NULL)";
		}
		//if($academicyear!=''){
			/*$sql = "select count(PARTICIPANTID) blmdijadwalkan, '-' as blminterview, '-' as sudah
from participants p
where INTERVIEWDATE = '00-00-0000'
and CURRENTSTEP = 6 
and ACADEMICYEAR =".$academicyear."
UNION
select '-' as blmdijadwalkan, count(PARTICIPANTID) blminterview, '-' as sudah
from participants p
where INTERVIEWDATE != '0000-00-00 00:00:00'
and CURRENTSTEP = 6
and INTERVIEWSCORE = 0
and ACADEMICYEAR =".$academicyear."
UNION
select '-' as blmdijadwalkan, '-' as blminterview, count(PARTICIPANTID) sudah
from participants p
where INTERVIEWDATE != '0000-00-00 00:00:00'
and (INTERVIEWSTATUS != '' or INTERVIEWSCORE != 0)
and CURRENTSTEP =6
and ACADEMICYEAR =".$academicyear."";*/
			$sql = "select count(P.PARTICIPANTID) blmdijadwalkan, '-' as blminterview, '-' as sudah
					from participants P
					JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)
					where P.INTERVIEWDATE = '0000-00-00 00:00:00'
					AND P.INTERVIEWSTATUS=''
					and P.CURRENTSTEP = 6
					AND P.INTERVIEWSCORE=0 ".$year.$per."
					UNION
					select '-' as blmdijadwalkan, count(P.PARTICIPANTID) blminterview, '-' as sudah
					from participants P
					JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)
					where P.INTERVIEWDATE != '0000-00-00 00:00:00'
					and P.CURRENTSTEP = 6
					and P.INTERVIEWSCORE = 0
					AND P.INTERVIEWSTATUS='' ".$year.$per."
					UNION
					select '-' as blmdijadwalkan, '-' as blminterview, count(P.PARTICIPANTID) sudah
					from participants P
					JOIN enrollment E ON (P.ENROLLMENTID=E.ENROLLMENTID)
					where P.INTERVIEWDATE != '0000-00-00 00:00:00'
					and (P.INTERVIEWSTATUS != '' or P.INTERVIEWSCORE != 0)
					and P.CURRENTSTEP =6
					".$year.$per;
		/*}else{
			$sql = "select count(PARTICIPANTID) blmdijadwalkan, '-' as blminterview, '-' as sudah
from participants p
where INTERVIEWDATE = '00-00-0000'
and CURRENTSTEP = 6
UNION
select '-' as blmdijadwalkan, count(PARTICIPANTID) blminterview, '-' as sudah
from participants p
where INTERVIEWDATE != '0000-00-00 00:00:00'
and CURRENTSTEP = 6
and INTERVIEWSCORE = 0
UNION
select '-' as blmdijadwalkan, '-' as blminterview, count(PARTICIPANTID) sudah
from participants p
where INTERVIEWDATE != '0000-00-00 00:00:00'
and (INTERVIEWSTATUS != '' or INTERVIEWSCORE != 0)
and CURRENTSTEP = 6";
		}*/
		$result = $this->db->query($sql);
        return $result->result_array();	
	}

    function getNumberAcounts($academicyear){
        $sql ="SELECT COUNT(PARTICIPANTID) AS JUMLAH FROM participants P JOIN users U ON (P.USERID=U.USERID)
WHERE P.CURRENTSTEP = 1
AND PROGRAMID= 0
AND ENROLLMENTID IS NULL
AND STUDYPROGRAMID = 0
AND LANGUAGEDELIVERYMAPPINGID IS NULL";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getScholarship($table,$id){
		$sql = "SELECT * FROM $table WHERE SCHOLARSHIPTYPEID = ?";
        $result = $this->db->query($sql, $id);
        return $result->row_array();
	}
	
	function GetAllScholarship(){
		$sql = "SELECT * FROM scholarshiptype WHERE ACTIVESTATUS='Y'";
        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function getSubjectMapping($participantid){
		$sql = "SELECT s.SUBJECTCODE, s.SUBJECTNAME, s.CREDIT, s.STUDYPROGRAMNAME, s.SUBJECTDESCRIPTION, s.STUDYPROGRAMID, em.ENROLLMENTID, s.CURICULUMYEAR,
		(CASE WHEN s.STUDYPROGRAMNAME LIKE '%International%' THEN 'English' ELSE 'Indonesia' END) AS LANGUAGE
            FROM enrollexchangemapping em
            JOIN subjectforexchange s ON(s.SUBJECTCODE=em.SUBJECTCODE AND s.STUDYPROGRAMID=em.STUDYPROGRAMID)
            WHERE PARTICIPANTID=? 
            ORDER BY s.STUDYPROGRAMNAME ASC";
  //       $sql = "SELECT s.SUBJECTCODE, s.SUBJECTNAME, s.CREDIT, s.STUDYPROGRAMNAME, s.SUBJECTDESCRIPTION, s.STUDYPROGRAMID, em.ENROLLMENTID, s.CURICULUMYEAR,
		// (CASE WHEN s.STUDYPROGRAMNAME LIKE '%International%' THEN 'English' ELSE 'Indonesia' END) AS LANGUAGE
  //           FROM enrollexchangemapping em
  //           JOIN subjectforexchange s ON(s.SUBJECTCODE=em.SUBJECTCODE)
  //           WHERE PARTICIPANTID=? 
  //           ORDER BY s.STUDYPROGRAMNAME ASC";
        $result = $this->db->query($sql, $participantid);
        return $result->result_array();
	}

	function getSubjectMapping2($participantid){
		$sql = "SELECT s.SUBJECTNAME,s.STUDYPROGRAMNAME AS STUDYPROGRAMSUBJECTNAME, s.STUDYPROGRAMID, sp.IGRACIASSTUDYPROGRAMID, sp.STUDYPROGRAMNAME
            FROM enrollexchangemapping em
            JOIN subjectforexchange s ON(s.SUBJECTCODE=em.SUBJECTCODE AND s.STUDYPROGRAMID=em.STUDYPROGRAMID)
            JOIN studyprogram sp ON (em.STUDYPROGRAMID=sp.IGRACIASSTUDYPROGRAMID)
            WHERE PARTICIPANTID=?
            ORDER BY s.STUDYPROGRAMNAME ASC";
        // $sql = "SELECT s.SUBJECTNAME,s.STUDYPROGRAMNAME AS STUDYPROGRAMSUBJECTNAME, s.STUDYPROGRAMID, sp.IGRACIASSTUDYPROGRAMID, sp.STUDYPROGRAMNAME
        //     FROM enrollexchangemapping em
        //     JOIN subjectforexchange s ON(s.SUBJECTCODE=em.SUBJECTCODE)
        //     JOIN studyprogram sp ON (em.STUDYPROGRAMID=sp.IGRACIASSTUDYPROGRAMID)
        //     WHERE PARTICIPANTID=?
        //     ORDER BY s.STUDYPROGRAMNAME ASC";
        $result = $this->db->query($sql, $participantid);
        return $result->result_array();
	}

	function getSubjectExchange($enrollmentid){
		$sql = "SELECT s.SUBJECTNAME, s.CREDIT, s.STUDYPROGRAMNAME, s.SUBJECTDESCRIPTION, s.SUBJECTCODE,
		(CASE WHEN s.STUDYPROGRAMNAME LIKE '%International%' THEN 'English' ELSE 'Indonesia' END) AS LANGUAGE, s.STUDYPROGRAMID, e.ENROLLMENTID, s.CURICULUMYEAR
            FROM enrollexchange e
            JOIN subjectforexchange s ON(s.SUBJECTCODE=e.SUBJECTCODE AND s.STUDYPROGRAMID=e.STUDYPROGRAMID)
            WHERE e.ENROLLMENTID=?
           ORDER BY s.STUDYPROGRAMID ASC";
  //       $sql = "SELECT s.SUBJECTNAME, s.CREDIT, s.STUDYPROGRAMNAME, s.SUBJECTDESCRIPTION, s.SUBJECTCODE,
		// (CASE WHEN s.STUDYPROGRAMNAME LIKE '%International%' THEN 'English' ELSE 'Indonesia' END) AS LANGUAGE, s.STUDYPROGRAMID, e.ENROLLMENTID, s.CURICULUMYEAR
  //           FROM enrollexchange e
  //           JOIN subjectforexchange s ON(e.SUBJECTCODE=s.SUBJECTCODE)
  //           WHERE e.ENROLLMENTID=?
  //          ORDER BY s.STUDYPROGRAMID ASC";
        $result = $this->db->query($sql, $enrollmentid);
        return $result->result_array();
	}

	function getStudySubject($id){
		$sql = "SELECT DISTINCT em.STUDYPROGRAMID AS STUDYPROGRAMSUBJECTID, s.IGRACIASSTUDYPROGRAMID, s.STUDYPROGRAMID, s.STUDYPROGRAMNAME
			FROM enrollexchangemapping em 
			JOIN studyprogram s ON (em.STUDYPROGRAMID=s.IGRACIASSTUDYPROGRAMID)
			WHERE em.PARTICIPANTID=?";
		//echo $sql;die;
		$result = $this->db->query($sql, $id);
        return $result->result_array();
	}

	function getStudySubjectBy($participantid){
		$sql = "SELECT DISTINCT sf.SUBJECTCODE, sf.SUBJECTNAME, sf.CREDIT, s.STUDYPROGRAMNAME, f.FACULTYNAMEENG, CONCAT(sf.ADMITSEMESTER,' (',(CASE WHEN MOD(ADMITSEMESTER,2)=1 THEN 'Odd' ELSE 'Even' END),')') AS SEMESTER
			FROM enrollexchangemapping em 
            JOIN subjectforexchange sf ON (sf.SUBJECTCODE=em.SUBJECTCODE AND sf.STUDYPROGRAMID=em.STUDYPROGRAMID)
            JOIN studyprogram s ON (em.STUDYPROGRAMID=s.IGRACIASSTUDYPROGRAMID)
            JOIN faculty f ON (s.FACULTYID=f.FACULTYID)
			WHERE em.PARTICIPANTID=".$participantid;
		// $sql = "SELECT DISTINCT sf.SUBJECTCODE, sf.SUBJECTNAME, sf.CREDIT, s.STUDYPROGRAMNAME, f.FACULTYNAMEENG, CONCAT(sf.ADMITSEMESTER,' (',(CASE WHEN MOD(ADMITSEMESTER,2)=1 THEN 'Odd' ELSE 'Even' END),')') AS SEMESTER
		// 	FROM enrollexchangemapping em 
  //           JOIN subjectforexchange sf ON (em.SUBJECTCODE=sf.SUBJECTCODE)
  //           JOIN studyprogram s ON (em.STUDYPROGRAMID=s.IGRACIASSTUDYPROGRAMID)
  //           JOIN faculty f ON (s.FACULTYID=f.FACULTYID)
		// 	WHERE em.PARTICIPANTID=".$participantid;
		$result = $this->db->query($sql);
        return $result->result_array();
	}

	function getSubjectAccepted($participantid){
		$sql = "SELECT s.SUBJECTNAME, s.CREDIT, sp.STUDYPROGRAMNAME,
		(CASE WHEN sp.STUDYPROGRAMNAME LIKE '%International%' THEN 'English' ELSE 'Indonesia' END) AS LANGUAGE
            FROM enrollexchangemapping em
            JOIN subjectforexchange s ON(s.SUBJECTCODE=em.SUBJECTCODE AND s.STUDYPROGRAMID=em.STUDYPROGRAMID)
            JOIN studyprogram sp ON (em.STUDYPROGRAMID=sp.IGRACIASSTUDYPROGRAMID)
            WHERE PARTICIPANTID=".$participantid;
  //       $sql = "SELECT s.SUBJECTNAME, s.CREDIT, sp.STUDYPROGRAMNAME,
		// (CASE WHEN sp.STUDYPROGRAMNAME LIKE '%International%' THEN 'English' ELSE 'Indonesia' END) AS LANGUAGE
  //           FROM enrollexchangemapping em
  //           JOIN subjectforexchange s ON(s.SUBJECTCODE=em.SUBJECTCODE)
  //           JOIN studyprogram sp ON (em.STUDYPROGRAMID=sp.IGRACIASSTUDYPROGRAMID)
  //           WHERE PARTICIPANTID=".$participantid;
        $result = $this->db->query($sql);
        return $result->result_array();
	}
	
}
?>
