<?php 
class ProgramModel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetProgramDegreeById($programId = 0)
    {
        $this->db->select('*');
        $this->db->from('programdegree');
        $this->db->join('degree','degree.DEGREEID = programdegree.DEGREEID');
        $this->db->join('program','program.PROGRAMID = programdegree.PROGRAMID');
        $this->db->where('programdegree.PROGRAMID',$programId);
        return $this->db->get();
    }

    function getProgramById($id=0){
        $this->db->select('*');
        $this->db->from('program');
        $this->db->where('PROGRAMID',$id);
        return $this->db->get();
    }

    function getAllData($type = null){
        $where = !empty($type) ? " AND PROGRAMTYPE = '$type'" : '';
        $sql = "SELECT * FROM program WHERE PROGRAMNAME!='CREDIT EARNING' $where ";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getAllData2($type = null){
        $where = !empty($type) ? " WHERE PROGRAMTYPE = '$type'" : '';
        $sql = "SELECT * FROM program $where ";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
}
?>