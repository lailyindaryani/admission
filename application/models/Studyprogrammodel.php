<?php 
class StudyProgramModel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetStudyProgramById($id = 0)
    {
        $sql = "SELECT * FROM studyprogram WHERE STUDYPROGRAMID = ?";
        $result = $this->db->query($sql, array($id));
        return $result->row_array();
    }


     function GetStudy($languagedeliveryid, $programdegreeId)
    {
        $sql ="SELECT * From
              (SELECT* From studyprogramlanguage 
                Where  languagedeliverymappingid
                IN (SELECT  languagedeliverymappingid
                From  languagedeliverymapping
                Where LANGUAGEDELIVERYID = ? 
                AND PROGRAMDEGREEID = ? )) as data join studyprogram using(STUDYPROGRAMID)";
                // echo $sql;

        $result = $this->db->query($sql, array($languagedeliveryid, $programdegreeId));
        return $result->result_array();
    }

    function GetAllData()
    {
        $sql = "SELECT * FROM studyprogram";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getAssignedData($programid, $degreeid, $languageid){
        $sql = "SELECT * FROM studyprogram
                WHERE STUDYPROGRAMID IN (
                  SELECT a.STUDYPROGRAMID
                  FROM studyprogramlanguage a, languagedeliverymapping b, programdegree c 
                  WHERE a.LANGUAGEDELIVERYMAPPINGID = b.LANGUAGEDELIVERYMAPPINGID
                  AND b.LANGUAGEDELIVERYID = ?
                  AND b.PROGRAMDEGREEID = c.PROGRAMDEGREEID
                  AND c.DEGREEID = ?
                  AND c.PROGRAMID = ?
                )";

        $result = $this->db->query($sql, array($languageid,$degreeid,$programid));
        return $result->result_array();
    }
	
	function getStudyProgram($academicyear, $period){
		$sql = "SELECT
	C.STUDYPROGRAMID, 
	C.STUDYPROGRAMNAME,
	COUNT(B.STUDYPROGRAMID) AS JUMLAH
FROM studyprogram C
	LEFT JOIN (SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME AS JUMLAH
				FROM studyprogram A
				JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
				JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
				WHERE B.ACCEPTANCESTATUS = 'ACCEPTED'";
		if($academicyear!='' && $academicyear!="UNDEFINED YEAR"){
			$sql .= " AND B.ACADEMICYEAR = ".$academicyear;
		}else if($academicyear=="UNDEFINED YEAR"){
			$sql .= " AND B.ACADEMICYEAR IS NULL ";
		}
		if($period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " AND (".$period.") ";
		}else if($period=="UNDEFINED YEAR"){
			$sql .= " AND (E.PERIOD IS NULL)";
		}
		$sql .= ")B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
			GROUP BY C.STUDYPROGRAMID, C.STUDYPROGRAMNAME";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getStudyProgramByEnrollment($programid,$academicyear){
        $sql = "SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME, COUNT(B.STUDYPROGRAMID) AS JUMLAH
				FROM studyprogram A
				LEFT JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
				WHERE B.ACCEPTANCESTATUS = 'ACCEPTED' AND B.PROGRAMID=$programid
				";
		if($academicyear!='x'){
			$sql .= " AND B.ACADEMICYEAR=".$academicyear."
					   GROUP BY A.STUDYPROGRAMID, A.STUDYPROGRAMNAME";
		}else{
			$sql .= " GROUP BY A.STUDYPROGRAMID, A.STUDYPROGRAMNAME";
		}
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getStudyProgramScholarship($scholarshiptypeid,$academicyear){
		$sql = "SELECT A.SCHOLARSHIPTYPEID AS STUDYPROGRAMID, A.SCHOLARSHIPTYPENAME AS STUDYPROGRAMNAME, COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN participants B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
				WHERE B.ACCEPTANCESTATUS = 'ACCEPTED' AND B.SCHOLARSHIPTYPEID=$scholarshiptypeid
				";
		if($academicyear!='x'){
			$sql .= " AND B.ACADEMICYEAR=".$academicyear."
					   GROUP BY A.SCHOLARSHIPTYPEID, A.SCHOLARSHIPTYPENAME";
		}else{
			$sql .= " GROUP BY A.SCHOLARSHIPTYPEID, A.SCHOLARSHIPTYPENAME";
		}
        $result = $this->db->query($sql);
        return $result->result_array();
	}

}
?>