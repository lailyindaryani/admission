<?php 
class CourseModel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetCourseById($id = 0)
    {
        $sql = "SELECT * FROM course WHERE COURSE = ?";
        $result = $this->db->query($sql, array($id));
        return $result->row_array();
    }

    function GetAllData()
    {
        $sql = "SELECT * FROM course";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getAssignedData($programid, $degreeid, $languageid){
        $sql = "SELECT * FROM course
                WHERE COURSE IN (
                  SELECT a.COURSE
                  FROM courselanguage a, languagedeliverymapping b, programdegree c 
                  WHERE a.LANGUAGEDELIVERYMAPPINGID = b.LANGUAGEDELIVERYMAPPINGID
                  AND b.LANGUAGEDELIVERYID = ?
                  AND b.PROGRAMDEGREEID = c.PROGRAMDEGREEID
                  AND c.DEGREEID = ?
                  AND c.PROGRAMID = ?
                )";

        $result = $this->db->query($sql, array($languageid,$degreeid,$programid));
        return $result->result_array();
    }
	
	function getCourse($languagedeliveryid, $programdegreeId){
        $sql = "SELECT *
				FROM (
					SELECT *
					FROM courselanguage
					WHERE languagedeliverymappingid
				IN (
					SELECT languagedeliverymappingid
					FROM languagedeliverymapping
					WHERE LANGUAGEDELIVERYID = ?
					AND PROGRAMDEGREEID = ?
				)
				) AS
					DATA JOIN course
					USING ( COURSEID) ";

        $result = $this->db->query($sql, array($languagedeliveryid, $programdegreeId));
        return $result->result_array();
    }

}
?>