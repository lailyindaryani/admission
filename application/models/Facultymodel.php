<?php 
class FacultyModel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetFacultyById($facultyId = 0)
    {
        $this->db->select('*');
        $this->db->from('faculty');
        $this->db->where('FACULTYID',$facultyId);
        return $this->db->get();
    }

    function getAllData(){
        $sql = "SELECT * FROM faculty";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
}
?>