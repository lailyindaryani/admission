<?php 
class ProgramDegreeModel extends CI_Model {

    private $table_name = "programdegree";
    private $primary_key = "";

    function GetProgramDegreeById($programId = 0)
    {
        $sql = "SELECT a.*, b.* FROM $this->table_name a JOIN degree b ON (a.degreeid = b.degreeid) WHERE a.programid = ? ";
        $result = $this->db->query($sql, array($programId));
        return $result->result_array();
    }

    //ini yang atas takut dipake iam
    function GetProgramDegreeById2($programDegreeId = 0)
    {
    	$sql = "SELECT a.*, b.* FROM $this->table_name a JOIN degree b ON (a.degreeid = b.degreeid) WHERE a.PROGRAMDEGREEID = ? ";
        $result = $this->db->query($sql, array($programDegreeId));
        return $result->row_array();	
    }

    function GetProgramDegreeByEnrollmentId($enrollmentId = 0)
    {
        $sql = "SELECT a.*, b.* FROM $this->table_name a JOIN degree b ON (a.degreeid = b.degreeid) JOIN enrollment c ON (c.programdegreeid = a.programdegreeid)  WHERE c.ENROLLMENTID = ? ";
        $result = $this->db->query($sql, array($enrollmentId));
        return $result->row_array();    
    }

    function editProgramDegree($programdegreeid, $data){
        $this->db->set($data);
        $this->db->where('PROGRAMDEGREEID',$programdegreeid);
        $this->db->insert($table_name);
    }

}
?>