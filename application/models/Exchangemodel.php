<?php 
class ExchangeModel extends CI_Model {   
    private $table_name = "subjectforexchange";
    
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    function getAllData(){
        $sql = "SELECT * FROM subjectforexchange";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getFilter($filter, $order){
        $sql = "SELECT DISTINCT ".$filter." FROM subjectforexchange ORDER BY ".$order." DESC";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getEnrollment(){
        $sql = "SELECT DISTINCT e.DESCRIPTION, e.ENROLLMENTID, d.DEGREE
            FROM enrollment e
            JOIN programdegree pd ON (e.PROGRAMDEGREEID=pd.PROGRAMDEGREEID)
            JOIN program p ON (pd.PROGRAMID=p.PROGRAMID)
            JOIN degree d ON (pd.DEGREEID=d.DEGREEID)
            WHERE p.STATUSSUBJECTSETTING='Y'
            ORDER BY e.ENROLLMENTID DESC";//e.ENROLLMENTSTATUS='OPEN' AND 
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getJumlahSubjectMapping($studyprogramid){
        $sql = "SELECT COUNT(*) AS JUMLAH
                FROM subjectforexchange s
                JOIN enrollexchange e ON (s.SUBJECTCODE=e.SUBJECTCODE AND s.STUDYPROGRAMID=e.STUDYPROGRAMID)
                WHERE s.STUDYPROGRAMID=?";
        // $sql = "SELECT COUNT(*) AS JUMLAH
        //         FROM subjectforexchange s
        //         JOIN enrollexchange e ON (s.SUBJECTCODE=e.SUBJECTCODE)
        //         WHERE s.STUDYPROGRAMID=?";//s.SUBJECTCODE=em.SUBJECTCODE AND s.STUDYPROGRAMID=em.STUDYPROGRAMID
        $result = $this->db->query($sql, array($studyprogramid));
        return $result->result_array();
    }

    function getPopulateSubject($studyprogramid, $degree, $curriculum, $semester){
        $sql = "SELECT SUBJECTID, SUBJECTCODE, STUDYPROGRAMNAME, CREDIT, CURICULUMYEAR, SUBJECTDESCRIPTION, SUBJECTNAME, CURICULUMYEAR, STUDYPROGRAMID,(CASE WHEN MOD(ADMITSEMESTER,2)=1 THEN 'ganjil' ELSE 'genap' END) AS SEMESTER
                FROM subjectforexchange 
                WHERE CURICULUMYEAR=".$curriculum;
        if($studyprogramid!=''){
            $sql .= "  AND STUDYPROGRAMID=".$studyprogramid;
        }
        // if($degree!=''){
        //     $sql .= "  AND DEGREE='".$degree."'";
        // }
         if($degree){
            if($degree=='S1'){
                $sql .= '  AND (DEGREE="'.$degree.'" OR DEGREE="D4")';
            }else{
                $sql .= ' AND DEGREE="'.$degree.'"';
            }            
        }
        if($semester!=''){
            $sql .= "  HAVING SEMESTER='".$semester."'";
        }
        //echo $sql;die;
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getAllDataX(){
        $sql = "SELECT SUBJECTCODE, STUDYPROGRAMNAME, CREDIT, CURICULUMYEAR, SUBJECTDESCRIPTION, SUBJECTNAME, CURICULUMYEAR, 'Y' as STATUS, DEGREE
                FROM subjectforexchange";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getExistProdi(){
        $sql = "SELECT DISTINCT STUDYPROGRAMID
                FROM enrollexchange";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getSubjectMapping($enrollmentid, $studyprogramid, $degree, $curriculum, $semester){
        $sql = "SELECT s.SUBJECTCODE, s.STUDYPROGRAMNAME, s.CREDIT, s.CURICULUMYEAR, s.SUBJECTDESCRIPTION, s.SUBJECTNAME, s.CURICULUMYEAR, s.STUDYPROGRAMID, (CASE WHEN MOD(s.ADMITSEMESTER,2)=1 THEN 'ganjil' ELSE 'genap' END) AS SEMESTER
                FROM subjectforexchange s
                JOIN enrollexchange e ON (s.SUBJECTCODE=e.SUBJECTCODE AND s.STUDYPROGRAMID=e.STUDYPROGRAMID)
                WHERE s.CURICULUMYEAR=".$curriculum;
                // $sql = "SELECT s.SUBJECTCODE, s.STUDYPROGRAMNAME, s.CREDIT, s.CURICULUMYEAR, s.SUBJECTDESCRIPTION, s.SUBJECTNAME, s.CURICULUMYEAR, s.STUDYPROGRAMID, (CASE WHEN MOD(s.ADMITSEMESTER,2)=1 THEN 'ganjil' ELSE 'genap' END) AS SEMESTER
                // FROM subjectforexchange s
                // JOIN enrollexchange e ON (s.SUBJECTCODE=e.SUBJECTCODE)
                // WHERE s.CURICULUMYEAR=".$curriculum;
        // if($enrollmentid!='' || $degree!='' || $studyprogramid!='' || $curriculum!=''){
        //     $sql .= ' ';
        // }
        if($enrollmentid!=''){
            $sql .= ' AND e.ENROLLMENTID='.$enrollmentid;
        }
        if($studyprogramid!=''){
            $sql .= ' AND s.STUDYPROGRAMID='.$studyprogramid;
        }
        if($degree){
            if($degree=='S1'){
                $sql .= '  AND (s.DEGREE="'.$degree.'" OR s.DEGREE="D4")';
            }else{
                $sql .= ' AND s.DEGREE="'.$degree.'"';
            }            
        }
        if($semester){
            $sql .= " HAVING SEMESTER='".$semester."'";
        }
        //echo $sql;die;
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getSubjectMapping2($enrollmentid, $studyprogramid, $degree, $curriculum, $semester){
        $sql = "SELECT s.SUBJECTCODE, s.STUDYPROGRAMNAME, s.CREDIT, s.CURICULUMYEAR, s.SUBJECTDESCRIPTION, s.SUBJECTNAME, s.CURICULUMYEAR, s.STUDYPROGRAMID, (CASE WHEN MOD(s.ADMITSEMESTER,2)=1 THEN 'ganjil' ELSE 'genap' END) AS SEMESTER
                FROM subjectforexchange s
                JOIN enrollexchange e ON (s.SUBJECTCODE=e.SUBJECTCODE AND s.STUDYPROGRAMID=e.STUDYPROGRAMID)
                WHERE s.CURICULUMYEAR=".$curriculum;
        if($enrollmentid!=''){
            $sql .= ' AND e.ENROLLMENTID='.$enrollmentid;
        }
        if($studyprogramid!=''){
            $sql .= ' AND s.STUDYPROGRAMID='.$studyprogramid;
        }
        if($degree){
            $sql .= ' AND s.DEGREE="'.$degree.'"';  
        }
        if($semester){
            $sql .= " HAVING SEMESTER='".$semester."'";
        }
        //echo $sql;die;
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function insert($table, $data){
        return $this->db->insert($table, $data);
    }

    function deleteEnrollexchange($where1, $where2, $where3){
        $this->db->where('ENROLLMENTID', $where1);
        $this->db->where('STUDYPROGRAMID', $where2);
        $this->db->where('CURICULUMYEAR', $where3);
        return $this->db->delete('enrollexchange');
    }

    function deleteEnrollexchange2($where1, $where2){
        $this->db->where('ENROLLMENTID', $where1);
        $this->db->where('CURICULUMYEAR', $where2);
        return $this->db->delete('enrollexchange');
    }

    function getEnrollmentExist($id, $studyprogramid, $curriculum){
        $this->db->where('ENROLLMENTID',$id);
        $this->db->where('STUDYPROGRAMID', $studyprogramid);
        $this->db->where('CURICULUMYEAR', $curriculum);
        return $this->db->get('enrollexchange')->result_array();
    }

    function getEnrollmentExist2($id, $curriculum){
        $this->db->where('ENROLLMENTID',$id);
        $this->db->where('CURICULUMYEAR', $curriculum);
        return $this->db->get('enrollexchange')->result_array();
    }

    function getStudyProgram($where){
        $sql = "SELECT DISTINCT STUDYPROGRAMID, STUDYPROGRAMNAME
                FROM subjectforexchange";
        if($where=='S1'){
            $sql .= " WHERE DEGREE='".$where."' OR DEGREE='D4'";
        }
       // print($sql);
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function deleteEnrollMapping($id){
        $this->db->where('PARTICIPANTID', $id);
        return $this->db->delete('enrollexchangemapping');
    }

    function getCountSubMapping($id){
        $sql = "SELECT SUM(s.CREDIT) AS COUNTCREDIT
            FROM enrollexchangemapping em
            JOIN subjectforexchange s ON (s.SUBJECTCODE=em.SUBJECTCODE AND s.STUDYPROGRAMID=em.STUDYPROGRAMID)
            WHERE em.PARTICIPANTID=?";
            // $sql = "SELECT SUM(s.CREDIT) AS COUNTCREDIT
            // FROM enrollexchangemapping em
            // JOIN subjectforexchange s ON (em.SUBJECTCODE=s.SUBJECTCODE)
            // WHERE em.PARTICIPANTID=?";
        $result = $this->db->query($sql, array($id));
        return $result->result_array();
    }

    function getCountSubMapping2($id){
        $sql = "SELECT SUM(s.CREDIT) AS COUNTCREDIT
            FROM enrollexchangemapping em
            JOIN subjectforexchange s ON (s.SUBJECTCODE=em.SUBJECTCODE AND s.STUDYPROGRAMID=em.STUDYPROGRAMID)
            JOIN studyprogram sp ON (em.STUDYPROGRAMID=sp.IGRACIASSTUDYPROGRAMID)
            WHERE em.PARTICIPANTID=?";
        // $sql = "SELECT SUM(s.CREDIT) AS COUNTCREDIT
        //     FROM enrollexchangemapping em
        //     JOIN subjectforexchange s ON (em.SUBJECTCODE=s.SUBJECTCODE)
        //     JOIN studyprogram sp ON (em.STUDYPROGRAMID=sp.IGRACIASSTUDYPROGRAMID)
        //     WHERE em.PARTICIPANTID=?";
        $result = $this->db->query($sql, array($id));
        return $result->result_array();
    }

    function deleteAllSubject(){
        return $this->db->truncate('subjectforexchange'); 
    }
}
?>