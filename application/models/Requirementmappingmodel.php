<?php
class RequirementMappingmodel extends CI_Model {

     private $table_name = "requirementmapping";
     private $primary_key = "REQUIREMENTID";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function getAllRequirementParticipant($program_degree_id = 0)
    {
        $sql = "SELECT a.*, b.* FROM $this->table_name a JOIN requirements b ON (a.REQUIREMENTID = b.REQUIREMENTID) WHERE a.PROGRAMDEGREEID = ? AND b.PRIVILEGE = '1'";
        $result = $this->db->query($sql, array($program_degree_id));
        return $result->result_array();
    }


}
?>