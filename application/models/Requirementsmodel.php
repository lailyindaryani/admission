<?php
class Requirementsmodel extends CI_Model {

     private $table_name = "requirements";
     private $primary_key = "REQUIREMENTID";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function getAllData(){
        $sql = "SELECT * FROM requirements";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

//    function GetParticipantById($participantId = 0)
//    {
//        $sql = "SELECT * FROM participants
//        LEFT JOIN users USING (USERID)
//        LEFT JOIN country USING (COUNTRYID)
//        WHERE PARTICIPANTID = ?";
//        $result = $this->db->query($sql, array($participantId));
//        return $result->row_array();
//    }
	
	function getSisaData($programdegreeid){
		$sql = "select * from requirements where requirementid not in (SELECT requirementid FROM requirements
JOIN (requirementmapping) USING (REQUIREMENTID)
JOIN (SELECT PROGRAMDEGREEID, PROGRAMID, DEGREEID FROM programdegree) AS programdegree USING (PROGRAMDEGREEID)
WHERE PROGRAMDEGREEID = ".$programdegreeid.")";
        $result = $this->db->query($sql);
        return $result->result_array();
	}
	
    function insert($data)
     {
         return $this->db->insert($this->table_name, $data);
     }
 
     function update($participant_id = 0, $data = array())
     {
         $this->db->where($this->primary_key, $participant_id);
         return $this->db->update($this->table_name, $data);
     }
 
     function GetModel()
     {
         $sql = "SELECT * FROM $this->table_name";
         $result = $this->db->query($sql);
 
         return $result->result_array();
     }

}
?>