<?php 
class CouponMappingModel extends CI_Model {

    private $table_name = "couponmapping";
    private $primary_key = "COUPONMAPPINGID";
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetCouponById($id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE $this->primary_key = ?";
        $result = $this->db->query($sql, array($id));
        return $result->row_array();
    }

    function GetAllCountryByCoupon($id = 0)
    {
        $sql = "SELECT * FROM $this->table_name
            LEFT JOIN country USING (COUNTRYID)
            WHERE COUPONID = ?";
        $result = $this->db->query($sql, array($id));
        return $result->result_array();
    }

}
?>