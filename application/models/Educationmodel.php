<?php 
class Educationmodel extends CI_Model {

    private $table_name = "education";
    private $primary_key = "EDUCATIONID";
    private $tes = "";

    //level 1 = highschool
    //level 2 = university
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function getEducationOfUser($id = 0)
    {
        $this->db->select('*');
        $this->db->where('PARTICIPANTID', $id);
        return $this->db->get('education')->result_array();
    }

    function insert($data)
    {
        return $this->db->insert($this->table_name, $data);
    }

    function update($education_id = 0, $data = array())
    {
        $this->db->where($this->primary_key, $education_id);
        return $this->db->update($this->table_name, $data);
    }

    function exists($education_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE EDUCATIONID = ?";
        $result = $this->db->query($sql, array($education_id));
        return $result->result_array();
    }

    function getEducation($participantid = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, array($participantid));
        return $result->result_array();
    }

    function educationData($participant_id = 0, $level = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE PARTICIPANTID = ? AND EDUCATIONLEVEL = ? ";
        $result = $this->db->query($sql, array($participant_id,$level));

        return $result->row_array();
    }

    function getEducationDetail($id){

    }

    function highschoolData($participant_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE PARTICIPANTID = ? AND EDUCATIONLEVEL='HIGHSCHOOL'";
        $result = $this->db->query($sql, array($participant_id));

        return $result->row_array();
    }

    function universityData($participant_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE PARTICIPANTID = ? AND EDUCATIONLEVEL='UNDERGRADUATE'";
        $result = $this->db->query($sql, array($participant_id));

        return $result->row_array();
    }
    public function delete($participantid,$where)
    {
        $this->db->delete($where,$participantid,'education');
    }
}
?>