<?php 
class LanguageDeliveryMappingModel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetLanguageDeliveryMappingById($languageid = 0, $programdegreeid = 0)
    {
        $sql = "SELECT * FROM languagedeliverymapping WHERE LANGUAGEDELIVERYID = ? AND PROGRAMDEGREEID = ?";
        $result = $this->db->query($sql, array($languageid, $programdegreeid));
        return $result->row_array();
    }
	
	function GetLanguageDeliveryIdById($id = 0)
    {
        $sql = "SELECT * FROM languagedeliverymapping WHERE LANGUAGEDELIVERYMAPPINGID = ?";
        $result = $this->db->query($sql, array($id));
        return $result->row_array();
    }


}
?>