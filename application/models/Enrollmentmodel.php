<?php 
class EnrollmentModel extends CI_Model {

    private $table_name = "enrollment";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function getAllData(){
        $sql = "SELECT * FROM enrollment";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function fetchAvailableAcademicYear($enrollmentid=null){
        $this->db->distinct();
        $this->db->select("PERIOD");
//        $this->db->where('ENROLLMENTID',$enrollmentid);
        return $this->db->get($this->table_name)->result_array();
    }

    function getActiveEnrollment()
    {
        $date = date('Y-m-d H:i:s');
        $sql = "SELECT * FROM enrollment WHERE STARTDATE <= '".$date."' AND ENDDATE >= '".$date."' AND ENROLLMENTSTATUS='OPEN'";
	   //$sql = "SELECT * FROM enrollment";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getEntrollmentById($id = 0)
    {
        $sql = "SELECT * FROM $this->table_name where ENROLLMENTID = ?";
        $result = $this->db->query($sql, array($id));
        return $result->row_array();
    }

    function getEnrollmentByProgramIdA($program_id = 0)
    {
		$date = date('Y-m-d H:i:s');
       /* $sql = "SELECT a.*, b.*, c.programname AS 'PROGRAMNAME' 
            FROM enrollment a 
            JOIN programdegree b ON(b.programdegreeid = a.programdegreeid) 
            JOIN program c ON(c.programid = b.programid) 
            WHERE b.programid = $program_id AND
			a.ENROLLMENTSTATUS='OPEN'";*/
			 $sql = "SELECT a.*, b.*, c.programname AS 'PROGRAMNAME' 
            FROM enrollment a 
            JOIN programdegree b ON(b.programdegreeid = a.programdegreeid) 
            JOIN program c ON(c.programid = b.programid) 
            WHERE b.programid = $program_id
			";
			//echo "<pre>"; print($sql);die;
		$result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getEnrollmentByProgramIdB($program_id = 0)
    {
		$date = date('Y-m-d H:i:s');
        $sql = "SELECT a.*, b.*, c.programname AS 'PROGRAMNAME' 
            FROM enrollment a 
            JOIN programdegree b ON(b.programdegreeid = a.programdegreeid) 
            JOIN program c ON(c.programid = b.programid) 
            WHERE b.programid = $program_id AND
			a.ENROLLMENTSTATUS='OPEN'";
		//echo "<pre>"; print($sql);die;
		$result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getEnrollment($academicyear,$period){
		if($academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period==''){
			$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID)
			JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR=".$academicyear.")B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
					JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR=".$academicyear.")B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
		}else if($academicyear=='UNDEFINED YEAR' && $period==''){
			$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID)
			JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR IS NULL)B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
					JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR IS NULL)B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
		}else
		if($academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period!='' && $period!='UNDEFINED YEAR'){
			$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID) 
			JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR=".$academicyear." AND (".$period."))B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID)
JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)					
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR=".$academicyear." AND (".$period."))B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
		}else if($academicyear=='UNDEFINED YEAR' && $period!='' && $period!='UNDEFINED YEAR'){
			$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID) 
			JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR IS NULL AND (".$period."))B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
					JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR IS NULL AND (".$period."))B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
		}else 
			if($academicyear!='' && $academicyear!='UNDEFINED YEAR' &&  $period=='UNDEFINED YEAR'){
			$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID) 
			JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR=".$academicyear." AND (E.PERIOD IS NULL))B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID)
JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)					
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR=".$academicyear." AND (E.PERIOD IS NULL))B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
		}else if($academicyear=='UNDEFINED YEAR' && $period=='UNDEFINED YEAR'){
			$sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID) 
			JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL))B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
					JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL))B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID
				";
		}else{
        $sql = "SELECT
	A.PROGRAMNAME, 
	A.PROGRAMID,
	 B.ACADEMICYEAR,
	COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
	LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
			JOIN participants D ON (C.PROGRAMID=D.PROGRAMID) 
			WHERE D.ACCEPTANCESTATUS = 'ACCEPTED')B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
			GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
				SELECT
					A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID,
					B.ACADEMICYEAR,
					COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
				FROM scholarshiptype A
				LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
					JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
					WHERE D.ACCEPTANCESTATUS = 'ACCEPTED')B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
					GROUP BY A.SCHOLARSHIPTYPENAME, 
					A.SCHOLARSHIPTYPEID";
		}
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getAcademicYear(){
		$sql = "SELECT DISTINCT
				CASE
				WHEN ACADEMICYEAR IS NULL THEN 'UNDEFINED YEAR'
				ELSE ACADEMICYEAR
				END AS ACADEMICYEAR
				FROM enrollment";
		$result = $this->db->query($sql);
        return $result->result_array();
        //$this->db->distinct();
        //$this->db->select("ACADEMICYEAR");
		//$this->db->where('PERIOD !=',NULL);
        //return $this->db->get($this->table_name)->result_array();
    }
	
	function getPeriod(){
       /*$this->db->distinct();
        $this->db->select("PERIOD");
		$this->db->where('PERIOD !=',NULL);
        return $this->db->get($this->table_name)->result_array();*/
		$sql = "SELECT DISTINCT
				CASE
				WHEN PERIOD IS NULL THEN 'UNDEFINED YEAR'
				ELSE PERIOD
				END AS PERIOD
				FROM enrollment";
		$result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getFilter($filtername){
		$this->db->distinct();
		$this->db->where('FILTERNAME',$filtername);
		$this->db->limit(1);
		return $this->db->get('defaultfilter')->result_array();
	}
	
	function updateFilter($id, $data)
    {
        $this->db->where('IDDEFAULTFILTER', $id);
        return $this->db->update('defaultfilter', $data);
    }
	
	function updateFilterStatus($status, $column, $data){
		$this->db->where('STATUS',$status);
		$this->db->where('COLUMN',$column);
		return $this->db->update('defaultfilter',$data);
	}
	
	function getFilterStatus($status, $column){
		$this->db->where('STATUS',$status);
		$this->db->where('COLUMN',$column);
		return $this->db->get('defaultfilter')->result_array();
	}
	function getAllFilterBy($column){
		$this->db->where('COLUMN',$column);
		return $this->db->get('defaultfilter')->result_array();
	}
}
?>