<?php 
class Couponmodel extends CI_Model {

    private $table_name = "coupon";
    private $primary_key = "COUPONID";
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetCouponById($id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE $this->primary_key = ?";
        $result = $this->db->query($sql, array($id));
        return $result->row_array();
    }

}
?>