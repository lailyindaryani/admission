<?php 
class DegreeModel extends CI_Model {
    public $id;
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetDecreeById($degreeId = 0)
    {
        $sql = "SELECT * FROM degree WHERE DEGREEID = ?";
        $result = $this->db->query($sql, array($degreeId));
        return $result->row_array();
    }

    function GetAllDegree()
    {
        $sql = "SELECT * FROM degree";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getAllData(){
        $sql = "SELECT * FROM degree ";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

}
?>