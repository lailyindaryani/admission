<?php 
class LanguageDeliveryModel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetLanguageDeliveryById($id = 0)
    {
        $sql = "SELECT * FROM languagedelivery WHERE LANGUAGEDELIVERYID = ?";
        $result = $this->db->query($sql, array($id));
        return $result->row_array();
    }
	
	function GetLanguageDeliveryByIdLanguageDeliveryMapping($languagedeliveryId = 0)
    {
      $sql = "SELECT a.*, b.* FROM languagedeliverymapping a JOIN languagedelivery b ON (a.LANGUAGEDELIVERYID = b.LANGUAGEDELIVERYID) WHERE a.LANGUAGEDELIVERYMAPPINGID = ? ";
        $result = $this->db->query($sql, array($languagedeliveryId));
        return $result->row_array();  
    }

    function GetAllData()
    {
        $sql = "SELECT * FROM languagedelivery";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
}
?>