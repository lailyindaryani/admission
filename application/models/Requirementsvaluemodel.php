<?php 
class RequirementsValueModel extends CI_Model {

    private $table_name = "requirementvalue";
    private $primary_key = "REQUIREMENTVALUEID";
    private $foreign_key = array(
            'USERS'     => 'USERID'
        );

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function insert($data)
    {
        return $this->db->insert($this->table_name, $data);
    }

    function update($requirement_value_id = 0, $data = array())
    {
        $this->db->where($this->primary_key, $requirement_value_id);
        return $this->db->update($this->table_name, $data);
    }

    function GetAllData($participant_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE PARTICIPANTID = ?";
        $result = $this->db->query($sql, array($participant_id));

        return $result->result_array();
    }

    function GetData($participant_id = 0, $requirement_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE PARTICIPANTID = ? AND REQUIREMENTID = ?";
        $result = $this->db->query($sql, array($participant_id, $requirement_id));

        return $result->row_array();
    }

    function GetDataByParticipantId($participant_id = 0)
    {
        $sql = "SELECT a.REQUIREMENTID, b.REQUIREMENTNAME, CONCAT(FILEURL, FILENAME) URL, a.FILENAME FROM requirementvalue a JOIN requirements b ON (a.REQUIREMENTID = b.REQUIREMENTID) WHERE a.PARTICIPANTID = ? AND b.TYPE = 'ATTACHMENT' ORDER BY REQUIREMENTNAME ";
        $result = $this->db->query($sql, array($participant_id));
        //echo $this->db->last_query(); die();
        return $result->result_array();
    }

}
?>