<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $pagetitle;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="International Office - Enrollment Application">
    <meta name="author" content="Direktorat Sistem Informasi Telkom University">

    <!-- CSS -->
    <link href="<?php echo base_url();?>themes/_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>themes/_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>themes/_assets/css/main.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>themes/_assets/css/my-custom-styles.css" rel="stylesheet" type="text/css">

    <!--[if lte IE 9]>
    <link href="<?php echo base_url();?>themes/_assets/css/main-ie.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>themes/_assets/css/main-ie-part2.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!-- Fav and touch icons -->
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="http://telkomuniversity.ac.id/grid/images/telu/telkom-university-icon.png"/>
    <link rel="stylesheet" href="<?php echo base_url();?>themes/_assets/css/skins/red.css" type="text/css" />

    <!-- simpan JS yang penting aja di Header -->
    <!-- Javascript -->
        <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/jquery/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/modernizr/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-tour/bootstrap-tour.custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/king-common.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/jquery-maskedinput/jquery.masked-input.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/jquery-ui/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/jqallrangesliders/jQAllRangeSliders-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/markdown/markdown.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/summernote/summernote.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/king-form-layouts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/datatable/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/highcharts.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/data.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/exporting.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/export-data.js"></script>
	<script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/king-table.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/init.default.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/datatable/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/loading-overlay/loading-overlay.js"></script>	

    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/bootbox.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/facebox.js"></script>
	
    <style>
        .error {
            color: #e80c4d !important;
            font-weight: bold;
        }

        .widget-content{
            background-color: #ffffff;
        }
        input.error {
            border: 1px solid #e80c4d !important;
        }

          .borderless td, .borderless th {
              border: none;
          }

    </style>

</head>

<body class="form-layouts">
<!-- WRAPPER -->
<div class="wrapper">




    <!-- TOP BAR -->
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <!-- logo -->
                <div class="col-md-2 logo" style="margin-bottom: 3px;margin-top: -7px;">
                    <a href="<?php echo base_url();?>" ><img src="<?php echo base_url();?>images/logoio.png" alt="International Office Telkom University" /></a>
                    <h1 class="sr-only">International Office Telkom University</h1>
                </div>
                <!-- end logo -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-9">
                            <div class="top-bar-right">
                                <!-- responsive menu bar icon -->
                                <a href="#" class="hidden-md hidden-lg main-nav-toggle"><i class="fa fa-bars"></i></a>
                                <!-- end responsive menu bar icon -->


                                <div class="notifications">
                                    <ul>


                                        <!-- notification: general -->
                                        <li class="notification-item general">
                                            <div class="btn-group">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <!-- <i class="fa fa-bell"></i><span class="count"></span> -->
                                                    <span class="circle"></span>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li class="notification-header">
                                                        <em>You have 8 notifications</em>
                                                    </li>

                                                    <li class="notification-footer">
                                                        <a href="#">View All Notifications</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <!-- end notification: general -->
                                    </ul>
                                </div>

                                <!-- logged user and the menu -->
                                <div class="logged-user">
                                    <div class="btn-group">
                                        <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
<!--                                             <img src="<?php echo base_url();?>themes/_assets/img/user-avatar.png" alt="User Avatar" /> -->
                                                    <img  src="<?php  
													$users=$this->session->userdata('users');
													$users=$users[0];
													if($users['USERGROUPID']==2){
														echo base_url().$participant['PHOTOURL'].$participant['PHOTOFILENAME'];
													}else{
														echo "";
													}	?>" style="max-width: 50px; max-height: 50px;border: 1px solid #ab1d1d; image-orientation: from-image;"></td>
												
            <span class="name"><?php
                $users=$this->session->userdata('users');
                $users=$users[0];
                echo htmlentities($users['FULLNAME'], ENT_NOQUOTES, 'UTF-8');

                ?></span> <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="<?=base_url()?>account/changePassword">
                                                    <i class="fa fa-key"></i>
                                                    <span class="text">Change Password</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url()?>login/logout">
                                                    <i class="fa fa-power-off"></i>
                                                    <span class="text">Logout</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end logged user and the menu -->
                            </div>
                            <!-- /top-bar-right -->
                        </div>
                    </div>
                    <!-- /row -->
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /top -->

    <!-- BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
    <div class="bottom">
        <div class="container">
            <div class="row">
                <!-- left sidebar -->
               <?php include $menu ?>
                <!-- end left sidebar -->

