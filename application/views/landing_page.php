<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>Login | KingAdmin - Admin Dashboard</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="KingAdmin Dashboard">
	<meta name="author" content="The Develovers">

	<!-- CSS -->
	<link href="<?php echo base_url();?>themes/_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/main.css" rel="stylesheet" type="text/css">

	<!--[if lte IE 9]>
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon57x57.png">
	<link rel="shortcut icon" href="<?php echo base_url();?>themes/_assets/ico/favicon.png">

</head>
<style>
	body{
		background-image: url("<?php echo base_url()?>images/b.jpg");
		height: 100%;

			/* Center and scale the image nicely */
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
	}
</style>
<body >
	<div class="wrapper full-page-wrapper page-auth page-login text-center">
		<div class="inner-page">
			 <?php $this->load->view('includes/messages'); ?>
			<div class="login-box center-block" style="background-color: #FFFFFF; width:50% !important">
                <div class="row">

	                <div class="col-sm-12" style="text-align: center">
	                    <img src="<?php echo base_url()?>images/io.png" style="width: 250px;margin:auto;">
	                <hr>
	                </div><br>
	                <div class="col-sm-6">
	                	<a href="<?php echo base_url();?>login/academic">
						    <img class="thumbnail" src="<?php echo base_url();?>/images/degree.jpg" alt="" style="width:100%">
						</a>
					</div>
					<div class="col-sm-6">
						<a href="<?php echo base_url();?>login/nonAcademic">
						    <img class="thumbnail" src="<?php echo base_url();?>/images/nondegree.jpg" alt="" style="width:100%">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer">&copy; <?php echo Date('Y'); ?> SISFO Telkom University</footer>

	<!-- Javascript -->
	<script src="<?php echo base_url();?>themes/_assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/bootstrap/bootstrap.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/plugins/modernizr/modernizr.js"></script>
</body>

</html>

