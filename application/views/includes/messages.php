<?php
if(!is_null($this->session->flashdata('msg_success'))) { ?>
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    <?php echo $this->session->flashdata('msg_success'); ?>
</div>
<?php } elseif(!is_null($this->session->flashdata('msg_error'))) { ?>
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-ban"></i> Failed!</h4>
	<?php echo $this->session->flashdata('msg_error'); ?>
</div>
<?php } elseif(!is_null($this->session->flashdata('msg_info'))) { ?>
<div class="alert alert-info alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-info"></i> Information!</h4>
	<?php echo $this->session->flashdata('msg_info'); ?>
</div>
<?php } elseif(!is_null($this->session->flashdata('msg_warning'))) { ?>
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i> Warning!</h4>
    <?php echo $this->session->flashdata('msg_warning'); ?>
</div>
<?php } ?>