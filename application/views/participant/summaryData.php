<?php
	//secho "<pre>"; print_r($participantDetail);die;
?>

<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Participant Card</h2>
    <em>Form Participant Card</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> This is your participant card : </h3>
            </div>
            <div class="widget-content">
                <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">

                           <!-- <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>participant/summarySubmitProcess">-->

                            <input type="hidden" name="participant" value="<?php echo $participant['PARTICIPANTID']; ?>">
							<input type="hidden" id="programType" name="programType" value="<?php echo $participant['PROGRAMTYPE']; ?>">

                            <div class="basic-info" id="DivIdToPrint">
                                <style>

                                    @media all {
                                        .page-break	{ display: none; }
                                    }

                                    @media print {
                                        .page-break	{ display: block; page-break-before: always; }
                                    }

                                </style>


                                <button class="btn btn-success" id="printPage" style="float: right">Print</button>
                                <h3 style="color: #6a1212"> Participant Card </h3>
                                <table class="table" border="0" >
                                    <tbody>
                                        <tr>
                                            <td rowspan="6" width="15%"><img  src="<?php echo base_url().$participant['PHOTOURL'].$participant['PHOTOFILENAME']; ?>" style="max-width: 165px; max-height: 250px;border: 1px solid #ab1d1d; image-orientation: from-image;"></td>
                                            <td width="15%"><strong>Full Name</strong></td>
                                            <td width="70%" colspan="2"><strong><?php echo htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8'); ?></strong></td>

                                        </tr>
                                        <tr>
											<td><strong>Participant Number</strong></td>
                                            <td colspan="2"><?php echo $participant['ADMISSIONID']; ?></td>
                                        </tr>										
                                        <tr>
											<td><strong>Select Program</strong></td>
                                            <td colspan="2"><?php echo $program['PROGRAMNAME'] ?></td>
                                        </tr>
                                        <?php if($participantDetail['PROGRAMTYPE']=='ACADEMIC'){?>
										<tr>
											<td><strong>Study Program</strong></td>
                                            <td colspan="2"><?php echo $studyProgram['STUDYPROGRAMNAME']; ?></td>
                                        </tr>
										 <tr style="line-height: 10px;">
											<td><strong>Faculty</strong></td>
                                            <td colspan="2"><?php echo $participantDetail['FACULTYNAMEENG']; ?></td>
                                        </tr>
										<tr style="line-height: 10px;">
											<td><strong>Interview Date</strong></td>
                                            <td colspan="2"><?php echo str_replace(' ','',str_replace('00:00:00','',$participant['INTERVIEWDATE'])); ?></td>
                                        </tr>
										<?php }else{ ?>
										<tr>
											<td><strong>Course</strong></td>
                                            <td colspan="2"><?php echo $participantDetail['COURSENAME']; ?></td>
                                        </tr>
										
										<?php } ?>								
                                    </tbody>
                                </table>
								<h5>For further information, please contact us at:</h5>
								<h3 style="color: #6a1212"> The International Office of Telkom University </h3>
                                <table class="table">
                                    <tbody>
                                        <tr>
											<td width="15%"><strong>Phone</strong></td>
                                            <td>+62 22 7564108 ext 2400</td>										
                                        </tr>
										<tr>
											<td width=""><strong>Mobile Phone (What�s App, Line)</strong></td>
                                            <td>+62 813 2112 3400</td>
                                        </tr>
                                        <tr>
											<td><strong>Web</strong></td>
                                            <td><a href="https://io.telkomuniversity.ac.id">https://io.telkomuniversity.ac.id</a></td>
                                        </tr>
                                        <tr>
											<td width="20%"><strong>Email</strong></td>
                                            <td>admission@io.telkomuniversity.ac.id</td>
                                        </tr>
                                    </tbody>
                                </table>
                               
                            </div>
                        </div>        
                    </div>

                    <div class="actions">
                        <a  href="<?php echo base_url()?>participant/supportingData/" type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</a>
						<a  href="<?php echo base_url()?>participant/acceptanceStatus/" type="button" class="btn btn-primary btn-next"><i class="fa fa-arrow-right"></i> Next </a>
                    </div>

                 <!--  </form>-->

                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('#printPage').click(function(){
			if($("#programType").val()=='ACADEMIC'){
				alert("Thank you for completing your registration, kindly check your interview schedule as mentioned in your participant card. We will contact you shortly for the detail interview.");
            }
			printDiv();
            $('#printPage').show();
            $('.action-buttons').show();
        });

        function printDiv() {
            $('#printPage').hide();
            $('.action-buttons').hide();

            var divToPrint = document.getElementById('DivIdToPrint');
            var newWin = window.open('', 'Print-Window');
            newWin.document.title = "Print Data";
            newWin.document.body.style.cssText = 'font-family:Arial !important';
            newWin.document.body.style.cssText = 'font-size:.7em !important';
            newWin.document.open();
            var css = "";
            css += " <link href='<?php echo base_url(); ?>themes/_assets/css/bootstrap.min.css' rel='stylesheet' type='text/css'>";
            css += "<style> pagebreak { page-break-after: always; }.beforepagebreak { page-break-before: always; }</style>";
            css += "<style> * {font: 11px arial;} </style>";
            css += "<style> .tablebody tr {line-height: 11px;} </style>";
            newWin.document.write('<html><head>' + css + '</head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
			newWin.document.close();
	   }
		
    });
</script>