<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Family Data</h2>
    <em>Form Family Data</em>
</div>

<div class="main-content">


<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
            </div>
            <div class="widget-content">
                <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">

                            <form class="form-horizontal" role="form" id="inputForm" method="post" action="<?php echo base_url()?>participant/familyDataProcess"> 
                                <input type="hidden" name="participantid" value="<?php echo $participant['PARTICIPANTID']; ?>">
                                <fieldset>
                                    <legend>Family Data</legend>
                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">Full Name of Father</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control required" name="father_name" onpaste="return blockSpecialChar(event)" onkeypress="return blockSpecialChar(event)" id="fathername" value="<?php echo $father['FULLNAME']; ?>" placeholder="Full Name of Father">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">Occupation</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control required" name="father_occupation" id="father-occupation" value="<?php echo $father['OCCUPATION']; ?>" placeholder="Occupation">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Home Address</label>
                                    <div class="col-sm-9">
                                        <!-- <input type="text" class="form-control" name="ticket-subject" placeholder="Passport No."> -->
                                        <textarea class="form-control" name="father_address" id="father-address required" rows="3" cols="30" placeholder="Home Address"><?php echo $father['ADDRESS']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Country</label>
                                    <div class="col-sm-3">
                                        <select name="father_country" onchange="father_hp_code()" id="father_country" class="select2 required">
                                                <?php 
                                                    if(!empty($listCountry))
                                                    {
                                                        foreach ($listCountry as $country) {
                                                            $selected = ($country['COUNTRYNAMEENG'] == $father['COUNTRY']) ? 'selected' : '';
                                                            echo "<option data-ext='".$country['PHONECODE']."' value='".$country['COUNTRYNAMEENG']."' $selected >".$country['COUNTRYNAMEENG']."</option>";
                                                        }
                                                    }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">City</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control required" name="father_city" id="father-city" value="<?php echo $father['CITY']; ?>" placeholder="City">
                                        <!-- <textarea class="form-control" name="ticket-message" rows="5" cols="30" placeholder="Message"></textarea> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Zip Code</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control required number" value="<?php echo $father['ZIPCODE']; ?>" name="father_zipcode" placeholder="Zip Code">
                                        <!-- <textarea class="form-control" name="ticket-message" id="ticket-message" rows="5" cols="30" placeholder="Message"></textarea> -->
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Phone No./Mobile</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="father_phone" name="father_phone" value="<?php echo $father['PHONE']; ?>" placeholder="Phone No./Mobile">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" name="father_email" value="<?php echo $father['EMAIL']; ?>" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">Full Name of Mother</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control required" onpaste="return blockSpecialChar(event)" onkeypress="return blockSpecialChar(event)" name="mother_name" value="<?php echo $mother['FULLNAME']; ?>" placeholder="Full Name of Mother">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">Occupation</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control required" name="mother_occupation" value="<?php echo $mother['OCCUPATION']; ?>" placeholder="Occupation">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Home Address</label>
                                    <div class="col-sm-9">
                                        <!-- <input type="text" class="form-control" name="ticket-subject" placeholder="Passport No."> -->
                                        <textarea class="form-control required" name="mother_address" rows="3" cols="30" placeholder="Home Address"><?php echo $mother['ADDRESS']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Country</label>
                                    <div class="col-sm-3">
                                        <select name="mother_country" onchange="mother_hp_code()" id="mother_country" class="select2 required">
                                                <?php 
                                                    if(!empty($listCountry))
                                                    {
                                                        foreach ($listCountry as $country) {
                                                            $selected = ($country['COUNTRYNAMEENG'] == $mother['COUNTRY']) ? 'selected' : '';
                                                            echo "<option data-ext='".$country['PHONECODE']."' value='".$country['COUNTRYNAMEENG']."' $selected >".$country['COUNTRYNAMEENG']."</option>";
                                                        }
                                                    }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">City</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control required" name="mother_city" value="<?php echo $mother['CITY']; ?>" placeholder="City">
                                        <!-- <textarea class="form-control" name="ticket-message" rows="5" cols="30" placeholder="Message"></textarea> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Zip Code</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control required number" name="mother_zipcode" value="<?php echo $mother['ZIPCODE']; ?>" placeholder="Zip Code">
                                        <!-- <textarea class="form-control" name="ticket-message" rows="5" cols="30" placeholder="Message"></textarea> -->
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Phone No./Mobile</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="mother_phone" name="mother_phone" value="<?php echo $mother['PHONE']; ?>" placeholder="Phone No./Mobile">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control email" name="mother_email" value="<?php echo $mother['EMAIL']; ?>" placeholder="Email">
                                    </div>
                                </div>
                                </fieldset>
                        </div>
                    </div>

                    <div class="actions">
                        <a href="<?php echo base_url()?>participant/personalData/" type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</a>
                        <button type="submit" value="family_data" class="btn btn-primary btn-next">Save & Next <i class="fa fa-arrow-right"></i></button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<script type="text/javascript">
    $(document).ready(function(){


        $("#inputForm").validate();

    });
	
	function father_hp_code(){
		var phone_ext = $('#father_country option:selected').attr('data-ext');
        var hp=$('#father_phone').val("+" + phone_ext);

        $("#father_country").on("change", function(e) {
            var phone_ext = $('#father_country option:selected').attr('data-ext');
            $('#father_phone').val("+" + phone_ext);
        });
	}
	
	function mother_hp_code(){
		
        var phone_ext1 = $('#mother_country option:selected').attr('data-ext');
        $('#mother_phone').val("+" + phone_ext1);

        $("#mother_country").on("change", function(e) {
            var phone_ext1 = $('#mother_country option:selected').attr('data-ext');
            $('#mother_phone').val("+" + phone_ext1);
        });
	}
	function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    }

</script>


