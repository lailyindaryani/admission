<script>
	function validasi_gambar(){
		var gambar_lama= $('#gambar_lama').attr('value');
		var gambar= $('#imgInp').val().replace(/\\/g, '/').replace(/.*\//, '');
		if(gambar_lama==''){
			if(gambar==''){
				alert('Please upload your pass photo!');
				return false;
			}
		}
	}
	function fileValidation(idFile){
        var fileInput = document.getElementById(idFile);
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .jpg, .png, .gif only.');
            fileInput.value = '';
            return false;
        }
		validateSize(fileInput);
    }
	function validateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 2) {
            alert('File size max 2 MB');
			$(file).val('');
			return false;
        }else{
			readURL(file);
		} 
    }
	$(document).ready(function(e){
	$("[name='pass_valid_until']").on('change',function(){
		var fileInput = this.value;
		var d1 = new Date();
		var d2 = new Date(fileInput);
		var months;
		months = (d2.getFullYear() - d1.getFullYear()) * 12;
		months -= d1.getMonth() + 1;
		months += d2.getMonth();
		if(months<=6){
			alert("Your passport's active period is less than 6 months, please make your passport renewal");
			$(this).val('');
			return false;
		}
		
	});
});
</script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Personal Data</h2>
    <em>Form Personal Data</em>
</div>

<div class="main-content">

<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
            </div>
            <div class="widget-content">
                <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">
                            <form class="form-horizontal" role="form" enctype="multipart/form-data" onsubmit="return validasi_gambar()" method="post" id="inputForm" action="<?php echo base_url()?>participant/personalDataProcess">
                            <fieldset>
                                <legend>Personal Data</legend>
                                <div class="form-group">
                                    <label for="ticket-subject" class="col-sm-3 control-label">Full Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" onpaste="return blockSpecialChar(event)" onkeypress="return blockSpecialChar(event)" class="form-control required" name="fullname" value="<?php echo $participant['FULLNAME']; ?>" id="fullname" placeholder="Fullname" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Passport Number</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control required" name="passport_number" value="<?php echo $participant['PASSPORTNO']; ?>" id="passport-number" placeholder="Passport No.">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Valid Until</label>
                                    <div class="col-sm-3">
                                        <?php /* <select name="pass_valid_until" class="select2">
                                            <option value="">Select Year</option>
                                            <?php 
                                                for ($year=Date('Y'); $year <= Date('Y')+10; $year++) { 
                                                    echo "<option value='$year'>".$year."</option>";
                                                }
                                            ?>
                                        </select> */ ?> 
                                        <input type="text" id="datepicker" class="form-control datetime-format required" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['PASSPORTVALID'])); ?>" name="pass_valid_until" placeholder="Passport Validity">
                                    </div>
									<div class="col-sm-4" align="left" style="text-size: 5px;">
										<p class="help-block"><em>Your passport's active period must be longer than 6 months</em></p>
									</div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Issued By</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control required" name="pass_issued_by" value="<?php echo $participant['PASSPORTISSUED']; ?>" id="pass-issued-by" placeholder="Issued By">
                                        <!-- <textarea class="form-control" name="ticket-message" id="ticket-message" rows="5" cols="30" placeholder="Message"></textarea> -->
                                    </div>
                                </div>
								<input type="hidden" id="program_name" value="<?php echo $participantDetail['PROGRAMNAME']?>">
								<input type="hidden" id="degree" value="<?php echo $participantDetail['DEGREE']?>">
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Place of Birth</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control required"  name="place_of_birth" value="<?php echo $participant['BIRTHPLACE']; ?>" placeholder="Place of Birth">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Date of Birth</label>
                                    <div class="col-sm-3">
                                        <input type="text" id="datepicker" class="form-control datetime-format required" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE'])); ?>" name="date_of_birth" placeholder="Date of Birth">
                                    </div>
									<div class="col-sm-4" align="left" style="text-size: 5px;">
										<p class="help-block"><em>Maximum 25 years old for REGULAR S1</em></p>
									</div>
                                </div>

                                <div class="form-group">

                                    <label for="ticket-message" class="col-sm-3 control-label">Sex</label>
                                    <div class="col-sm-9">
                                        <label class="control-inline ">
                                            <input type="radio" name="sex" class="required" value="Male" <?php if($participant['GENDER'] == 'Male') echo 'checked';  ?>>
                                            <span><i></i>Male</span>
                                        </label>
                                        <label class="control-inline ">
                                            <input type="radio" name="sex" value="Female" <?php if($participant['GENDER'] == 'Female') echo 'checked';  ?>>
                                            <span><i></i>Female</span>
                                        </label>
                                        <label for="sex" generated="true" class="error"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Marital Status </label>
                                    <div class="col-sm-9">
                                        <label class="control-inline ">
                                            <input type="radio" name="marital_status" class="required" value="married" <?php if($participant['MARITALSTATUS'] == 'married') echo 'checked';  ?>>
                                            <span><i></i>Married</span>
                                        </label>
                                        <label class="control-inline ">
                                            <input type="radio" name="marital_status" class="required" value="single" <?php if($participant['MARITALSTATUS'] == 'single') echo 'checked';  ?>>
                                            <span><i></i>Single</span>
                                        </label>
                                        <label for="marital_status" generated="true" class="error"></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label" >Religion</label>
                                    <div class="col-sm-9">
                                        <label class="control-inline "><input type="radio" name="religion" value="Islam" class="required" <?php if($participant['RELIGION'] == 'Islam') echo 'checked';  ?>> <span><i></i>Islam</span></label>
                                        <label class="control-inline "><input type="radio" name="religion" value="Christianity" class="required" <?php if($participant['RELIGION'] == 'Christianity') echo 'checked';  ?>> <span><i></i>Christianity</span></label>
                                        <label class="control-inline "><input type="radio" name="religion" value="Buddhism" class="required" <?php if($participant['RELIGION'] == 'Buddhism') echo 'checked';  ?>> <span><i></i>Buddhism</span></label>
                                        <label class="control-inline "><input type="radio" name="religion" value="Hinduism" class="required" <?php if($participant['RELIGION'] == 'Hinduism') echo 'checked';  ?>> <span><i></i>Hinduism</span></label>
                                        <label class="control-inline "><input type="radio" name="religion" value="Others" class="required" <?php if($participant['RELIGION'] == 'Others') echo 'checked';  ?>> <span><i></i>Others</span></label>
                                        <label for="religion" generated="true" class="error"></label>
                                        <!--<select name="religion" class="select2">
                                            <option value="ISLAM" <?php /*if($participant['RELIGION'] == 'ISLAM') echo 'selected';  */?>>Islam</option>
                                            <option value="CRISTIAN" <?php /*if($participant['RELIGION'] == 'CHRISTIANITY') echo 'selected';  */?>>Christianity</option>
                                            <option value="BUDDHA" <?php /*if($participant['RELIGION'] == 'BUDDHISM') echo 'selected';  */?>>Buddhism</option>
                                            <option value="HINDU" <?php /*if($participant['RELIGION'] == 'HINDUISM') echo 'selected';  */?>>Hinduism</option>
                                            <option value="HINDU" <?php /*if($participant['RELIGION'] == 'JUDAISM') echo 'selected';  */?>>Judaism</option>
                                            <option value="HINDU" <?php /*if($participant['RELIGION'] == 'OTHERS') echo 'selected';  */?>>Others</option>
                                        </select>-->

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Nationality</label>
                                    <div class="col-sm-9">
                                        <select name="nationality" id="nationality" class="select2 required">
                                                <?php 
                                                    if(!empty($listCountry))
                                                    {
                                                        foreach ($listCountry as $country) {
                                                            $selected="";
                                                            if($country['COUNTRYNAMEENG']==$participant['NATIONALITY'])
                                                                $selected="selected";
                                                            echo "<option $selected value='". $country['COUNTRYNAMEENG']."'>".$country['COUNTRYNAMEENG']."</option>";
                                                        }
                                                    }
                                                ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">First Language</label>
                                    <div class="col-sm-9">
                                        <input type="text"  class="form-control required" name="first_language" value="<?php echo $participant['FIRSTLANGUAGE']; ?>" id="first-language" placeholder="First Language">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Profiency In English </label>
                                    <div class="col-sm-9">
                                        <label class="control-inline ">
                                            <input type="radio" name="profiency_in_english" class="required" value = "Fluent" <?php if($participant['PROFICIENCYEN'] == 'Fluent') echo 'checked';  ?>>
                                            <span><i></i>Fluent</span>
                                        </label>
                                        <label class="control-inline ">
                                            <input type="radio" name="profiency_in_english" class="required" value = "Good" <?php if($participant['PROFICIENCYEN'] == 'Good') echo 'checked';  ?>>
                                            <span><i></i>Good</span>
                                        </label>
                                        <label class="control-inline ">
                                            <input type="radio" name="profiency_in_english" class="required" value = "Fair" <?php if($participant['PROFICIENCYEN'] == 'Fair') echo 'checked';  ?>>
                                            <span><i></i>Fair</span>
                                        </label>
                                        <label for="profiency_in_english" generated="true" class="error"></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Profiency In Indonesia</label>
                                    <div class="col-sm-9">
                                        <label class="control-inline ">
                                            <input type="radio" name="profiency_in_indonesia" class="required" value = "Fluent" <?php if($participant['PROFICIENCYINA'] == 'Fluent') echo 'checked';  ?>>
                                            <span><i></i>Fluent</span>
                                        </label>
                                        <label class="control-inline ">
                                            <input type="radio" name="profiency_in_indonesia" class="required" value = "Good" <?php if($participant['PROFICIENCYINA'] == 'Good') echo 'checked';  ?>>
                                            <span><i></i>Good</span>
                                        </label>
                                        <label class="control-inline ">
                                            <input type="radio" name="profiency_in_indonesia" class="required" value = "Fair" <?php if($participant['PROFICIENCYINA'] == 'Fair') echo 'checked';  ?>>
                                            <span><i></i>Fair</span>
                                        </label>
                                        <label for="profiency_in_indonesia" generated="true" class="error"></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Mailing Address</label>
                                    <div class="col-sm-9">
                                        <!-- <input type="text" class="form-control" name="ticket-subject" id="ticket-subject" placeholder="Passport No."> -->
                                        <textarea class="form-control required" name="mailing_address" id="mailing-address" rows="3" cols="30" placeholder="Mailing Address"><?php echo $participant['ADDRESS']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Country</label>
                                    <div class="col-sm-3">
                                        <select name="country_id" id="country" class="select2 required">
                                                <?php 
                                                    if(!empty($listCountry))
                                                    {
                                                        foreach ($listCountry as $country) {
                                                            $selected="";
                                                            if($country['COUNTRYID']==$participant['COUNTRYID'])
                                                                $selected="selected";
                                                            echo "<option $selected value='".$country['COUNTRYID']."'>".$country['COUNTRYNAMEENG']."</option>";
                                                        }
                                                    }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">City</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control required" value="<?php echo $participant['CITY']; ?>" name="city" id="city" placeholder="City">
                                        <!-- <textarea class="form-control" name="ticket-message" id="ticket-message" rows="5" cols="30" placeholder="Message"></textarea> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-2 col-sm-offset-3 control-label">Zip Code</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control required number" value="<?php echo $participant['ZIPCODE']; ?>" name="zip_code" id="zip-code" placeholder="Zip Code" onkeypress="return _only_number_(event,this)">
                                        <!-- <textarea class="form-control" name="ticket-message" id="ticket-message" rows="5" cols="30" placeholder="Message"></textarea> -->
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Phone No./Mobile</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control required" name="mobile_phone" value="<?php echo $participant['PHONE']; ?>" id="mobile-phone" placeholder="Phone No./Mobile" onkeypress="return _only_number_(event,this)">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Facsimile Number</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="fax_number" value="<?php echo $participant['FAX']; ?>" id="fax-number" onkeypress="return _only_number_(event,this)" placeholder="Fax No">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" value="<?php echo $user['EMAIL']; ?>" name="email" id="email" placeholder="Email" readOnly>
                                    </div>
                                </div>
								<input type="hidden" id="gambar_lama" value="<?php echo $participant['PHOTOFILENAME'];?>" />
                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">Pass Photo</label>
                                    <div class="col-sm-9">
                                        <input type="file" name="userfile" size="20" id="imgInp" onchange="fileValidation('imgInp')" value=""/>
										 <p class="help-block"><em>Valid file type: .gif,.jpg, .png. File size max: 2 MB</em></p>
                                        <img style="max-height: 200px;max-width: 150px; image-orientation: from-image;" id="displayImage" alt="Photo" src="<?php echo base_url().$participant['PHOTOURL'].$participant['PHOTOFILENAME']; ?>"/>

                                    </div>
                                </div>

                            </fieldset>                     
                        </div>

                    </div>

                    <div class="actions">
                        <a type="button" href="<?php echo base_url()?>participant/selectProgram/" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</a>
                        <button type="submit" value="personal_data" class="btn btn-primary btn-next">Save & Next <i class="fa fa-arrow-right"></i></button>
                    </div>
                    </form>  
                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<!-- /content-wrapper -->
<script type="text/javascript">
    $(document).ready(function(e){
		$("[name='date_of_birth']").on('change',function(){
			var fileInput = $(this).val();
			var today = new Date();
			var birthDate = new Date(fileInput);
			var age = today.getFullYear() - birthDate.getFullYear();
			var m = today.getMonth() - birthDate.getMonth();
			if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())){
				age--;
			}
			var program_name = $('#program_name').val();
			var degree = $('#degree').val();
			if(age>25 && program_name=='REGULAR' && degree=='S1'){
				alert("YOU CAN'T CONTINUE REGISTRATION, YOUR AGE EXCEEDS 25 YEARS FOR REGULAR S1 PROGRAM");
				$(this).val('');
				return false;
			}
		});
		
		

        function UrlExists(url)
        {
            var http = new XMLHttpRequest();
            http.open('HEAD', url, false);
            http.send();
            return http.status!=404;
        }
        //passport_number
        /*var urls = '<?php echo base_url();?>';
        var name = $('#passport-number').val();

        if(UrlExists(urls+'uploads/'+name+'/'+name+'.jpg')){
            $('#displayImage').attr('src',urls+'uploads/'+name+'/'+name+'.jpg');
        } else if(UrlExists(urls+'uploads/'+name+'/'+name+'.jpeg')){
            $('#displayImage').attr('src',urls+'uploads/'+name+'/'+name+'.jpeg');
        } else if(UrlExists(urls+'uploads/'+name+'/'+name+'.gif')){
            $('#displayImage').attr('src',urls+'uploads/'+name+'/'+name+'.gif');
        } else if(UrlExists(urls+'uploads/'+name+'/'+name+'.png')){
            $('#displayImage').attr('src',urls+'uploads/'+name+'/'+name+'.png');
        }*/

        function readURL(input) {
            if (input.files && input.files[0]) {
                $("#displayImage").css("display", "block");
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#displayImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
       

        $('.datetime-format').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
			});	

        $("#inputForm").validate({
           /* errorPlacement: function(error, element) {
                if (element.attr("name") == "email" )
                    error.insertAfter(".some-class");
                else if  (element.attr("name") == "phone" )
                    error.insertAfter(".some-other-class");
                else
                    error.insertAfter(element);
            }*/
        });
    });
	function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    }

</script>
<!-- <script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script> -->


