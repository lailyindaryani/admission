			<?php date_default_timezone_set('Asia/Jakarta'); ?>
			<form action="<?php echo base_url()?>staff/updateEducationInfo" class="form-horizontal modal-form" role="form" method="post" id="edit-education-info-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Education Info</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="educationid" value="<?php echo $education['EDUCATIONID']?>">
                    <input type="hidden" name="participantid" value="<?php echo $education['PARTICIPANTID']?>">
					<?php if((strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') === false && $participant['DEGREE']=='S1') || ($participant['DEGREE']=='D3' && strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') === false)){ ?>
                            <div class="form-group">
                                <label for="ticket-message" class="col-sm-3 control-label">High School Name</label>
                                <div class="col-sm-9">                                    
                                    <input type="text" class="form-control required" name="schoolname" value="<?php echo $education['SCHOOLNAME']?>" placeholder="High School">
                                    </div>
                                </div>
                            <div class="form-group">
                                <label for="ticket-message" class="col-sm-3 control-label">From (Year)</label>
                                <div class="col-sm-3">
                                    <input type="text" class="date-picker-year form-control required" name="start" value="<?php echo $education['START']?>" placeholder="From (Year)">
                                </div>
                                <label for="ticket-message" class="col-sm-1 control-label">To</label>
                                <div class="col-sm-3">
                                    <input type="text" class="date-picker-year form-control required" name="end" value="<?php echo $education['END']?>" placeholder="To">
                                </div>
                            </div>

					<?php }else if($participant['DEGREE']=='S2' && strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') === false){ ?>
                            <div class="form-group">
                               
									<label for="ticket-message" class="col-sm-3 control-label">University/College Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control required" name="schoolname" value="<?php echo $education['SCHOOLNAME']?>" placeholder="University / College">
                                    </div>
                                </div>
                            <div class="form-group">
                                <label for="ticket-message" class="col-sm-3 control-label">From (Year)</label>
                                <div class="col-sm-3">
                                    <input type="text" class="date-picker-year form-control required" name="start" value="<?php echo $education['START']?>" placeholder="From (Year)">
                                </div>
                                <label for="ticket-message" class="col-sm-1 control-label">To</label>
                                <div class="col-sm-3">
                                    <input type="text" class="date-picker-year form-control required" name="end" value="<?php echo $education['END']?>" placeholder="To">
                                </div>
                            </div>

                                <div class="form-group">
                                    <label for="ticket-message" class="col-sm-3 control-label">GPA (Out of 4.0)</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control required" name="degree" value="<?php echo $education['DEGREES']?>" placeholder="Degress/Diplomas">
                                    </div>
                                </div>
					<?php }else if(strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') !== false){ ?>
						<div class="form-group">
									<label for="ticket-message" class="col-sm-3 control-label">University/College Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control required" name="schoolname" value="<?php echo $education['SCHOOLNAME']?>" placeholder="University / College">
                                
                                </div>
                            </div>
							<div class="form-group">
									<label for="ticket-message" class="col-sm-3 control-label">Semester</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control required" name="grade" value="<?php echo $education['GRADE']?>" placeholder="Semester">
                                
                                </div>
                            </div>
					<?php }else if($participant['PROGRAMTYPE']=='NON_ACADEMIC'){ ?>
						<div class="form-group">
									<label for="ticket-message" class="col-sm-3 control-label">University/College Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control required" name="schoolname" value="<?php echo $education['SCHOOLNAME']?>" placeholder="University / College">
                                
                                </div>
                            </div>
							<div class="form-group">
									<label for="ticket-message" class="col-sm-3 control-label">Semester</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control required" name="studyprogram" value="<?php echo $education['STUDYPROGRAM']?>" placeholder="Semester">
                                
                                </div>
                            </div>
					<?php }	?>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
			
			<script type="text/javascript">
    $(document).ready(function(){
		$('.date-picker-year').datepicker({
            format: "yyyy",
			weekStart: 1,
			orientation: "bottom",
			language: "{{ app.request.locale }}",
			keyboardNavigation: false,
			viewMode: "years",
			minViewMode: "years"
        });

        $("#inputForm").validate();
    });
</script>