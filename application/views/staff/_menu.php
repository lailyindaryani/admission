<div class="col-md-2 left-sidebar">

    <!-- main-nav -->
    <nav class="main-nav">

        <ul class="main-menu">
			<li>
                <a href="#" class="js-sub-menu-toggle">
                    <i class="fa fa-home fa-fw"></i><span class="text">Dashboard</span>
                    <i class="toggle-icon fa fa-angle-left"></i>
                </a>
                <ul class="sub-menu ">					
					<li><a href="<?php echo base_url()?>staff/dashboardEnrollment"><span class="text">Summary Reports</span></a></li>
                    <li><a href="<?php echo base_url()?>staff/dashboardCountry"><span class="text">Summary by Country</span></a></li>
                    <li><a href="<?php echo base_url()?>staff/dashboardStudyProgram"><span class="text">Summary by Study Program</span></a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url()?>staff/enrollment"><i class="fa fa-hand-o-up  fa-fw"></i><span class="text">Enrollment</span></a></li>
            <li><a href="<?php echo base_url()?>staff/participants"><i class="fa fa-hand-o-up  fa-users"></i><span class="text">Participants</span></a></li>
            <li><a href="<?php echo base_url()?>staff/reports"><i class="fa fa-copy"></i><span class="text">Reports</span></a></li>
           <li><a href="<?php echo base_url()?>staff/useractivation"><i class="fa fa-hand-o-up  fa-users"></i><span class="text">User Activation</span></a></li>
          
        </ul>
    </nav>
    <!-- /main-nav -->

    <div class="sidebar-minified js-toggle-minified">
        <i class="fa fa-angle-left"></i>
    </div>


</div>
