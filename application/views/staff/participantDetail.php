<?php
	date_default_timezone_set('Asia/Jakarta');
  //echo strpos($participant['PROGRAMNAME'],'STUDENT EXCHANGE');die;
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<style>
    .avatar{
        width: 100px;
		image-orientation: from-image;
    }
	.mce-notification {display: none !important;}
</style>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><a href="<?php echo base_url()?>staff/participants">Participants</a></li>
                <li class="active">Detail</li>
            </ul>
        </div>
        <div class="col-lg-8 ">
            <div class="top-content">
                <ul class="list-inline quick-access">
				<?php if($participant['INJECT']=='Y'){?>
					 <li>
                        <a>
                            <div class="quick-access-item bg-color-red">
                                <i class="fa fa-check-circle"></i>
                                <h5>PARTICIPANT STATUS</h5>
                                <em>INJECT</em>
                            </div>
                        </a>
                    </li>
				<?php } ?>
                    <li>
                        <?php
                        switch ($participant['CURRENTSTEP']){
                            case 1:
                                $currentstep = "Program Selection";
                                break;
                            case 2:
                                $currentstep = "Personal Data";
                                break;
                            case 3:
                                $currentstep = "Family Data";
                                break;
                            case 4:
                                $currentstep = "Education Info";
                                break;
                            case 5:
                                $currentstep = "Requirements Docs";
                                break;
                            case 6:
                                $currentstep = "Summary";
                                break;
                            case 7:
                                $currentstep = "Acceptance Status";
                                break;
                            default:
                                $currentstep = "Undefined";
                                break;
                        }
                        ?>
                        <a>
                            <div class="quick-access-item bg-color-green">
                                <i class="fa fa-tasks"></i>
                                <h5>CURRENT STEP</h5>
                                <em><?php echo $currentstep?></em>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a>
                            <div class="quick-access-item bg-color-blue">
                                <i class="fa fa-check-circle"></i>
                                <h5>ACCEPTANCE STATUS</h5>
                                <em><?php echo $participant['ACCEPTANCESTATUS']!=null?$participant['ACCEPTANCESTATUS']:'NOT SET'?></em>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Participant</h2>
            <em>Participant Detail</em>
        </div>
		
        <div class="main-content">
            <!-- NAV TABS -->
            <ul class="nav nav-tabs nav-tabs-custom-colored tabs-iconized">
                <li class="active"><a href="#profile-tab" data-toggle="tab"><i class="fa fa-user"></i> Profile</a></li>
				 <li ><a href="#participantCard-tab" data-toggle="tab"><i class="fa fa-book"></i> Participant Card </a></li>
                <li ><a href="#verification-tab" data-toggle="tab"><i class="fa fa-book"></i> Data Verification </a></li>
                <li ><a href="#interview-tab" data-toggle="tab" id="interviewTab"><i class="fa fa-book"></i> Interview Report </a></li>
                <!--        <li><a href="#activity-tab" data-toggle="tab"><i class="fa fa-money"></i> Payment Verification </a></li>-->
                <!--        <li><a href="#activity-tab" data-toggle="tab"><i class="fa fa-bar-chart"></i> Selection Score </a></li>-->
                <li ><a href="#acceptance-tab" data-toggle="tab"><i class="fa  fa-check-square-o"></i> Acceptance</a></li>
				<li ><a href="#confirmation-tab" id="confirmationTab" data-toggle="tab"><i class="fa  fa-check-square-o"></i> Confirmation Of Attendance</a></li>
            </ul>
			 <?php $this->load->view('includes/messages'); ?>
            <!-- END NAV TABS -->
            <div class="tab-content profile-page">
                <!-- PROFILE TAB CONTENT -->
                <div class="tab-pane profile active" id="profile-tab">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="user-info-left">
                                <?php
                                    if($participant['PHOTOURL']!=null){
										
                                       echo "<img src='".base_url().$participant['PHOTOURL'].$participant['PHOTOFILENAME']."' alt='".$participant['PHOTOFILENAME']."' class='avatar putar' />";
                                    }
                                    else{
                                        echo "<img src='".base_url()."images/default-avatar.png' alt='Profile Picture' class='avatar' />";
                                    }
                                ?>
                                <h2><?php echo htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')?><sup class="sr-only">online</sup></h2>
                                <h5>Current Step: </h5>
                                <em><?php echo $currentstep?></em>
                                <br>
                                <div class="container">
                                  <!-- Trigger the modal with a button -->
                                  <br>
                                  <button type="submit" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" >Send Email</button>
                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal" role="dialog">
                                    <form enctype="multipart/form-data" action="<?php echo base_url()?>staff/processReminder" method="post">
                                                <?php if($msg_success=$this->session->flashdata('msg_success'));
                                            $msg_class = $this->session->flashdata('msg_class');
                                            ?>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="alert <?php echo  $msg_class ?>">
                                                <?php echo  $msg; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="modal-dialog" style="width: 80%;">
                                        <input type="hidden" name="participantid" value="<?php echo $this->uri->segment(3); ?>">
										
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Kirim ke</h4>
                                        </div>
                                        <div class="modal-body">
                                        <label for="ticket-subject" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="email" id="email"                      placeholder="Email" value="<?php echo $participant['EMAIL']?>">
                                                <br>
                                            </div>
                                        <br>
                                        <label for="ticket-name" class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="name" id="name"                   placeholder="Fullname" value="<?php echo htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')?>">
                                                <br>
                                            </div>
                                            <?php
                                          switch ($participant['CURRENTSTEP']){
                                              case 1:
                                                  $message = "Choose Study Program:\n\n"; 
                                                $message .= "Dear ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8').",\n";
                                                $message .= "Thank you for your interest and registration at Telkom University. Kindly continue your registration by "; 
                                                 $message .= "choosing the study program and completing your registration process. Please note that the selection will "; 
                                                 $message .= "only be applied to those who have chosen the study program and completed all steps ";
                                                 $message .= "(including upload the required documents in the next step).\n\n";
                                                 $message .= "We look forward to having you in our candidate list.";
                                                                                                  break;
                                              case 2:
                                                  $message = "Upload documents:\n\nDear ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8').",\n";
                                                  $message .= "Thank you for your interest and registration at Telkom University. We noted that you have completed your personal data and choose the study program. Please note that the selection will only be applied to ";
                                                  $message .= "those who have completed all required steps (including upload the required documents).\n\n";
                                                  $message .= "Therefore, kindly continue your registration by uploading these documents:\n";
                                                  $message .= "• Scan of your Passport. \n";
                                                        $message .= "• Scan of your high school Certificate &amp; transcript. \n";
                                                       $message .= "• Scan of your motivation letter.\n\n";
                                                       $message .= "Additional documents for master program:\n";
                                                        $message .= "• Scan of your University Certificate &amp; transcript \n";
                                                         $message .= "• Curriculum Vitae (CV) \n";
                                                        $message .= "• Thesis Plan \n\n";
                                                         $message .= "We look forward to having you in our candidate list.";
                                                  break;
                                              case 3:
                                                  $message = "Written test:\n\n";
                                                    $message .= "Dear ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8').",\n";
                                                    $message .= "Thank you for your interest and registration at Telkom University. ";
                                                    $message .= "We cordially invite you to have the online test which will be available for one week (you may start at any ";
                                                    $message .= "time in your convenience) start from DATE until DATE Indonesian time (GMT +7).\n\n";
                                                    $message .= "To do the test, please login to your registration page:https://io.telkomuniversity.ac.id/inapps/ ";
                                                    $message .= "Choose the menu on the left side below the photo &gt;&gt; Computer Based Test.";
                                                   $message .= " Click Start (if you have started the test, you have to finish it at once).\n\n";
                                                    $message .= "1. Please follow the instruction\n";
                                                    $message .= "2. Please try the trial test and read the instruction. Be sure to save your answer and check if it is accepted.\n";
                                                    $message .= "3. If you click the next page, you cannot back to the previous page.\n";
                                                    $message .= "4. Do not refresh the page, because it will move to the next page automatically.\n";
                                                    $message .= "5. The maximum time for multiple choice test is 4 minutes each.\n";
                                                    $message .= "6. The maximum time for essay (7 questions) is 80 minutes. Kindly submit your answers via email once you finish the test to admission@io.telkomuniversity.ac.id.\n";
                                                    $message .= "7. Always remember to save answer on each page.\n\n";
                                                    $message .= "PS: Kindly submit all the required documents (academic bachelor transcript &amp; certificate, Curriculum Vitae, personal letter, and copy passport). We will invite you for the interview once we have your online test done.\n";
                                                    $message .= "\nThank you.";
                                                  break;
                                              case 4:
                                                  $message = "Interview:\n\n";
                                                    $message .= "Dear ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8').",\n";
                                                    $message .= "Thank you for your interest and registration at Telkom University. ";
                                                    $message .= "We cordially invite you to have an interview on DATE & Time Indonesian time (GMT +7). Please be online 20 minutes prior to the interview schedule. ";
                                                    $message .= "\n\nThe Skype ID of International Office Telkom University is: iotelkomuniv. ";
                                                    $message .= "Kindly confirm your attendance and inform your Skype ID.\nFor further assistance or any inquiries, please contact us (mail to admission@io.telkomuniversity.ac.id or +6281321123400).\n\n";
                                                    $message .= "We look forward to having you in our candidate list.";
                                                  break;
                                              case 5:
                                                  $message = "Requirements Docs";
                                                  break;
                                              case 6:
                                                  $message = "Summary";
                                                  break;
                                              case 7:
                                                  $message = "Acceptance Status";
                                                  break;
                                              default:
                                                  $message = "";
                                                  break;
                                          }?>

                                             <label for="isi" class="col-sm-3 control-label">Pesan</label>
											  <div class="col-sm-9">
                                            <textarea name="pesan" id="message" class="mceEditor form-control" rows="5"><?php echo $message?></textarea><br>
											 </div><br>
											<label for="ticket-attachment" class="col-sm-3 control-label">Attachment</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" name="attachment" id="attachment" placeholder="Attachment">
                                                <br>
                                            </div>
											<br></br>
                                            <button type="submit" name="kirim" class="btn btn-info btn-lg" value="kirim">Kirim</button>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                      
                                    </div>
                                </form>
                                  </div>
                                  
                                </div>

                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="user-info-right">
                                <div class="basic-info">
                                    <h3><i class="fa fa-square"></i> Basic Information</h3>
									<?php if($participant!=null) { ?>
                                    <p class="data-row">
                                        <span class="data-name">Username/Email</span>
                                        <span class="data-value"><?php echo $participant['EMAIL']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Passport Number</span>
                                        <span class="data-value"><?php echo $participant['PASSPORTNO']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Valid Until</span>
                                        <span class="data-value"><?php echo date('d/M/Y',strtotime($participant['PASSPORTVALID']))?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Issued By</span>
                                        <span class="data-value"><?php echo $participant['PASSPORTISSUED']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Birth Place</span>
                                        <span class="data-value"><?php echo $participant['BIRTHPLACE']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Birth Date</span>
                                        <span class="data-value"><?php echo date('d/M/Y',strtotime($participant['BIRTHDATE']))?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Gender</span>
                                        <span class="data-value"><?php echo $participant['GENDER']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Marital Status</span>
                                        <span class="data-value"><?php echo $participant['MARITALSTATUS']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Religion</span>
                                        <span class="data-value"><?php echo $participant['RELIGION']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Nationality</span>
                                        <span class="data-value">
                              <?php echo $participant['NATIONALITY']?>
                            </span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">First Language</span>
                                        <span class="data-value"><?php echo $participant['FIRSTLANGUAGE']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Proficiency In Bahasa</span>
                                        <span class="data-value"><?php echo $participant['PROFICIENCYINA']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Proficiency In English</span>
                                        <span class="data-value"><?php echo $participant['PROFICIENCYEN']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Address</span>
                                        <span class="data-value"><?php echo $participant['ADDRESS']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">City</span>
                                        <span class="data-value"><?php echo $participant['CITY']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Country</span>
                                        <span class="data-value"><?php echo $participant['COUNTRYNAMEINA']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Zip Code</span>
                                        <span class="data-value"><?php echo $participant['ZIPCODE']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Phone</span>
                                        <span class="data-value"><?php echo $participant['PHONE']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Fax</span>
                                        <span class="data-value"><?php echo $participant['FAX']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name"></span>
                                        <span class="data-value"><a href="<?php echo base_url()?>staff/modalEditProfileInfo/<?php echo $participant['PARTICIPANTID']?>" id="bt-edit-profile-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal"> Edit</a></span>
                                    </p>
									<?php } ?>
                                </div>
                                <hr>
                                <div class="contact_info">
                                    <h3><i class="fa fa-square"></i> Registration Information</h3>
									<?php if($participant['PROGRAMNAME']!=null) { ?>
                                    <p class="data-row">
                                        <span class="data-name">Academic Year</span>
                                        <span class="data-value"><?php echo $participant['ACADEMICYEAR']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Program</span>
                                        <span class="data-value"><?php echo $participant['PROGRAMNAME']?></span>
                                    </p>
                                    <p class="data-row">
                                        <span class="data-name">Degree</span>
                                        <span class="data-value"><?php echo $participant['DEGREE']?></span>
                                    </p>
                                    <?php if($participant['PROGRAMTYPE']=='ACADEMIC') {
                                    echo '<p class="data-row">';
                                    echo '<span class="data-name">Study Program</span>';
                                    echo '<span class="data-value"> '.$participant['STUDYPROGRAMNAME'].'</span>';
                                    echo '</p>';
                                    }else{
									echo '<p class="data-row">';
                                    echo '<span class="data-name">Course</span>';
                                    echo '<span class="data-value"> '.$participant['COURSENAME'].'</span>';
                                    echo '</p>';
									} ?>
                                    <input type="hidden" id="thisparticipantId" value="<?php echo $participant['PARTICIPANTID']?>">
									<input type="hidden" id="thisprogramType" value="<?php echo $participant['PROGRAMTYPE']?>">
									<input type="hidden" id="thisprogramId" value="<?php echo $participant['PROGRAMID']?>">
									<input type="hidden" id="thisenrollmentId" value="<?php echo $participant['ENROLLMENTID']?>">
									<input type="hidden" id="thisdegreetId" value="<?php echo $participant['DEGREEID']?>">
									<input type="hidden" id="thisacceptstatus" value="<?php echo $participant['ACCEPTANCESTATUS']?>">
									<input type="hidden" id="thislanguagedeliveryId" value="<?php echo $participant['LANGUAGEDELIVERYMAPPINGID']?>">
									<p class="data-row">
                                        <span class="data-name"></span>
                                       <!--  <span class="data-value"><a id="edit_modal_btn" href="<?php echo base_url()?>staff/modalEditRegistrationInfo/<?=$participant['PARTICIPANTID']."/".$participant['PROGRAMTYPE']?>" id="bt-edit-registration-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal2"> Edit</a></span> -->
                                    </p>
									
									<?php } ?>
                                </div>
                                <hr>
                                <div class="contact_info">
                                    <h3><i class="fa fa-square"></i> Family</h3>
                                    <?php if($father!=null) { ?>
                                        <div class="media">
                                            <p class="data-row">
                                                <span class="data-name">Full Name</span>
                                                <span class="data-value"><?php echo htmlentities($father['FULLNAME'], ENT_NOQUOTES, 'UTF-8')?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Relationship</span>
                                                <span class="data-value"><?php echo $father['RELATIONSHIP']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Occupation</span>
                                                <span class="data-value"><?php echo $father['OCCUPATION']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Address</span>
                                                <span class="data-value"><?php echo $father['ADDRESS']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">City</span>
                                                <span class="data-value"><?php echo $father['CITY']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Country</span>
                                                <span class="data-value"><?php echo $father['COUNTRYNAMEENG']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Zip Code</span>
                                                <span class="data-value"><?php echo $father['ZIPCODE']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Phone</span>
                                                <span class="data-value"><?php echo $father['PHONE']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Email</span>
                                                <span class="data-value"><?php echo $father['EMAIL']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name"></span>
                                                <span class="data-value"><a href="<?php echo base_url()?>staff/modalEditFamilyInfo/<?php echo $father['FAMILYID']?>" id="bt-edit-family-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal"> Edit</a></span>
                                            </p>
                                        </div>
                                            <hr class="inner-separator">
                                    <?php } ?>
                                    <?php if($mother!=null) { ?>
                                        <div class="media">
                                            <p class="data-row">
                                                <span class="data-name">Full Name</span>
                                                <span class="data-value"><?php echo htmlentities($mother['FULLNAME'], ENT_NOQUOTES, 'UTF-8')?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Relationship</span>
                                                <span class="data-value"><?php echo $mother['RELATIONSHIP']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Occupation</span>
                                                <span class="data-value"><?php echo $mother['OCCUPATION']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Address</span>
                                                <span class="data-value"><?php echo $mother['ADDRESS']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">City</span>
                                                <span class="data-value"><?php echo $mother['CITY']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Country</span>
                                                <span class="data-value"><?php echo $mother['COUNTRYNAMEENG']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Zip Code</span>
                                                <span class="data-value"><?php echo $mother['ZIPCODE']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Phone</span>
                                                <span class="data-value"><?php echo $mother['PHONE']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Email</span>
                                                <span class="data-value"><?php echo $mother['EMAIL']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name"></span>
                                                <span class="data-value"><a href="<?php echo base_url()?>staff/modalEditFamilyInfo/<?php echo $mother['FAMILYID']?>" id="bt-edit-family-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal"> Edit</a></span>
                                            </p>
                                        </div>
<!--                                        <hr class="inner-separator">-->
                                    <?php } ?>
                                </div>
                                <hr>
                                <div class="contact_info">
                                    <h3><i class="fa fa-square"></i> Education</h3>
                                    <?php //highschool!=null && $participant['DEGREE']=='S1' 
									if(($highschool!=null && $participant['DEGREE']=='S1' && strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') === false) || ($highschool!=null && $participant['DEGREE']=='D3' && strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') === false)){ ?>
                                    <div class="media">
                                        <p class="data-row">
                                            <span class="data-name">School Name</span>
                                            <span class="data-value"><?php echo $highschool['SCHOOLNAME']?></span>
                                        </p>
                                        <p class="data-row">
                                            <span class="data-name">Year</span>
                                            <span class="data-value"><?php echo $highschool['START']?> to <?php echo $highschool['END']?></span>
                                        </p>
                                        <p class="data-row">
                                            <span class="data-name"></span>
                                            <span class="data-value"><a href="<?php echo base_url()?>staff/modalEditEducationInfo/<?php echo $highschool['PARTICIPANTID']?>/<?php echo $highschool['EDUCATIONLEVEL']?>" id="bt-edit-education-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal"> Edit</a></span>
                                        </p>
                                    </div>
                                    <?php } ?>

                                    <?php //$university!=null && $participant['DEGREE']=='S2'
									if($participant['DEGREE']=='S2' && strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') === false && $university!=null){ ?>
                                        <div class="media">
                                            <p class="data-row">
                                                <span class="data-name">University/College Name</span>
                                                <span class="data-value"><?php echo $university['SCHOOLNAME']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Year</span>
                                                <span class="data-value"><?php echo $university['START']?> to <?php echo $university['END']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">GPA (Out of 4.0)</span>
                                                <span class="data-value"><?php echo $university['DEGREES']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name"></span>
                                                <span class="data-value"><a href="<?php echo base_url()?>staff/modalEditEducationInfo/<?php echo $university['PARTICIPANTID']?>/<?php echo $university['EDUCATIONLEVEL']?>" id="bt-edit-education-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal"> Edit</a></span>
                                            </p>
                                        </div>
                                    <?php } ?>
									<?php //$university!=null && $participant['DEGREE']=='S2'
									if(strpos($participant['PROGRAMNAME'], 'STUDENT EXCHANGE') !== false){ ?>
                                        <div class="media">
                                            <p class="data-row">
                                                <span class="data-name">University/College Name</span>
                                                <span class="data-value"><?php echo $university['SCHOOLNAME']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Semester</span>
                                                <span class="data-value"><?php echo $university['GRADE']?> </span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name"></span>
                                                <span class="data-value"><a href="<?php echo base_url()?>staff/modalEditEducationInfo/<?php echo $university['PARTICIPANTID']?>/<?php echo $university['EDUCATIONLEVEL']?>" id="bt-edit-education-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal"> Edit</a></span>
                                            </p>
                                        </div>
                                    <?php } ?>
									<?php //$university!=null && $participant['DEGREE']=='S2'
									if($participant['PROGRAMTYPE']=='NON_ACADEMIC'){ ?>
                                        <div class="media">
                                            <p class="data-row">
                                                <span class="data-name">University/College Name</span>
                                                <span class="data-value"><?php echo $university['SCHOOLNAME']?></span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name">Study Program</span>
                                                <span class="data-value"><?php echo $university['STUDYPROGRAM']?> </span>
                                            </p>
                                            <p class="data-row">
                                                <span class="data-name"></span>
                                                <span class="data-value"><a href="<?php echo base_url()?>staff/modalEditEducationInfo/<?php echo $university['PARTICIPANTID']?>/<?php echo $university['EDUCATIONLEVEL']?>" id="bt-edit-education-info" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-modal"> Edit</a></span>
                                            </p>
                                        </div>
                                    <?php } ?>
                                </div>
                                <hr>
                            <!--    <div class="about">
                                    <h3><i class="fa fa-square"></i> About Me</h3>
                                    <p></p>
                                </div>-->
                            </div>
                        </div>
						
                    </div>
                </div>
                <!-- END PROFILE TAB CONTENT -->

				 <!-- PARTICIPANT CARD TAB CONTENT -->
                <div class="tab-pane activity" id="participantCard-tab">
				 <div class="project-section activity">
						<input type="hidden" name="thisisparticipantId" value="<?php echo $participant['PARTICIPANTID']?>">
						<div class="basic-info" id="DivIdToPrint">
                                <style>

                                    @media all {
                                        .page-break	{ display: none; }
                                    }

                                    @media print {
                                        .page-break	{ display: block; page-break-before: always; }
                                    }

                                </style>


                                <button class="btn btn-success" id="printPage" style="float: right">Print</button>
                                <h3 style="color: #6a1212"> Participant Card </h3>
                                <table class="table" border="0" >
                                    <tbody>
                                        <tr>
                                            <td rowspan="6" width="15%"><img  src="<?php echo base_url().$participant['PHOTOURL'].$participant['PHOTOFILENAME']; ?>" style="max-width: 165px; max-height: 250px;border: 1px solid #ab1d1d; image-orientation: from-image;"></td>
                                            <td width="15%"><strong>Full Name</strong></td>
                                            <td width="70%" colspan="2"><strong><?php echo htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8') ?></strong></td>

                                        </tr>
                                        <tr>
											<td><strong>Participant Number</strong></td>
                                            <td colspan="2"><?php echo $participant['ADMISSIONID']; ?></td>
                                        </tr>
                                        <tr>
											<td><strong>Select Program</strong></td>
                                            <td colspan="2"><?php echo $participant['PROGRAMNAME'] ?></td>
                                        </tr>
										<?php if($participant['PROGRAMTYPE']=='ACADEMIC'){?>
										<tr>
											<td><strong>Study Program</strong></td>
                                            <td colspan="2"><?php echo $participant['STUDYPROGRAMNAME']; ?></td>
                                        </tr>
										 <tr style="line-height: 10px;">
											<td><strong>Faculty</strong></td>
                                            <td colspan="2"><?php echo $participant['FACULTYNAMEENG']; ?></td>
                                        </tr>
										<?php }else{ ?>
										<tr>
											<td><strong>Course</strong></td>
                                            <td colspan="2"><?php echo $participant['COURSENAME']; ?></td>
                                        </tr>	
										<?php } ?>
                                        <tr style="line-height: 10px;">
											<td><strong>Interview Date</strong></td>
                                            <td colspan="2"><?php echo str_replace(' ','',str_replace('00:00:00','',$participant['INTERVIEWDATE'])); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
								<h5>For further information, please contact us at:</h5>
								<h3 style="color: #6a1212"> The International Office of Telkom University </h3>
                                <table class="table">
                                    <tbody>
                                        <tr>
											<td width="15%"><strong>Phone</strong></td>
                                            <td>+62 22 7564108 ext 2400</td>										
                                        </tr>
										<tr>
											<td width=""><strong>Mobile Phone (What’s App, Line)</strong></td>
                                            <td>+62 813 2112 3400</td>
                                        </tr>
                                        <tr>
											<td><strong>Web</strong></td>
                                            <td><a href="https://io.telkomuniversity.ac.id">https://io.telkomuniversity.ac.id</a></td>
                                        </tr>
                                        <tr>
											<td width="20%"><strong>Email</strong></td>
                                            <td>admission@io.telkomuniversity.ac.id</td>
                                        </tr>
                                    </tbody>
                                </table>
                               
                            </div>
						</div>
                   
                </div>
                <!-- END PARTICIPANT CARD TAB CONTENT -->
				
                <!-- DATA VERIFICATION TAB CONTENT -->
                <div class="tab-pane activity" id="verification-tab">
				  <form action="<?php echo base_url()?>staff/saveRequirement" enctype="multipart/form-data" method="post" role="form">
						<input type="hidden" name="thisisparticipantId" value="<?php echo $participant['PARTICIPANTID']?>">
                    <div class="project-section activity">
                        <ul class="list-unstyled activity-list">
						 
                            <?php
                            foreach ($requirements as $rq){
                                $iconstatus = "";
                                $times = "";
                                $privilege = $rq['PRIVILEGE'];
                                if ($rq['VALUE']!="" OR $rq['FILENAME']!="") { //PRIVILEGE 1 (PARTICIPANT)UTK DIVERIFIKASI
									if($privilege==1){
										if($rq['VERIFICATIONSTATUS']=="VERIFIED"){
											$iconstatus = "icon-success fa-check";
											$status = 'Verified';
										}
										else{
											$iconstatus = "icon-warning fa-question";
											$status = 'Need to verify';
										}
										$updatedate = $rq['VERIFICATIONDATE']==""?"No time set":date('d F Y',strtotime($rq['VERIFICATIONDATE']));									
										$content2 = '<span class="timestamp">Verification date: '.$updatedate.'</span>';
									}else if($privilege==3 AND $rq['TYPE']=='ATTACHMENT'){//PRIVILEGE 3 (STAFF) HASIL UPLOAD/INSERT		
										$iconstatus = "icon-success fa-check";
										$status = 'Verified';
										$updatedate = $rq['INPUTDATE']==""?"No time set":date('d F Y',strtotime($rq['INPUTDATE']));	
										$content2 = '<span class="timestamp">Upload successfully : '.$updatedate.'</span>';
									}else{
										$iconstatus = "icon-success fa-check";
										$status = 'Verified';
										$updatedate = $rq['INPUTDATE']==""?"No time set":date('d F Y',strtotime($rq['INPUTDATE']));	
										$content2 = '<span class="timestamp">Insert successfully : '.$updatedate.'</span>';
									}
                                    
								?>
								<?php    }
								else {
									if($privilege==3){
                                        $iconstatus = "icon-warning fa-pencil";
                                        $status = 'Set value';
										if($rq['TYPE']=='ATTACHMENT'){
											$type='file';
											$bawah = '<p class="help-block"><em>Valid file type: .jpg, .jpeg, .pdf. File size max: 1 MB</em></p>';
										}else{
											$type='text';
											$bawah ='';
										}	
										$content2 = '<div class="col-md-3 pull-left" ><input type="'.$type.'" class="data form-control" name="'.$rq['REQUIREMENTID'].'_data" id="'.$rq['REQUIREMENTID'].'_data"/></div><div class="col-md-3"><button type="submit" onclick="return submitData('.$rq['REQUIREMENTID'].')" class="btn btn-sm btn-primary" id="thisData">Save</button></div><br></br>'.$bawah;		
										echo '<input type="hidden" name="thisisrequirementId[]" value="'.$rq['REQUIREMENTID'].'">';
								   
									}else{
										$iconstatus = "icon-danger fa-times";
                                        $status = 'Not set';
										if($rq['ISREQUIRED']=='NO'){								
											$content2 = '<span class="timestamp">This requirement is not required</span>';
										}else{
											$updatedate = $rq['VERIFICATIONDATE']==""?"No time set":date('d F Y',strtotime($rq['VERIFICATIONDATE']));									
											$content2 = '<span class="timestamp">Verification date: '.$updatedate.'</span>';
										}										
									}
                                }
								$content = '<a href="'.base_url().'staff/participantDetail/'.$participantId.'/viewRequirement/'.$rq['REQUIREMENTID'].'" title="'.$status.'" class="text text-info" data-toggle="modal" data-target="#edit-modal">'.$rq['REQUIREMENTNAME'].'</a>';
                                ?>
                                <li>
                                    <i class="fa fa-2x activity-icon <?php echo $iconstatus?> pull-left"></i>
                                    <p>
										<?php echo $content; 
										echo $content2; ?>
                                    </p>
                                </li>
								
                            <?php } ?>
							   
                        </ul>
                    </div>
					</form>
                </div>
                <!-- END DATA VERIFICATION TAB CONTENT -->


                <!-- INTERVIEW TAB CONTENT -->
                <div class="tab-pane activity" id="interview-tab">
                    <div class="project-section activity">
                        <form action="<?php echo base_url()?>staff/updateInterview" method="post" class="form-horizontal" role="form">
                            <fieldset>
                            <div class="form-group">
                                <input type="hidden" id="participantid" name="participantid" value="<?php echo $participantId?>">
                                <label for="ticket-type" class="col-sm-2 control-label">Interview Status</label>
                                <div class="col-sm-4">
                                   <select name="interviewStatus" class="form-control">
                                       <option value="">-select status-</option>
										<option value="Accept" <?php if($participant['INTERVIEWSTATUS'] == 'Accept') echo 'selected';  ?>>Accept</option>
										<option value="Reject" <?php if($participant['INTERVIEWSTATUS'] == 'Reject') echo 'selected';  ?>>Reject</option>
                                   </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Interview Date</label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="interviewDate" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['INTERVIEWDATE']));?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Score</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="score" value="<?php echo $participant['INTERVIEWSCORE']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-custom-primary" data-rel="back"><i class="fa fa-floppy-o"></i> Save</button>
                                </div>
                            </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <!-- END INTERVIEW TAB CONTENT -->

                <!-- SETTINGS TAB CONTENT -->
                <div class="tab-pane settings" id="acceptance-tab">
                    <form action="<?php echo base_url()?>staff/setAcceptanceStatus" method="post" name="set_status" class="form-horizontal" onsubmit="return validateForm()" role="form">
					 <input type="hidden" value="<?php echo $participantId?>" name="idparticipant"/>
					 <input type="hidden" value="<?php echo $participant['USERID']?>" id="userid" name="userid"/>
                        <input type="hidden" data-program-type="<?php echo $participant['PROGRAMTYPE']?>" value="" data-type="<?php echo $typeAccept?>" data-program="<?php echo $idProgram?>"  data-enrollmentid="<?php echo $enrollmentid?>" data-degree="<?php echo $degree?>" data-statusSubject="<?php echo $statusSubject?>" data-programName="<?php echo $programName?>" data-beasiswa="<?php echo $typeBeasiswa?>" id="i" name="participantid" >
                        <input type="hidden" name="thisprogramname" id="thisprogramname">
						 <input type="hidden" name="programdegreeid" id="programdegree-id">
                         <input type="hidden" name="language_delivery_mapping_id" id="languagedeliverymapping-id">
                         <input type="hidden" name="statussubjectsetting" id="statussubjectsetting" value="">
						<fieldset>
						<?php
							if(($participant['INTERVIEWSTATUS']!='' && $participant['INTERVIEWSCORE']!=0 && $participant['PROGRAMTYPE']=='ACADEMIC') || ($participant['PROGRAMTYPE']=='NON_ACADEMIC' && $participant['CURRENTSTEP']==7)){?>
                            <h3><i class="fa fa-square"></i> Acceptance Status</h3>	
							<div class="form-group">
							<div class="col-md-5">
							<?php
								if($participant['ACCEPTANCESTATUS'] == 'ACCEPTED'){
									if(strpos($participant['PROGRAMNAME'],'STUDENT EXCHANGE') !== false || strpos($participant['PROGRAMNAME'],'CREDIT EARNING') !== false){
                    echo '<a href="'.base_url().'Pdf/templateTypeSECE/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
                  }
                  else if(strpos($participant['PROGRAMNAME'],'REGULAR') !== false){
                    echo '<a href="'.base_url().'Pdf/templateTypeNone/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
                  }else
                          if(strpos($participant['PROGRAMNAME'],'SCHOLARSHIP') !== false){//$participant['PROGRAMID'] == '3' || $participant['PROGRAMID'] == 3){//SCHOLARSHIP
													if($participant['SCHOLARSHIPTYPEID']=='1'){
														echo '<a href="'.base_url().'Pdf/templateTypeA/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='2'){
														echo '<a href="'.base_url().'Pdf/templateTypeB/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='3'){
														echo '<a href="'.base_url().'Pdf/templateTypeC/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='4'){
														echo '<a href="'.base_url().'Pdf/templateTypeD/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='5'){
														echo '<a href="'.base_url().'Pdf/templateTypeX/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}?><!--STUDENT EXCHANGE-->
                                                <?php }
								}
							?>
							</div>	
								</div>							
							<div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Fullname</label> 
                                <div class="col-sm-5">
                                    <input type="text" class="form-control"  value="<?php echo $participant['FULLNAME']?>" readOnly>
                                </div>
                            </div>							
                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Status</label> 
                                <div class="col-sm-5">
                                    <select name="acceptancestatus" id="acceptancestatus" class="select form-control">
                                        <option value="">NOT SET</option>
                                        <option value="ACCEPTED" <?php if($participant['ACCEPTANCESTATUS'] == 'ACCEPTED') echo 'selected';  ?>>ACCEPTED</option>
                                        <option value="UNACCEPTED" <?php if($participant['ACCEPTANCESTATUS'] == 'UNACCEPTED') echo 'selected';  ?>>NOT ACCEPTED</option>
                                    </select>
                                </div>
                            </div>
                            <div id="datePublish">
                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Publish Acceptance Status Date</label>
								<div class="col-sm-2">
                                    <input type="date" class="form-control"  name="publishdate" value="<?php echo $participant['PUBLISHDATE']?>">
                                    
                                </div>
                                <div class="col-sm-2">
                                    <input type="time" class="form-control"  name="publishtime" value="<?php echo $participant['PUBLISHTIME']?>">
                                    
                                </div>
                            </div>
							 </div>
							<div id="iniacceptanceType">
							 <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Acceptance Type</label>
                                <div class="col-sm-5">
                                    <select name="acceptanceType" id="acceptanceType" class="select form-control">
                                        <option value="">NOT SET</option>
										<?php
                                                    if(!empty($listProgram))
                                                    {

                                                        foreach ($listProgram as $program) {
                                                        $selected = $program['PROGRAMID'] == $participant['PROGRAMID'] && $participant['ACCEPTANCESTATUS'] == 'ACCEPTED' ? 'selected' : '';
                                                ?>
                                                        <option data-programname="<?php echo $program['PROGRAMNAME']; ?>" data-subjectstatus="<?php echo $program['STATUSSUBJECTSETTING']; ?>" data-type="<?php echo $program['PROGRAMTYPE']; ?>" value="<?php echo $program['PROGRAMID'];?>" <?php echo $selected; ?>><?php echo $program['PROGRAMNAME']; ?></option>
														
                                                <?php
                                                        }
                                                    }
                                                ?>
                                    </select>
                                </div>
                            </div>
							</div>
							<div id="dataEnrollment">
							<div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Enrollment</label>
                                <div class="col-sm-5">
                                    <select id="selectEnrollment" name="enrollment_id" class="select form-control">
										<option value="">-- Select Enrollment --</option>
										<?php if(!empty($listEnrollment)){
												foreach ($listEnrollment as $enrollment) {
													if($enrollment['ENROLLMENTID'] == $participant['ENROLLMENTID'] && $participant['ACCEPTANCESTATUS']=='ACCEPTED'){                                                    ?>
                                                        <option value="<?php echo $enrollment['ENROLLMENTID'];?>" selected="selected"><?php echo $enrollment['DESCRIPTION']; ?></option>
                                        <?php
													}
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
							</div>
              <div id="dataSubject" style="margin-bottom: 10px;">
                <div class="table-primary">
                                            <h5><b>Daftar Mata Kuliah yang telah diplih</b></h5>
                                            <table id="tableMapping" class="table table-sorting table-hover  table-striped datatable" style="width:60%;" align="left">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Matakuliah</th>                                     
                                                        <th>Study Program</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
              </div>
              
							<div id="dataDegree">
							<div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Degree</label>
                                <div class="col-sm-5">
									<input type="text" name="degreename" id="degreeId" readonly class="form-control">
                                    <input type="hidden" name="degreeid" id="degreeIdHidden">
                                </div>
                            </div>
							</div>
							<div id="dataLanguage">
							<div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Language Delivery</label>
                                <div class="col-sm-5">
                                   	<select id="selectLanguage" name="language_delivery_id" class="select form-control">
                                                <option value=""> -- Select Language Delivery -- </option>
                                                
                                                 <?php
                                                    if(!empty($listLanguageDelivery))
                                                    {
                                                        foreach ($listLanguageDelivery as $data) {
                                                        if($data['LANGUAGEDELIVERYID'] == $languageDeliveryId['LANGUAGEDELIVERYID'] && $participant['ACCEPTANCESTATUS']=='ACCEPTED'){
															
                                                ?>
                                                        <option value="<?php echo $data['LANGUAGEDELIVERYID'];?>" selected="selected"><?php echo $data['LANGUAGE']; ?></option>
                                                <?php
															}else{ ?>
														<option value="<?php echo $data['LANGUAGEDELIVERYID'];?>"><?php echo $data['LANGUAGE']; ?></option>	
												<?php		}
                                                        }
                                                    }
                                                ?>
                                            </select>
                                </div>
                            </div>
							</div>
							<div id="dataStudy">
							<div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Study Program</label>
                                <div class="col-sm-5">
                                   <select id="selectStudyprogram" name="study_program_id" class="select form-control">
                                        <option value=""> -- Select Study Program -- </option>
                                                <?php
                                                    if(!empty($listStudyProgram))
                                                    {

                                                        foreach ($listStudyProgram as $data) {
                                                       if($data['STUDYPROGRAMID'] == $participant['STUDYPROGRAMID'] && $participant['ACCEPTANCESTATUS']=='ACCEPTED'){
                                                ?>
                                                        <option value="<?php echo $data['STUDYPROGRAMID'];?>" selected="selected"><?php echo $data['STUDYPROGRAMNAME']; ?></option>
                                                <?php
													   }
                                                        }
                                                    }
                                                ?> 

                                           </select>
                                </div>
                            </div>
							</div>
							<div class="form-group" id="dataCourse">
                                        <label for="ticket-type" class="col-sm-2 control-label">Course</label>
                                        <div class="col-sm-5">
                                            <select id="selectCourse" name="study_course_id" class="select form-control">
                                            <option value=""> -- Select Course-- </option>
                                          
                                                  <?php
                                                    if(!empty($listCourse))
                                                    {

                                                        foreach ($listCourse as $data) {
                                                       if($data['COURSEID'] == $participant['COURSEID']){
                                                ?>
                                                        <option value="<?php echo $data['COURSEID'];?>" selected="selected"><?php echo $data['COURSENAME']; ?></option>
                                                <?php
													   }
                                                        }
                                                    }
                                                ?> 

                                           </select>
                                        </div>
                                    </div>
							 <div class="form-group" id="academicYear">
										<label for="ticket-type" class="col-sm-2 control-label">Academic Year</label>
                                        <div class="col-sm-2">
                                           <input type="number" style="" class="date-picker-year form-control" name="academicYear" value="<?php echo substr($academicYear['SCHOOLYEAR'],0,4); ?>" >
                                        </div>
										<label for="ticket-type" class="col-sm-1 control-label">&nbsp&nbsp&nbsp&nbsp&nbsp/</label>
                                        <div class="col-sm-2">
                                            <input type="number" style="" class="date-picker-year form-control" name="academicYear2" value="<?php echo substr($academicYear['SCHOOLYEAR'],5,4); ?>" >
                                        </div>
                                    </div>
								<div class="form-group" id="scholarshipType">
                                <label for="ticket-type" class="col-sm-2 control-label">Scholarship Type</label>
                                <div class="col-sm-5">
                                    <select name="typeScholarship" id="typeScholarship" class="select form-control">
                                        <option value="" selected>NOT SET</option>
										<?php if(!empty($listScholarship)){
												foreach ($listScholarship as $scholarship) {
													if($scholarship['SCHOLARSHIPTYPEID'] == $participant['SCHOLARSHIPTYPEID'] && $participant['ACCEPTANCESTATUS']=='ACCEPTED'){                                                    ?>
                                                        <option value="<?php echo $scholarship['SCHOLARSHIPTYPEID'];?>" selected="selected"><?php echo $scholarship['SCHOLARSHIPTYPENAME']; ?></option>
                                        <?php
													}else{ ?>
														<option value="<?php echo $scholarship['SCHOLARSHIPTYPEID'];?>"><?php echo $scholarship['SCHOLARSHIPTYPENAME']; ?></option>
										<?php		}
                                                }
                                            }
                                        ?>
                                        <!--<option value="1" <?php if($participant['SCHOLARSHIPTYPEID'] == '1' && $participant['ACCEPTANCESTATUS'] == 'ACCEPTED') echo 'selected';  ?>>A Type Scholarship</option>
                                        <option value="2" <?php if($participant['SCHOLARSHIPTYPEID'] == '2' && $participant['ACCEPTANCESTATUS'] == 'ACCEPTED') echo 'selected';  ?>>B Type Scholarship</option>
										<option value="3" <?php if($participant['SCHOLARSHIPTYPEID'] == '3' && $participant['ACCEPTANCESTATUS'] == 'ACCEPTED') echo 'selected';  ?>>C Type Scholarship</option>
										<option value="4" <?php if($participant['SCHOLARSHIPTYPEID'] == '4' && $participant['ACCEPTANCESTATUS'] == 'ACCEPTED') echo 'selected';  ?>>D Type Scholarship</option>-->
                                    </select>
                                </div>
							</div>
							<div id="dateAccept">
                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Accept Date</label>
                                <div class="col-sm-5">
                                    <input type="date" class="form-control" name="acceptancedate" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))?>">
                                </div>
                            </div>
							 </div>
							 <div id="datePayment">
                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Payment Date</label>
                                <div class="col-sm-5">
                                    <input type="date" class="form-control" name="paymentdate" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))?>">
                                </div>
                            </div>
							 </div>
							 <div id="dateDeadline">
                             <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Deadline Date</label>
                                <div class="col-sm-5">
                                    <input type="date" class="form-control" name="deadlinedate" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))?>">
                                </div>
                            </div>
							 </div>
							 <div id="dateArrival">
                             <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Arrival Date</label>
                                <div class="col-sm-5">
                                    <input type="date" class="form-control" name="arrivaldate" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))?>">
                                </div>
                            </div>
							</div>
							<div id="dateStarting">
                             <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Starting Date</label>
                                <div class="col-sm-5">
                                    <input type="date" class="form-control" name="startingdate" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE']))?>">
                                </div>
                            </div>
							</div>
							 <div id="dateStudentExchange">
                            <div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Date Periode</label>
								<div class="col-sm-2">
                                    <input type="date" class="form-control"  id="startingdateSECE" name="startingdateSECE" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE']))?>">
                                    
                                </div>
								<label for="ticket-type" class="col-sm-1 control-label">&nbsp&nbsp&nbsp&nbsp&nbspto</label>
                                <div class="col-sm-2">
                                    <input type="date" class="datepicker form-control" id="endingdateSECE" name="endingdateSECE" value="<?php echo str_replace(' ','',str_replace('00:00:00','',$participant['ENDINGDATE']))?>">
                                    
                                </div>
                            </div>
							 </div>
                            <div class="form-group">
                                <div class="col-sm-7">
                                    <textarea name="note" class="form-control" rows="5" cols="60"><?php echo $participant['NOTE']?></textarea>
                                </div>
                            </div>
							
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-custom-primary"><i class="fa fa-floppy-o"></i> Set Status</button>
                                </div>
                            </div>
							
							<?php }else if($participant['INTERVIEWSTATUS']=='' && $participant['INTERVIEWSCORE']==0 && $participant['PROGRAMTYPE']=='ACADEMIC'){
								echo '<h3>
										You have not filled in the interview value<br></h3>';
							}else{
								echo '<h3>
										You have not verified all of the documents<br></h3>';
							}?>
                        </fieldset>
					
                    </form>
                    <hr/>
                </div>
                <!-- END SETTINGS TAB CONTENT -->
				<!-- CONFIRMATION OF ATTENDANCE TAB CONTENT -->
                <div class="tab-pane activity" id="confirmation-tab">
                    <div class="project-section activity">
                            <fieldset>
							<div class="form-group">
								<label for="ticket-type" class="col-sm-2 control-label">Upload Confirmation of Admission</label>
								<div class="col-sm-6">
								<?php if($participant['LOAFILE']!=null){
									echo '<a href="'.base_url().$participant['PHOTOURL'].$participant['LOAFILE'].'" target="_blank">'.$participant['LOAFILE'].'</a>';
								}else{echo "Participant Have not Uploaded Confirmation of Admission";}?>
								 </div>
                            </div><br><br>
							<div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Confirm Arrival Date</label>
                                <div class="col-sm-4">
									<input type="date" readonly="readonly" class="form-control" name="arrivaldate" value="<?php echo  str_replace(' ','',str_replace('00:00:00','',$participant['CONFIRMDATE'])) ?>">
                                </div>
                            </div>
							<br><br>
							<div class="form-group">
                                <label for="ticket-type" class="col-sm-2 control-label">Upload Arrival Ticket</label>
                                <div class="col-sm-6">
                                    <?php if ($participant['ARRIVALTICKET'] != null){?>
										<a href="<?php echo base_url().$participant['PHOTOURL'].$participant['ARRIVALTICKET']?>" target="_blank"><?php echo $participant['ARRIVALTICKET']?></a>
                                    <?php }else{echo "Participant Have not Uploaded Ticket";}?>
                                </div>
                            </div>
                            </fieldset>
                    </div>
                </div>
                <!-- END CONFIRMATION TAB CONTENT -->
            </div>
        </div>




    </div>
    <!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!-- MODAL CONTENT -->
<div id="edit-modal2" class="modal fade" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <!-- Content will be loaded here from "remote.php" file -->
        </div>
    </div>
</div>

<div id="edit-modal" class="modal fade" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <!-- Content will be loaded here from "remote.php" file -->
        </div>
    </div>
</div> 

<script type="text/javascript">

tinymce.init({
        selector: "#message",
        theme: "modern",
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons template paste textcolor"
       ],
       content_css: "css/content.css",
       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons", 
       style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    }); 
var baseurl = '<?php echo base_url(); ?>';
	$('#edit-modal2').on('shown.bs.modal', function(){
		$("#enrollment-academic").hide();
    $("#enrollment-nonacademic").hide();
		$("#select-program").select2();
		$("#select-enrollment").select2();
		$("#select-language").select2();
		$("#select-studyprogram").select2();
		$("#select-course").select2();

        $('div.product-chooser').on('click', '.product-chooser-item', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);
        });

        var thisprogramId = $('#thisprogramId').val();
		var thisenrollmentId = $('#thisenrollmentId').val();
		if(thisprogramId!=''){
			 loadEnrollment(thisenrollmentId);
		}

        $('#select-enrollment').on("change", function(e) { 
            var value = $('#select-enrollment').select2('val');

            if(value != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                        data:{ enrollmentid:value},
                        success:function(response)
                        {
                            $('#degree-id').val(response.DEGREE);
                            $('#degree-id-hidden').val(response.DEGREEID);
                            $('#programdegree-id').val(response.PROGRAMDEGREEID);
							
							$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                            $("#select-studyprogram").select2("val", 0);
							$("#study-language-container").show('slow');
                            $('#select-language').empty();
                            $("#select-language").append($('<option>', {value: '', selected: 'selected', text: '-- Select Study Program --'}));
							$("#select-language").append($('<option>', {value: '1', text: 'Indonesian'}));
							$("#select-language").append($('<option>', {value: '2', text: 'English'}));
                            $("#select-language").select2("val", 0);
                            if(response.SHORTCOURSE == true){
                                $('.study-program-container').hide();
                            } else {
                                $('.study-program-container').show();
                                $('#language-delivery-container').show();
                            }
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );
            } else {
                $('#degree-id').val("");
                $('#degree-id-hidden').val("");
                $('#programdegree-id').val("");
                $('.study-program-container').show();
                $('#language-delivery-container').show();
				$("#dataCourse").show();
            }
        });
       // Getstudy();
    function Getstudy(){
			var type = $("#select-program").select2().find(":selected").data("type");
            var programdegreeid = $('#programdegree-id').val();
            var languagedeliveryid = $('#select-language').val();
            if(programdegreeid != "" && languagedeliveryid != "")
            {

                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid, type: type},
                        success:function(response)
                        {
							if(type=='ACADEMIC'){
								$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                            $.each(response, function(index, value) {
                                $("#select-studyprogram").append($('<option>', {value: value['STUDYPROGRAMID'], text: value['STUDYPROGRAMNAME']}));
                            });
                            $("#select-studyprogram").select2("val", 0);
							}else{
								$("#dataCourse").show('slow');
								$('#select-course').empty();
								$("#select-course").append($('<option>', {value: '', text: '-- Select Course --'}));
								$.each(response, function(index, value) {
									$("#select-course").append($('<option>', {value: value['COURSEID'], text: value['COURSENAME']}));
								});
								$("#select-course").select2("val", 0);
								
							}
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );   
            } else {
              //  alert('Please select Enrollment data');
                $("#select-language").select2('val', "");
            }

		

    }        

        $('#select-language').on("change", function(e) { 
        Getstudy();
        });

         $('#select-studyprogram').on("change", function(e) { 
            var programdegreeid = $('#programdegree-id').select2('val');
            var languagedeliveryid = $('#select-language').select2('val');
            // alert(programdegreeid);
            if(programdegreeid != "" && languagedeliveryid != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid},
                        success:function(response)
                        {
							
                           for (i = 0; i < response.length; i++) {
                                    alert(response[i].STUDYPROGRAMNAME);
                                } 
                            // $('#languagedeliverymapping-id').val(response.LANGUAGEDELIVERYMAPPINGID);
                            // $('#studyprogram-name').val(response.STUDYPROGRAMNAME);
                            // $('#studyprogram-id').val(response.STUDYPROGRAMID);

                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );   
            } else {
                alert('Please select Study Program');
                $("#select-studyprogram").select2('val', "");
            }
        });

        $('#select-program').on("change", function(e) { 
            var value = $('#select-program').select2('val');
            var type = $("#select-program").select2().find(":selected").data("type");
			var userid = $("#userid").val();
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value, participantid: userid},
                    success:function(response)
                    {
                        $("#enrollment-academic").hide();
                        $("#enrollment-nonacademic").hide();

                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: 'Select Enrollment Program'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                            });
                            $("#select-enrollment").select2("val", 0);
							
							$('#degree-id').val('');
							$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', selected: 'selected', text: '-- Select Study Program --'}));
                            $("#select-studyprogram").select2("val", 0);
							$("#study-language-container").show('slow');
                            $('#select-language').empty();
                            $("#select-language").append($('<option>', {value: '', selected: 'selected', text: '-- Select Study Program --'}));
							$("#select-language").append($('<option>', {value: '1', text: 'Indonesian'}));
							$("#select-language").append($('<option>', {value: '2', text: 'English'}));
                            $("#select-language").select2("val", 0);
							$('#select-course').empty();
                            $("#select-course").append($('<option>', {value: '', selected: 'selected', text: '-- Select Course --'}));
                            $("#select-course").select2("val", 0);
                        
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            );
        });
		
		function loadEnrollment(enrollment_id) {
        var value = $('#select-program').select2('val');
        var type = $("#select-program").select2().find(":selected").data("type");
		var userid = $("#userid").val();
		//alert(userid);die;
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value, participantid: userid},
                    success:function(response)
                    {
                        
                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: 'Select Enrollment Program'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                            });
                            $("#select-enrollment").select2("val", enrollment_id);
                            loadDegree(enrollment_id);
                        
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            );
    }
	 function loadDegree(enrollment_id)
    {
		
		var type = $("#acceptanceType").select2().find(":selected").data("type");
        $.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                data:{ enrollmentid:enrollment_id},
                success:function(response)
                {
                    $('#degree-id').val(response.DEGREE);
                    $('#degree-id-hidden').val(response.DEGREEID);
                    $('#programdegree-id').val(response.PROGRAMDEGREEID);
                    if(type=='ACADEMIC'){
						$('.study-program-container').show();
					}else{
						$('#dataCourse').show();
					}                        
                        $('#language-delivery-container').show();
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
    }
		
	});
	
	$('#edit-modal2').on('hidden.bs.modal', function () {
		location.reload();
	})
	
    $(document).ready(function() {
      var table1 = $("#tableMapping").dataTable({
            "ajax": {
                "url": "<?php echo base_url()?>staff/dataTableSubjectMappingExchange",
                "type": "POST",
                "data" :{
                  filter: $('input[name="idparticipant"]').val()
                }
            },
            "searching": false,
            "paging":false,
            "bInfo": false,
            "destroy": true,
            "columns": [
               // { "data" : "SUBJECTCODE"},
                // { "data": "ACTION" },
                { "data": "SUBJECTNAME" , width: "50%"},
                { "data": "STUDYPROGRAMNAME", width: "50%" }//,
                // { "data": "LANGUAGE" },
                // { "data": "CREDIT" }
            ],
        });
		var thisacceptstatus = $("#thisacceptstatus").val();
		// if(thisacceptstatus!=''){
		// 	$("#edit_modal_btn").attr("disabled", "disabled");
		// }
    $('.date-picker-year').datepicker({
            format: "yyyy",
			weekStart: 1,
			orientation: "bottom",
			language: "{{ app.request.locale }}",
			keyboardNavigation: false,
			viewMode: "years",
			minViewMode: "years"
        });
        $('#acceptancestatus').on('change',function(){
			var acceptancestatus = $(this).val();
			if(acceptancestatus!='ACCEPTED' && acceptancestatus!='UNACCEPTED'){
				$('#datePayment').hide();
				$('#dateAccept').hide();
				$('#dateArrival').hide();
				$('#dateStarting').hide();
				$('#dateDeadline').hide();
				$('#scholarshipType').hide();
				$('#dataEnrollment').hide();
				$('#dataDegree').hide();
				$('#dataLanguage').hide();
				$('#dataStudy').hide();
				$('#dataCourse').hide();
				$('#iniacceptanceType').hide();
                $('#datePublish').hide();
				$('#academicYear').hide();
				$('#dateStudentExchange').hide();
        $("#dataSubject").hide();
			}else if(acceptancestatus=='ACCEPTED'){
				$('#iniacceptanceType').show();
                $('#datePublish').show();
				$("#acceptanceType").removeAttr("disabled");
				$("#selectEnrollment").removeAttr("disabled");
				$("#selectLanguage").removeAttr("disabled");
				$("#selectStudyprogram").removeAttr("disabled");
				$("#selectCourse").removeAttr("disabled");
			}else if(acceptancestatus=='UNACCEPTED'){
                $('#datePublish').show();
                $('#datePayment').hide();
				$('#dateAccept').hide();
				$('#dateArrival').hide();
				$('#dateStarting').hide();
				$('#dateDeadline').hide();
				$('#dataCourse').hide();
				$('#scholarshipType').hide();
				$('#dataEnrollment').hide();
				$('#dataDegree').hide();
				$('#dataLanguage').hide();
				$('#dataStudy').hide();
				$('#iniacceptanceType').hide();
				$('#academicYear').hide();
				$('#dateStudentExchange').hide();
				$('#iniacceptanceType').hide();
        $("#dataSubject").hide();
            }
		});

        $(".modal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });

        $(document).on("click", ".add-button", function () {
            $('#studyprogramid').select2('val','');
            options.empty();
            $('#language').select2('val','');
            $('.modal-form #programdegreeid').val($(this).data('id'));
        });

        $(document).on("click", "#bt-edit-registration-info", function () {
   //        alert("DETAILL");
			// var thisparticipantId= $('#thisparticipantId').val();
			// var thisprogramType= $('#thisprogramType').val();
			// var thisprogramId= $('#thisprogramId').val();
			// var thisenrollmentId= $('#thisenrollmentId').val();
			// var thisdegreetId= $('#thisdegreetId').val();
			// var thislanguagedeliveryId= $('#thislanguagedeliveryId').val();
   //          $("#edit-registration-info #participantid").val(thisparticipantId);

   //          $("#enrollment").select2({
   //              // minimumResultsForSearch: -1,
   //              placeholder: "Select Enrollment",
   //              allowClear: true
   //          });

   //          $("#edit-registration-info #program").select2({
   //              // minimumResultsForSearch: -1,
   //              placeholder: "Select Program",
   //              allowClear: true
   //          });

   //          $("#edit-registration-info #degree").select2({
   //              // minimumResultsForSearch: -1,
   //              placeholder: "Select Degree",
   //              allowClear: true
   //          });

   //          $("#edit-registration-info #studyprogram").select2({
   //              // minimumResultsForSearch: -1,
   //              placeholder: "Select Study Program",
   //              allowClear: true
   //          });
			
   //          loadProgram(thisprogramType,thisprogramId);
   //          loadEnrollment(thisprogramId,thisenrollmentId);
   //          loadDegree(thisenrollmentId,thisdegreetId);
   //          loadStudyProgram(thisprogramId,thisdegreetId,thislanguagedeliveryId);
        });

        $('#edit-registration-info #program').on("change", function(e) {
            $("#edit-registration-info #enrollment").empty();
            $("#edit-registration-info #enrollment").select2('val','');

            $("#edit-registration-info #degree").empty();
            $("#edit-registration-info #degree").select2('val','');

            $("#edit-registration-info #studyprogram").empty();
            $("#edit-registration-info #studyprogram").select2('val','');
            loadEnrollment(this.value,'');
        });

        $('#edit-registration-info #enrollment').on("change", function(e) {
            $("#edit-registration-info #degree").empty();
            $("#edit-registration-info #degree").select2('val','');

            $("#edit-registration-info #studyprogram").empty();
            $("#edit-registration-info #studyprogram").select2('val','');
            loadDegree(this.value,'');
        });

        $('#edit-registration-info #degree').on("change", function(e) {
            $("#edit-registration-info #studyprogram").empty();
            $("#edit-registration-info #studyprogram").select2('val','');
            loadStudyProgram($('#program').val(),this.value,'')
        });

        $('#edit-registration-info-form').validate({
            rules: {
                enrollment: {
                    required: true
                },
                program: {
                    required: true
                },
                degree: {
                    required: true
                },
                studyprogram: {
                    required: true
                }
            },
            submitHandler: function (form) {
                var participantid   = $("#edit-registration-info #participantid").val();
                var enrollment      = $("#edit-registration-info #enrollment").val();
                var program         = $("#edit-registration-info #program").val();
                var degree          = $("#edit-registration-info #degree").val();
                var studyprogram = $("#edit-registration-info #studyprogram").val();

                $.ajax({
                    type    : "POST",
                    data    : {participantid:participantid, enrollment:enrollment, program:program, degree:degree, studyprogram:studyprogram},
                    url     : "<?php echo base_url()?>staff/updateRegistInfo",
                    success : function(data) {
                        location.reload();
                        return false;
                    },
                    error   : function (e) {
                        console.log(e);
                        return false;
                    }
                });
            }
        });
		
		<?php if($this->session->flashdata('pesan')!=null) { ?>
			$(document).ready(function(){
			alert('SET STATUS SUCCESSFULL');
			});
		<?php } ?>
		<?php if($this->session->flashdata('eror')!=null) { ?>
			$(document).ready(function(){
			alert('FAILED SET STATUS');
			});
		<?php } ?>
		
		$('#printPage').click(function(){
			alert("Thank you for completing your registration, kindly check your interview schedule as mentioned in your participant card. We will contact you shortly for the detail interview.");
            printDiv();
            $('#printPage').show();
            $('.action-buttons').show();
        });

        function printDiv() {
            $('#printPage').hide();
            $('.action-buttons').hide();

            var divToPrint = document.getElementById('DivIdToPrint');
            var newWin = window.open('', 'Print-Window');
            newWin.document.title = "Print Data";
            newWin.document.body.style.cssText = 'font-family:Arial !important';
            newWin.document.body.style.cssText = 'font-size:.7em !important';
            newWin.document.open();
            var css = "";
            css += " <link href='<?php echo base_url(); ?>themes/_assets/css/bootstrap.min.css' rel='stylesheet' type='text/css'>";
            css += "<style> pagebreak { page-break-after: always; }.beforepagebreak { page-break-before: always; }</style>";
            css += "<style> * {font: 11px arial;} </style>";
            css += "<style> .tablebody tr {line-height: 11px;} </style>";
            newWin.document.write('<html><head>' + css + '</head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
			newWin.document.close();
	   }
		
    });
	

    function loadProgram(str,val){
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url()?>staff/ajaxFetchProgram",
            data    : {programtype: str},
            success : function (data) {
                data = $.parseJSON( data );
                var options = $("#edit-registration-info #program");
                options.empty();

                $.each(data, function() {
                    options.append($("<option />").val(this.PROGRAMID).text(this.PROGRAMNAME));
                });
                options.select2("val",val);
            },
            error : function (e) {
                console.log(e.responseText);
            }
        });
    }

    function loadDegree(str,val){
		var acceptstatus = $("#edit-registration-info #acceptstatus").val();
		var thisacceptstatus = $("#thisacceptstatus").val();
		//alert(thisacceptstatus);die;
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url()?>staff/ajaxFetchDegree",
            data    : {enrollmentid: str},
            success : function (data) {
                data = $.parseJSON( data );
                var options = $("#edit-registration-info #degree");

                options.empty();
                $("#edit-registration-info #studyprogram").empty();

                $.each(data, function() {
                    options.append($("<option />").val(this.DEGREEID).text(this.DEGREE));
                });
                options.select2("val",val);
				if(thisacceptstatus!=''){
						$("#edit-registration-info-form #select-program").attr("disabled", "disabled");
						$("#edit-registration-info-form #select-enrollment").attr("disabled", "disabled");
						$("#edit-registration-info-form #select-language").attr("disabled", "disabled");
						$("#edit-registration-info-form #select-studyprogram").attr("disabled", "disabled");
						$("#edit-registration-info-form #select-course").attr("disabled", "disabled");
					}
            },
            error : function (e) {
                console.log(e.responseText);
            }
        });
    }
	
	function loadEnrollment(str,val){
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url()?>staff/ajaxFetchEnrollment",
            data    : {programid: str},
            success : function (data) {
                data = $.parseJSON( data );
                var options = $("#edit-registration-info #enrollment");
                options.empty();

                $.each(data, function() {
                    options.append($("<option />").val(this.ENROLLMENTID).text(this.DESCRIPTION));
                });
                options.select2("val",val);
            },
            error : function (e) {
                console.log(e.responseText);
            }
        });
    }

    function loadStudyProgram(programid,degreeid,val){
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url()?>staff/ajaxFetchStudyProgram",
            data    : {programid: programid, degreeid:degreeid},
            success : function (data) {
                data = $.parseJSON( data );

                var options = $("#edit-registration-info #studyprogram");
                options.empty();
                $.each(data, function() {
                    options.append($("<option />").val(this.LANGUAGEDELIVERYMAPPINGID).text(this.STUDYPROGRAMNAME+' - '+this.LANGUAGE));
                });
                options.select2("val",val);
            },
            error : function (e) {
                console.log(e.responseText);
            }
        });
    }
</script>
<script>
		$('#datePayment').hide();
		$('#dateAccept').hide();
		$('#dateArrival').hide();
		$('#dateStarting').hide();
		$('#dateDeadline').hide();
		$('#scholarshipType').hide();
		$('#dataEnrollment').hide();
		$('#dataDegree').hide();
		$('#dataLanguage').hide();
		$('#dataStudy').hide();
		$('#academicYear').hide();
		$('#dataCourse').hide();
		$('#dateStudentExchange').hide();
		$("#dataSubject").hide();
		var typeAccept = $('#i').attr('data-type');
		var typeBeasiswa = $('#i').attr('data-beasiswa');
		var idProgram = $('#i').attr('data-program');
		var programType = $('#i').attr('data-program-type');
    var programName =  $('#i').attr('data-programName');
     var statusSubject = $("#i").attr('data-statusSubject');
     var enrollmentid = $("#i").attr('data-enrollmentid');
     $("#thisprogramname").val(programName);
		if(programType=='NON_ACADEMIC'){
			$("#interview-tab").hide();
			$("#interviewTab").hide();
			$("#confirmation-tab").hide();
			$("#confirmationTab").hide();
		}
		
		if(typeAccept=='ACCEPTED'){
			$('#dataEnrollment').show('slow');
			$('#dataDegree').show('slow');
			//$('#dataLanguage').show('slow');
			if(programType!='ACADEMIC'){
				$('#dataCourse').show('slow');
			}else{
        if(statusSubject=='Y'){
          $("#dataSubject").show('slow');
          $("#selectEnrollment").select("val", enrollmentid);
          $('#dataStudy').show('slow');
          //getStudySubject();
          //alert();
        }else{
          $('#dataLanguage').show('slow');
          $('#dataStudy').show('slow');
        }
				$('#academicYear').show('slow');
			}		
			
			if(programName.includes('REGULAR') == true && programType=='ACADEMIC'){//(idProgram=='5' || idProgram=='4') && programType=='ACADEMIC'){//REGULAR
				$("#dateAccept").show('slow');
				$("#datePayment").show('slow');				
				$("#dateDeadline").show('slow');
				$("#dateArrival").show('slow');
				$("#dateStarting").show('slow');
				$('#dateStudentExchange').hide();
			}else if(programName.includes('STUDENT EXCHANGE') == true && programType=='ACADEMIC'){//(idProgram=='1' || idProgram=='2') && programType=='ACADEMIC'){//STUDENT EXCHANGE & CREDIT EARNING
				$('#datePayment').show('show');
				$("#dateAccept").show('slow');
				$("#dateDeadline").show('slow');
				$("#dateArrival").show('slow');
				$('#dateStarting').hide();
				$('#dateStudentExchange').show('slow');
			}else if(programName.includes('SCHOLARSHIP') == true && programType=='ACADEMIC'){//idProgram=='3' && programType=='ACADEMIC'){
				$('#datePayment').hide();
				$("#scholarshipType").show('slow');
				$("#dateAccept").show('slow');
				$("#dateDeadline").show('slow');
				$("#dateArrival").show('slow');
				$("#dateStarting").show('slow');
				$('#dateStudentExchange').hide();
				//if(typeBeasiswa=='4'){
					$('#datePayment').show('slow');
						document.getElementById("datePayment").required = true;
				//}
			}else if(programType!='ACADEMIC'){
				$("#dateAccept").hide();
				$("#datePayment").hide();				
				$("#dateDeadline").hide();
				$("#dateStarting").hide();
				$('#dateStudentExchange').hide();
				$("#scholarshipType").hide();
				$('#dateArrival').show();
			}
		}else{
			$('#iniacceptanceType').hide();
		}
		
		if(typeAccept!='' && typeAccept!=null){
			$("#acceptanceType").attr("disabled", "disabled");
			$("#selectEnrollment").attr("disabled", "disabled");
			$("#selectLanguage").attr("disabled", "disabled");
			$("#selectStudyprogram").attr("disabled", "disabled");
			$("#selectCourse").attr("disabled", "disabled");
		}
				
		$('#acceptanceType').on('change',function(){
      //alert();
			var userid = $("#userid").val();
			var acceptanceType = $(this).val();
      var subjectstatus = $("#acceptanceType").find(":selected").data("subjectstatus");
      var programname = $("#acceptanceType").find(":selected").data("programname");
      console.log(subjectstatus);
      console.log(programname);
			var statusAcceptance = $('#acceptancestatus').val();
      var enrollmentid = $("#i").attr('data-enrollmentid');
      var degree = $("#i").attr('data-degree');
      console.log(enrollmentid);
      console.log(degree);
      $("#thisprogramname").val(programname);
      $("#statussubjectsetting").val(subjectstatus);
			$('#dataEnrollment').show('slow');
			$('#dataDegree').show('slow');
			//$('#dataLanguage').show('slow');
			if(programType!='ACADEMIC'){
				$('#dataCourse').show('slow');
        $("#dataSubject").hide();
        $('#dataStudy').hide();
			}else{
        if(subjectstatus=='Y'){
          $("#dataSubject").show('slow');
          $('#dataStudy').show();
          $('#dataLanguage').hide();
          
         // alert(degree);
          getStudySubject();
        }else{
          $('#dataStudy').show('slow');
          $('#dataLanguage').show('slow');
          $("#dataSubject").hide();
          $("#selectEnrollment").select("val", '');
        }				
				$('#academicYear').show('slow');
			}
			
			if(programname.includes('REGULAR')==true && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC'){//(acceptanceType=='5' && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC') || (acceptanceType=='4' && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC')){
				$("#scholarshipType").hide();
				$("#dateAccept").show('slow');
				$("#datePayment").show('slow');
				$("#dateDeadline").show('slow');
				$("#dateArrival").show('slow');
				$("#dateStarting").show('slow');
				$('#dateStudentExchange').hide();
			}else if(programname.includes('SCHOLARSHIP')==true && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC'){//acceptanceType=='3' && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC'){
				$("#scholarshipType").show('slow');
				$('#datePayment').hide();
				$('#dateAccept').hide();
				$('#dateArrival').hide();
				$('#dateStarting').hide();
				$('#dateDeadline').hide();
				$('#dateStudentExchange').hide();
			}else if((programname.includes('STUDENT EXCHANGE')==true && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC') || (programname.includes('CREDIT EARNING')==true && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC')){//(acceptanceType=='1' && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC') || (acceptanceType=='2' && statusAcceptance=='ACCEPTED' && programType=='ACADEMIC')){
				$('#datePayment').show('slow');
				$("#dateAccept").show('slow');
				$("#dateDeadline").show('slow');
				$("#dateArrival").show('slow');
				$('#dateStarting').hide();
				$('#dateStudentExchange').show('slow');
				$("#scholarshipType").hide();
			}else if(statusAcceptance=='UNACCEPTED'){
				$('#iniacceptanceType').hide();
			}else if(programType!='ACADEMIC'){
				$("#dateAccept").hide();
				$("#datePayment").hide();				
				$("#dateDeadline").hide();
				$("#dateStarting").hide();
				$('#dateStudentExchange').hide();
				$("#scholarshipType").hide();
				$('#dateArrival').show();
			}
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:acceptanceType, participantid: userid},
                    success:function(response)
                    {
                            $("#selectEnrollment").show('slow');
                            $('#selectEnrollment').empty();
                            $("#selectEnrollment").append($('<option>', {value: '', text: '-- Select Enrollment --'}));
                            $.each(response, function(index, value) {
                                // if(value['ENROLLMENTID']==enrollmentid){
                                //   $("#selectEnrollment").append($('<option selected>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                                // }else{
                                  $("#selectEnrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));  
                                //}
                                
                            });

							             if(subjectstatus=='Y'){
                            $("#selectEnrollment").val(enrollmentid);
                             $("#degreeId").val(degree);
                           }else{
                            $('#degreeId').val('');
                           }
							             
							
							/*$("#selectStudyprogram").show('slow');
                            $('#selectStudyprogram').empty();
                            $("#selectStudyprogram").append($('<option>', {value: '', selected: 'selected', text: '-- Select Study Program --'}));*/
					
					},
                    error: function() 
                    {
                        alert("Invalide!");
                    }
				});
		});
		$('#typeScholarship').on('change',function(){
			var scholarshipType = $(this).val();
			var statusAcceptance = $('#acceptancestatus').val();
			//if(scholarshipType=='4' && statusAcceptance=='ACCEPTED'){				
				$("#dateAccept").show('slow');
				$("#datePayment").show('slow');
				$("#dateDeadline").show('slow');
				$("#dateArrival").show('slow');
				$("#dateStarting").show('slow');
				$('#dateStudentExchange').hide();
			/*}else if(scholarshipType!='4' && statusAcceptance=='ACCEPTED'){
				$("#dateAccept").show('slow');
				$("#dateDeadline").show('slow');
				$("#dateArrival").show('slow');
				$("#dateStarting").show('slow');
				$("#datePayment").hide();
			}*/
		});
		
		$('#selectEnrollment').on("change", function(e) { 
            var value = $(this).val();
			var type = $("#acceptanceType").select().find(":selected").data("type");
      var subjectstatus = $("#acceptanceType").select().find(":selected").data("subjectstatus");
			$('#i').val(type);
            if(value != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                        data:{ enrollmentid:value},
                        success:function(response)
                        {
                            $('#degreeId').val(response.DEGREE);
                            $('#degreeIdHidden').val(response.DEGREEID);
                            $('#programdegree-id').val(response.PROGRAMDEGREEID);
							
							if(type=='ACADEMIC'){
                if(subjectstatus=='Y'){
                  $('#dataSubject').show();
                  $('#dataStudy').show();
                  $('#dataLanguage').hide();
                }else{
                  $('#dataStudy').show();
                  $('#dataLanguage').show();
                  $('#dataSubject').hide();
                }								
							}else{
								$('#dataCourse').show();
                $('#dataSubject').hide();
                $('#dataStudy').hide();
                $('#dataLanguage').show();
							}                        
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );
            } else {
                $('#degreeId').val("");
                $('#degreeIdHidden').val("");
                $('#programdegree-id').val("");
               if(type=='ACADEMIC'){
								if(subjectstatus=='Y'){
                  $('#dataSubject').show();
                  $('#dataStudy').show();
                  $('#dataLanguage').hide();
                }else{
                  $('#dataStudy').show();
                  $('#dataLanguage').show();
                  $('#dataSubject').hide();
                } 
							}else{
								$('#dataCourse').show();
                $('#dataLanguage').show();
                $('#dataSubject').hide();
                $('#dataStudy').hide();
							}                        
							
            }
        });
		
		$('#selectLanguage').on("change", function(e) { 
		var type = $("#acceptanceType").select().find(":selected").data("type");
			var programdegreeid = $('#programdegree-id').val();
            var languagedeliveryid = $(this).val();
            if(programdegreeid != "" && languagedeliveryid != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid, type: type},
                        success:function(response)
                        {
							//language delivery change
							if(type=='ACADEMIC'){
								$("#dataStudy").show('slow');
								$('#selectStudyprogram').empty();
								$("#selectStudyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
								$.each(response, function(index, value) {
									$("#selectStudyprogram").append($('<option>', {value: value['STUDYPROGRAMID'], text: value['STUDYPROGRAMNAME']}));
								});
							}else{
								$("#dataCourse").show('slow');
								$('#selectCourse').empty();
								$("#selectCourse").append($('<option>', {value: '', text: '-- Select Course --'}));
								$.each(response, function(index, value) {
									$("#selectCourse").append($('<option>', {value: value['COURSEID'], text: value['COURSENAME']}));
								});
							}
							
							
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );   
            } else {
                $("#selectLanguage").val('');
            }
        });
	
	var dataE = $('#selectEnrollment').val();
	var type = $("#acceptanceType").select().find(":selected").data("type");
  var subjectstatus = $("#acceptanceType").select().find(":selected").data("subjectstatus");
	if(dataE!=''){
		$.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                data:{ enrollmentid:dataE},
                success:function(response)
                {
                    $('#degreeId').val(response.DEGREE);
                    $('#degreeIdHidden').val(response.DEGREEID);
                    $('#programdegree-id').val(response.PROGRAMDEGREEID);
                    //alert(subjectstatus);
                    if(type=='ACADEMIC'){
                      if(subjectstatus!='Y'){
                        $('#dataLanguage').show();
                      }
      								$('#dataStudy').show();
      							}else{
      								$('#dataCourse').show();
                      $('#dataLanguage').show();
      							}                        
      							
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
	}
	
    if (location.hash) {
        $("a[href='" + location.hash + "']").tab("show");
    }
    $(document.body).on("click", "a[data-toggle]", function(event) {
        location.hash = this.getAttribute("href");
    });

$(window).on("popstate", function() {
    var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
    $("a[href='" + anchor + "']").tab("show");
});

function submitData(data2){
	var type = $('#'+data2+'_data').attr('type');
	var element = this;
	if(type=="file"){
		var fileInput = document.getElementById(data2+'_data');
		var filePath = fileInput.value;
		var allowedExtensions = /(\.jpg|\.jpeg|\.pdf)$/i;
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .jpeg/.jpg/.pdf only.');
			fileInput.value = '';
			return false;
		}else{
			//Image cek Size
			if (fileInput.files && fileInput.files[0].size/ 1024 / 1024 > 1) {
				alert("File too large. File size max: 1 MB"); // Do your thing to handle the error.
				fileInput.value = null; // Clear the field.
				return false;
			}else{
				$.ajax(
				{
					type:"post",
					//  url: "<?php echo base_url(); ?>staff/saveRequirement",
					data:{type: type},
						success:function(response){
							//alert(response);
							$(element).closest("form").submit();
						}
					}
				);
			}
		}
	}else{
		$.ajax(
				{
					type:"post",
					//  url: "<?php echo base_url(); ?>staff/saveRequirement",
					data:{type: type},
						success:function(response){
							//alert(response);
							$(element).closest("form").submit();
						}
					}
				);
	}	
}
function validateForm() {
    var acceptancestatus = document.forms["set_status"]["acceptancestatus"].value;
	var enrollment_id = document.forms["set_status"]["enrollment_id"].value;
	var language_delivery_id = document.forms["set_status"]["language_delivery_id"].value;
	var study_program_id = document.forms["set_status"]["study_program_id"].value;
	var acceptancedate = document.forms["set_status"]["acceptancedate"].value;
	var paymentdate = document.forms["set_status"]["paymentdate"].value;
	var deadlinedate = document.forms["set_status"]["deadlinedate"].value;
	var arrivaldate = document.forms["set_status"]["arrivaldate"].value;
	var startingdate = document.forms["set_status"]["startingdate"].value;
	var typeScholarship = document.forms["set_status"]["typeScholarship"].value;
	var acceptanceType = document.forms["set_status"]["acceptanceType"].value;
	var startingdateSECE = document.forms["set_status"]["startingdateSECE"].value;
	var endingdateSECE = document.forms["set_status"]["endingdateSECE"].value;
    if (acceptancestatus == "ACCEPTED") {
		if(acceptanceType == 5){
			if(enrollment_id=='' || language_delivery_id==''||study_program_id==''||acceptancedate==''||paymentdate==''||deadlinedate==''||arrivaldate==''||startingdate==''){
				alert("The field is required");
				return false;
			}			
		}else if(acceptanceType==3 ){
			if(enrollment_id=='' || language_delivery_id==''||study_program_id==''||acceptancedate==''||deadlinedate==''||arrivaldate==''||startingdate==''){
				alert("The field is required");
				return false;
			}
		}else if(acceptanceType==1){
			if(enrollment_id=='' || language_delivery_id==''||study_program_id==''||acceptancedate==''||deadlinedate==''||arrivaldate==''||startingdateSECE==''|| endingdateSECE==''){
				alert("The field is required");
				return false;
			}
		}       
    }
}

function getStudySubject(){
  $.ajax({
            type:"post",
            dataType: 'json',
            url: "<?php echo base_url(); ?>ajax/getStudySubject",
            data:{ 
                filter:$("input[name='idparticipant']").val()
            },
            success:function(data){
               // $("#credit_total").html(data);
              $("#selectStudyprogram").empty();
              var html = "<option>-- Study Program --</option>";
              for(var i in data){
                html += "<option value='"+data[i].STUDYPROGRAMID+"'>"+data[i].STUDYPROGRAMNAME+"</option>";
              }
              $("#selectStudyprogram").append(html);
            },
            error: function(){
                alert("Invalide!");
            }
        })
}
</script>