<?php //echo "<pre>"; print_r($participant);die;?>
<form action="<?php echo base_url()?>staff/updateRegistInfo" class="form-horizontal modal-form" role="form" method="post" id="edit-registration-info-form">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Update Registration Info</h4>
    </div>
    <div class="modal-body">
		<input type="hidden" name="programdegreeid" id="programdegree-id">
        <input type="hidden" name="language_delivery_mapping_id" id="languagedeliverymapping-id">
		<input type="hidden" name="user_id" id="user_id" value="<?php echo $participant['USERID']?>">
		<input type="hidden" name="participant_id" value="<?php echo $participant['PARTICIPANTID']?>">
		 <input type="hidden" id="thisprogramId" value="<?php echo $participant['PROGRAMID']?>">
		  <input type="hidden" id="thisenrollmentId" value="<?php echo $participant['ENROLLMENTID']; ?>">
		  <input type="hidden" id="acceptstatus" value="<?php echo $participant['ACCEPTANCESTATUS']; ?>">
		   <input type="hidden" name="thisProgramType" id="thisProgramType" value="<?php echo $participant['PROGRAMTYPE']; ?>">
		<div class="form-group">
            <label for="ticket-message" class="col-sm-3 control-label">Select Program</label>
                <div class="col-sm-9">
                <select id="select-program" name="program_id" class="select2 required" required>
					<option value="">-- Select Program --</option>
                    <?php
                        if(!empty($listProgram)){
							foreach ($listProgram as $program) {
                                $selected = $program['PROGRAMID'] == $participant['PROGRAMID'] ? 'selected' : '';?>
                                <option data-type="<?php echo $program['PROGRAMTYPE']; ?>" value="<?php echo $program['PROGRAMID'];?>" <?php echo $selected; ?>><?php echo $program['PROGRAMNAME']; ?></option>
                    <?php
                            }
                        }?>
                </select>
                </div>
        </div>
        <div id="enrollment-academic">
			<div class="form-group">
                <label for="ticket-message" class="col-sm-3 control-label">Enrollment</label>
                <div class="col-sm-9">
                    <select id="select-enrollment" name="enrollment_id" class="select2 required">
                        <option value="">-- Select Enrollment --</option>
						<?php
                            if(!empty($listEnrollment))
                                                        {

                                                            foreach ($listEnrollment as $enrollment) {
                                                           if($enrollment['ENROLLMENTID'] == $participant['ENROLLMENTID']){                                                    ?>
                                                            <option value="<?php echo $enrollment['ENROLLMENTID'];?>" selected="selected"><?php echo $enrollment['DESCRIPTION']; ?></option>
                                                    <?php
														   }
                                                            }
                                                        }
                                                    ?>
                    </select>
                </div>
            </div>
        </div>
		<div class="form-group">
            <label for="ticket-message" class="col-sm-3 control-label">Degree</label>
            <div class="col-sm-9">
                <input type="text" name="degreename" id="degree-id" readonly class="form-control">
                <input type="hidden" name="degreeid" id="degree-id-hidden">
            </div>
        </div>
                                    <div class="form-group" id="language-delivery-container">
                                        <label for="ticket-message" class="col-sm-3 control-label">Language Delivery</label>
                                        <div class="col-sm-9">
                                            <select id="select-language" name="language_delivery_id" class="select2 required">
                                                <option value=""> -- Select Language Delivery -- </option>
                                                
                                                 <?php
                                                    if(!empty($listLanguageDelivery))
                                                    {

                                                        foreach ($listLanguageDelivery as $data) {
                                                        if($data['LANGUAGEDELIVERYID'] == $languageDeliveryId['LANGUAGEDELIVERYID']){
                                                echo '<option value="'.$data['LANGUAGEDELIVERYID'].'" selected="selected">'.$data['LANGUAGE'].'</option>';
														}else{ ?>
														<option value="<?php echo $data['LANGUAGEDELIVERYID'];?>"><?php echo $data['LANGUAGE']; ?></option>	
												<?php		}
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php  
                                      if($participant['PROGRAMTYPE'] == 'ACADEMIC')
                                        {?>
                                            <div class="form-group" id="study-program-container">
                                        <label for="ticket-message" class="col-sm-3 control-label">Study Program</label>
                                        <div class="col-sm-9">
                                            <select id="select-studyprogram" name="study_program_id" class="select2 required">
                                            <option value=""> -- Select Study Program -- </option>
                                          
                                                  <?php
                                                    if(!empty($listStudyProgram))
                                                    {

                                                        foreach ($listStudyProgram as $data) {
                                                       if($data['STUDYPROGRAMID'] == $participant['STUDYPROGRAMID']){
                                                ?>
                                                        <option value="<?php echo $data['STUDYPROGRAMID'];?>" selected="selected"><?php echo $data['STUDYPROGRAMNAME']; ?></option>
                                                <?php
													   }
                                                        }
                                                    }
                                                ?> 

                                           </select>
                                        </div>
                                    </div>
                                    <?php 
                                    }else{ ?>
										<div class="form-group" id="course-container">
                                        <label for="ticket-type" class="col-sm-3 control-label">Course</label>
                                        <div class="col-sm-9">
                                            <select id="select-course" name="study_course_id" class="select2 required">
                                            <option value=""> -- Select Course-- </option>
                                          
                                                  <?php
                                                    if(!empty($listCourse))
                                                    {

                                                        foreach ($listCourse as $data) {
                                                       if($data['COURSEID'] == $participant['COURSEID']){
                                                ?>
                                                        <option value="<?php echo $data['COURSEID'];?>" selected="selected"><?php echo $data['COURSENAME']; ?></option>
                                                <?php
													   }
                                                        }
                                                    }
                                                ?> 

                                           </select>
                                        </div>
                                    </div>
								<?php	}?>
                                    
                    </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
                
<script type="text/javascript">

var baseurl = '<?php echo base_url(); ?>';
    $(document).ready(function(){
        $("#enrollment-academic").hide();
       //$("#course-container").hide();


        $('div.product-chooser').on('click', '.product-chooser-item', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);
        });

        <?php 
            if(!empty($participant['PROGRAMID']))
            { ?>
                loadEnrollment(<?php echo $participant['ENROLLMENTID']; ?>);
                <?php /*loadDegree(<?php echo $participant['PROGRAMID']?>, <?php echo $participant['DEGREEID'] ?>); */?>
            <?php 
            }
        ?>

        $('#select-enrollment').on("change", function(e) { 
            var value = $('#select-enrollment').select2('val');

            if(value != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                        data:{ enrollmentid:value},
                        success:function(response)
                        {
                            $('#degree-id').val(response.DEGREE);
                            $('#degree-id-hidden').val(response.DEGREEID);
                            $('#programdegree-id').val(response.PROGRAMDEGREEID);
							
							$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                            $("#select-studyprogram").select2("val", 0);
							
                            
                                $('.study-program-container').show();
                                $('#language-delivery-container').show();
                            
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );
            } else {
                $('#degree-id').val("");
                $('#degree-id-hidden').val("");
                $('#programdegree-id').val("");
                $('.study-program-container').show();
                $('#language-delivery-container').show();
				$("#course-container").show();
            }
        });
        $("#inputForm").validate({
           /* errorPlacement: function(error, element) {
                if (element.attr("name") == "email" )
                    error.insertAfter(".some-class");
                else if  (element.attr("name") == "phone" )
                    error.insertAfter(".some-other-class");
                else
                    error.insertAfter(element);
            }*/

        });
       // Getstudy();
    function Getstudy(){
			 var type = $("#select-program").select2().find(":selected").data("type");
            var programdegreeid = $('#programdegree-id').val();
            var languagedeliveryid = $('#select-language').val();
            // alert (programdegreeid);
            if(programdegreeid != "" && languagedeliveryid != "")
            {
				
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid, type: type},
                        success:function(response)
                        {
							if(type=='ACADEMIC'){
								$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                            $.each(response, function(index, value) {
                                $("#select-studyprogram").append($('<option>', {value: value['STUDYPROGRAMID'], text: value['STUDYPROGRAMNAME']}));
                            });
                            $("#select-studyprogram").select2("val", 0);
							}else{
								$("#course-container").show('slow');
								$('#select-course').empty();
								$("#select-course").append($('<option>', {value: '', text: '-- Select Course --'}));
								$.each(response, function(index, value) {
									$("#select-course").append($('<option>', {value: value['COURSEID'], text: value['COURSENAME']}));
								});
								$("#select-course").select2("val", 0);
								
							}
							
							
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );  
				
				}else {
              //  alert('Please select Enrollment data');
                $("#select-language").select2('val', "");
            }
    }  
	
        $('#select-language').on("change", function(e) { 
			Getstudy();	
        });

         $('#select-studyprogram').on("change", function(e) { 
            var programdegreeid = $('#programdegree-id').select2('val');
            var languagedeliveryid = $('#select-language').select2('val');
            // alert(programdegreeid);
            if(programdegreeid != "" && languagedeliveryid != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid},
                        success:function(response)
                        {
							
                           for (i = 0; i < response.length; i++) {
                                    alert(response[i].STUDYPROGRAMNAME);
                                } 
                            // $('#languagedeliverymapping-id').val(response.LANGUAGEDELIVERYMAPPINGID);
                            // $('#studyprogram-name').val(response.STUDYPROGRAMNAME);
                            // $('#studyprogram-id').val(response.STUDYPROGRAMID);

                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );   
            } else {
                alert('Please select Study Program');
                $("#select-studyprogram").select2('val', "");
            }
        });

        $('#select-program').on("change", function(e) { 
            var value = $('#select-program').select2('val');
            var type = $("#select-program").select2().find(":selected").data("type");
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value},
                    success:function(response)
                    {
                        $("#enrollment-academic").hide();
                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: 'Select Enrollment Program'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                            });
                            $("#select-enrollment").select2("val", 0);
							
							$('#degree-id').val('');
							$('#select-course').empty();
                            $("#select-course").append($('<option>', {value: '', selected: 'selected', text: '-- Select Course --'}));
                            $("#select-course").select2("val", 0);
							
							$("#study-program-container").show('slow');
                            $('#select-studyprogram').empty();
                            $("#select-studyprogram").append($('<option>', {value: '', selected: 'selected', text: '-- Select Study Program --'}));
                            $("#select-studyprogram").select2("val", 0);
                        
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            );
        });
    });

    //ini untuk edit; beda dengan yang diatas
    function loadEnrollment(enrollment_id) {
        var value = $('#select-program').select2('val');
        var type = $("#select-program").select2().find(":selected").data("type");
		var participant_id = $("#user_id").val();
		
		//alert(participant_id);
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value, participantid: participant_id},
                    success:function(response)
                    {
                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: 'Select Enrollment Program'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                            });
                            $("#select-enrollment").select2("val", enrollment_id);
                            loadDegree(enrollment_id);
                        
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            );
    }

    function sortcourseHtml(id, title, description, img_url, selected, checked)
    {
        return '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">' +
            '<div class="product-chooser-item '+selected+' ">' +
                '<a href="'+img_url+'" target="_blank"><img src="'+img_url+'" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="'+title+'"></a>' +
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">' +
                    '<span class="title">'+title+'</span>' +
                    '<span class="description">'+description+'</span>' +
                    '<input type="radio" name="enrollmentn_id" value="'+id+'" '+checked+'>' +
                '</div>' +
                '<div class="clear"></div>' +
            '</div>' +
        '</div>';

        /*var tes1 = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">' +
            '<div class="product-chooser-item">' +
                '<img src="'+img_url+'" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="'+title+'">' +
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">' +
                    '<span class="title">'+title+'</span>' +
                    '<span class="description">'+description+'</span>' +
                    '<input type="radio" name="enrollment_id" value="'+id+'">' +
                '</div>' +
                '<div class="clear"></div>' +
            '</div>' +
        '</div>';

        var tesxx = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">'+
            '<div class="product-chooser-item selected">'+
                '<img src="http://renswijnmalen.nl/bootstrap/desktop_mobile.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile and Desktop">'+
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">'+
                    '<span class="title">Mobile and Desktop</span>'+
                    '<span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</span>' +
                    '<input type="radio" name="product" value="mobile_desktop" checked="checked">'+
                '</div>'+
                '<div class="clear"></div>'+
            '</div>'+
        '</div>';

        var tesxx1 = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">'+
            '<div class="product-chooser-item">'+
                '<img src="http://renswijnmalen.nl/bootstrap/desktop_mobile.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile and Desktop">'+
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">'+
                    '<span class="title">Mobile and Desktop</span>'+
                    '<span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</span>' +
                    '<input type="radio" name="product" value="mobile_desktop">'+
                '</div>'+
                '<div class="clear"></div>'+
            '</div>'+
        '</div>'; */

        //return tesxx + ' ' + tesxx1;
    }

    function loadDegree(enrollment_id)
    {
		var type = $("#select-program").select2().find(":selected").data("type");
        $.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                data:{ enrollmentid:enrollment_id},
                success:function(response)
                {
                    $('#degree-id').val(response.DEGREE);
                    $('#degree-id-hidden').val(response.DEGREEID);
                    $('#programdegree-id').val(response.PROGRAMDEGREEID);
					if(type=='ACADEMIC'){
						$('#study-program-container').show();
					}else{
						$('#course-container').show();
					}                        
                        $('#language-delivery-container').show();
                    
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
    }
</script>