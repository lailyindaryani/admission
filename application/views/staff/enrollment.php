<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
		<div class="col-lg-4 ">
				<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Home</a></li>
                        <li class="active">Enrollment</li>
				</ul>
		</div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
		<h2>Enrollment</h2>
		<em>Enrollment Data</em>
</div>

<div class="main-content">
<div class="row">
		<div class="col-md-12">
				<!-- SUPPOR TICKET FORM -->
				<div class="widget">
						<div class="widget-header">
								<!--<h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
						</div>

						<div class="row" style="border: 1px solid #ccc; margin:5px;">
								<div class="col-md-12">
										<div class="widget-content">
												<div class="row form-horizontal">
														<div class="col-md-4">
																<div class="form-group">
																		<label for="academicYear" class="col-sm-5 control-label">Academic Year</label>
																		<div class="col-md-6">
																				<select id="academicYear" name="academicYear" class="filter">
																						<option value="">-All-</option>
																						<?php foreach ((array)$academicyears as $pg){ ?>
																								<option value=<?=$pg['ACADEMICYEAR']?>><?=$pg['ACADEMICYEAR']?></option>
																						<?php } ?>
																				</select>
																		</div>
																</div>
														</div>

														<div class="col-md-4">
																<div class="form-group">
																		<label for="program" class="col-sm-5 control-label">Program</label>
																		<div class="col-md-6">
																				<select id="program" name="program" class="filter">
																						<option value="">-All-</option>
																						<?php foreach ($programs as $pg){ ?>
																								<option value=<?=$pg['PROGRAMID']?>><?=$pg['PROGRAMNAME']?></option>
																						<?php } ?>
																				</select>
																		</div>
																</div>
														</div>
														
														<div class="col-md-4">
																<div class="form-group">
																		<label for="status" class="col-sm-5 control-label">Status</label>
																		<div class="col-md-6">
																				<select id="status" name="status" class="filter">
																						<option value="">-All-</option>
																						<option value="OPEN">OPEN</option>
																						<option value="CLOSED">CLOSED</option>
																				</select>
																		</div>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>

						<div class="widget-content">
												<div class="table-basic">
														<table id="tableProgram" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Description</th>
																		<th >Program</th>
																		<th >Academic Year</th>
																		<th >Period</th>
																		<th >Start Date</th>
																		<th >End Date</th>
																		<th >Input By</th>
																		<th >Action</th>
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div>
						</div>
				</div>
			</div>
</div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->




<script type="text/javascript">
		$(document).ready(function() {
				$("#academicYear").select2();
				$("#program").select2();
				$("#status").select2();
				$(".filter").on("change", function (e) {
						console.log($("#program").val());
            $('#tableProgram').DataTable().ajax.reload();
        });

				var dt= $('#tableProgram').dataTable( {
						//"bJQueryUI": true,
						"order": [],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
								/*[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]*/
								 [-1,30, 40, 50, 100],
                ["All",30, 40, 50, 100, ]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>staff/dataTablePopulateEnrollment",
						"fnRowCallback":
								function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                                        var participantLink = "<button onclick=\"location.href='<?php echo base_url()?>staff/participants?enrollmentId=" +aData[8]+ "'\" title='Show Participants' class='btn btn-xs btn-primary'><i class='fa fa-list'></i></button>";
										$(nRow).html(
												'<td>'+aData[0]+'</td>' +
														'<td>'+aData[1]+'</td>' +
														'<td>'+aData[2]+'</td>' +
														'<td>'+aData[3]+'</td>' +
														'<td>'+aData[4]+'</td>' +
														'<td>'+aData[5]+'</td>' +
														'<td>'+aData[6]+'</td>' +
														'<td>'+aData[7]+'</td>' +
														'<td>'+participantLink+'</td>'

										);
										return nRow;
								},
						"fnServerData": function ( sSource, aoData, fnCallback ) {
								/* Add some extra data to the sender */
								aoData.push(
									{ "name": "academicyear", "value": $("#academicYear").val() },
									{ "name": "programid", "value": $("#program").val() },
									{ "name": "status", "value": $("#status").val() },
								);
								$.getJSON( sSource, aoData, function (json) {
										/* Do whatever additional processing you want on the callback, then tell DataTables */
										fnCallback(json)
								} );
						}
				} );
				
				 $("input[type='search']").on('keyup',function(){
            $('#tableProgram').DataTable().destroy();
            $('#tableProgram').DataTable().search(this.value,true).draw();
        });
		});
</script>
