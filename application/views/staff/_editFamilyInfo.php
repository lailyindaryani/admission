            <form action="<?php echo base_url()?>staff/updateFamilyInfo" class="form-horizontal modal-form" role="form" method="post" id="edit-profile-info-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Family Info</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="familyid" id="familyid" value="<?php echo $family['FAMILYID']?>">
                    <input type="hidden" name="participantid" id="participantid" value="<?php echo $family['PARTICIPANTID']?>">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Full Name</label>
                        <div class="col-sm-9 ">
                            <input type="text" class="form-control" onpaste="return blockSpecialChar(event)" onkeypress="return blockSpecialChar(event)" name="fullname" id="fullname" placeholder="Full Name" value="<?php echo $family['FULLNAME']?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Relationship</label>
                        <div class="col-sm-9 ">
                            <input type="text" class="form-control" name="relationship" id="relationship" placeholder="Relationship" disabled value="<?php echo $family['RELATIONSHIP']?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9 ">
                            <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="<?php echo $family['ADDRESS']?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">City</label>
                        <div class="col-sm-9 ">
                            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $family['CITY']?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Country</label>
                        <div class="col-sm-9 ">
                            <select name="country" id="country" class="">
                                <?php
                                if(!empty($listCountry))
                                {
                                    foreach ($listCountry as $country) {
                                        if($country['COUNTRYID']==$family['COUNTRYID'])
                                            echo "<option value='".$country['COUNTRYID']."' selected>".$country['COUNTRYNAMEENG']."</option>";
                                        else
                                            echo "<option value='".$country['COUNTRYID']."'>".$country['COUNTRYNAMEENG']."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Zip Code</label>
                        <div class="col-sm-9 ">
                            <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip Code" value="<?php echo $family['ZIPCODE']?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9 ">
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $family['PHONE']?>">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>

            <script type="text/javascript">
                $("#edit-profile-info-form #country").select2({
                    placeholder: "Select Country",
                    allowClear: true,
                });
				function blockSpecialChar(e){
					var k;
					document.all ? k = e.keyCode : k = e.which;
					return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
				}
            </script>