
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
		<div class="col-lg-4 ">
				<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="<?php echo base_url().'staff/dashboardEnrollment'?>">Home</a></li>
						<li class="active">Summary by Study Program</li>
				</ul>
		</div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
		<h2>Summary Report</h2>
		<em>Summary Report of Students by Study Program</em>
</div>

<div class="main-content">
<div class="row">
		<div class="col-md-12">
				<!-- SUPPOR TICKET FORM -->
				<div class="widget">
						<div class="widget-header">
								<!--<h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
						</div>

						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						<div class="widget-content">
												<div class="table-basic">
														<table id="tableProgram" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Study Program</th>
																		<th >Number of Students</th>
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div><!-- style="display:none" -->
					<?php echo '<table id="datatable" style="display:none" class="table table-sorting table-hover  table-striped datatable">';
                        echo '<thead>';
                        echo '<tr>';?>
							<?php echo '<th></th>';?>
							<?php for( $i=0; $i<sizeof($data_studyprogram) ; $i++ ){
                           echo "<th >".$data_studyprogram[$i]['STUDYPROGRAMNAME']."</th>";
							}?>
                        <?php echo '</tr>';
                        echo '</thead>';
                        echo '<tbody >';
                        echo '<tr>';?>
							<?php echo '<td font size="5">Participants</td>';?>
							<?php for( $i=0; $i<sizeof($data_studyprogram) ; $i++ ){
							echo "<td>".$data_studyprogram[$i]['JUMLAH']."</td>";
							}?>
                        <?php echo '</tr>';
                        echo '</tbody>';
                    echo '</table>';?>
						</div>
				</div>
			</div>
</div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->




<script type="text/javascript">
		$(document).ready(function() {
				var dt= $('#tableProgram').dataTable( {
						//"bJQueryUI": true,
						"order": [[ 2, "DESC" ]],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						 "columnDefs": [
                { "width": "8%", "targets": 0},
             //   { "width": "12%", "targets": 3},
            ],
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
								[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>staff/datatableStudyProgram2",
						"fnRowCallback":
								function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                       var participantLink = "<button class='btn btn-xs btn-primary'><i class='fa fa-list'></i></button>";
										$(nRow).html(
												'<td>'+aData[0]+'</td>' +
														'<td>'+aData[1]+'</td>' +
														'<td>'+aData[2]+'</td>' 

										);
										return nRow;
								},
				} );
				
	Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Summary Report of Students by Study Program'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Number of Participants'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
		});
</script>
