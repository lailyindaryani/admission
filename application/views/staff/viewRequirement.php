    <?php
	date_default_timezone_set('Asia/Jakarta'); ?>
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $requirementvalue['REQUIREMENTNAME']!=null?$requirementvalue['REQUIREMENTNAME']:'<em>Not set</em>'?></h4>
    </div>
    <div class="modal-body project-section general-info">

                
	<?php if($privilege==1 AND $requirementvalue['TYPE']=='ATTACHMENT'){?>
                <p class="data-row">
                    <span class="data-name">Status:</span>
                    <span class="data-value">
                        <?php
                            if(@$requirementvalue['VERIFICATIONSTATUS']=='VERIFIED')
                                $flag='success';
                            elseif(@$requirementvalue['VERIFICATIONSTATUS']=='UNVERIFIED' || @$requirementvalue['VERIFICATIONSTATUS']=='')
                                $flag='warning';
                        ?>
                        <span class="label label-<?php echo $flag?>"><?php echo @$requirementvalue['VERIFICATIONSTATUS']!=null?$requirementvalue['VERIFICATIONSTATUS']:'Not Set'?></span>
                    </span>
                </p>

                <p class="data-row">
                    <span class="data-name">Uploaded By:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['UPLOADER']!=null?$requirementvalue['UPLOADER']:'<em>Not set</em>'?>
                    </span>
                </p>

                <p class="data-row">
                    <span class="data-name">Uploaded Date:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['INPUTDATE']!=null?date('l, d F Y',strtotime($requirementvalue['INPUTDATE'])):'<em>Not set</em>'?>
                    </span>
                </p>

                <p class="data-row">
                    <span class="data-name">Verified By:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['VERIFICATOR']!=null?$requirementvalue['VERIFICATOR']:'<em>Not set</em>'?>
                    </span>
                </p>

                <p class="data-row">
                    <span class="data-name">Verification Date:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['VERIFICATIONDATE']!=null?date('d F Y',strtotime($requirementvalue['VERIFICATIONDATE'])):'<em>Not set</em>'?>
                    </span>
                </p>
	<?php }else if($privilege==1 OR $privilege==3 AND $requirementvalue['TYPE']=='TEXT'){?>
				<p class="data-row">
                    <span class="data-name">Input By:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['UPLOADER']!=null?$requirementvalue['UPLOADER']:'<em>Not set</em>'?>
                    </span>
                </p>

                <p class="data-row">
                    <span class="data-name">Input Date:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['INPUTDATE']!=null?date('l, d F Y',strtotime($requirementvalue['INPUTDATE'])):'<em>Not set</em>'?>
                    </span>
                </p>
	<?php }else if($privilege==3 AND $requirementvalue['TYPE']=='ATTACHMENT'){?>
		 <p class="data-row">
                    <span class="data-name">Uploaded By:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['UPLOADER']!=null?$requirementvalue['UPLOADER']:'<em>Not set</em>'?>
                    </span>
                </p>

                <p class="data-row">
                    <span class="data-name">Uploaded Date:</span>
                    <span class="data-value">
                        <?php echo @$requirementvalue['INPUTDATE']!=null?date('l, d F Y',strtotime($requirementvalue['INPUTDATE'])):'<em>Not set</em>'?>
                    </span>
                </p>
	<?php }?>	
	
                <p class="data-row">
                <?php if ($requirementvalue['TYPE']=='ATTACHMENT'){ ?>
                    <span class="data-name">Attachment:</span>
                    <span class="data-value">
                        <?php if(@$requirementvalue['FILEURL']!=null){ 
							if(substr($requirementvalue['FILENAME'],-3)=='jpg' || substr($requirementvalue['FILENAME'],-3)=='JPG' || substr($requirementvalue['FILENAME'],-4)=='jpeg' || substr($requirementvalue['FILENAME'],-4)=='JPEG'){
								echo '<a download="'.$requirementvalue['FILENAME'].'" href="'.base_url().$requirementvalue['FILEURL'].$requirementvalue['FILENAME'].'" title="'.$requirementvalue['FILENAME'].'">';
								echo '<img style="max-height: 700px;max-width: 500px; image-orientation: from-image;" id="displayImage" alt="Photo" src="'.base_url().$requirementvalue['FILEURL'].$requirementvalue['FILENAME'].'"/>';
								echo '</a>';
							}else{?>
                            <a href="<?php echo base_url().$requirementvalue['FILEURL'].$requirementvalue['FILENAME']?>" class='text text-info'><i class='fa fa-paperclip' aria-hidden='true'></i> <?php echo @$requirementvalue['FILENAME']?></a>
							<?php }
						} else{ ?>
                            <em>Not set</em>
                        <?php } ?>
                    </span>
                <?php } else{ ?>
                    <span class="data-name">Value:</span>
                    <span class="data-value">
                            <?php echo $requirementvalue['VALUE']?>
                    </span>
                <?php } ?>
                </p>
				<?php if($privilege==1){?>
					<p class="data-row">
                    <span class="data-name"></span>
                    <span class="data-value">
                        <?php
                          //  if ($requirementvalue['TYPE']=='ATTACHMENT') {
                                if(@$requirementvalue['FILENAME']!='' || @$requirementvalue['VALUE']!='') {
                                    if (@$requirementvalue['VERIFICATIONSTATUS'] == 'UNVERIFIED' || @$requirementvalue['VERIFICATIONSTATUS']=='') { ?>
                                        <a href="<?php echo base_url()?>staff/verifyRequirementValue/<?php echo $requirementvalue['PARTICIPANTID'].'/'.$requirementvalue['REQUIREMENTVALUEID']?>" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Verify</a>
                                    <?php } else { ?>
                                        <a href="<?php echo base_url()?>staff/unverifyRequirementValue/<?php echo $requirementvalue['PARTICIPANTID'].'/'.$requirementvalue['REQUIREMENTVALUEID']?>" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Unverify</a>
                                    <?php }
                                }
                           // }
                            ?>
                    </span>
                </p>
			<?php	} ?>
                
    </div>
