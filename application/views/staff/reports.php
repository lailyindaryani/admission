<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li class="active">Reports</li>
        </ul>
    </div>
</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Reports</h2>
    <em>Participant Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <!--<h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>

            <div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">
                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Completeness of Document</span>
                                    <select id="requirement" name="requirement" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($requirements as $rq){ ?>
                                            <option value="<?=$rq['REQUIREMENTID']?>"><?=$rq['REQUIREMENTNAME']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Enrollment</span>
                                    <select id="enrollment" name="enrollment" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($enrollments as $row){
                                            $selectedenrollment = $row['ENROLLMENTID'] == $enrollmentId ? "selected" : "";
                                            ?>
                                            <option value="<?=$row['ENROLLMENTID']?>" <?=$selectedenrollment?>><?=$row['DESCRIPTION']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Nationality</span>
                                    <select id="country" name="country" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($countries as $row){
                                            ?>
                                            <option value="<?=$row['COUNTRYID']?>"><?=$row['COUNTRYNAMEENG']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                        </div>

                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Acceptance Status</span>
                                    <select id="acceptance" name="acceptance" class="filter">
                                        <option value="">-All-</option>
                                        <option value="ACCEPTED">Accepted</option>
                                        <option value="UNACCEPTED">Not Accepted</option>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Study Program</span>
                                    <select id="studyprogram" name="studyprogram" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($studyprograms as $sp){ ?>
                                            <option value="<?=$sp['STUDYPROGRAMID']?>"><?=$sp['STUDYPROGRAMNAME']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Period</span>
                                    <select id="period" name="period" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($periods as $pr){ ?>
                                            <option value="<?=$pr['PERIOD']?>"><?=$pr['PERIOD']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Faculty</span>
                                    <select name="faculty" id="faculty" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($faculties as $faculty){?>
                                            <option value="<?=$faculty['FACULTYID']?>"><?=$faculty['FACULTYNAMEENG']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Program Type</span>
                                    <select name="program-type" id="program-type" class="filter">
                                        <option value="">-All-</option>
                                        <option value="ACADEMIC">Academic</option>
                                        <option value="NON_ACADEMIC">Non Academic</option>
                                    </select>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="widget-content">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <div class="top-content">
                        <ul class="list-inline quick-access">
                            <li>
                                <h4>Number of participants: <em id="participant-number">4</em></h4>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table table-responsive">
                    <table id="tableProgram" class="table table-sorting table-hover table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Fullname</th>
                            <th >Passport</th>
                            <th >Gender</th>
                            <th >Nationality</th>
                            <th >Enrollment</th>
                            <th >Period</th>
                            <th >Faculty</th>
                            <th >Study Program</th>
                            <th >Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->




<script type="text/javascript">
    $(document).ready(function() {
        $("#requirement").select2();
        $("#acceptance").select2();
        $("#studyprogram").select2();
        $("#enrollment").select2();
        $("#faculty").select2();
        $("#program-type").select2();
        $("#country").select2();
        $("#period").select2();

        var enrollmentid    = "";
        var studyprogramid  = "";
        var documentid      = "";
        var acceptance      = "";
        var programtype     = "";
        var country         = "";
        var period          = "";

        $(".filter").on("change", function (e) {
            $('#tableProgram').DataTable().ajax.reload();
        });

        var table = $('#tableProgram').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    },
                    title: "International Office | Participants List",
                    messageTop: function () { 
                        return "Total Participants : "+$("#participant-number").text();
                    },
                    messageBottom: null,
                },
                'colvis',
                'excel',
                'pdf',
                'csv',
            ],
            "order": [],
            "columnDefs": [
                { "width": "15%", "targets": 1},
                {
                    "targets"   : 9,
                    "data"      : 9,
                    "render"    : function ( data, type, row, meta ) {
                        var status = "<span class='label label-warning'>NOT SET</span>";

                        if(data=='ACCEPTED')
                            status = "<span class='label label-success'>ACCEPTED</span>";
                        else if(data=='UNACCEPTED')
                            status = "<span class='label label-default'>NOT ACCEPTED</span>";
                        
                        return status;
                    }
                }
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
             /*   [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]*/
				 [-1,30, 40, 50, 100],
                ["All",30, 40, 50, 100, ]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url()?>staff/dataTablePopulateReports",
            // "fnRowCallback":
            //     function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //         var status = "<span class='label label-warning'>NOT SET</span>";
            //
            //         if(aData[9]=='ACCEPTED')
            //             status = "<span class='label label-success'>ACCEPTED</span>";
            //         else if(aData[9]=='UNACCEPTED')
            //             status = "<span class='label label-default'>NOT ACCEPTED</span>";
            //
            //         $(nRow).html(
            //             '<td>'+aData[0]+'</td>' +
            //                 "<td>"+aData[1]+"</td>" +
            //                 '<td>'+aData[2]+'</td>' +
            //                 '<td>'+aData[3]+'</td>' +
            //                 '<td>'+aData[4]+'</td>' +
            //                 '<td>'+aData[5]+'</td>' +
            //                 '<td>'+aData[6]+'</td>' +
            //                 '<td>'+aData[7]+'</td>' +
            //                 '<td>'+aData[8]+'</td>' +
            //                 '<td>'+status+'</td>'
            //         );
            //         return nRow;
            //     },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                    { "name": "enrollmentid", "value": $("#enrollment").val() },
                    { "name": "studyprogramid", "value": $("#studyprogram").val() },
                    { "name": "acceptance", "value": $("#acceptance").val() },
                    { "name": "faculty", "value": $("#faculty").val() },
                    { "name": "documentid", "value": $("#requirement").val() },
                    { "name": "programtype", "value": $("#program-type").val() },
                    { "name": "country", "value": $("#country").val() },
                    { "name": "period", "value": $("#period").val() }
                );
                $.getJSON( sSource, aoData, function (json) {
                    /* Do whatever additional processing you want on the callback, then tell DataTables */
                    fnCallback(json);
                    $("#participant-number").text(json.iTotalRecords)
                    // console.log(json.iTotalRecords);
                } );
            }
        } );
		
		 $("input[type='search']").on('keyup',function(){
            $('#tableProgram').DataTable().destroy();
            $('#tableProgram').DataTable().search(this.value,true).draw();
        });
    });
</script>
