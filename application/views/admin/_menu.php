<div class="col-md-2 left-sidebar">

    <!-- main-nav -->
    <nav class="main-nav">

        <ul class="main-menu">
		<li>
                <a href="#" class="js-sub-menu-toggle">
                    <i class="fa fa-home fa-fw"></i><span class="text">Dashboard</span>
                    <i class="toggle-icon fa fa-angle-left"></i>
                </a>
                <ul class="sub-menu ">					
					<li><a href="<?php echo base_url()?>admin/dashboardEnrollment"><span class="text">Summary Reports</span></a></li>
                    <li><a href="<?php echo base_url()?>admin/dashboardCountry"><span class="text">Summary by Country</span></a></li>
                    <li><a href="<?php echo base_url()?>admin/dashboardStudyProgram"><span class="text">Summary by Study Program</span></a></li>
                </ul>
            </li>
            <li class="<?=$page=='program'?'active':''?>"><a href="<?php echo base_url()?>admin/programList"><i class="fa fa-briefcase fa-fw"></i><span class="text">Program List</span></a></li>
            <li class="<?=$page=='subject'?'active':''?>"><a href="<?php echo base_url()?>admin/subjectList"><i class="fa fa-book fa-fw"></i><span class="text">Pengaturan Matakuliah</span></a></li>
            <li class="<?=$page=='enrollment'?'active':''?>"><a href="<?php echo base_url()?>admin/enrollmentList"><i class="fa fa-arrows-alt fa-fw"></i><span class="text">Enrollment</span></a></li>
            <li class="<?=$page=='coupon'?'active':''?>"><a href="<?php echo base_url()?>admin/couponList"><i class="fa fa-ticket  fa-fw"></i><span class="text">Coupon</span></a></li>
            <li>
                <a href="#" class="js-sub-menu-toggle">
                    <i class="fa fa-check-circle-o fa-fw"></i><span class="text">Questionaire</span>
                    <i class="toggle-icon fa fa-angle-left"></i>
                </a>
                <ul class="sub-menu ">
                    <li><a href="<?php echo base_url()?>admin/questionaireSetting"><span class="text">Questionaire Setting</span></a></li>
                    <li><a href="<?php echo base_url()?>admin/questionaireEnrollment"><span class="text">Questionaire Results</span></a></li>
                </ul>
            </li>
           <!-- <li><a href="typography.html"><i class="fa fa-hand-o-up  fa-fw"></i><span class="text">Export to iGracias</span></a></li>-->
            <li class="">
                <a href="#" class="js-sub-menu-toggle">
                    <i class="fa fa-edit fw"></i><span class="text">Masterdata</span>
                    <i class="toggle-icon fa fa-angle-left"></i>
                </a>
                <ul class="sub-menu " style="<?=$page=='master'?'display:block':''?>">
                    <li class="<?=$subpage=='country'?'active':''?>"><a href="<?php echo base_url()?>admin/countryList"><span class="text">Country</span></a></li>
                    <li class="<?=$subpage=='requirement'?'active':''?>"><a href="<?php echo base_url()?>admin/requirementsList"><span class="text">Requirements</span></a></li>
                    <li class="<?=$subpage=='degree'?'active':''?>"><a href="<?php echo base_url()?>admin/degreeList"><span class="text">Degree</span></a></li>
                    <li class="<?=$subpage=='faculty'?'active':''?>"><a href="<?php echo base_url()?>admin/facultyList"><span class="text">Faculty</span></a></li>
                    <li class="<?=$subpage=='studyprogram'?'active':''?>"><a href="<?php echo base_url()?>admin/studyProgramList"><span class="text">Study Program</span></a></li>
                    <li class="<?=$subpage=='course'?'active':''?>"><a href="<?php echo base_url()?>admin/courseList"><span class="text">Course</span></a></li>
                    <li class="<?=$subpage=='language'?'active':''?>"><a href="<?php echo base_url()?>admin/languageList"><span class="text">Language</span></a></li>
                    <li class="<?=$subpage=='scholarshiptype'?'active':''?>"><a href="<?php echo base_url()?>admin/scholarshiptypeList"><span class="text">Scholarship</span></a></li>
                </ul>
            </li>
            <li class="<?=$page=='user'?'active':''?>"><a href="<?php echo base_url()?>admin/userManagement"><i class="fa fa-user  fa-fw"></i><span class="text">User Management</span></a></li>
        </ul>
    </nav>
    <!-- /main-nav -->

    <div class="sidebar-minified js-toggle-minified">
        <i class="fa fa-angle-left"></i>
    </div>


</div>