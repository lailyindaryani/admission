<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/FacultyList"> Faculty</a></li>
            <li class="active"> Faculty List</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Faculty List</h2>
    <em>Faculty Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
<!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>
            <div class="widget-content">
			<?php $this->load->view('includes/messages'); ?>
                <div class="form-group">
                    <div class="btn-group">
                        <button onclick="location.href='<?php echo base_url()?>admin/facultyAdd'" class="btn btn-danger"> Add New &nbsp;
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="table-basic">
                    <table id="tableFaculty" class="table table-sorting table-hover  table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Faculty Name INA</th>
                            <th >Faculty Name ENG</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<!--edit-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/facultyUpdate" class="form-horizontal modal-form" role="form" method="post" id="ioform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update  Faculty</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="facultyid" id="facultyid" value="">
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label"> Faculty Name INA</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="facultynameina" id="facultynameina" placeholder=" Faculty Name INA" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label"> Faculty Name ENG</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="facultynameeng" id="facultynameeng" placeholder=" Faculty Name ENG" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this  Faculty?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).on("click", ".edit-button", function () {
        var facultyid = $(this).data('id');

        $.ajax({
            type    : "POST",
            url     : "<?php echo base_url()?>admin/FacultyAjaxGetDetail",
            data    : {facultyid: facultyid},
            success : function (data) {
                data = $.parseJSON( data );
                $('.modal-form #facultyid').val(data[0].FACULTYID);
                $('.modal-form #facultynameina').val(data[0].FACULTYNAMEINA);
                $('.modal-form #facultynameeng').val(data[0].FACULTYNAMEENG);
            }
        });
    });

    $('#ioform').validate({
        rules: {
            facultynameina: {
                required: true
            },
            facultynameeng: {
                required: true
            }
        },
        submitHandler: function (form) {
            var facultyid = $(".modal-form #facultyid").val();
            var facultynameina = $(".modal-form #facultynameina").val();
            var facultynameeng = $(".modal-form #facultynameeng").val();
            $.ajax({
                type    : "POST",
                data    : {facultyid:facultyid, facultynameina:facultynameina, facultynameeng:facultynameeng},
                url     : "<?php echo base_url()?>admin/facultyUpdate",
                success : function(data) {
					alert('UPDATE DATA SUCCESSFULL');
                    $('#tableFaculty').DataTable().ajax.reload();
                    $("#edit-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
					alert('FAILED UPDATE DATA');
                    console.log(e);
                    return false;
                }
            });
        }
    });

    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var facultyid = $(this).data('id');

        $.ajax({
            method  : 'POST',
            data    : {facultyid:facultyid},
            url     : "<?php echo base_url()?>admin/facultyDrop",
            success : function (data) {
                $('#tableFaculty').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
                return false;
            }
        });
    });

    $(document).ready(function() {
        var dt= $('#tableFaculty').dataTable( {
            "order": [[ 2, "ASC" ]],
            "columnDefs": [
                { "width": "10%", "targets": 0},
                { "width": "15%", "targets": 3}
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateFaculty",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>'+aData[2]+'</td>' +
                        '<td>' +
                        '   <button data-toggle="modal" data-id="'+aData[3]+'" title="edit" class="edit-button btn btn-xs btn-primary" href="#edit-modal"><span class="fa fa-edit"></span></button>  ' +
                        '   <button data-id="'+aData[3]+'" data-toggle="modal" data-target="#confirm-delete-modal" class="delete-button btn btn-xs btn-danger"><span class="fa fa-trash"></span></button>' +
                        '</td>'
                    );
                    return nRow;
                },
        } );
    });
</script>



