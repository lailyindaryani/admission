<?php 
	date_default_timezone_set('Asia/Jakarta');
    // echo "<pre>"; print_r($programs);
   // echo "<pre>"; print_r($degrees);die;
?>
<form action="<?php echo base_url()?>admin/enrollmentUpdate" enctype="multipart/form-data" class="form-horizontal modal-form" role="form" method="post" id="ioform">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Update Enrollment</h4>
    </div>
    <div class="modal-body">
        <input type="hidden" name="enrollmentid" id="enrollmentid" value="<?php echo $enrollment['ENROLLMENTID']?>">
       <!--  <div class="form-group">
            <label for="" class="col-sm-3 control-label">Program</label>
            <div class="col-sm-9 ">
                <label for="program" class="form-control" id="program"><?php echo $enrollment['PROGRAMNAME']?></label>
            </div>
        </div> -->

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Enrollment Name</label>
            <div class="col-sm-9">
                <input id="description" name="description" class="form-control" type="text" placeholder="Enrollment Name" value="<?php echo $enrollment['DESCRIPTION']?>" required>
            </div>
        </div>

         <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Program</label>
                                        <div class="col-sm-9 ">
                                            <select name="program" class="" id="program" required>
                                                <option></option>
                                                <?php 
                                                    foreach ($programs as $row){ 
                                                        if($row['PROGRAMID']==$enrollment['PROGRAMID']){
                                                            echo '<option value="'.$row['PROGRAMID'].'" selected>'.$row['PROGRAMNAME'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$row['PROGRAMID'].'" >'.$row['PROGRAMNAME'].'</option>';
                                                        }
                                                    } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Degree</label>
                                        <div class="col-sm-9 ">
                                            <select name="degree" id="degree" required>
                                                <option></option>
                                                 <?php 
                                                    foreach ($degrees as $row){ 
                                                        if($row['DEGREEID']==$enrollment['DEGREEID']){
                                                            echo '<option value="'.$row['DEGREEID'].'_'.$row['PROGRAMDEGREEID'].'" selected>'.$row['DEGREE'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$row['DEGREEID'].'_'.$row['PROGRAMDEGREEID'].'" >'.$row['DEGREE'].'</option>';
                                                        }
                                                    } ?> 
                                            </select>
                                        </div>
                                    </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Academic Year</label>
            <div class="col-sm-9">
                <select name="year" class="" id="year" required>
                    <?php
                    $yearnow = date('Y');
                    for($i = $yearnow-1;$i<=$yearnow+1;$i++){
                        echo "<option value=$i>$i</option>";
                    }
                    ?>
                </select>
            </div>
        </div>

        <!--<div class="form-group">
            <label for="" class="col-sm-3 control-label">Period</label>
            <div class="col-sm-9">
                <input type="text" name="period" id="period" placeholder="Enter period" class="form-control" value="<?=$enrollment['PERIOD']?>" required/>
                <p class="help-block"><em>Example: <?=date('Y')?>-1; or <?=date('Y')?>-2; or <?=date('M Y')?></em></p>
            </div>
        </div>-->
		<div class="form-group">
            <label for="" class="col-sm-3 control-label">Period</label>
			<div class="col-sm-5">
                <select name="period_year" class="" id="period_year" required>
                    <option></option>
                    <?php
                        $yearnow = date('Y');
						$s=$enrollment['PERIOD'];
						$a=explode('-',$s);
                        for($i = $yearnow-1;$i<=$yearnow+1;$i++){
							if($a[0]==$i){
								 echo "<option value=$i selected='selected'>$i</option>";
							}else{
								 echo "<option value=$i>$i</option>";
							}
							//$selected = $i==$yearnow?"selected":"";
                           
                        }
                    ?>
                </select>
            </div>
			<input type="hidden" name="period_lama" id="period" value="<?php echo $enrollment['PERIOD']?>" required/>							
            <div class="col-sm-4">
				<select name="period_bulan" class="" id="period_bulan" required>
					<option></option>
						<?php
						//$month = date('n');
							for($i = 1;$i<=12;$i++){
								if($a[1]==$i){//$a[0==$i]
									echo "<option value=$i selected='selected'>$i</option>";
								}else{
									echo "<option value=$i>$i</option>";
								}
                                //$selected = $i==$month?"selected":"";
                                //echo "<option value=$i $selected>$i</option>";
                            }
                        ?>
                </select>
                                           <!-- <input type="text" name="period" id="period" placeholder="Enter period" class="form-control" required/>
                                            <p class="help-block"><em>Example: <?=date('Y')?>-1; or <?=date('Y')?>-2; or <?=date('M Y')?></em></p>-->
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Start Date</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input id="startdate" name="startdate" class="form-control" type="text" value="<?php echo $enrollment['STARTDATE']?>" required>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">End Date</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input id="enddate" name="enddate" class="form-control" type="text" value="<?php echo $enrollment['ENDDATE']?>" required>
                </div>
            </div>
        </div>


        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Picture</label>
            <div class="col-sm-9">
                <input type="file" id="imagefile" name="imagefile"/>
                <p class="help-block" id="current-image">
                    <?php if(!empty($enrollment['IMAGE'])) { ?>
                        <a class="text text-primary" href="<?php echo base_url()."uploads/".$enrollment['IMAGE']?>" target="_blank"><em><?php echo substr($enrollment['IMAGE'],0,50)?></em></a>
                        <button type="button" data-id="<?php echo $enrollment['ENROLLMENTID']?>" class="btn btn-link" id="bt-remove-file" title="Remove File"> <i class='fa fa-times text-danger'></i></button>
                    <?php }else{ ?>
                        <em>No Data</em>
                    <?php } ?>
                </p>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Program Description</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="programdescription" id="programdescription" placeholder="Program Description"><?php echo $enrollment['PROGRAMDESCRIPTION']?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
                <label class="radio radio-inline">
                    <input type="radio" id="statusT" name="status" value="OPEN" checked> OPEN
                </label>
                <label class="radio radio-inline">
                    <input type="radio" id="statusF" name="status" value="CLOSED"> CLOSED
                </label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="form-group">
            <button type="submit" id="update" class="btn btn-primary">Update</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $('#program').on("change", function(e) {
            var programid = $("#program").val();

            $.ajax({
                type    : "GET",
                url     : "<?php echo base_url()?>admin/ajaxGetDegreeByProgram/"+programid,
                success : function (data) {
                    data = $.parseJSON( data );
                    var options = $("#degree");
                    options.empty();
                    $.each(data, function() {
                        options.append($("<option />").val(this.DEGREEID).text(this.DEGREE));
                    });
                },
                error : function (e) {
                    console.log(e.responseText);
                }
            });
        });
    $("#program").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select program",
        });
    $("#degree").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select degree",
        });
    $("#year").select2({minimumResultsForSearch: -1});
    $("#year").select2("val","<?php echo $enrollment['ACADEMICYEAR']?>");
	$("#period_bulan").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select month period",
        });
        //$("#period_bulan").select2('val','');
		
		$("#period_year").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select year period",
        });
        //$("#period_year").select2('val','');
    <?php
    if($enrollment['ENROLLMENTSTATUS']=='OPEN')
        echo "$('#statusT').prop('checked', 'true')";
    else
        echo "$('#statusF').prop('checked', 'true')";
    ?>

    $("#startdate").datepicker({
        defaultDate: new Date(),
        autoclose: true
    });

    $("#enddate").datepicker({
        defaultDate: new Date(),
        autoclose: true
    });

    $("#bt-remove-file").on('click',function () {
        if(confirm("are you sure?")){
            var enrollid = $(this).data('id');

            $.ajax({
                type    : "POST",
                data    : {enrollmentid:enrollid},
                url     : "<?php echo base_url()?>admin/enrollmentRemoveFile",
                success : function(data) {
                    $('.modal-form #current-image').html("<em>No Data</em>");
                    return false;
                },
                error   : function (e) {
                    console.log(e);
                    return false;
                }
            });
        }
    });

    $("#ioform" ).validate({
        rules: {
            description:{
                required: true,
            },
            year: {
                required: true,
            },
            period:{
                required: true,
            },
            startdate: {
                required: true,
            },
            enddate: {
                required: true,
            }
        }
    });
</script>