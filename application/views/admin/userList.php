<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/userManagement">User Management</a></li>
            <li class="active">User List</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Users List</h2>
    <em>User Management</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
<!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>
            <div class="widget-content">
                <div class="form-group">
                    <div class="btn-group">
                        <button onclick="location.href='<?php echo base_url()?>admin/userAdd'" class="btn btn-danger"> Add New &nbsp;
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
				  <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <div class="top-content">
                        <ul class="list-inline quick-access">
                            <li>
                                <h4>Number of participants: <em id="participant-number">4</em></h4>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table table-responsive">
                    <table id="tableUsers" class="table table-sorting table-hover table-striped datatable display">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Full Name</th>
                            <th >Email</th>
                            <th >Group</th>
                            <th >Status</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<!--edit-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/userUpdate" class="form-horizontal modal-form" role="form" method="post" id="userdataform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update User Data</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="userid" id="userid" value="">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Full Name</label>
                        <div class="col-sm-9 ">
                            <input type="text" onpaste="return blockSpecialChar(event)" onkeypress="return blockSpecialChar(event)" class="form-control" name="fullname" id="fullname" placeholder="Enter full name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Group</label>
                        <div class="col-sm-9 ">
                            <select name="usergroup" class="" id="usergroup">
                                <option></option>
                                <?php foreach ((array)$groups as $row){ ?>
                                    <option value="<?=$row['USERGROUPID']?>"><?=$row['USERGROUPNAME']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" placeholder="Enter Email" name="email" id="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-9">
                            <label class="control-inline fancy-radio col-sm-4">
                                <input type="radio" id="statusY" name="status" value="Y">
                                <span><i></i>Active</span>
                            </label>

                            <label class="control-inline fancy-radio col-sm-4">
                                <input type="radio" id="statusN" name="status" value="N">
                                <span><i></i>Not Active</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--change password-->
<div class="modal fade" id="change-password-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/userUpdatePassword" class="form-horizontal modal-form" role="form" method="post" id="change-password-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Change User Password</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="userid" id="userid" value="">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">New Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" placeholder="Enter Password" name="password" id="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Confirm Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" placeholder="Retype Password" name="repassword" id="repassword">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this User?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).on("click", ".edit-button", function () {
        var userid = $(this).data('id');
        $.ajax({
            url     : "<?php echo base_url()?>admin/userAjaxGetDetail/"+userid,
            success : function (data) {
                data = $.parseJSON( data );
                $('.modal-form #userid').val(data.USERID);
                $('.modal-form #fullname').val(data.FULLNAME);
                $('.modal-form #usergroup').select2("val",(data.USERGROUPID));
                $('.modal-form #email').val(data.EMAIL);
                if(data.ACTIVESTATUS=='Y')
                    $("#statusY").prop("checked", true)
                else
                    $("#statusN").prop("checked", true)
            }
        });
    });

    $('#userdataform').validate({
        rules: {
            fullname: {
                required: true
            },
            usergroup: {
                required : true
            },
            email: {
                required: true
            },
            status: {
                required: true
            },
        },
        submitHandler: function (form) {
            var userid      = $("#userdataform #userid").val();
            var fullname    = $("#userdataform #fullname").val();
            var usergroup   = $("#userdataform #usergroup").val();
            var email       = $("#userdataform #email").val();
            var status      = $("input[name='status']:checked").val();

            $.ajax({
                type    : "POST",
                data    : {userid:userid, fullname:fullname, usergroup:usergroup, email:email, status:status},
                url     : "<?php echo base_url()?>admin/userUpdate",
                success : function(data) {
                    $('#tableUsers').DataTable().ajax.reload();
                    $("#edit-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
                    console.log(e);
                }
            });
        }
    });

    $(document).on("click", ".change-password-button", function () {
        var userid = $(this).data('id');
        $('.change-password-form #userid').val(data.USERID);
    });

    $('#change-password-form').validate({
        rules: {
            password: {
                required: true,
            },
            repassword:{
                required: true,
                equalTo: "#password"
            }
        },
        submitHandler: function (form) {
            var userid      = $("#change-password-form #userid").val();
            var password    = $("#change-password-form #password").val();

            $.ajax({
                type    : "POST",
                data    : {userid:userid, password:password},
                url     : "<?php echo base_url()?>admin/userChangePassword",
                success : function(data) {
                    $('#tableUsers').DataTable().ajax.reload();
                    $("#change-password-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
                    console.log(e);
                }
            });
        }
    });

    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var userid = $(this).data('id');
        $.ajax({
            method  : 'POST',
            data    : {userid:userid},
            url     : "<?php echo base_url()?>admin/userDrop",
            success : function (data) {
                $('#tableUsers').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
             //   return false;
            }
        });
    });

    $(document).ready(function() {
        $("#usergroup").select2({
            minimumResultsForSearch: -1,
            placeholder : 'Select Group'
        });

        $(".filter").on("change", function () {
            $('#tableUsers').DataTable().ajax.reload();
        });

		var dt= $('#tableUsers').DataTable( {
            "order": [[ 1, "ASC" ]],
            "columnDefs": [
                { "width": "15%", "targets": 4, "searchable": true},
                { "width": "15%", "targets": 5, "searchable": true},
				 {
                    "targets"   : 5,
                    "data"      : 5
                },
                {"targets": 2, "searchable": true},
                {"targets": 3, "searchable": true},
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                [-1, 30, 40, 50, 100, -1],
                ["All", 30, 40, 50, 100]
            ],
            "fnDrawCallback": function() {
                //initAction();
            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateUsers",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $(nRow).html(
                        '<td >'+aData[0]+'</td>' +
                        '<td >'+aData[1]+'</td>' +
                        '<td>'+aData[2]+'</td>' +
                        '<td >'+aData[3]+'</td>' +
                        '<td >'+(aData[4]=='Y'?'Active':'Not Active')+'</td>' +
                        '<td>' +
                        '   <button data-toggle="modal" data-id="'+aData[5]+'" title="Edit Detail" class="edit-button btn btn-xs btn-primary" href="#edit-modal"><span class=\"fa fa-edit\"></span></button>  ' +
                        '   <button data-toggle="modal" data-id="'+aData[5]+'" title="Change Password" class="edit-button btn btn-xs btn-default" href="#change-password-modal"><span class=\"fa fa-lock\"></span></button>  ' +
                        '   <button data-id="'+aData[5]+'" data-toggle="modal" data-target="#confirm-delete-modal" class="delete-button btn btn-xs btn-danger" title="Delete"><span class=\"fa fa-trash\"></span></button>' +
                        '</td>'
                    );
                    return nRow;
                },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                $.getJSON( sSource, aoData, function (json) {
                    // /* Do whatever additional processing you want on the callback, then tell DataTables
                    fnCallback(json);

                    $("#participant-number").text(json.iTotalRecords);

                } );
            }
        } );
/*
        $("input[type='search']").on('keyup',function(){
            $('#tableUsers').DataTable().destroy();
            $('#tableUsers').DataTable().search(this.value,true).draw();
        });*/
        
    });
	function blockSpecialChar(e){
					var k;
					document.all ? k = e.keyCode : k = e.which;
					return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
				}
</script>



