<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/questionaireEnrollment">Questionaire</a></li>
            <li class="active">Questionaire List</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Questionaire List</h2>
    <em>Questionaire Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
<!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>
            <div class="widget-content">
                <div class="table-basic">
                    <table id="tableQuestionaire" class="table table-sorting table-hover  table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Enrollment</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->




<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
   
    $(document).ready(function() {
        var dt= $('#tableQuestionaire').dataTable( {
            //"bJQueryUI": true,
            "order": [[ 1, "ASC" ]],
            /*  sDom: "T<'clearfix'>" +
             "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
             "t"+
             "<'row'<'col-sm-6'i><'col-sm-6'p>>",
             "tableTools": {
             },*/
            "columnDefs": [
                { "width": "8%", "targets": 0},
                { "width": "15%", "targets": 2},
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                /*   [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]*/
				 [-1,30, 40, 50, 100],
                ["All",30, 40, 50, 100, ]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateEnrollmentQuestionaire",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    var manageLink = "<?php echo base_url()?>admin/questionaireResult/"+aData[2];
                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>' +
                        '   <button onclick="location.href=\'' + manageLink + '\'" title="Manage"><span class=\"fa fa-list\"></span></button>' +
                        '</td>'

                    );
                    return nRow;
                },
        } );
		
		 $("input[type='search']").on('keyup',function(){
            $('#tableQuestionaire').DataTable().destroy();
            $('#tableQuestionaire').DataTable().search(this.value,true).draw();
        });
    });
</script>



