
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/languageList">Scholarship</a></li>
                <li class="active">Scholarship Add</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Scholarship Add</h2>
            <em>Scholarship Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/scholarshiptypeStore" class="form-horizontal" role="form" method="post" id="ioform">
                                <fieldset>
                                    <legend>Scholarship Data</legend>
                                    <div class="form-group">
                                        <label for="ticket-subject" class="col-sm-3 control-label">Scholarshiptype Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="scholarshiptypename" id="scholarshiptypename" placeholder="Language Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ticket-subject" class="col-sm-3 control-label">Active Status</label>
                                        <div class="col-sm-9">
                                            <select name="activestatus" class="" id="activestatus">
                                                <option></option>
                                                <option value="Y">Yes</option>
                                                <option value="N">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/scholarshiptypeList'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $( "#ioform" ).validate({
        rules: {
            scholarshiptypename: {
                required: true
            },
            activestatus: {
                required: true
            }
        }
    });
    $(document).ready(function() {
        $("#activestatus").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select Active Status",
        });
    });     
</script>