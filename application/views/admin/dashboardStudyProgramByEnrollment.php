
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
		<div class="col-lg-6 ">
				<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="<?php echo base_url().'admin/dashboardEnrollment'?>">Home</a></li>
						<li><a href="<?php echo base_url().'admin/dashboardEnrollment'?>">Summary by Enrollment</a></li>
						<li class="active">Summary Study Program by Enrollment</li>
				</ul>
		</div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
		<h2>Summary Report</h2>
		<em >Summary Report of Study Program <?php echo $programname; ?>'s Students <?= $academicyear?></em>
</div>

<div class="main-content">
<div class="row">
		<div class="col-md-12">
				<!-- SUPPOR TICKET FORM -->
				<div class="widget">
						<div class="widget-header">
								<!--<h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
						</div>

						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						<div class="widget-content">
												<div class="table-basic">
														<table id="tableProgram" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Study Program</th>
																		<th ><?php echo $programname?></th>	
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div><!-- style="display:none" -->
					<?php echo '<table id="datatable2" style="display:none" class="table table-sorting table-hover  table-striped datatable">';
                        echo '<thead>';
                        echo '<tr>';?>
							<?php echo '<th></th>';?>
							<?php for( $i=0; $i<sizeof($data_studyprogramby) ; $i++ ){
                           echo "<th >".$data_studyprogramby[$i]['STUDYPROGRAMNAME']."</th>";
							}?>
                        <?php echo '</tr>';
                        echo '</thead>';
                        echo '<tbody >';
                        echo '<tr>';?>
							<?php echo '<td font size="5">Participants</td>';?>
							<?php for( $i=0; $i<sizeof($data_studyprogramby) ; $i++ ){
							echo "<td>".$data_studyprogramby[$i]['JUMLAH']."</td>";
							}?>
                        <?php echo '</tr>';
                        echo '</tbody>';
                    echo '</table>';?>
					<input type="hidden" value="<?php echo $programname?>" id="programname">
						</div>
				</div>
			</div>
</div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->




<script type="text/javascript">
		$(document).ready(function() {
				var dt= $('#tableProgram').dataTable( {
						//"bJQueryUI": true,
						"order": [[ 1, "DESC" ]],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						 "columnDefs": [
                { "width": "8%", "targets": 0},
             //   { "width": "12%", "targets": 3},
            ],
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
								[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>admin/datatableStudyProgramByEnrollment/<?php echo $programid.'/'.$programname.'/'.$academicyear;?>",
						"fnRowCallback":
								function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
										$(nRow).html(
												'<td>'+aData[0]+'</td>' +
												'<td>'+aData[1]+'</td>' +
												'<td>'+aData[2]+'</td>' 		
										);
										return nRow;
								},
				} );
	var programname = $('#programname').val();	
	$('#container').highcharts({
        data: {
            table: document.getElementById('datatable2'),
            switchRowsAndColumns: true,
            endRow: 1
        },
        chart: {
            type: 'pie'
        },
        title: {
            text: "Summary Report of Study Program "+programname+"'s Students "
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Number of Participants in Regular Program'
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                    this.point.y +' '+ this.point.name.toLowerCase();
            }
        }
    });
		});
</script>
