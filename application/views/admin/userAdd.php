
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/enrollmentList">User Management</a></li>
                <li class="active">User Add</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>User Add</h2>
            <em>User Management</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/userStore" class="form-horizontal" role="form" method="post" id="ioform">
                                <fieldset>
                                    <legend>User Data</legend>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Full Name</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Enter full name">
                                        </div>
										<div class="col-sm-9" align="middle" style="text-size: 1px;">
											<p style="font-size: 11px;" class="help-block"><em>Special character not allowed (ex: @'"óè etc)</em></p>
										</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Group</label>
                                        <div class="col-sm-9 ">
                                            <select name="usergroup" class="" id="usergroup">
                                                <option></option>
                                                <?php foreach ((array)$groups as $row){ ?>
                                                    <option value="<?=$row['USERGROUPID']?>"><?=$row['USERGROUPNAME']?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" placeholder="Enter Email" name="email" id="email" onchange="return validateEmail()">
                                        </div>
										<div class="col-sm-9" align="middle" style="text-size: 1px;">
							<p style="font-size: 11px;" class="help-block"><em>To register, kindly use email address Google Mail</em></p>
						</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Enter Password" name="password" id="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Confirm Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Retype Password" name="repassword" id="repassword">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Initial Status</label>
                                        <div class="col-sm-9">
                                            <label class="control-inline fancy-radio col-sm-2">
                                                <input type="radio" id="statusY" name="status" value="N">
                                                <span><i></i>Active</span>
                                            </label>

                                            <label class="control-inline fancy-radio col-sm-2">
                                                <input type="radio" id="statusN" name="status" value="N">
                                                <span><i></i>Not Active</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/userManagement'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#usergroup").select2({
            minimumResultsForSearch: -1,
            placeholder : 'Select Group'
        });
    });

    $("#ioform" ).validate({
        rules: {
            fullname: {
                required: true
            },
            email: {
                required: true
            },
            usergroup: {
                required: true
            },
            password: {
                required: true,
            },
            repassword:{
                required: true,
                equalTo: "#password"
            }
        }
    });
	
	function validateEmail(){
		var emailField = $("#email").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailField) == false){
			var emailField = $("#email").val('');
            alert('Please type a valid email address');
            return false;
        }
		var type = 'gmail.com';
			var file = $('#email').val();
			var valid;
			if (file.substr(file.length - type.length, type.length).toLowerCase() == type.toLowerCase()) {
				valid = true;
			}
			if (valid!=true) {
				$('#email').val('');
				alert("To register, kindly use email address other than Yahoo Mail");
			}
        return true;
	}
</script>