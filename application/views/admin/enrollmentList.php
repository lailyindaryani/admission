<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/enrollmentList">Enrollment</a></li>
            <li class="active">Enrollment List</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Enrollment List</h2>
    <em>Enrollment Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
<!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>
			<div class="row" style="border: 1px solid #ccc; margin:5px;">
								<div class="col-md-12">
										<div class="widget-content">
												<div class="row form-horizontal">
														<div class="col-md-4">
																<div class="form-group">
																		<label for="status" class="col-sm-5 control-label">Status</label>
																		<div class="col-md-6">
																				<select id="status" name="status" class="filter">
																						<option value="">-All-</option>
																						<option value="OPEN">OPEN</option>
																						<option value="CLOSED">CLOSED</option>
																				</select>
																		</div>
																</div>
														</div>
												</div>
										</div>
								</div>
			</div>
            <div class="widget-content">
			<?php $this->load->view('includes/messages'); ?>
                <div class="form-group">
                    <div class="btn-group">
                        <button onclick="location.href='<?php echo base_url()?>admin/enrollmentAdd'" class="btn btn-danger"> Add New &nbsp;
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="table-basic">
                    <table id="tableEnrollment" class="table table-sorting table-hover  table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Description</th>
                            <th >Program</th>
                            <th >Degree</th>
                            <th >Period</th>
                            <th >Year</th>
                            <th >Status</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<!--edit-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this enrollment?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var enrollmentid = $(this).data('id');

        $.ajax({
            method  : 'POST',
            data    : {enrollmentid:enrollmentid},
            url     : "<?php echo base_url()?>admin/enrollmentDrop",
            success : function (data) {
                $('#tableEnrollment').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
                return false;
            }
        });
    });

    $(document).ready(function() {
        $(".modal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });
		
		$("#status").select2();
		$("#status").on("change", function (e) {
            $('#tableEnrollment').DataTable().ajax.reload();
        });
        var dt= $('#tableEnrollment').dataTable( {
            //"bJQueryUI": true,
            "order": [],
            "columnDefs": [
                { "width": "10%", "targets": 5},
                { "width": "10%", "targets": 6},
                { "width": "10%", "targets": 7}
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
             /*   [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]*/
				 [-1,30, 40, 50, 100],
                ["All",30, 40, 50, 100, ]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateEnrollment",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    var editButton = "<a href='<?php echo base_url()?>admin/enrollmentEdit/"+aData[7]+"' class='btn btn-xs btn-primary' data-toggle='modal' data-target='#edit-modal'><span class='fa fa-edit'></span></a>";
                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>'+aData[2]+'</td>' +
                        '<td>'+aData[3]+'</td>' +
                        '<td>'+aData[4]+'</td>' +
                        '<td>'+aData[5]+'</td>' +
                        '<td>'+aData[6]+'</td>' +
                        '<td>' +
                        editButton +
                        '   <button data-id="'+aData[7]+'" data-toggle="modal" data-target="#confirm-delete-modal" class="btn btn-xs btn-danger delete-button" title="Delete"><span class=\"fa fa-trash\"></span></button>' +
                        '</td>'
                    );
                    return nRow;
                },
			"fnServerData": function ( sSource, aoData, fnCallback ) {
								/* Add some extra data to the sender */
								aoData.push(
									{ "name": "status", "value": $("#status").val() },
								);
								$.getJSON( sSource, aoData, function (json) {
										/* Do whatever additional processing you want on the callback, then tell DataTables */
										fnCallback(json)
								} );
						}
        } );
		
		 $("input[type='search']").on('keyup',function(){
            $('#tableEnrollment').DataTable().destroy();
            $('#tableEnrollment').DataTable().search(this.value,true).draw();
        });
    });
</script>



