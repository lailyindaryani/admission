
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/questionaireEnrollment">Questionaire</a></li>
            <li class="active">Questionaire Result</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Questionaire Result</h2>
    <em>Questionaire Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
<!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>
			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            <div class="widget-content">			
                <div class="table-basic">
                    <table id="tableQuestionaire" class="table table-sorting table-hover  table-striped datatable">
                        <thead>
                        <tr>
							<th >No</th>
							<th> Option Value </th>
							<th >Count Answer</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                           <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<input type="hidden" id="enrollmentName" value="<?php echo $enrollmentName; ?>">
				<?php echo '<table id="datatable" style="display:none" class="table table-sorting table-hover  table-striped datatable">';
                        echo '<thead>';
                        echo '<tr>';?>
							<?php echo '<th></th>';?>
							<?php for( $i=0; $i<sizeof($data_answer) ; $i++ ){
                           echo "<th >".$data_answer[$i]['OPTIONVALUE']."</th>";
							}?>
                        <?php echo '</tr>';
                        echo '</thead>';
                        echo '<tbody >';
                        echo '<tr>';?>
							<?php echo '<td font size="5">'.$data_answer[0]['QUESTION'].'</td>';?>
							<?php for( $i=0; $i<sizeof($data_answer) ; $i++ ){
							echo "<td>".$data_answer[$i]['JUMLAH']."</td>";
							}?>
                        <?php echo '</tr>';
                        echo '</tbody>';
                    echo '</table>';?>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<!--edit-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/questionaireUpdate" class="form-horizontal modal-form" role="form" method="post" id="ioform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Questionaire</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="questionaireid" id="questionaireid" value="">
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Questionaire Value</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="questionairevalue" id="questionairevalue" placeholder="Enter questionaire value"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this questionaire?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->



<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var questionaireid = $(this).data('id');

        $.ajax({
            method  : 'POST',
            data    : {questionaireid:questionaireid},
            url     : "<?php echo base_url()?>admin/questionaireDrop",
            success : function (data) {
                $('#tableQuestionaire').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
                return false;
            },
            error   :   function (e) {
                console.log(e);
                return false;
            }
        });
    });
	
	

    $(document).ready(function() {
        var dt= $('#tableQuestionaire').dataTable( {
            //"bJQueryUI": true,
            "order": [[ 1, "ASC" ]],
            /*  sDom: "T<'clearfix'>" +
             "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
             "t"+
             "<'row'<'col-sm-6'i><'col-sm-6'p>>",
             "tableTools": {
             },*/
            "columnDefs": [
                { "width": "8%", "targets": 0},
             //   { "width": "12%", "targets": 3},
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateQuestionaireResultAnswer/<?=$questionid.'/'.$enrollmentid?>",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    var manageLink = "<?php echo base_url()?>admin/questionaireAnswer/"+aData[2];
                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>'+aData[2]+'</td>' 
                     //   '<td>' +
                     //   '   <button onclick="location.href=\'' + manageLink + '\'" title="Manage"><span class=\"fa fa-list\"></span></button>' +
                      //  '   <button data-id="'+aData[3]+'" data-toggle="modal" data-target="#confirm-delete-modal" class="delete-button"><span class=\"fa fa-trash\"></span></button>' +
                     //   '</td>'

                    );
                    return nRow;
                },
        } );
		var enrollmentName = $('#enrollmentName').val();
	Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Questionaire Result Answer '+enrollmentName
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Count Answer'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
});
</script>



