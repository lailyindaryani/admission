<script>
	function validasi_privilege(){
		var checkbox= document.querySelector('input[name="privilege"]:checked');
		if(!checkbox) {
			alert('Please select privilege!');
			return false;
		}
	}
</script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/requirementsList">Requirements</a></li>
                <li class="active">Requirements Add</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Requirements Add</h2>
            <em>Requirements Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/requirementsStore" onsubmit="return validasi_privilege()"  class="form-horizontal" role="form" method="post" id="ioform" enctype="multipart/form-data">
                                <fieldset>
                                    <legend>Requirements Data</legend>
                                    <div class="form-group">
                                        <label for="requirementsname" class="col-sm-3 control-label required">Requirements Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="requirementsname" id="requirementsname" placeholder="Requirements Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="requirementstype" class="col-sm-3 control-label required">Type</label>
                                        <div class="col-sm-9">
                                            <select name="requirementstype" id="requirementstype">
                                                <option value="">Select Type</option>
                                                <option value="ATTACHMENT">Attachment</option>
                                                <option value="TEXT">Text</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="template-file" class="col-sm-3 control-label">Template File</label>
                                            <div class="col-sm-9">
                                                <input type="file" id="template-file" name="template-file">
                                                <p class="help-block"><em>Example block-level help text here.</em></p>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="privilege" class="col-sm-3 control-label">Privilege</label>
                                        <div class="col-sm-9">
                                            <?php foreach ($listprivileges as $lp){ ?>
                                                <label class="fancy-checkbox">
                                                    <input name="privilege" id="privilege" value="<?=$lp[0]?>" type="checkbox" class="form-control">
                                                    <span><i></i><?=$lp[1]?></span>
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="privilege" class="col-sm-3 control-label">Is Required</label>
                                        <div class="col-sm-9">
										<label class="fancy-checkbox">
                                           <input type="checkbox" class="form-control" name="isrequired" value="YES" checked> 
										   <span><i></i>Required</span> 
										   </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/requirementsList'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#requirementstype").select2({minimumResultsForSearch: -1});
    });

    $( "#ioform" ).validate({
        rules: {
            requirementsname: {
                required: true
            },
            requirementstype: {
                required: true
            },
        }
    });
	
	$('input[type="checkbox"]').on('change', function() {
		$('input[name="' + this.name + '"]').not(this).prop('checked', false);
	});
</script>