
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/programList">Coupon</a></li>
            <li class="active">Manage</li>
        </ul>
    </div>
</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Coupon</h2>
    <em>Manage Coupon</em>
</div>

<div class="main-content">
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
        </div>
        <div class="widget-content">
            <div class="wizard-wrapper">
                <div id="manageWizard" class="wizard">
                    <ul class="steps">
                        <li data-target="#step1" class="active"><span class="badge badge-info">1</span> Select Enrollment<span class="chevron"></span></li>
                        <li data-target="#step2"><span class="last"><span class="badge">2</span>Select Country</li>
                    </ul>
                </div>
                <div class="step-content">
                    <div class="step-pane active" id="step1">
                        <form action="<?php echo base_url()?>admin/processSelectEnrollment" class="form-horizontal" role="form" id="ioform" data-parsley-errors-container="#error1" method="POST">
                            <fieldset>
                                <legend>Select Enrollment</legend>
                                <div class="form-group">
                                    <label for="enrollment" class="col-sm-3 control-label">Select Enrollment<span id="error1"></span></label>
                                    <input type="hidden" value="<?php echo $couponid?>" name="couponid">
                                    <div class="col-sm-9">
                                        <select name="enrollmentid" id="enrollmentid" class="">
                                            <option>Select Enrollment</option>
                                        <?php foreach ($enrollments as $en){
                                            if($en['ENROLLMENTID']==$assignedenrollment['ENROLLMENTID'])
                                                $selected="selected";
                                            ?>
                                                <option value="<?php echo $en['ENROLLMENTID']?>" <?php echo @$selected?>><?php echo $en['DESCRIPTION']?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="actions">
                    <button type="button" id="btNextWizard" class="btn btn-danger btn-next" data-last="Finish"> Next <i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<div class="modal fade" id="confirm" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish save changes?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script>
<script src="<?php echo base_url();?>themes/_assets/js/plugins/parsley-validation/parsley.min.js"></script>

<script type="text/javascript">
//    $("#btPrevWizard").click(function () {
//        $("#ioform").submit();
//    });
    $('#enrollmentid').select2();
    $("#btNextWizard").click(function () {
        $("#ioform").submit();
    });
</script>