<div class="col-md-2 left-sidebar">

    <!-- main-nav -->
    <nav class="main-nav">

        <ul class="main-menu">
            <li><a href="typography.html"><i class="fa fa-hand-o-up  fa-fw"></i><span class="text">Program Selection</span></a></li>
            <li><a href="typography.html"><i class="fa fa-user  fa-fw"></i><span class="text">Personal Data</span></a></li>
            <li><a href="typography.html"><i class="fa fa-users fa-fw"></i><span class="text">Family Data</span></a></li>
            <li><a href="typography.html"><i class="fa fa-graduation-cap fa-fw"></i><span class="text">Educational Data </span></a></li>
            <li><a href="typography.html"><i class="fa fa-file-text fa-fw"></i><span class="text">Supporting Data</span></a></li>
            <li><a href="typography.html"><i class="fa fa-check-square fa-fw"></i><span class="text">Acceptance Status</span></a></li>

        </ul>
    </nav>
    <!-- /main-nav -->

    <div class="sidebar-minified js-toggle-minified">
        <i class="fa fa-angle-left"></i>
    </div>


</div>