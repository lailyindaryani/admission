
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/degreeList">Degree</a></li>
                <li class="active">Degree Add</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Degree Add</h2>
            <em>Degree Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/degreeStore" class="form-horizontal" role="form" method="post" id="ioform">
                                <fieldset>
                                    <legend>Degree Data</legend>
                                    <div class="form-group">
                                        <label for="degreename" class="col-sm-3 control-label">Degree Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="degreename" id="degreename" placeholder="Degree Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Type</label>
                                        <div class="col-sm-9">
                                            <select name="degreetype" class="" id="degreetype">
                                                <option value="">SELECT TYPE</option>
                                                <option value="DEGREE">DEGREE</option>
                                                <option value="SHORT COURSE">SHORT COURSE</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/degreeList'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#degreetype").select2();
    });

    $( "#ioform" ).validate({
        rules: {
            degreename: {
                required: true
            },
            degreetype: {
                required: true
            }
        }
    });
</script>