
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Form Layouts</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Program List</h2>
            <em>Program Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/programStore" class="form-horizontal" role="form" method="post" id="ioform">
                                <fieldset>
                                    <legend>Program Data</legend>
                                    <div class="form-group">
                                        <label for="ticket-subject" class="col-sm-3 control-label">Program Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="programname" id="programname" placeholder="Program Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Program Type</label>
                                        <div class="col-sm-9 ">
                                            <select name="programtype" class="" id="programtype">
                                                <option></option>
                                                <option value="ACADEMIC">Academic</option>
                                                <option value="NON_ACADEMIC">Non Academic</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Status Pengaturan Matakuliah</label>
                                        <div class="col-sm-9 ">
                                            <select name="statussubjectsetting" class="" id="statussubjectsetting">
                                                <option></option>
                                                <option value="Y">Ada</option>
                                                <option value="N">Tidak Ada</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group divmaxcredit">
                                        <label for="" class="col-sm-3 control-label">Maksimal SKS</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" placeholder="Maksimal SKS" oninput="validateNumber(this)" name="maxcredit" class="form-control" id="maxcredit">
                                        </div>
                                    </div>
                                    <div class="form-group divmaxstudy">
                                        <label for="" class="col-sm-3 control-label">Maksimal Program Studi</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" placeholder="Maksimal Program Studi" oninput="validateNumber(this)" name="maxstudy" class="form-control" id="maxstudy">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/programList'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".divmaxcredit").hide();
        $(".divmaxstudy").hide();
        $("#programtype").select2({
            minimumResultsForSearch: -1,
            placeholder : 'Select Program Type'
        });
        $("#statussubjectsetting").select2({
            minimumResultsForSearch: -1,
            placeholder : 'Pilih Status Pengaturan Matakuliah'
        });

        $( "#ioform" ).validate({
            rules: {
                programname: {
                    required: true
                },
                programtype: {
                    required: true
                }
            }
        });

        $("#statussubjectsetting").change(function(){
            //alert();
            if($(this).val()=='Y'){
                $(".divmaxcredit").show();
                $(".divmaxstudy").show();
            }else{
                $(".divmaxcredit").hide();
                $(".divmaxstudy").hide();
            }
        });
    });
    
    function validateNumber(root){
        var reet = root.value;
        var arr1= reet.length;
        var ruut = reet.charAt(arr1-1);
        if (reet.length > 0){
            var regex = /[0-9]|\./;
            if (!ruut.match(regex)){
                var reet = reet.slice(0, -1);
                $(root).val(reet);
            }
        }
    }
</script>