
<script>
	function validasi_privilege(){
		var checkbox= document.querySelector('input[name="privilege"]:checked');
		if(!checkbox) {
			alert('Please select privilege!');
			return false;
		}
	}
</script>
<form action="<?php echo base_url()?>admin/requirementsUpdate" onsubmit="return validasi_privilege()" class="form-horizontal modal-form" role="form" method="post" id="edit-form" enctype="multipart/form-data">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Update Requirements</h4>
    </div>
    <div class="modal-body">
        <input type="hidden" name="requirementsid" id="requirementsid" value="<?=$requirement['REQUIREMENTID']?>">
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Requirements Name</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="requirementsname" id="requirementsname" placeholder="Requirements Name" value="<?=$requirement['REQUIREMENTNAME']?>">
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Type</label>
            <div class="col-sm-9">
                <select name="requirementstype" id="requirementstype">
                    <option value="">--Select Type--</option>
                    <option value="ATTACHMENT">Attachment</option>
                    <option value="TEXT">Text</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Template File</label>
            <div class="col-sm-9">
                <input type="file" id="template-file" name="template-file"/>
                <p class="help-block" id="current-file">
                    <?php if(!empty($requirement['TEMPLATEFILE'])) { ?>
                        <a class="text text-primary" href="<?php echo base_url().$requirement['TEMPLATEFILEURL']?>" target="_blank"><em><?=substr($requirement['TEMPLATEFILE'],0,50)?></em></a>
                        <button type="button" data-id="<?=$requirement['REQUIREMENTID']?>" class="btn btn-link" id="bt-remove-file" title="Remove File"> <i class='fa fa-times text-danger'></i></button>
                    <?php }else{ ?>
                        <em>No Data</em>
                    <?php } ?>
                </p>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Privilege<br/>
                <em>Default: Admin Only</em>
            </label>
            <div class="col-sm-9">
                <?php foreach ($listprivileges as $lp){
					if($lp[0]==$assignedprivilege){
						echo '<label class="fancy-checkbox">';
						echo ' <input name="privilege" id="privilege" value="'.$lp[0].'" checked="checked" type="checkbox" class="form-control">';
						echo '<span><i></i>'.$lp[1].'</span></label>';
					}else{
						echo '<label class="fancy-checkbox">';
						echo ' <input name="privilege" id="privilege" value="'.$lp[0].'" type="checkbox" class="form-control">';
						echo '<span><i></i>'.$lp[1].'</span></label>';
					}
                    ?>
                <?php }?>
            </div>
        </div>
		<div class="form-group">
            <label for="" class="col-sm-3 control-label">Is Required</label>
                <div class="col-sm-9">
					<label class="fancy-checkbox">
					<?php if($requirement['ISREQUIRED']=='YES'){
						echo '<input type="checkbox" class="form-control" name="isrequired" value="YES" checked>'; 
						echo '<span><i></i>Required</span>';
					}else{
						echo '<input type="checkbox" class="form-control" name="isrequired" value="YES">';
						echo '<span><i></i>Required</span>';
					}?>						 
					</label>
                </div>
        </div>
    </div>

    <div class="modal-footer">
        <div class="form-group">
            <button type="submit" id="update" class="btn btn-primary">Update</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</form>

<script type="text/javascript">
	$('input[type="checkbox"]').on('change', function() {
		$('input[name="' + this.name + '"]').not(this).prop('checked', false);
	});
    $("#requirementstype").select2({minimumResultsForSearch: -1});
    $("#requirementstype").select2("val","<?=$requirement['TYPE']?>");

    $("#bt-remove-file").on('click',function () {
        if(confirm("are you sure?")){
            var reqid = $(this).data('id');

            $.ajax({
                type    : "POST",
                data    : {requirementsid:reqid},
                url     : "<?php echo base_url()?>admin/requirementsRemoveFile",
                success : function(data) {
                    $('.modal-form #current-file').html("<em>No Data</em>");
                    return false;
                },
                error   : function (e) {
                    console.log(e);
                    return false;
                }
            });
        }
    });

    $('#edit-form').validate({
        rules: {
            requirementstype: {
                required: true
            },
            requirementsname: {
                required: true
            }
        }
    });
</script>