
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/questionaireEnrollment">Questionaire</a></li>
                <li class="active">Add Question</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Add Question</h2>
            <em>Questionaire Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/questionaireUpdate" class="form-horizontal" role="form" method="post" id="question-edit">
                                <fieldset>
                                    <input type="hidden" name="questionid" id="questionid" value="<?=$question['QUESTIONID']?>">
                                    <legend>Questionaire Data</legend>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Question</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="question" id="question" placeholder="Ask your question" value="<?=$question['QUESTION']?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Question Type</label>
                                        <div class="col-sm-9">
                                            <?php
                                            $istext = "";
                                            $isoption = "checked";

                                            if(!$question_option || $question_option[0]['ISTEXT']=='Y'){
                                                $istext = "checked";
                                                $isoption = "";
                                            }
                                            ?>

                                            <label class="radio radio-inline">
                                                <input type="radio" class="question-type" id="question-multi-choice" name="question-type" <?=$isoption?> value="option"> Multiple Choice
                                            </label>
                                            <label class="radio radio-inline">
                                                <input type="radio" class="question-type" id="question-text" name="question-type" <?=$istext?> value="text"> Text
                                            </label>
                                        </div>
                                    </div>

                                        <div class="form-group" id="option-multi-choice" style="display: none">
                                            <label for="" class="col-sm-3 control-label"></label>
                                            <div class="col-md-9">
                                                <div class="table-basic">
                                                    <table id="tableOption" class="table table-sorting table-hover table-striped datatable">
                                                        <thead>
                                                        <tr>
                                                            <th >No</th>
                                                            <th >Options</th>
                                                            <th >Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group">
                                                    <div class="btn-group">
                                                        <button type="button" data-id="<?=$question['QUESTIONID']?>" data-toggle="modal" data-target="#modal-add-option" class="btn btn-danger button-add-option">Add Option
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/questionaireSetting'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->

<!--add option-->
<div class="modal fade" id="modal-add-option" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/questionaireOptionStore" class="form-horizontal modal-form" role="form" method="post" id="option-add">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Option</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="questionid" id="questionid" value="<?=$question['QUESTIONID']?>">
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Option Value</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="optionvalue" id="optionvalue" placeholder="Enter option value"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Add</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--end of add option-->

<!--edit option-->
<div class="modal fade" id="modal-edit-option" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/questionaireOptionUpdate" class="form-horizontal modal-form" role="form" method="post" id="option-edit">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Edit Option</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="optionid" id="optionid">
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Option Value</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="optionvalue" id="optionvalue" placeholder="Enter option value"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--end of edit option-->

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this option?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of delete option-->
<!--END OF MODAL-->



<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    <?php
        if(!$question_option || $question_option[0]['ISTEXT']=='Y') { ?>
            $("#option-multi-choice").hide();
    <?php } else{ ?>
        $("#option-multi-choice").show();
    <?php } ?>


    $(document).ready(function() {
        $("#questionairetype").select2();
        $("#question-text").on('click',function () {
            $("#option-multi-choice").hide();
        });

        $("#question-multi-choice").on('click',function () {
            $("#option-multi-choice").show();
        });

        var dt= $('#tableOption').dataTable( {
            "order": [[ 1, "ASC" ]],
            "columnDefs": [
                { "width": "15%", "targets": 0},
                { "width": "20%", "targets": 2},
            ],
            "bProcessing": true,
            "bServerSide": true,
            "fnDrawCallback": function() {
                //initAction();
            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateQuestionaireOption/<?=$question['QUESTIONID']?>",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>' +
                        '   <button type="button" data-toggle="modal" data-id="'+aData[2]+'" title="edit" class="edit-button" href="#modal-edit-option"><span class=\"fa fa-edit\"></span></button>  ' +
                        '   <button type="button" data-id="'+aData[2]+'" data-toggle="modal" data-target="#confirm-delete-modal" class="delete-button"><span class=\"fa fa-trash\"></span></button>' +
                        '</td>'

                    );
                    return nRow;
                }
        } );

        $(document).on("click", ".edit-button", function () {
            var optionid = $(this).data('id');

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url()?>admin/questionaireOptionAjaxGetDetail",
                data    : {optionid: optionid},
                success : function (data) {
                    data = $.parseJSON( data );
                    $('#option-edit #optionid').val(data.OPTIONID);
                    $('#option-edit #optionvalue').val(data.OPTIONVALUE);
                }
            });
        });

        $('#option-edit').validate({
            rules: {
                optionvalue: {
                    required: true
                },
            },
            submitHandler: function (form) {
                var optionid        = $("#option-edit #optionid").val();
                var optionvalue     = $("#option-edit #optionvalue").val();

                $.ajax({
                    type    : "POST",
                    data    : {
                        optionid     : optionid,
                        optionvalue  : optionvalue,
                    },
                    url     : "<?php echo base_url()?>admin/questionaireOptionUpdate",
                    success : function(data) {
                        console.log(data);
                        $('#tableOption').DataTable().ajax.reload();
                        $("#modal-edit-option").modal('hide');
                        return false;
                    },
                    error   : function (e) {
                        console.log(e);
                        return false;
                    }
                });
            }
        });

        $(document).on("click", ".delete-button", function () {
            var ID = $(this).data('id');
            $('#delete-confirm-button').data('id', ID)
        });

        $('#delete-confirm-button').click(function(){
            var optionid = $(this).data('id');

            $.ajax({
                method  : 'POST',
                data    : {optionid:optionid},
                url     : "<?php echo base_url()?>admin/questionaireOptionDrop",
                success : function (data) {
                    $('#tableOption').DataTable().ajax.reload();
                    $("#confirm-delete-modal").modal('hide');
                    return false;
                }
            });
        });

        $('#question-edit').validate({
            rules: {
                question        : {
                    required: true
                },
                'question-type' : {
                    required: true
                }
            }
        });

        $('#option-add').validate({
            rules: {
                questionid: {
                    required: true
                },
                optionvalue: {
                    required: true
                }
            },
            submitHandler: function (form) {
                var questionid = $(".modal-form #questionid").val();
                var optionvalue = $(".modal-form #optionvalue").val();

                $.ajax({
                    type    : "POST",
                    data    : {questionid:questionid,optionvalue:optionvalue},
                    url     : "<?php echo base_url()?>admin/questionaireOptionStore",
                    success : function(data) {
                        $('#tableOption').DataTable().ajax.reload();
                        $("#modal-add-option").modal('hide');
                        return false;
                    },
                    error   : function (e) {
                        console.log(e);
                        return false;
                    }
                });
            }
        });
    });
</script>