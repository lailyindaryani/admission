
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/account">Account</a></li>
                <li class="active">Change Password</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Account</h2>
            <em>Change Password</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">

                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>account/doChangePassword" class="form-horizontal" role="form" method="post" id="ioform">
                                <fieldset>
                                    <legend>User Data</legend>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Old Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Enter Password" name="oldpassword" id="oldpassword">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">New Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Enter Password" name="newpassword" id="newpassword">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Confirm Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Retype Password" name="repassword" id="repassword">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save Change</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>themes/_assets/js/plugins/jquery-gritter/jquery.gritter.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#usergroup").select2({
            minimumResultsForSearch: -1,
            placeholder : 'Select Group'
        });
    });

    $("#ioform" ).validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            oldpassword: {
                required    : true,
                remote      : {
                    url     : "<?php echo base_url()?>account/checkPassword",
                    type    : "post",
                    data    : {password:$("#oldpassword").val()},
                }
            },
            newpassword     : {
                required    : true,
            },
            repassword:{
                required    : true,
                equalTo     : "#newpassword",
            }
        },
        messages: {
            oldpassword: {
                remote: "Old password is wrong",
            },
        },
        submitHandler: function (form) {
            $.ajax({
                type    : "POST",
                data    : {newpassword:$("#newpassword").val()},
                url     : "<?php echo base_url()?>account/doChangePassword",
                success : function(data) {
                    $.gritter.add({
                        // (string | mandatory) the heading of the notification
                        title: 'Password Changed',
                        // (string | mandatory) the text inside the notification
                        text: 'Password change success!',
                        // (string | optional) the image to display on the left
                        // image: 'http://a0.twimg.com/profile_images/59268975/jquery_avatar_bigger.png',
                        // (bool | optional) if you want it to fade out on its own or just sit there
                        sticky: false,
                        // (int | optional) the time you want it to be alive for before fading out
                        time: ''
                    });
                    return false;
                },
                error   : function (e) {
                    alert("Something wrong!");
                    return false;
                }
            });
        }
    });
</script>