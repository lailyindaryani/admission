<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title><?=$pagetitle?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="KingAdmin Dashboard">
	<meta name="author" content="The Develovers">

	<!-- CSS -->
	<link href="<?php echo base_url();?>themes/_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/main.css" rel="stylesheet" type="text/css">

	<!--[if lte IE 9]>
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon57x57.png">
	<link rel="shortcut icon" href="<?php echo base_url();?>themes/_assets/ico/favicon.png">

</head>

<body>
	<div class="wrapper full-page-wrapper page-auth page-login text-center">
		<div class="inner-page">

			<div class="login-box center-block" style="background-color: #FFFFFF">
                <div style="text-align: center">
                    <img src="<?php echo base_url()?>images/io.png" style="width: 250px;margin:auto;">
                </div><br>
				<form action="<?php echo base_url()?>account/doResetPassword" class="form-horizontal" role="form" method="post" id="ioform">
					<fieldset>
						<input type='hidden' name='email' id='email' value='<?=$email?>'>
						<input type='hidden' name='id' id='id' value='<?=$id?>'>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">New Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Enter Password" name="newpassword" id="newpassword">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Confirm Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Retype Password" name="repassword" id="repassword">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Change Password</button>
                                        </div>
                                    </div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

	<footer class="footer">&copy; <?php echo Date('Y'); ?> SISFO Telkom University</footer>

	<!-- Javascript -->
	<script src="<?php echo base_url();?>themes/_assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/bootstrap/bootstrap.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/plugins/modernizr/modernizr.js"></script>
	<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
	<script src="<?php echo base_url()?>themes/_assets/js/plugins/jquery-gritter/jquery.gritter.js"></script>

	<script type="text/javascript">
		    $("#ioform" ).validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            newpassword     : {
                required    : true,
            },
            repassword:{
                required    : true,
                equalTo     : "#newpassword",
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type    : "POST",
                data    : {newpassword:$("#newpassword").val(), email:$("#email").val(), id:$("#id").val()},
                url     : "<?php echo base_url()?>account/doResetPassword",
                success : function(data) {

                	$.gritter.add({
						// (string | mandatory) the heading of the notification
						title: 'Password Changed',
						// (string | mandatory) the text inside the notification
						text: 'Password reset success!',
						// (function | optional) function called after it closes
						after_close: function(){
							window.location.replace("<?php echo base_url()?>login");
						},
						time: 2000,
					});
                    return false;
                },
                error   : function (e) {
                    alert("Something wrong!");
                    return false;
                }
            });
        }
    });
	</script>
</body>

</html>

