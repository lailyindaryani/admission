
</div>
<!-- /row -->
</div>
<!-- /container -->
</div>
<!-- END BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
</div>
<!-- /wrapper -->

<!-- FOOTER -->
<footer class="footer">
    &copy; <?php echo DATE('Y'); ?> SISFO Telkom University
</footer>
<!-- END FOOTER -->

</body>
    
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/general.function.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/jquery-ui/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/datatable/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/init.default.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/datatable/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/bootbox.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/facebox.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/select2/select2.min.js"></script>


<?php /*
<!-- Simpan sini yang gak kepake / belum kepake secara general. sebaiknya gunakan di setiap file masing2 saja.  -->
<!--
	
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/loading-overlay/loading-overlay.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/_assets/js/king-table.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/jqallrangesliders/jQAllRangeSliders-min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/markdown/markdown.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/summernote/summernote.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/king-form-layouts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-tour/bootstrap-tour.custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/king-common.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/modernizr/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/jquery-maskedinput/jquery.masked-input.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>
-->
*/ ?>


<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
	});
</script>
</html>

