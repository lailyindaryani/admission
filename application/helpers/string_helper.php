<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if(!function_exists('month_name')) 
{
	function month_name() {
	    $month = array(
	    		'ind'	=> array(1 => 'Januari', 2 => 'Februari',3=> 'Maret', 4 => 'April', 5=> 'Mei', 6 => 'Juni',7=> 'Juli', 8 => 'Agustus', 9=> 'September', 10 => 'Oktober', 11=> 'Nopember', 12 => 'Desember'),
	    		'eng'	=> array(1 => 'January', 2 => 'February',3=> 'March', 4 => 'April', 5=> 'May', 6 => 'June',7=> 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11=> 'November', 12 => 'December')
	    	);
		return $month;
	}
}

if(!function_exists('payment_file_name')) 
{
	function payment_file_name($title = NULL, $image = NULL, $type = NULL) {
	    if(!is_null($title)) $title = str_replace(' ', '_', $title);
	    $indentity_name = !is_null($type) ? strtolower($type) .'_' : '';

		return $indentity_name.$title.'_'.time(). '.'.pathinfo($image['image']['name'], PATHINFO_EXTENSION);
	}
}